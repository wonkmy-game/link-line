
const { ccclass, property } = cc._decorator;

@ccclass
export default class LaunchScene extends cc.Component {

    @property({ type: cc.Node })
    select_panel: cc.Node = null;

    @property({ type: cc.Node })
    start_btn: cc.Node = null;

    onLoad() {


        this.start_btn.on(cc.Node.EventType.TOUCH_START, this.startGame, this)
    }

    startGame() {
        this.select_panel.active = true;
    }

    start() {
    }
}