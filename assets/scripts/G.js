window.G={
    originBallList:[],//用于重置功能啊老哥
    BallList:[], //每次刚开始的时候加载当前关卡的balllist node，每次完成一条线的时候，删除掉对应的balls;
    storkelist:[],//一共几条线，每条线的点存在这里，是二维数组
    LevelJson:null,
    check_all_storke_interset(){
        // console.log(this.storkelist)
        if(this.storkelist.length < 2){
            return false;
        }
        //最多会有5条，不管几条，循环对比，每两条都要判断下是否相交
        for(let i=0;i< this.storkelist.length;i++){
            //每一条都和index比自己大的线对比
            for(let j=0;j< this.storkelist.length;j++){
                if(j>i){
                    let res = this.check_two_storke_interset(this.storkelist[i],this.storkelist[j]);
                    if(res!=false){ console.log('G:'+res); return res; }
                    
                }
            }
        }
        return false;
    },
    check_two_storke_interset(list1,list2){
        //判断两个数组是否香交，不交叉返回false，交叉返回交叉点坐标
        //先遍历list1中每个点到list2中每个点距离，复杂的N方
        for(let i=0; i<list1.length-1;i++){
            for(let j=0; j< list2.length-1;j++){
                //判断两点是否相交，如果滑动过快，这个方法会判断不出来
                var a = list1[i][0]-list2[j][0];
                var b = list1[i][1]-list2[j][1];
                var dis = Math.sqrt(a*a+b*b);
                if(dis < 20){
                    return list1[i];
                }


               //改用判断线段是否相交的方法,来判断是否重叠,但是这个方法无法知道具体交叉的点是哪个，退而求其次，取某个线段的起点

                // let res = this.checkCross(list1[i],list1[i+1],list2[j],list2[j+1])
                // if(res){
                //     return list1[i];
                // }
               
            }
        }
        return false;
    },
    reset(){
        //清空G里面的信息
        G.BallList = [];
        G.storkelist = [];
        G.LevelJson = null;
    },
    
//计算向量叉乘  
     crossMul(v1,v2){  
        return   v1.x*v2.y-v1.y*v2.x;  
    } , 
//判断两条线段是否相交  
     checkCross(p1,p2,p3,p4){  
        var v1_1={x:p1.x-p3.x,y:p1.y-p3.y} 
        var v1_2={x:p2.x-p3.x,y:p2.y-p3.y} 
        var v1_3={x:p4.x-p3.x,y:p4.y-p3.y} 
        var vv1=this.crossMul(v1_1,v1_3)*this.crossMul(v1_2,v1_3)  
        var v2_1={x:p3.x-p1.x,y:p3.y-p1.y}  
        var v2_2={x:p4.x-p1.x,y:p4.y-p1.y}  
        var v2_3={x:p2.x-p1.x,y:p2.y-p1.y} 
        var vv2=this.crossMul(v2_1,v2_3)*this.crossMul(v2_2,v2_3);
        return (vv1<0&&vv2<0)?true:false 
    },
    ShowChayeRewardAD() {
        //admob
		if (cc.sys.platform == cc.sys.ANDROID){
			let res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Interstitial_Reward", "ShowInterstitialRewardAD", "()I");
			return res;
		}
        
    },
    ShowRewardAD(TYPE){
		if (cc.sys.platform == cc.sys.ANDROID) {
			let res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Reward", "ShowRewardAD", "()I");
			return res;
		}else if(cc.sys.platform == cc.sys.WECHAT_GAME) {
             if (window["rewardedVideoAd"] != undefined) {
		            window["rewardedVideoAd"].show()
                        .catch(() => {
                            console.log('激励视频 广告显示失败')
		                })
                     window['reward_type'] = TYPE;
	        } else {
		        cc.find("Canvas/dialog").getComponent(cc.Label).string = '暂无广告资源，请稍后再试！';
		            setTimeout(() => {
                        cc.find("Canvas/dialog").active = false;
		        }, 2);
	        }
        }
        
    },
    GetRewardAD(){
		if (cc.sys.platform == cc.sys.ANDROID) {
			let res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Reward", "GetRewardAD", "()I");
			return res;
		}
        return true;
    },
    GetInterstitialRewardAD(){
		if (cc.sys.platform == cc.sys.ANDROID) {
			let res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Interstitial_Reward", "GetInterstitialRewardAD", "()I");
			return res;
		}
        return true;
    },
    ShowInterstitialAD(){
        if (cc.sys.platform == cc.sys.ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Interstitial", "ShowInterstitialAD", "()V");
        } else if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            if (window["interstitialAd"] != undefined) {
		        window["interstitialAd"].show()["catch"](function (err) {
			        console.log(err); // Global.bannerAd.show().catch(err => {console.log(err)});
		        });
	        }
        }
    },
	AnalysticLogin() {
        if (cc.sys.platform == cc.sys.ANDROID) {
            return;
        }
        // return true;
		cocosAnalytics.init({
			appID: "647407103",              // 游戏ID
			version: "1.0.0",           // 游戏/应用版本号
			storeID: "WECHAT",     // 分发渠道
			engine: "cocos"           // 游戏引擎
		});
		//开启(关闭)本地日志的输出
		cocosAnalytics.enableDebug(false);
		// 开始登陆
		cocosAnalytics.CAAccount.loginStart({
			channel: 'WECHAT'  // 获客渠道，指获取该客户的广告渠道信息   
		});

		// 登陆成功
		cocosAnalytics.CAAccount.loginSuccess({
			userID: "id",
			age: 1,             // 年龄
			sex: 1,             // 性别：1为男，2为女，其它表示未知
			channel: 'WECHAT'  // 获客渠道，指获取该客户的广告渠道信息   
		});

	},
	AnalysticCustomEvent(key, value) {
        if (cc.sys.platform == cc.sys.ANDROID) {
            return;
        }
		let EventId = key;
		let EventValue = {
			'Vaule': value
		}
		// 事件完成
		// 参数：事件ID（必填）, 不得超过30个字符
		cocosAnalytics.CACustomEvent.onSuccess(EventId, EventValue);
	},
	wxtopshare() {
		if (cc.sys.platform == cc.sys.WECHAT_GAME) {
			wx.showShareMenu({
				success: (res) => {
					console.log('开启被动转发成功！');
				},
				fail: (res) => {
					console.log(res);
					console.log('开启被动转发失败！');
				}
			});

			wx.onShareAppMessage(function () {
				return {
					title: '画线小挑战~',
					// imageUrlId: '3In1gKQRT2eZ5PbRwWY1Gw==',
					// imageUrl: 'https://mmocgame.qpic.cn/wechatgame/H8OjaAYSeOhg2YwBicoYRP80nr6fhoPCEIWjCiaVDoKmCMCSRL0ibGMzRbQmRmsCcDk/0'
				}
			})
		}

	},
    share() {
        if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            // let index = new Random_Num().random(9);
            // console.log(WXImgids[Number(index)]);
            wx.shareAppMessage({
                title: '画线小挑战~',
                // imageUrlId: WXImgids[index[0]].id,
                // imageUrl: WXImgids[index[0]].url
            })
        }
    }

};