// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        ButtonAudio:cc.AudioClip,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
    },

    start () {
        var self = this;
        this.node.on('click', function (button) {
            //The event is a custom event, you could get the Button component via first argument
            /*通过模块化脚本操作
            var GameStart = require("GameStart");
            new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
            */
            /*通过找组件操作
            cc.find('Canvas/MainBG').getComponent('GameStart').start();
            */
            /*直接重新加载场景
            cc.director.loadScene("Home");
            */
            G.reset();
            Storage.Set_Info('level',"0");
            cc.find('Canvas/MainBG').getComponent('GameStart').start();
            cc.find('Canvas/TIPS/全部通关').active = false;
            //音效
           cc.audioEngine.playEffect(self.ButtonAudio, false);
         })
    },

    // update (dt) {},
});
