// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html



const { ccclass, property } = cc._decorator;
const COOKIE_LEVEL = "level"
@ccclass
export default class LevelBtn extends cc.Component {
    id: number = -1;

    @property({ type: cc.Label })
    numNumber: cc.Label = null;

    init(id: number) {
        this.id = id;
        this.numNumber.string = String(this.id);
    }

    goToLevel() {
        Storage.Set_Info('level', this.numNumber.string - 1);

        cc.director.loadScene("Home");
    }

}
