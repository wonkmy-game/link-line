// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import GameMgr from "./GameMgr";

const {ccclass, property} = cc._decorator;

@ccclass
export default class DianziMove extends cc.Component {

    ID:number=-1;
    waypoint_id:number=0;

    init(storkelist,self,name){
        var spName = this.stroke_color(name);
        this.ID=this.getIDByColor(spName);
        cc.resources.load("dzs/"+spName,cc.SpriteFrame,(err,sf:cc.SpriteFrame)=>{
            this.node.getComponent(cc.Sprite).spriteFrame=sf;
        })
        var canvasPos1 = storkelist[0];
        var worldPos1 = self.node.convertToWorldSpaceAR(canvasPos1);
        var nodePos1 = self.Draw.convertToNodeSpaceAR(worldPos1);
        this.node.setPosition(nodePos1);
    }

    startMove(storkelist,self,startPos,endPos,startBall:cc.Node,endBall:cc.Node){
        if(GameMgr.gameOver==true)return;
        if(this.waypoint_id>=storkelist.length){
            cc.game.emit("show_ball_mask",startPos,endPos,this.ID,startBall,endBall);
            
            this.node.destroy();
            return;
        }
        var canvasPos = storkelist[this.waypoint_id];
        var worldPos = self.node.convertToWorldSpaceAR(canvasPos);
        var nodePos = self.Draw.convertToNodeSpaceAR(worldPos);
        cc.tween(this.node).to(0.02, { position: nodePos }).call(() => {
            this.waypoint_id+=2;
            this.startMove(storkelist,self,startPos,endPos,startBall,endBall);
        }).start();
    }

    getIDByColor(colorName){
        switch (colorName) {
            case "dz-red":
                return 0;
            case "dz-yellow":
                return 1;
            case "dz-blue":
                return 2;
            case "dz-green":
                return 3;
        }
    }

    stroke_color(str){
        switch(str){
            case 'redball':
                return "dz-red";
            case 'yellowball':
                return "dz-yellow";
            case 'blueball':
                return "dz-blue"
            case 'greenball':
                return "dz-green"
        }

    }
    
    onCollisionEnter(other, self) {
        if(other.node.getComponent('DianziMove').ID != this.ID){
            GameMgr.gameOver=true;
            cc.game.emit("gameover",this.node,this.node.parent)
        }
    }
}
