// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        Guide:cc.Node,
        level:"0",
       
    },

    // LIFE-CYCLE CALLBACKS:

    start () {
        // 判断是不是第一关，是的话，加个提示的手势
        this.level = Storage.Get_Info('level');
        if(this.level == "0"){
            this.showguidelist();
        }else{
            this.Guide.active = false;
        }
    },

    showguidelist () {
        this.Guide.active = true;
        //用户还没画
        // this.Guide.setPosition(cc.v2(0, 240))
        cc.tween(this.Guide)
            .to(0.01,{position:cc.v2(0, 240)})
            .bezierTo(2,cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200))
            .union()
            .repeatForever()
            .start();
    },
    close(){
        this.level = Storage.Get_Info('level');
        if(this.level != "0"){
            return;
        }
        cc.tween(this.Guide).stopAll;
        this.Guide.active = false;
    },

    showguidelist_again () {
        this.level = Storage.Get_Info('level');
        if(this.level != "0"){
            return;
        }
        this.Guide.active = true;
        if(G.BallList.length >2){
            //用户还没画
            cc.tween(this.Guide)
            .to(0.01,{position:cc.v2(0, 240)})
            .bezierTo(2,cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200))
            .union()
            .repeatForever()
            .start();
        }else{
            //用户画了，要判断画的哪条
            if(G.BallList[0]._name == "redball"){
                //hard code hahaha
                cc.tween(this.Guide)
                .to(0.01,{position:cc.v2(0, 240)})
                .bezierTo(2,cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200))
                .union()
                .repeatForever()
                .start();
            }else{
                cc.tween(this.Guide)
                .to(0.01,{position:cc.v2(0, 0)})
                .bezierTo(2,cc.v2(0, 0), cc.v2(500, -225), cc.v2(0, -450))
                .union()
                .repeatForever()
                .start();
            }
        }
    },
});
