// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import LevelBtn from "./LevelBtn";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SelectPanel extends cc.Component {

    @property({type:cc.Prefab})
    level_btn: cc.Prefab = null;

    @property({type:cc.ScrollView})
    scrollView: cc.ScrollView = null;


    cfgArr:Array<number>

    protected onLoad(): void {
        this.node.getChildByName("splash").on(cc.Node.EventType.TOUCH_START,this.hideSelectPanel,this);

        this.generatorLevelBtn();
    }

    generatorLevelBtn() {
        let content = this.scrollView.node.getChildByName("view").getChildByName("content");
        content.width = 644;
        // content.height = 22 * 80;
        // content.height = 22;
        for (let i = 1; i <= 2; i++) {
            let newLevel_btn = cc.instantiate(this.level_btn);
            newLevel_btn.setParent(this.scrollView.node.getChildByName("view").getChildByName("content"));
            newLevel_btn.setPosition(0,(i-1)*-100);
            newLevel_btn.getComponent(LevelBtn).init(i);
        }
    }

    hideSelectPanel(){
        this.node.active=false;
    }
}
