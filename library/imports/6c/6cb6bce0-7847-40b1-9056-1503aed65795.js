"use strict";
cc._RF.push(module, '6cb6bzgeEdAsZBWFQOu1leV', 'testlevel');
// scripts/testlevel.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    editlevel: cc.Node
  },
  // LIFE-CYCLE CALLBACKS:
  // onLoad () {},
  start: function start() {
    var str1 = {
      "tips": "300",
      "bg": "yuan",
      "balls": [{
        "color": "red",
        "Pos": [{
          "x": 123,
          "y": 123
        }, {
          "x": 234,
          "y": 234
        }]
      }, {
        "color": "green",
        "Pos": [{
          "x": 0,
          "y": 340
        }, {
          "x": 0,
          "y": -100
        }]
      }, {
        "color": "yellow",
        "Pos": [{
          "x": 0,
          "y": -350
        }, {
          "x": 0,
          "y": 100
        }]
      }]
    };
    var obj = JSON.stringify(str1, null, '\t'); //this.saveForBrowser(obj,"level");

    this.node.on('click', function () {
      cc.resources.load("perfab/draw", function (err, prefab) {
        var newNode = cc.instantiate(prefab);
        newNode.parent = cc.find('Canvas/MainBG/Draw');
      });
    });
    this.editlevel.on('click', function () {
      cc.find('Canvas/MainBG/Draw').removeAllChildren();
      G.reset();
      G.BallList = cc.find('Canvas/MainBG/BG').children.concat();
    });
  } // update (dt) {},

});

cc._RF.pop();