"use strict";
cc._RF.push(module, 'b7f29fC9ndHkJ0nB5H6FRN+', 'LevelBtn');
// scripts/LevelBtn.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var COOKIE_LEVEL = "level";
var LevelBtn = /** @class */ (function (_super) {
    __extends(LevelBtn, _super);
    function LevelBtn() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.id = -1;
        _this.numNumber = null;
        return _this;
    }
    LevelBtn.prototype.init = function (id) {
        this.id = id;
        this.numNumber.string = String(this.id);
    };
    LevelBtn.prototype.goToLevel = function () {
        Storage.Set_Info('level', this.numNumber.string - 1);
        cc.director.loadScene("Home");
    };
    __decorate([
        property({ type: cc.Label })
    ], LevelBtn.prototype, "numNumber", void 0);
    LevelBtn = __decorate([
        ccclass
    ], LevelBtn);
    return LevelBtn;
}(cc.Component));
exports.default = LevelBtn;

cc._RF.pop();