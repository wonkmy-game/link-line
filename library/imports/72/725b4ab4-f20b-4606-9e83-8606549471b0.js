"use strict";
cc._RF.push(module, '725b4q08gtGBp6DhgZUlHGw', 'storage');
// scripts/storage.js

"use strict";

window.Storage = {
  Change_storage: function Change_storage(Type, Number) {
    var Storage_number = Storage.Get_Info(Type);
    Storage_number += Number;
    cc.sys.localStorage.setItem(Type, Storage_number);
  },
  Get_Info: function Get_Info(Type) {
    var starnum = cc.sys.localStorage.getItem(Type);

    if (!starnum) {
      starnum = 0;
    } else {
      starnum = parseInt(starnum);
    }

    return starnum;
  },
  Set_Info: function Set_Info(Type, starnum) {
    cc.sys.localStorage.setItem(Type, starnum);
  }
}; //wx.getOpenDataContext().postMessage({
//     message: Level_Pass_Now
// });
//cc.sys.localStorage.setItem(key, value)
//cc.sys.localStorage.getItem(key)

cc._RF.pop();