"use strict";
cc._RF.push(module, '52b6e5B1OpDmqrDtECFVJbm', 'SelectPanel');
// scripts/SelectPanel.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var LevelBtn_1 = require("./LevelBtn");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SelectPanel = /** @class */ (function (_super) {
    __extends(SelectPanel, _super);
    function SelectPanel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.level_btn = null;
        _this.scrollView = null;
        return _this;
    }
    SelectPanel.prototype.onLoad = function () {
        this.node.getChildByName("splash").on(cc.Node.EventType.TOUCH_START, this.hideSelectPanel, this);
        this.generatorLevelBtn();
    };
    SelectPanel.prototype.generatorLevelBtn = function () {
        var content = this.scrollView.node.getChildByName("view").getChildByName("content");
        content.width = 644;
        // content.height = 22 * 80;
        // content.height = 22;
        for (var i = 1; i <= 2; i++) {
            var newLevel_btn = cc.instantiate(this.level_btn);
            newLevel_btn.setParent(this.scrollView.node.getChildByName("view").getChildByName("content"));
            newLevel_btn.setPosition(0, (i - 1) * -100);
            newLevel_btn.getComponent(LevelBtn_1.default).init(i);
        }
    };
    SelectPanel.prototype.hideSelectPanel = function () {
        this.node.active = false;
    };
    __decorate([
        property({ type: cc.Prefab })
    ], SelectPanel.prototype, "level_btn", void 0);
    __decorate([
        property({ type: cc.ScrollView })
    ], SelectPanel.prototype, "scrollView", void 0);
    SelectPanel = __decorate([
        ccclass
    ], SelectPanel);
    return SelectPanel;
}(cc.Component));
exports.default = SelectPanel;

cc._RF.pop();