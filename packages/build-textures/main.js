function onBeforeBuildFinish(options, callback) {
    let textures = {};
    let audios = {};
    let spines = {};

    options.bundles.forEach(bundle => {
        let buildResults = bundle.buildResults;

        let assets = buildResults.getAssetUuids();
        const textureType = cc.js._getClassId(cc.Texture2D);
        const audioType = cc.js._getClassId(cc.AudioClip);
        const spineType = cc.js._getClassId(sp.SkeletonData);

        for (let i = 0; i < assets.length; ++i) {
            let asset = assets[i];
            let url = Editor.assetdb.uuidToUrl(asset);
            if (url != undefined && url != null && url.split("/").length >= 0) {
                let arr = url.split("/");
                const assetName = arr.pop();

                const assetType = buildResults.getAssetType(asset);
                let path = buildResults.getNativeAssetPath(asset);

                if (options.md5Cache && cc.loader.md5Pipe) {
                    path = cc.loader.md5Pipe.transformURL(path);
                }

                if (assetType === textureType) {
                    textures[assetName] = path;
                }
                else if (assetType === audioType) {
                    audios[assetName] = path;
                }
                else if (assetType === spineType) {
                    // spines[assetName] = path;
                }

            }
        }

    });

    var fs = require("fs");
    let data = { textures, audios };
    fs.writeFileSync(`${options.buildPath}/${options.title}.json`, JSON.stringify(data), function (err) {
        if (!err) {
            Editor.log("写入成功！");
        }
    });
    callback();
}

module.exports = {
    load() {
        Editor.Builder.on('build-finished', onBeforeBuildFinish);

    },

    unload() {
        Editor.Builder.removeListener('build-finished', onBeforeBuildFinish);
    }
};
