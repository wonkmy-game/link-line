
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/testlevel.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6cb6bzgeEdAsZBWFQOu1leV', 'testlevel');
// scripts/testlevel.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    editlevel: cc.Node
  },
  // LIFE-CYCLE CALLBACKS:
  // onLoad () {},
  start: function start() {
    var str1 = {
      "tips": "300",
      "bg": "yuan",
      "balls": [{
        "color": "red",
        "Pos": [{
          "x": 123,
          "y": 123
        }, {
          "x": 234,
          "y": 234
        }]
      }, {
        "color": "green",
        "Pos": [{
          "x": 0,
          "y": 340
        }, {
          "x": 0,
          "y": -100
        }]
      }, {
        "color": "yellow",
        "Pos": [{
          "x": 0,
          "y": -350
        }, {
          "x": 0,
          "y": 100
        }]
      }]
    };
    var obj = JSON.stringify(str1, null, '\t'); //this.saveForBrowser(obj,"level");

    this.node.on('click', function () {
      cc.resources.load("perfab/draw", function (err, prefab) {
        var newNode = cc.instantiate(prefab);
        newNode.parent = cc.find('Canvas/MainBG/Draw');
      });
    });
    this.editlevel.on('click', function () {
      cc.find('Canvas/MainBG/Draw').removeAllChildren();
      G.reset();
      G.BallList = cc.find('Canvas/MainBG/BG').children.concat();
    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcdGVzdGxldmVsLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiZWRpdGxldmVsIiwiTm9kZSIsInN0YXJ0Iiwic3RyMSIsIm9iaiIsIkpTT04iLCJzdHJpbmdpZnkiLCJub2RlIiwib24iLCJyZXNvdXJjZXMiLCJsb2FkIiwiZXJyIiwicHJlZmFiIiwibmV3Tm9kZSIsImluc3RhbnRpYXRlIiwicGFyZW50IiwiZmluZCIsInJlbW92ZUFsbENoaWxkcmVuIiwiRyIsInJlc2V0IiwiQmFsbExpc3QiLCJjaGlsZHJlbiIsImNvbmNhdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLFNBQVMsRUFBQ0osRUFBRSxDQUFDSztFQURMLENBSFA7RUFPTDtFQUVBO0VBRUFDLEtBWEssbUJBV0k7SUFDTCxJQUFJQyxJQUFJLEdBQUc7TUFBQyxRQUFPLEtBQVI7TUFDWCxNQUFLLE1BRE07TUFFWCxTQUFRLENBQ0o7UUFDSSxTQUFRLEtBRFo7UUFFSSxPQUFNLENBQUM7VUFBQyxLQUFJLEdBQUw7VUFBUyxLQUFJO1FBQWIsQ0FBRCxFQUFtQjtVQUFDLEtBQUksR0FBTDtVQUFTLEtBQUk7UUFBYixDQUFuQjtNQUZWLENBREksRUFLSjtRQUNDLFNBQVEsT0FEVDtRQUVDLE9BQU0sQ0FBQztVQUFDLEtBQUksQ0FBTDtVQUFPLEtBQUk7UUFBWCxDQUFELEVBQWlCO1VBQUMsS0FBSSxDQUFMO1VBQU8sS0FBSSxDQUFDO1FBQVosQ0FBakI7TUFGUCxDQUxJLEVBU0g7UUFDQyxTQUFRLFFBRFQ7UUFFQyxPQUFNLENBQUM7VUFBQyxLQUFJLENBQUw7VUFBTyxLQUFJLENBQUM7UUFBWixDQUFELEVBQWtCO1VBQUMsS0FBSSxDQUFMO1VBQU8sS0FBSTtRQUFYLENBQWxCO01BRlAsQ0FURztJQUZHLENBQVg7SUFnQkEsSUFBSUMsR0FBRyxHQUFDQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUgsSUFBZixFQUFvQixJQUFwQixFQUF5QixJQUF6QixDQUFSLENBakJLLENBa0JMOztJQUNBLEtBQUtJLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBcUIsWUFBVTtNQUMzQlosRUFBRSxDQUFDYSxTQUFILENBQWFDLElBQWIsQ0FBa0IsYUFBbEIsRUFBaUMsVUFBVUMsR0FBVixFQUFlQyxNQUFmLEVBQXVCO1FBQ3BELElBQUlDLE9BQU8sR0FBR2pCLEVBQUUsQ0FBQ2tCLFdBQUgsQ0FBZUYsTUFBZixDQUFkO1FBQ0FDLE9BQU8sQ0FBQ0UsTUFBUixHQUFpQm5CLEVBQUUsQ0FBQ29CLElBQUgsQ0FBUSxvQkFBUixDQUFqQjtNQUNGLENBSEY7SUFJSCxDQUxEO0lBTUEsS0FBS2hCLFNBQUwsQ0FBZVEsRUFBZixDQUFrQixPQUFsQixFQUEwQixZQUFVO01BQ2hDWixFQUFFLENBQUNvQixJQUFILENBQVEsb0JBQVIsRUFBOEJDLGlCQUE5QjtNQUNBQyxDQUFDLENBQUNDLEtBQUY7TUFDQUQsQ0FBQyxDQUFDRSxRQUFGLEdBQWF4QixFQUFFLENBQUNvQixJQUFILENBQVEsa0JBQVIsRUFBNEJLLFFBQTVCLENBQXFDQyxNQUFyQyxFQUFiO0lBQ0gsQ0FKRDtFQUtILENBekNJLENBNENMOztBQTVDSyxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgZWRpdGxldmVsOmNjLk5vZGUsXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIC8vIG9uTG9hZCAoKSB7fSxcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdmFyIHN0cjEgPSB7XCJ0aXBzXCI6XCIzMDBcIixcclxuICAgICAgICBcImJnXCI6XCJ5dWFuXCIsXHJcbiAgICAgICAgXCJiYWxsc1wiOltcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJjb2xvclwiOlwicmVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIlBvc1wiOlt7XCJ4XCI6MTIzLFwieVwiOjEyM30se1wieFwiOjIzNCxcInlcIjoyMzR9XVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICBcImNvbG9yXCI6XCJncmVlblwiLFxyXG4gICAgICAgICAgICAgXCJQb3NcIjpbe1wieFwiOjAsXCJ5XCI6MzQwfSx7XCJ4XCI6MCxcInlcIjotMTAwfV1cclxuICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgXCJjb2xvclwiOlwieWVsbG93XCIsXHJcbiAgICAgICAgICAgICAgXCJQb3NcIjpbe1wieFwiOjAsXCJ5XCI6LTM1MH0se1wieFwiOjAsXCJ5XCI6MTAwfV1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgXX07XHJcbiAgICAgICAgbGV0IG9iaj1KU09OLnN0cmluZ2lmeShzdHIxLG51bGwsJ1xcdCcpXHJcbiAgICAgICAgLy90aGlzLnNhdmVGb3JCcm93c2VyKG9iaixcImxldmVsXCIpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwicGVyZmFiL2RyYXdcIiwgZnVuY3Rpb24gKGVyciwgcHJlZmFiKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbmV3Tm9kZSA9IGNjLmluc3RhbnRpYXRlKHByZWZhYik7XHJcbiAgICAgICAgICAgICAgICBuZXdOb2RlLnBhcmVudCA9IGNjLmZpbmQoJ0NhbnZhcy9NYWluQkcvRHJhdycpO1xyXG4gICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5lZGl0bGV2ZWwub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0RyYXcnKS5yZW1vdmVBbGxDaGlsZHJlbigpO1xyXG4gICAgICAgICAgICBHLnJlc2V0KCk7XHJcbiAgICAgICAgICAgIEcuQmFsbExpc3QgPSBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0JHJykuY2hpbGRyZW4uY29uY2F0KCk7XHJcbiAgICAgICAgfSlcclxuICAgIH0sXHJcbiAgIFxyXG5cclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9LFxyXG59KTtcclxuIl19