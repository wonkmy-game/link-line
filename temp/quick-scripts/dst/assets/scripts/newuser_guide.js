
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/newuser_guide.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2c272vE7ptCJp9ujU5sPYLd', 'newuser_guide');
// scripts/newuser_guide.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    Guide: cc.Node,
    level: "0"
  },
  // LIFE-CYCLE CALLBACKS:
  start: function start() {
    // 判断是不是第一关，是的话，加个提示的手势
    this.level = Storage.Get_Info('level');

    if (this.level == "0") {
      this.showguidelist();
    } else {
      this.Guide.active = false;
    }
  },
  showguidelist: function showguidelist() {
    this.Guide.active = true; //用户还没画
    // this.Guide.setPosition(cc.v2(0, 240))

    cc.tween(this.Guide).to(0.01, {
      position: cc.v2(0, 240)
    }).bezierTo(2, cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200)).union().repeatForever().start();
  },
  close: function close() {
    this.level = Storage.Get_Info('level');

    if (this.level != "0") {
      return;
    }

    cc.tween(this.Guide).stopAll;
    this.Guide.active = false;
  },
  showguidelist_again: function showguidelist_again() {
    this.level = Storage.Get_Info('level');

    if (this.level != "0") {
      return;
    }

    this.Guide.active = true;

    if (G.BallList.length > 2) {
      //用户还没画
      cc.tween(this.Guide).to(0.01, {
        position: cc.v2(0, 240)
      }).bezierTo(2, cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200)).union().repeatForever().start();
    } else {
      //用户画了，要判断画的哪条
      if (G.BallList[0]._name == "redball") {
        //hard code hahaha
        cc.tween(this.Guide).to(0.01, {
          position: cc.v2(0, 240)
        }).bezierTo(2, cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200)).union().repeatForever().start();
      } else {
        cc.tween(this.Guide).to(0.01, {
          position: cc.v2(0, 0)
        }).bezierTo(2, cc.v2(0, 0), cc.v2(500, -225), cc.v2(0, -450)).union().repeatForever().start();
      }
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcbmV3dXNlcl9ndWlkZS5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsIkd1aWRlIiwiTm9kZSIsImxldmVsIiwic3RhcnQiLCJTdG9yYWdlIiwiR2V0X0luZm8iLCJzaG93Z3VpZGVsaXN0IiwiYWN0aXZlIiwidHdlZW4iLCJ0byIsInBvc2l0aW9uIiwidjIiLCJiZXppZXJUbyIsInVuaW9uIiwicmVwZWF0Rm9yZXZlciIsImNsb3NlIiwic3RvcEFsbCIsInNob3dndWlkZWxpc3RfYWdhaW4iLCJHIiwiQmFsbExpc3QiLCJsZW5ndGgiLCJfbmFtZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLEtBQUssRUFBQ0osRUFBRSxDQUFDSyxJQUREO0lBRVJDLEtBQUssRUFBQztFQUZFLENBSFA7RUFTTDtFQUVBQyxLQVhLLG1CQVdJO0lBQ0w7SUFDQSxLQUFLRCxLQUFMLEdBQWFFLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFiOztJQUNBLElBQUcsS0FBS0gsS0FBTCxJQUFjLEdBQWpCLEVBQXFCO01BQ2pCLEtBQUtJLGFBQUw7SUFDSCxDQUZELE1BRUs7TUFDRCxLQUFLTixLQUFMLENBQVdPLE1BQVgsR0FBb0IsS0FBcEI7SUFDSDtFQUNKLENBbkJJO0VBcUJMRCxhQXJCSywyQkFxQlk7SUFDYixLQUFLTixLQUFMLENBQVdPLE1BQVgsR0FBb0IsSUFBcEIsQ0FEYSxDQUViO0lBQ0E7O0lBQ0FYLEVBQUUsQ0FBQ1ksS0FBSCxDQUFTLEtBQUtSLEtBQWQsRUFDS1MsRUFETCxDQUNRLElBRFIsRUFDYTtNQUFDQyxRQUFRLEVBQUNkLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxHQUFUO0lBQVYsQ0FEYixFQUVLQyxRQUZMLENBRWMsQ0FGZCxFQUVnQmhCLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxHQUFULENBRmhCLEVBRStCZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFDLEdBQVAsRUFBWSxFQUFaLENBRi9CLEVBRWdEZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBQyxHQUFWLENBRmhELEVBR0tFLEtBSEwsR0FJS0MsYUFKTCxHQUtLWCxLQUxMO0VBTUgsQ0EvQkk7RUFnQ0xZLEtBaENLLG1CQWdDRTtJQUNILEtBQUtiLEtBQUwsR0FBYUUsT0FBTyxDQUFDQyxRQUFSLENBQWlCLE9BQWpCLENBQWI7O0lBQ0EsSUFBRyxLQUFLSCxLQUFMLElBQWMsR0FBakIsRUFBcUI7TUFDakI7SUFDSDs7SUFDRE4sRUFBRSxDQUFDWSxLQUFILENBQVMsS0FBS1IsS0FBZCxFQUFxQmdCLE9BQXJCO0lBQ0EsS0FBS2hCLEtBQUwsQ0FBV08sTUFBWCxHQUFvQixLQUFwQjtFQUNILENBdkNJO0VBeUNMVSxtQkF6Q0ssaUNBeUNrQjtJQUNuQixLQUFLZixLQUFMLEdBQWFFLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFiOztJQUNBLElBQUcsS0FBS0gsS0FBTCxJQUFjLEdBQWpCLEVBQXFCO01BQ2pCO0lBQ0g7O0lBQ0QsS0FBS0YsS0FBTCxDQUFXTyxNQUFYLEdBQW9CLElBQXBCOztJQUNBLElBQUdXLENBQUMsQ0FBQ0MsUUFBRixDQUFXQyxNQUFYLEdBQW1CLENBQXRCLEVBQXdCO01BQ3BCO01BQ0F4QixFQUFFLENBQUNZLEtBQUgsQ0FBUyxLQUFLUixLQUFkLEVBQ0NTLEVBREQsQ0FDSSxJQURKLEVBQ1M7UUFBQ0MsUUFBUSxFQUFDZCxFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsR0FBVDtNQUFWLENBRFQsRUFFQ0MsUUFGRCxDQUVVLENBRlYsRUFFWWhCLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxHQUFULENBRlosRUFFMkJmLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQUMsR0FBUCxFQUFZLEVBQVosQ0FGM0IsRUFFNENmLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFDLEdBQVYsQ0FGNUMsRUFHQ0UsS0FIRCxHQUlDQyxhQUpELEdBS0NYLEtBTEQ7SUFNSCxDQVJELE1BUUs7TUFDRDtNQUNBLElBQUdlLENBQUMsQ0FBQ0MsUUFBRixDQUFXLENBQVgsRUFBY0UsS0FBZCxJQUF1QixTQUExQixFQUFvQztRQUNoQztRQUNBekIsRUFBRSxDQUFDWSxLQUFILENBQVMsS0FBS1IsS0FBZCxFQUNDUyxFQURELENBQ0ksSUFESixFQUNTO1VBQUNDLFFBQVEsRUFBQ2QsRUFBRSxDQUFDZSxFQUFILENBQU0sQ0FBTixFQUFTLEdBQVQ7UUFBVixDQURULEVBRUNDLFFBRkQsQ0FFVSxDQUZWLEVBRVloQixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsR0FBVCxDQUZaLEVBRTJCZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFDLEdBQVAsRUFBWSxFQUFaLENBRjNCLEVBRTRDZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBQyxHQUFWLENBRjVDLEVBR0NFLEtBSEQsR0FJQ0MsYUFKRCxHQUtDWCxLQUxEO01BTUgsQ0FSRCxNQVFLO1FBQ0RQLEVBQUUsQ0FBQ1ksS0FBSCxDQUFTLEtBQUtSLEtBQWQsRUFDQ1MsRUFERCxDQUNJLElBREosRUFDUztVQUFDQyxRQUFRLEVBQUNkLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFUO1FBQVYsQ0FEVCxFQUVDQyxRQUZELENBRVUsQ0FGVixFQUVZaEIsRUFBRSxDQUFDZSxFQUFILENBQU0sQ0FBTixFQUFTLENBQVQsQ0FGWixFQUV5QmYsRUFBRSxDQUFDZSxFQUFILENBQU0sR0FBTixFQUFXLENBQUMsR0FBWixDQUZ6QixFQUUyQ2YsRUFBRSxDQUFDZSxFQUFILENBQU0sQ0FBTixFQUFTLENBQUMsR0FBVixDQUYzQyxFQUdDRSxLQUhELEdBSUNDLGFBSkQsR0FLQ1gsS0FMRDtNQU1IO0lBQ0o7RUFDSjtBQTFFSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgR3VpZGU6Y2MuTm9kZSxcclxuICAgICAgICBsZXZlbDpcIjBcIixcclxuICAgICAgIFxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgLy8g5Yik5pat5piv5LiN5piv56ys5LiA5YWz77yM5piv55qE6K+d77yM5Yqg5Liq5o+Q56S655qE5omL5Yq/XHJcbiAgICAgICAgdGhpcy5sZXZlbCA9IFN0b3JhZ2UuR2V0X0luZm8oJ2xldmVsJyk7XHJcbiAgICAgICAgaWYodGhpcy5sZXZlbCA9PSBcIjBcIil7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd2d1aWRlbGlzdCgpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgc2hvd2d1aWRlbGlzdCAoKSB7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIC8v55So5oi36L+Y5rKh55S7XHJcbiAgICAgICAgLy8gdGhpcy5HdWlkZS5zZXRQb3NpdGlvbihjYy52MigwLCAyNDApKVxyXG4gICAgICAgIGNjLnR3ZWVuKHRoaXMuR3VpZGUpXHJcbiAgICAgICAgICAgIC50bygwLjAxLHtwb3NpdGlvbjpjYy52MigwLCAyNDApfSlcclxuICAgICAgICAgICAgLmJlemllclRvKDIsY2MudjIoMCwgMjQwKSwgY2MudjIoLTUwMCwgMjApLCBjYy52MigwLCAtMjAwKSlcclxuICAgICAgICAgICAgLnVuaW9uKClcclxuICAgICAgICAgICAgLnJlcGVhdEZvcmV2ZXIoKVxyXG4gICAgICAgICAgICAuc3RhcnQoKTtcclxuICAgIH0sXHJcbiAgICBjbG9zZSgpe1xyXG4gICAgICAgIHRoaXMubGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgIGlmKHRoaXMubGV2ZWwgIT0gXCIwXCIpe1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNjLnR3ZWVuKHRoaXMuR3VpZGUpLnN0b3BBbGw7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgc2hvd2d1aWRlbGlzdF9hZ2FpbiAoKSB7XHJcbiAgICAgICAgdGhpcy5sZXZlbCA9IFN0b3JhZ2UuR2V0X0luZm8oJ2xldmVsJyk7XHJcbiAgICAgICAgaWYodGhpcy5sZXZlbCAhPSBcIjBcIil7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIGlmKEcuQmFsbExpc3QubGVuZ3RoID4yKXtcclxuICAgICAgICAgICAgLy/nlKjmiLfov5jmsqHnlLtcclxuICAgICAgICAgICAgY2MudHdlZW4odGhpcy5HdWlkZSlcclxuICAgICAgICAgICAgLnRvKDAuMDEse3Bvc2l0aW9uOmNjLnYyKDAsIDI0MCl9KVxyXG4gICAgICAgICAgICAuYmV6aWVyVG8oMixjYy52MigwLCAyNDApLCBjYy52MigtNTAwLCAyMCksIGNjLnYyKDAsIC0yMDApKVxyXG4gICAgICAgICAgICAudW5pb24oKVxyXG4gICAgICAgICAgICAucmVwZWF0Rm9yZXZlcigpXHJcbiAgICAgICAgICAgIC5zdGFydCgpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAvL+eUqOaIt+eUu+S6hu+8jOimgeWIpOaWreeUu+eahOWTquadoVxyXG4gICAgICAgICAgICBpZihHLkJhbGxMaXN0WzBdLl9uYW1lID09IFwicmVkYmFsbFwiKXtcclxuICAgICAgICAgICAgICAgIC8vaGFyZCBjb2RlIGhhaGFoYVxyXG4gICAgICAgICAgICAgICAgY2MudHdlZW4odGhpcy5HdWlkZSlcclxuICAgICAgICAgICAgICAgIC50bygwLjAxLHtwb3NpdGlvbjpjYy52MigwLCAyNDApfSlcclxuICAgICAgICAgICAgICAgIC5iZXppZXJUbygyLGNjLnYyKDAsIDI0MCksIGNjLnYyKC01MDAsIDIwKSwgY2MudjIoMCwgLTIwMCkpXHJcbiAgICAgICAgICAgICAgICAudW5pb24oKVxyXG4gICAgICAgICAgICAgICAgLnJlcGVhdEZvcmV2ZXIoKVxyXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgY2MudHdlZW4odGhpcy5HdWlkZSlcclxuICAgICAgICAgICAgICAgIC50bygwLjAxLHtwb3NpdGlvbjpjYy52MigwLCAwKX0pXHJcbiAgICAgICAgICAgICAgICAuYmV6aWVyVG8oMixjYy52MigwLCAwKSwgY2MudjIoNTAwLCAtMjI1KSwgY2MudjIoMCwgLTQ1MCkpXHJcbiAgICAgICAgICAgICAgICAudW5pb24oKVxyXG4gICAgICAgICAgICAgICAgLnJlcGVhdEZvcmV2ZXIoKVxyXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG59KTtcclxuIl19