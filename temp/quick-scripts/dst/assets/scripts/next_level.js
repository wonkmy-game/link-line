
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/next_level.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c13a9ud2dhNwJ0nXd0mJQtS', 'next_level');
// scripts/next_level.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    Draw: cc.Node,
    ButtonAudio: cc.AudioClip
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {// this.Draw = cc.find('Canvas/MainBG/Draw')
  },
  start: function start() {
    var self = this;
    this.node.on('click', function (button) {
      //The event is a custom event, you could get the Button component via first argument

      /*通过模块化脚本操作
      var GameStart = require("GameStart");
      new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
      */

      /*通过找组件操作
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      */

      /*直接重新加载场景
      cc.director.loadScene("Home");
      */
      console.log('xiayiguanchufale');
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      cc.find('Canvas/TIPS/PASS_BG').active = false;
      self.Draw.removeAllChildren();
      cc.resources.load("perfab/draw", function (err, prefab) {
        var newNode = cc.instantiate(prefab);
        newNode.parent = self.Draw;
      }); //音效

      cc.audioEngine.playEffect(self.ButtonAudio, false);
      var level = Storage.Get_Info('level');

      if (Number(level) % 2 == 0) {
        G.ShowInterstitialAD();
      } // G.AnalysticCustomEvent('LEVEL', level);

    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcbmV4dF9sZXZlbC5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsIkRyYXciLCJOb2RlIiwiQnV0dG9uQXVkaW8iLCJBdWRpb0NsaXAiLCJvbkxvYWQiLCJzdGFydCIsInNlbGYiLCJub2RlIiwib24iLCJidXR0b24iLCJjb25zb2xlIiwibG9nIiwiZmluZCIsImdldENvbXBvbmVudCIsImFjdGl2ZSIsInJlbW92ZUFsbENoaWxkcmVuIiwicmVzb3VyY2VzIiwibG9hZCIsImVyciIsInByZWZhYiIsIm5ld05vZGUiLCJpbnN0YW50aWF0ZSIsInBhcmVudCIsImF1ZGlvRW5naW5lIiwicGxheUVmZmVjdCIsImxldmVsIiwiU3RvcmFnZSIsIkdldF9JbmZvIiwiTnVtYmVyIiwiRyIsIlNob3dJbnRlcnN0aXRpYWxBRCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLElBQUksRUFBQ0osRUFBRSxDQUFDSyxJQURBO0lBRVJDLFdBQVcsRUFBQ04sRUFBRSxDQUFDTztFQUZQLENBSFA7RUFRTDtFQUVBQyxNQVZLLG9CQVVLLENBQ047RUFDSCxDQVpJO0VBY0xDLEtBZEssbUJBY0k7SUFDTCxJQUFJQyxJQUFJLEdBQUcsSUFBWDtJQUNBLEtBQUtDLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBVUMsTUFBVixFQUFrQjtNQUNwQzs7TUFDQTtBQUNaO0FBQ0E7QUFDQTs7TUFDWTtBQUNaO0FBQ0E7O01BQ1k7QUFDWjtBQUNBO01BQ1lDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGtCQUFaO01BQ0FmLEVBQUUsQ0FBQ2dCLElBQUgsQ0FBUSxlQUFSLEVBQXlCQyxZQUF6QixDQUFzQyxXQUF0QyxFQUFtRFIsS0FBbkQ7TUFDQVQsRUFBRSxDQUFDZ0IsSUFBSCxDQUFRLHFCQUFSLEVBQStCRSxNQUEvQixHQUF3QyxLQUF4QztNQUNBUixJQUFJLENBQUNOLElBQUwsQ0FBVWUsaUJBQVY7TUFDQW5CLEVBQUUsQ0FBQ29CLFNBQUgsQ0FBYUMsSUFBYixDQUFrQixhQUFsQixFQUFpQyxVQUFVQyxHQUFWLEVBQWVDLE1BQWYsRUFBdUI7UUFDcEQsSUFBSUMsT0FBTyxHQUFHeEIsRUFBRSxDQUFDeUIsV0FBSCxDQUFlRixNQUFmLENBQWQ7UUFDQUMsT0FBTyxDQUFDRSxNQUFSLEdBQWlCaEIsSUFBSSxDQUFDTixJQUF0QjtNQUNGLENBSEYsRUFoQm9DLENBb0JwQzs7TUFDREosRUFBRSxDQUFDMkIsV0FBSCxDQUFlQyxVQUFmLENBQTBCbEIsSUFBSSxDQUFDSixXQUEvQixFQUE0QyxLQUE1QztNQUNBLElBQUl1QixLQUFLLEdBQUdDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFaOztNQUNDLElBQUdDLE1BQU0sQ0FBQ0gsS0FBRCxDQUFOLEdBQWdCLENBQWhCLElBQXFCLENBQXhCLEVBQTBCO1FBQ2xDSSxDQUFDLENBQUNDLGtCQUFGO01BQ1MsQ0F6Qm1DLENBMEJwQzs7SUFDRixDQTNCRjtFQTRCSCxDQTVDSSxDQThDTDs7QUE5Q0ssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcblxyXG5jYy5DbGFzcyh7XHJcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXHJcblxyXG4gICAgcHJvcGVydGllczoge1xyXG4gICAgICAgIERyYXc6Y2MuTm9kZSxcclxuICAgICAgICBCdXR0b25BdWRpbzpjYy5BdWRpb0NsaXAsXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIG9uTG9hZCAoKSB7XHJcbiAgICAgICAgLy8gdGhpcy5EcmF3ID0gY2MuZmluZCgnQ2FudmFzL01haW5CRy9EcmF3JylcclxuICAgIH0sXHJcblxyXG4gICAgc3RhcnQgKCkge1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICB0aGlzLm5vZGUub24oJ2NsaWNrJywgZnVuY3Rpb24gKGJ1dHRvbikge1xyXG4gICAgICAgICAgICAvL1RoZSBldmVudCBpcyBhIGN1c3RvbSBldmVudCwgeW91IGNvdWxkIGdldCB0aGUgQnV0dG9uIGNvbXBvbmVudCB2aWEgZmlyc3QgYXJndW1lbnRcclxuICAgICAgICAgICAgLyrpgJrov4fmqKHlnZfljJbohJrmnKzmk43kvZxcclxuICAgICAgICAgICAgdmFyIEdhbWVTdGFydCA9IHJlcXVpcmUoXCJHYW1lU3RhcnRcIik7XHJcbiAgICAgICAgICAgIG5ldyBHYW1lU3RhcnQoKS5zdGFydCgpOyAvLyDorr/pl67pnZnmgIHlj5jph4/kvJrmiqXplJnvvIzph4fnlKjnu4Tku7bmn6Xmib7nmoTmlrnms5VcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgLyrpgJrov4fmib7nu4Tku7bmk43kvZxcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL01haW5CRycpLmdldENvbXBvbmVudCgnR2FtZVN0YXJ0Jykuc3RhcnQoKTtcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgLyrnm7TmjqXph43mlrDliqDovb3lnLrmma9cclxuICAgICAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKFwiSG9tZVwiKTtcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ3hpYXlpZ3VhbmNodWZhbGUnKVxyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHJykuZ2V0Q29tcG9uZW50KCdHYW1lU3RhcnQnKS5zdGFydCgpO1xyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvVElQUy9QQVNTX0JHJykuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHNlbGYuRHJhdy5yZW1vdmVBbGxDaGlsZHJlbigpO1xyXG4gICAgICAgICAgICBjYy5yZXNvdXJjZXMubG9hZChcInBlcmZhYi9kcmF3XCIsIGZ1bmN0aW9uIChlcnIsIHByZWZhYikge1xyXG4gICAgICAgICAgICAgICAgdmFyIG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xyXG4gICAgICAgICAgICAgICAgbmV3Tm9kZS5wYXJlbnQgPSBzZWxmLkRyYXc7XHJcbiAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgLy/pn7PmlYhcclxuICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHNlbGYuQnV0dG9uQXVkaW8sIGZhbHNlKTtcclxuICAgICAgICAgICBsZXQgbGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgICAgICBpZihOdW1iZXIobGV2ZWwpICUgMiA9PSAwKXtcclxuXHRcdFx0XHRHLlNob3dJbnRlcnN0aXRpYWxBRCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIEcuQW5hbHlzdGljQ3VzdG9tRXZlbnQoJ0xFVkVMJywgbGV2ZWwpO1xyXG4gICAgICAgICB9KVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyB1cGRhdGUgKGR0KSB7fSxcclxufSk7XHJcbiJdfQ==