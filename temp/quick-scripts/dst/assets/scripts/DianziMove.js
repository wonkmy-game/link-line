
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/DianziMove.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0571e5EiyhCjJDWYBkC+u9Q', 'DianziMove');
// scripts/DianziMove.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("./GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DianziMove = /** @class */ (function (_super) {
    __extends(DianziMove, _super);
    function DianziMove() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ID = -1;
        _this.waypoint_id = 0;
        return _this;
    }
    DianziMove.prototype.init = function (storkelist, self, name) {
        var _this = this;
        var spName = this.stroke_color(name);
        this.ID = this.getIDByColor(spName);
        cc.resources.load("dzs/" + spName, cc.SpriteFrame, function (err, sf) {
            _this.node.getComponent(cc.Sprite).spriteFrame = sf;
        });
        var canvasPos1 = storkelist[0];
        var worldPos1 = self.node.convertToWorldSpaceAR(canvasPos1);
        var nodePos1 = self.Draw.convertToNodeSpaceAR(worldPos1);
        this.node.setPosition(nodePos1);
    };
    DianziMove.prototype.startMove = function (storkelist, self, startPos, endPos, startBall, endBall) {
        var _this = this;
        if (GameMgr_1.default.gameOver == true)
            return;
        if (this.waypoint_id >= storkelist.length) {
            cc.game.emit("show_ball_mask", startPos, endPos, this.ID, startBall, endBall);
            this.node.destroy();
            return;
        }
        var canvasPos = storkelist[this.waypoint_id];
        var worldPos = self.node.convertToWorldSpaceAR(canvasPos);
        var nodePos = self.Draw.convertToNodeSpaceAR(worldPos);
        cc.tween(this.node).to(0.02, { position: nodePos }).call(function () {
            _this.waypoint_id += 2;
            _this.startMove(storkelist, self, startPos, endPos, startBall, endBall);
        }).start();
    };
    DianziMove.prototype.getIDByColor = function (colorName) {
        switch (colorName) {
            case "dz-red":
                return 0;
            case "dz-yellow":
                return 1;
            case "dz-blue":
                return 2;
            case "dz-green":
                return 3;
        }
    };
    DianziMove.prototype.stroke_color = function (str) {
        switch (str) {
            case 'redball':
                return "dz-red";
            case 'yellowball':
                return "dz-yellow";
            case 'blueball':
                return "dz-blue";
            case 'greenball':
                return "dz-green";
        }
    };
    DianziMove.prototype.onCollisionEnter = function (other, self) {
        if (other.node.getComponent('DianziMove').ID != this.ID) {
            GameMgr_1.default.gameOver = true;
            cc.game.emit("gameover", this.node, this.node.parent);
        }
    };
    DianziMove = __decorate([
        ccclass
    ], DianziMove);
    return DianziMove;
}(cc.Component));
exports.default = DianziMove;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcRGlhbnppTW92ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLDRFQUE0RTtBQUM1RSxtQkFBbUI7QUFDbkIsc0ZBQXNGO0FBQ3RGLDhCQUE4QjtBQUM5QixzRkFBc0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUV0RixxQ0FBZ0M7QUFFMUIsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBd0MsOEJBQVk7SUFBcEQ7UUFBQSxxRUFtRUM7UUFqRUcsUUFBRSxHQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ2IsaUJBQVcsR0FBUSxDQUFDLENBQUM7O0lBZ0V6QixDQUFDO0lBOURHLHlCQUFJLEdBQUosVUFBSyxVQUFVLEVBQUMsSUFBSSxFQUFDLElBQUk7UUFBekIsaUJBVUM7UUFURyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxFQUFFLEdBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUMsTUFBTSxFQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUMsVUFBQyxHQUFHLEVBQUMsRUFBaUI7WUFDakUsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBQyxFQUFFLENBQUM7UUFDckQsQ0FBQyxDQUFDLENBQUE7UUFDRixJQUFJLFVBQVUsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDL0IsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM1RCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCw4QkFBUyxHQUFULFVBQVUsVUFBVSxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsTUFBTSxFQUFDLFNBQWlCLEVBQUMsT0FBZTtRQUEzRSxpQkFlQztRQWRHLElBQUcsaUJBQU8sQ0FBQyxRQUFRLElBQUUsSUFBSTtZQUFDLE9BQU87UUFDakMsSUFBRyxJQUFJLENBQUMsV0FBVyxJQUFFLFVBQVUsQ0FBQyxNQUFNLEVBQUM7WUFDbkMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUMsUUFBUSxFQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLFNBQVMsRUFBQyxPQUFPLENBQUMsQ0FBQztZQUV6RSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3BCLE9BQU87U0FDVjtRQUNELElBQUksU0FBUyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDN0MsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDckQsS0FBSSxDQUFDLFdBQVcsSUFBRSxDQUFDLENBQUM7WUFDcEIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxNQUFNLEVBQUMsU0FBUyxFQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3RFLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVELGlDQUFZLEdBQVosVUFBYSxTQUFTO1FBQ2xCLFFBQVEsU0FBUyxFQUFFO1lBQ2YsS0FBSyxRQUFRO2dCQUNULE9BQU8sQ0FBQyxDQUFDO1lBQ2IsS0FBSyxXQUFXO2dCQUNaLE9BQU8sQ0FBQyxDQUFDO1lBQ2IsS0FBSyxTQUFTO2dCQUNWLE9BQU8sQ0FBQyxDQUFDO1lBQ2IsS0FBSyxVQUFVO2dCQUNYLE9BQU8sQ0FBQyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQztJQUVELGlDQUFZLEdBQVosVUFBYSxHQUFHO1FBQ1osUUFBTyxHQUFHLEVBQUM7WUFDUCxLQUFLLFNBQVM7Z0JBQ1YsT0FBTyxRQUFRLENBQUM7WUFDcEIsS0FBSyxZQUFZO2dCQUNiLE9BQU8sV0FBVyxDQUFDO1lBQ3ZCLEtBQUssVUFBVTtnQkFDWCxPQUFPLFNBQVMsQ0FBQTtZQUNwQixLQUFLLFdBQVc7Z0JBQ1osT0FBTyxVQUFVLENBQUE7U0FDeEI7SUFFTCxDQUFDO0lBRUQscUNBQWdCLEdBQWhCLFVBQWlCLEtBQUssRUFBRSxJQUFJO1FBQ3hCLElBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUM7WUFDbkQsaUJBQU8sQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDO1lBQ3RCLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBQyxJQUFJLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDdEQ7SUFDTCxDQUFDO0lBbEVnQixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBbUU5QjtJQUFELGlCQUFDO0NBbkVELEFBbUVDLENBbkV1QyxFQUFFLENBQUMsU0FBUyxHQW1FbkQ7a0JBbkVvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4vR2FtZU1nclwiO1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEaWFuemlNb3ZlIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuXHJcbiAgICBJRDpudW1iZXI9LTE7XHJcbiAgICB3YXlwb2ludF9pZDpudW1iZXI9MDtcclxuXHJcbiAgICBpbml0KHN0b3JrZWxpc3Qsc2VsZixuYW1lKXtcclxuICAgICAgICB2YXIgc3BOYW1lID0gdGhpcy5zdHJva2VfY29sb3IobmFtZSk7XHJcbiAgICAgICAgdGhpcy5JRD10aGlzLmdldElEQnlDb2xvcihzcE5hbWUpO1xyXG4gICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwiZHpzL1wiK3NwTmFtZSxjYy5TcHJpdGVGcmFtZSwoZXJyLHNmOmNjLlNwcml0ZUZyYW1lKT0+e1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWU9c2Y7XHJcbiAgICAgICAgfSlcclxuICAgICAgICB2YXIgY2FudmFzUG9zMSA9IHN0b3JrZWxpc3RbMF07XHJcbiAgICAgICAgdmFyIHdvcmxkUG9zMSA9IHNlbGYubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoY2FudmFzUG9zMSk7XHJcbiAgICAgICAgdmFyIG5vZGVQb3MxID0gc2VsZi5EcmF3LmNvbnZlcnRUb05vZGVTcGFjZUFSKHdvcmxkUG9zMSk7XHJcbiAgICAgICAgdGhpcy5ub2RlLnNldFBvc2l0aW9uKG5vZGVQb3MxKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydE1vdmUoc3RvcmtlbGlzdCxzZWxmLHN0YXJ0UG9zLGVuZFBvcyxzdGFydEJhbGw6Y2MuTm9kZSxlbmRCYWxsOmNjLk5vZGUpe1xyXG4gICAgICAgIGlmKEdhbWVNZ3IuZ2FtZU92ZXI9PXRydWUpcmV0dXJuO1xyXG4gICAgICAgIGlmKHRoaXMud2F5cG9pbnRfaWQ+PXN0b3JrZWxpc3QubGVuZ3RoKXtcclxuICAgICAgICAgICAgY2MuZ2FtZS5lbWl0KFwic2hvd19iYWxsX21hc2tcIixzdGFydFBvcyxlbmRQb3MsdGhpcy5JRCxzdGFydEJhbGwsZW5kQmFsbCk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciBjYW52YXNQb3MgPSBzdG9ya2VsaXN0W3RoaXMud2F5cG9pbnRfaWRdO1xyXG4gICAgICAgIHZhciB3b3JsZFBvcyA9IHNlbGYubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoY2FudmFzUG9zKTtcclxuICAgICAgICB2YXIgbm9kZVBvcyA9IHNlbGYuRHJhdy5jb252ZXJ0VG9Ob2RlU3BhY2VBUih3b3JsZFBvcyk7XHJcbiAgICAgICAgY2MudHdlZW4odGhpcy5ub2RlKS50bygwLjAyLCB7IHBvc2l0aW9uOiBub2RlUG9zIH0pLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLndheXBvaW50X2lkKz0yO1xyXG4gICAgICAgICAgICB0aGlzLnN0YXJ0TW92ZShzdG9ya2VsaXN0LHNlbGYsc3RhcnRQb3MsZW5kUG9zLHN0YXJ0QmFsbCxlbmRCYWxsKTtcclxuICAgICAgICB9KS5zdGFydCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldElEQnlDb2xvcihjb2xvck5hbWUpe1xyXG4gICAgICAgIHN3aXRjaCAoY29sb3JOYW1lKSB7XHJcbiAgICAgICAgICAgIGNhc2UgXCJkei1yZWRcIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiAwO1xyXG4gICAgICAgICAgICBjYXNlIFwiZHoteWVsbG93XCI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gMTtcclxuICAgICAgICAgICAgY2FzZSBcImR6LWJsdWVcIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiAyO1xyXG4gICAgICAgICAgICBjYXNlIFwiZHotZ3JlZW5cIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiAzO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdHJva2VfY29sb3Ioc3RyKXtcclxuICAgICAgICBzd2l0Y2goc3RyKXtcclxuICAgICAgICAgICAgY2FzZSAncmVkYmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJkei1yZWRcIjtcclxuICAgICAgICAgICAgY2FzZSAneWVsbG93YmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJkei15ZWxsb3dcIjtcclxuICAgICAgICAgICAgY2FzZSAnYmx1ZWJhbGwnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiZHotYmx1ZVwiXHJcbiAgICAgICAgICAgIGNhc2UgJ2dyZWVuYmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJkei1ncmVlblwiXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuICAgIFxyXG4gICAgb25Db2xsaXNpb25FbnRlcihvdGhlcizCoHNlbGYpwqB7XHJcbiAgICAgICAgaWYob3RoZXIubm9kZS5nZXRDb21wb25lbnQoJ0RpYW56aU1vdmUnKS5JRCAhPSB0aGlzLklEKXtcclxuICAgICAgICAgICAgR2FtZU1nci5nYW1lT3Zlcj10cnVlO1xyXG4gICAgICAgICAgICBjYy5nYW1lLmVtaXQoXCJnYW1lb3ZlclwiLHRoaXMubm9kZSx0aGlzLm5vZGUucGFyZW50KVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=