
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/SelectPanel.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '52b6e5B1OpDmqrDtECFVJbm', 'SelectPanel');
// scripts/SelectPanel.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var LevelBtn_1 = require("./LevelBtn");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SelectPanel = /** @class */ (function (_super) {
    __extends(SelectPanel, _super);
    function SelectPanel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.level_btn = null;
        _this.scrollView = null;
        return _this;
    }
    SelectPanel.prototype.onLoad = function () {
        this.node.getChildByName("splash").on(cc.Node.EventType.TOUCH_START, this.hideSelectPanel, this);
        this.generatorLevelBtn();
    };
    SelectPanel.prototype.generatorLevelBtn = function () {
        var content = this.scrollView.node.getChildByName("view").getChildByName("content");
        content.width = 644;
        // content.height = 22 * 80;
        // content.height = 22;
        for (var i = 1; i <= 2; i++) {
            var newLevel_btn = cc.instantiate(this.level_btn);
            newLevel_btn.setParent(this.scrollView.node.getChildByName("view").getChildByName("content"));
            newLevel_btn.setPosition(0, (i - 1) * -100);
            newLevel_btn.getComponent(LevelBtn_1.default).init(i);
        }
    };
    SelectPanel.prototype.hideSelectPanel = function () {
        this.node.active = false;
    };
    __decorate([
        property({ type: cc.Prefab })
    ], SelectPanel.prototype, "level_btn", void 0);
    __decorate([
        property({ type: cc.ScrollView })
    ], SelectPanel.prototype, "scrollView", void 0);
    SelectPanel = __decorate([
        ccclass
    ], SelectPanel);
    return SelectPanel;
}(cc.Component));
exports.default = SelectPanel;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU2VsZWN0UGFuZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQiw0RUFBNEU7QUFDNUUsbUJBQW1CO0FBQ25CLHNGQUFzRjtBQUN0Riw4QkFBOEI7QUFDOUIsc0ZBQXNGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFdEYsdUNBQWtDO0FBRTVCLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXlDLCtCQUFZO0lBQXJEO1FBQUEscUVBaUNDO1FBOUJHLGVBQVMsR0FBYyxJQUFJLENBQUM7UUFHNUIsZ0JBQVUsR0FBa0IsSUFBSSxDQUFDOztJQTJCckMsQ0FBQztJQXRCYSw0QkFBTSxHQUFoQjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQyxJQUFJLENBQUMsQ0FBQztRQUUvRixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsdUNBQWlCLEdBQWpCO1FBQ0ksSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNwRixPQUFPLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUNwQiw0QkFBNEI7UUFDNUIsdUJBQXVCO1FBQ3ZCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDekIsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbEQsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDOUYsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QyxZQUFZLENBQUMsWUFBWSxDQUFDLGtCQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDL0M7SUFDTCxDQUFDO0lBRUQscUNBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFDLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBN0JEO1FBREMsUUFBUSxDQUFDLEVBQUMsSUFBSSxFQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUMsQ0FBQztrREFDQztJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFDLElBQUksRUFBQyxFQUFFLENBQUMsVUFBVSxFQUFDLENBQUM7bURBQ0U7SUFOaEIsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQWlDL0I7SUFBRCxrQkFBQztDQWpDRCxBQWlDQyxDQWpDd0MsRUFBRSxDQUFDLFNBQVMsR0FpQ3BEO2tCQWpDb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgTGV2ZWxCdG4gZnJvbSBcIi4vTGV2ZWxCdG5cIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZWxlY3RQYW5lbCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoe3R5cGU6Y2MuUHJlZmFifSlcbiAgICBsZXZlbF9idG46IGNjLlByZWZhYiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoe3R5cGU6Y2MuU2Nyb2xsVmlld30pXG4gICAgc2Nyb2xsVmlldzogY2MuU2Nyb2xsVmlldyA9IG51bGw7XG5cblxuICAgIGNmZ0FycjpBcnJheTxudW1iZXI+XG5cbiAgICBwcm90ZWN0ZWQgb25Mb2FkKCk6IHZvaWQge1xuICAgICAgICB0aGlzLm5vZGUuZ2V0Q2hpbGRCeU5hbWUoXCJzcGxhc2hcIikub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsdGhpcy5oaWRlU2VsZWN0UGFuZWwsdGhpcyk7XG5cbiAgICAgICAgdGhpcy5nZW5lcmF0b3JMZXZlbEJ0bigpO1xuICAgIH1cblxuICAgIGdlbmVyYXRvckxldmVsQnRuKCkge1xuICAgICAgICBsZXQgY29udGVudCA9IHRoaXMuc2Nyb2xsVmlldy5ub2RlLmdldENoaWxkQnlOYW1lKFwidmlld1wiKS5nZXRDaGlsZEJ5TmFtZShcImNvbnRlbnRcIik7XG4gICAgICAgIGNvbnRlbnQud2lkdGggPSA2NDQ7XG4gICAgICAgIC8vIGNvbnRlbnQuaGVpZ2h0ID0gMjIgKiA4MDtcbiAgICAgICAgLy8gY29udGVudC5oZWlnaHQgPSAyMjtcbiAgICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gMjsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgbmV3TGV2ZWxfYnRuID0gY2MuaW5zdGFudGlhdGUodGhpcy5sZXZlbF9idG4pO1xuICAgICAgICAgICAgbmV3TGV2ZWxfYnRuLnNldFBhcmVudCh0aGlzLnNjcm9sbFZpZXcubm9kZS5nZXRDaGlsZEJ5TmFtZShcInZpZXdcIikuZ2V0Q2hpbGRCeU5hbWUoXCJjb250ZW50XCIpKTtcbiAgICAgICAgICAgIG5ld0xldmVsX2J0bi5zZXRQb3NpdGlvbigwLChpLTEpKi0xMDApO1xuICAgICAgICAgICAgbmV3TGV2ZWxfYnRuLmdldENvbXBvbmVudChMZXZlbEJ0bikuaW5pdChpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGhpZGVTZWxlY3RQYW5lbCgpe1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlPWZhbHNlO1xuICAgIH1cbn1cbiJdfQ==