
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/replay.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '45b59rn59JF1JEASzsa37Iw', 'replay');
// scripts/replay.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    ButtonAudio: cc.AudioClip
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {},
  start: function start() {
    var self = this;
    this.node.on('click', function (button) {
      //The event is a custom event, you could get the Button component via first argument

      /*通过模块化脚本操作
      var GameStart = require("GameStart");
      new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
      */

      /*通过找组件操作
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      */

      /*直接重新加载场景
      cc.director.loadScene("Home");
      */
      G.reset();
      Storage.Set_Info('level', "0");
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      cc.find('Canvas/TIPS/全部通关').active = false; //音效

      cc.audioEngine.playEffect(self.ButtonAudio, false);
    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xccmVwbGF5LmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiQnV0dG9uQXVkaW8iLCJBdWRpb0NsaXAiLCJvbkxvYWQiLCJzdGFydCIsInNlbGYiLCJub2RlIiwib24iLCJidXR0b24iLCJHIiwicmVzZXQiLCJTdG9yYWdlIiwiU2V0X0luZm8iLCJmaW5kIiwiZ2V0Q29tcG9uZW50IiwiYWN0aXZlIiwiYXVkaW9FbmdpbmUiLCJwbGF5RWZmZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztFQUNMLFdBQVNELEVBQUUsQ0FBQ0UsU0FEUDtFQUdMQyxVQUFVLEVBQUU7SUFDUkMsV0FBVyxFQUFDSixFQUFFLENBQUNLO0VBRFAsQ0FIUDtFQU9MO0VBRUFDLE1BVEssb0JBU0ssQ0FDVCxDQVZJO0VBWUxDLEtBWkssbUJBWUk7SUFDTCxJQUFJQyxJQUFJLEdBQUcsSUFBWDtJQUNBLEtBQUtDLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBVUMsTUFBVixFQUFrQjtNQUNwQzs7TUFDQTtBQUNaO0FBQ0E7QUFDQTs7TUFDWTtBQUNaO0FBQ0E7O01BQ1k7QUFDWjtBQUNBO01BQ1lDLENBQUMsQ0FBQ0MsS0FBRjtNQUNBQyxPQUFPLENBQUNDLFFBQVIsQ0FBaUIsT0FBakIsRUFBeUIsR0FBekI7TUFDQWYsRUFBRSxDQUFDZ0IsSUFBSCxDQUFRLGVBQVIsRUFBeUJDLFlBQXpCLENBQXNDLFdBQXRDLEVBQW1EVixLQUFuRDtNQUNBUCxFQUFFLENBQUNnQixJQUFILENBQVEsa0JBQVIsRUFBNEJFLE1BQTVCLEdBQXFDLEtBQXJDLENBZm9DLENBZ0JwQzs7TUFDRGxCLEVBQUUsQ0FBQ21CLFdBQUgsQ0FBZUMsVUFBZixDQUEwQlosSUFBSSxDQUFDSixXQUEvQixFQUE0QyxLQUE1QztJQUNELENBbEJGO0VBbUJILENBakNJLENBbUNMOztBQW5DSyxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgQnV0dG9uQXVkaW86Y2MuQXVkaW9DbGlwLFxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgfSxcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLCBmdW5jdGlvbiAoYnV0dG9uKSB7XHJcbiAgICAgICAgICAgIC8vVGhlIGV2ZW50IGlzIGEgY3VzdG9tIGV2ZW50LCB5b3UgY291bGQgZ2V0IHRoZSBCdXR0b24gY29tcG9uZW50IHZpYSBmaXJzdCBhcmd1bWVudFxyXG4gICAgICAgICAgICAvKumAmui/h+aooeWdl+WMluiEmuacrOaTjeS9nFxyXG4gICAgICAgICAgICB2YXIgR2FtZVN0YXJ0ID0gcmVxdWlyZShcIkdhbWVTdGFydFwiKTtcclxuICAgICAgICAgICAgbmV3IEdhbWVTdGFydCgpLnN0YXJ0KCk7IC8vIOiuv+mXrumdmeaAgeWPmOmHj+S8muaKpemUme+8jOmHh+eUqOe7hOS7tuafpeaJvueahOaWueazlVxyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKumAmui/h+aJvue7hOS7tuaTjeS9nFxyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHJykuZ2V0Q29tcG9uZW50KCdHYW1lU3RhcnQnKS5zdGFydCgpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKuebtOaOpemHjeaWsOWKoOi9veWcuuaZr1xyXG4gICAgICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJIb21lXCIpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBHLnJlc2V0KCk7XHJcbiAgICAgICAgICAgIFN0b3JhZ2UuU2V0X0luZm8oJ2xldmVsJyxcIjBcIik7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy9NYWluQkcnKS5nZXRDb21wb25lbnQoJ0dhbWVTdGFydCcpLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy9USVBTL+WFqOmDqOmAmuWFsycpLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvL+mfs+aViFxyXG4gICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3Qoc2VsZi5CdXR0b25BdWRpbywgZmFsc2UpO1xyXG4gICAgICAgICB9KVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyB1cGRhdGUgKGR0KSB7fSxcclxufSk7XHJcbiJdfQ==