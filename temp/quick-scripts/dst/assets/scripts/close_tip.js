
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/close_tip.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0332eDs/ptLRZzgGKTRm85Z', 'close_tip');
// scripts/close_tip.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    TIPS_BG_Node: cc.Node,
    ButtonAudio: cc.AudioClip
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {// this.Draw = cc.find('Canvas/MainBG/Draw')
  },
  start: function start() {
    var self = this;
    this.node.on('click', function (button) {
      //The event is a custom event, you could get the Button component via first argument

      /*通过模块化脚本操作
      var GameStart = require("GameStart");
      new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
      */

      /*通过找组件操作
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      */

      /*直接重新加载场景
      cc.director.loadScene("Home");
      */
      //展示提示，首先将提示的图片找到
      cc.find('Canvas/TIPS/Tip').active = false; //音效

      cc.audioEngine.playEffect(self.ButtonAudio, false);
    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcY2xvc2VfdGlwLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiVElQU19CR19Ob2RlIiwiTm9kZSIsIkJ1dHRvbkF1ZGlvIiwiQXVkaW9DbGlwIiwib25Mb2FkIiwic3RhcnQiLCJzZWxmIiwibm9kZSIsIm9uIiwiYnV0dG9uIiwiZmluZCIsImFjdGl2ZSIsImF1ZGlvRW5naW5lIiwicGxheUVmZmVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLFlBQVksRUFBQ0osRUFBRSxDQUFDSyxJQURSO0lBRVJDLFdBQVcsRUFBQ04sRUFBRSxDQUFDTztFQUZQLENBSFA7RUFRTDtFQUVBQyxNQVZLLG9CQVVLLENBQ047RUFDSCxDQVpJO0VBY0xDLEtBZEssbUJBY0k7SUFDTCxJQUFJQyxJQUFJLEdBQUcsSUFBWDtJQUNBLEtBQUtDLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBVUMsTUFBVixFQUFrQjtNQUNwQzs7TUFDQTtBQUNaO0FBQ0E7QUFDQTs7TUFDWTtBQUNaO0FBQ0E7O01BQ1k7QUFDWjtBQUNBO01BQ1c7TUFDQWIsRUFBRSxDQUFDYyxJQUFILENBQVEsaUJBQVIsRUFBMkJDLE1BQTNCLEdBQW9DLEtBQXBDLENBYnFDLENBY3JDOztNQUNBZixFQUFFLENBQUNnQixXQUFILENBQWVDLFVBQWYsQ0FBMEJQLElBQUksQ0FBQ0osV0FBL0IsRUFBNEMsS0FBNUM7SUFDRCxDQWhCRjtFQWlCSCxDQWpDSSxDQW1DTDs7QUFuQ0ssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcblxyXG5jYy5DbGFzcyh7XHJcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXHJcblxyXG4gICAgcHJvcGVydGllczoge1xyXG4gICAgICAgIFRJUFNfQkdfTm9kZTpjYy5Ob2RlLFxyXG4gICAgICAgIEJ1dHRvbkF1ZGlvOmNjLkF1ZGlvQ2xpcCxcclxuICAgIH0sXHJcblxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuICAgICAgICAvLyB0aGlzLkRyYXcgPSBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0RyYXcnKVxyXG4gICAgfSxcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLCBmdW5jdGlvbiAoYnV0dG9uKSB7XHJcbiAgICAgICAgICAgIC8vVGhlIGV2ZW50IGlzIGEgY3VzdG9tIGV2ZW50LCB5b3UgY291bGQgZ2V0IHRoZSBCdXR0b24gY29tcG9uZW50IHZpYSBmaXJzdCBhcmd1bWVudFxyXG4gICAgICAgICAgICAvKumAmui/h+aooeWdl+WMluiEmuacrOaTjeS9nFxyXG4gICAgICAgICAgICB2YXIgR2FtZVN0YXJ0ID0gcmVxdWlyZShcIkdhbWVTdGFydFwiKTtcclxuICAgICAgICAgICAgbmV3IEdhbWVTdGFydCgpLnN0YXJ0KCk7IC8vIOiuv+mXrumdmeaAgeWPmOmHj+S8muaKpemUme+8jOmHh+eUqOe7hOS7tuafpeaJvueahOaWueazlVxyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKumAmui/h+aJvue7hOS7tuaTjeS9nFxyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHJykuZ2V0Q29tcG9uZW50KCdHYW1lU3RhcnQnKS5zdGFydCgpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKuebtOaOpemHjeaWsOWKoOi9veWcuuaZr1xyXG4gICAgICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJIb21lXCIpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgIC8v5bGV56S65o+Q56S677yM6aaW5YWI5bCG5o+Q56S655qE5Zu+54mH5om+5YiwXHJcbiAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL1RJUFMvVGlwJykuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgLy/pn7PmlYhcclxuICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHNlbGYuQnV0dG9uQXVkaW8sIGZhbHNlKTtcclxuICAgICAgICAgfSlcclxuICAgIH0sXHJcblxyXG4gICAgLy8gdXBkYXRlIChkdCkge30sXHJcbn0pO1xyXG4iXX0=