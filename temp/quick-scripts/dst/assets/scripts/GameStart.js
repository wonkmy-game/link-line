
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/GameStart.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '52952eOu/BK950/ZvvmzZ4x', 'GameStart');
// scripts/GameStart.js

"use strict";

var _GameMgr = _interopRequireDefault(require("./GameMgr"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    BG_Node: cc.Node,
    redball: cc.Prefab,
    yellowball: cc.Prefab,
    greenball: cc.Prefab,
    purpleball: cc.Prefab,
    blueball: cc.Prefab,
    Level: cc.Label,
    BGM: cc.AudioClip,
    backMenu: cc.Node,
    zhuzi1: cc.Node,
    zhuzi2: cc.Node,
    _fx_list: []
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    //播放bgm
    var bgm = cc.audioEngine.playMusic(this.BGM, true);
    cc.audioEngine.setVolume(bgm, 0.15);
    this.backMenu.on(cc.Node.EventType.TOUCH_START, this.backToMenu, this);
    cc.director.getCollisionManager().enabled = true;
    cc.game.on("show_ball_mask", this.showMask, this);
    cc.game.on("gameover", this.OnGameover, this);
  },
  OnGameover: function OnGameover(node, parent) {
    var _this = this;

    var newPos = node.position;
    cc.game.off("gameover");
    node.destroy();
    cc.resources.load("perfab/col_fx", cc.Prefab, function (err, fx) {
      var newFx = cc.instantiate(fx);
      parent.addChild(newFx);
      newFx.scale = 0.5;
      newFx.setPosition(newPos);
    });
    this.scheduleOnce(function () {
      _this.showWrongIcon(new cc.Vec3(newPos.x, newPos.y - 50, 0), parent);

      _this.scheduleOnce(function () {
        _this.backToMenu();
      }, 2);
    }, 2);
  },
  showWrongIcon: function showWrongIcon(pos, parent) {
    var self = this;
    cc.resources.load("perfab/wrong", function (err, prefab) {
      var newNode = cc.instantiate(prefab);
      newNode.parent = parent;
      newNode.setPosition(pos);
      newNode.scale = 1.5;
      setTimeout(function () {
        newNode.active = false;
        newNode.destroy();
      }, 2000);
    });
  },
  showMask: function showMask(startPos, endPos, ballId, startBall, endBall) {
    var self = this;
    var level = Storage.Get_Info('level');
    var color = this.getIDByColor(ballId);
    var startPosNewBall = this.load_prefab(color);
    var endPosNewBall = this.load_prefab(color);
    self.BG_Node.addChild(endPosNewBall);
    self.BG_Node.addChild(startPosNewBall);
    var scaleValue = 1;
    var flipX = 1;

    if (Number(level) + 1 == 1) {
      if (startPos.y < endPos.y) {
        //从下往上画线
        startPosNewBall.angle = 35;
        startPosNewBall.scale = new cc.Vec3(-1.6, 1.6, 1.6);
        endPosNewBall.angle = 35;
        endPosNewBall.scale = new cc.Vec3(1.6, 1.6, 1.6);
        scaleValue = 3;
      } else {
        //从上往下画线
        startPosNewBall.angle = 35;
        startPosNewBall.scale = new cc.Vec3(1.6, 1.6, 1.6);
        endPosNewBall.angle = 35;
        endPosNewBall.scale = new cc.Vec3(-1.6, 1.6, 1.6);
        scaleValue = -3;
      }
    } else if (Number(level) + 1 == 2) {
      if (startPos.y < endPos.y) //从下往上画线
        {
          startPosNewBall.angle = 0;
          startPosNewBall.scale = new cc.Vec3(-1, 1, 1);
          endPosNewBall.angle = 0;
          endPosNewBall.scale = new cc.Vec3(1, 1, 1);
          scaleValue = 2;
        } else {
        startPosNewBall.angle = 0;
        startPosNewBall.scale = new cc.Vec3(1, 1, 1);
        endPosNewBall.angle = 0;
        endPosNewBall.scale = new cc.Vec3(-1, 1, 1);
        scaleValue = -2;
      }
    }

    var final_pos = self.BG_Node.convertToNodeSpaceAR(endPos);
    var final_pos1 = self.BG_Node.convertToNodeSpaceAR(startPos);
    endPosNewBall.setPosition(final_pos.x, final_pos.y, 0);
    startPosNewBall.setPosition(final_pos1.x, final_pos1.y, 0);
    cc.tween(endPosNewBall).to(0.3, {
      scale: scaleValue,
      opacity: 0
    }).call(function () {
      endPosNewBall.destroy();
    }).start();
    cc.tween(startPosNewBall).to(0.3, {
      scale: -scaleValue,
      opacity: 0
    }).call(function () {
      startPosNewBall.destroy();
    }).start();
  },
  getIDByColor: function getIDByColor(ballId) {
    switch (ballId) {
      case 0:
        return "redball";

      case 1:
        return "yellowball";

      case 2:
        return "blueball";

      case 3:
        return "greenball";
    }
  },
  backToMenu: function backToMenu() {
    _GameMgr["default"].gameOver = false;
    cc.director.loadScene('launch');
  },
  start: function start() {
    var self = this;
    G.reset(); // console.log('清空G');

    this.BG_Node = cc.find('Canvas/MainBG/BG');
    this.BG_Node.removeAllChildren(); //读取当前是哪一个关卡

    var level = Storage.Get_Info('level');

    if (Number(level) >= 2) {
      //全部通关
      //cc.find('Canvas/TIPS/全部通关').active = true;
      level = 0;
      Storage.Set_Info('level', 0);
      this.backToMenu();
      return;
    }

    if (Number(level) + 1 == 1) {
      this.zhuzi1.setPosition(446.924, this.zhuzi1.position.y - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi2.setPosition(-398.924, this.zhuzi2.position.y - _GameMgr["default"].GlobalOffsetY, 0);
    }

    if (Number(level) + 1 == 2) {
      this.zhuzi1.setPosition(465, 384 - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi1.angle = 0;
      this.zhuzi2.setPosition(-455, -6 - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi2.angle = 180;
    } // if (cc.sys.platform === cc.sys.ANDROID) {
    //     self.Level.string = 'Level ' + (Number(level) + 1);
    // } else {
    //     self.Level.string = '关卡 ' + (Number(level) + 1);
    // }


    cc.resources.load('level/' + level, function (err, jsonAsset) {
      var level_json_info = jsonAsset.json;
      G.LevelJson = level_json_info; //self.loadbg(level_json_info.bg);

      self.loadballs(level_json_info.balls, Number(level) + 1);
    }); // G.AnalysticLogin();
    // G.wxtopshare();
  },
  // loadbg(str) {
  //     // console.log(str);
  //     var self = this;
  //     cc.resources.load("BG/" + str, cc.SpriteFrame, function (err, spriteFrame) {
  //         self.BG_Node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
  //     });
  // },
  loadballs: function loadballs(balls, level) {
    var self = this; // G.stokelist = balls.length;

    for (var i = 0; i < balls.length; i++) {
      var newnode1 = this.load_prefab(balls[i].color);
      self.BG_Node.addChild(newnode1);
      var ballOffset = 10;

      if (level == 1) {
        ballOffset = 10;
        newnode1.setPosition(balls[i].Pos[0] + balls[i].Pos[0] * 0.194, balls[i].Pos[1] - balls[i].Pos[0] * 0.194 - _GameMgr["default"].GlobalOffsetY - (balls[i].flipX == 1 ? ballOffset : -ballOffset));
      } else if (level == 2) {
        ballOffset = -8;
        newnode1.setPosition(balls[i].Pos[0], balls[i].Pos[1]);
      }

      newnode1.angle = balls[i].Rot;

      if (level == 1) {
        newnode1.scale = new cc.Vec3(balls[i].scale * balls[i].flipX * _GameMgr["default"].tubeScale, balls[i].scale * _GameMgr["default"].tubeScale, balls[i].scale);
      } else if (level == 2) {
        newnode1.scale = new cc.Vec3(balls[i].scale * balls[i].flipX, balls[i].scale, balls[i].scale);
      }

      G.BallList.push(newnode1);
      G.originBallList = G.BallList.concat(); // console.log(newnode1.position)
    }
  },
  load_prefab: function load_prefab(color) {
    switch (color) {
      case "redball":
        var node = cc.instantiate(this.redball);
        return node;

      case "yellowball":
        var node = cc.instantiate(this.yellowball);
        return node;

      case "blueball":
        var node = cc.instantiate(this.blueball);
        return node;

      case "purpleball":
        var node = cc.instantiate(this.purpleball);
        return node;

      case "greenball":
        var node = cc.instantiate(this.greenball);
        return node;

      default:
        console.log('颜色不对');
    }
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcR2FtZVN0YXJ0LmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiQkdfTm9kZSIsIk5vZGUiLCJyZWRiYWxsIiwiUHJlZmFiIiwieWVsbG93YmFsbCIsImdyZWVuYmFsbCIsInB1cnBsZWJhbGwiLCJibHVlYmFsbCIsIkxldmVsIiwiTGFiZWwiLCJCR00iLCJBdWRpb0NsaXAiLCJiYWNrTWVudSIsInpodXppMSIsInpodXppMiIsIl9meF9saXN0Iiwib25Mb2FkIiwiYmdtIiwiYXVkaW9FbmdpbmUiLCJwbGF5TXVzaWMiLCJzZXRWb2x1bWUiLCJvbiIsIkV2ZW50VHlwZSIsIlRPVUNIX1NUQVJUIiwiYmFja1RvTWVudSIsImRpcmVjdG9yIiwiZ2V0Q29sbGlzaW9uTWFuYWdlciIsImVuYWJsZWQiLCJnYW1lIiwic2hvd01hc2siLCJPbkdhbWVvdmVyIiwibm9kZSIsInBhcmVudCIsIm5ld1BvcyIsInBvc2l0aW9uIiwib2ZmIiwiZGVzdHJveSIsInJlc291cmNlcyIsImxvYWQiLCJlcnIiLCJmeCIsIm5ld0Z4IiwiaW5zdGFudGlhdGUiLCJhZGRDaGlsZCIsInNjYWxlIiwic2V0UG9zaXRpb24iLCJzY2hlZHVsZU9uY2UiLCJzaG93V3JvbmdJY29uIiwiVmVjMyIsIngiLCJ5IiwicG9zIiwic2VsZiIsInByZWZhYiIsIm5ld05vZGUiLCJzZXRUaW1lb3V0IiwiYWN0aXZlIiwic3RhcnRQb3MiLCJlbmRQb3MiLCJiYWxsSWQiLCJzdGFydEJhbGwiLCJlbmRCYWxsIiwibGV2ZWwiLCJTdG9yYWdlIiwiR2V0X0luZm8iLCJjb2xvciIsImdldElEQnlDb2xvciIsInN0YXJ0UG9zTmV3QmFsbCIsImxvYWRfcHJlZmFiIiwiZW5kUG9zTmV3QmFsbCIsInNjYWxlVmFsdWUiLCJmbGlwWCIsIk51bWJlciIsImFuZ2xlIiwiZmluYWxfcG9zIiwiY29udmVydFRvTm9kZVNwYWNlQVIiLCJmaW5hbF9wb3MxIiwidHdlZW4iLCJ0byIsIm9wYWNpdHkiLCJjYWxsIiwic3RhcnQiLCJHYW1lTWdyIiwiZ2FtZU92ZXIiLCJsb2FkU2NlbmUiLCJHIiwicmVzZXQiLCJmaW5kIiwicmVtb3ZlQWxsQ2hpbGRyZW4iLCJTZXRfSW5mbyIsIkdsb2JhbE9mZnNldFkiLCJqc29uQXNzZXQiLCJsZXZlbF9qc29uX2luZm8iLCJqc29uIiwiTGV2ZWxKc29uIiwibG9hZGJhbGxzIiwiYmFsbHMiLCJpIiwibGVuZ3RoIiwibmV3bm9kZTEiLCJiYWxsT2Zmc2V0IiwiUG9zIiwiUm90IiwidHViZVNjYWxlIiwiQmFsbExpc3QiLCJwdXNoIiwib3JpZ2luQmFsbExpc3QiLCJjb25jYXQiLCJjb25zb2xlIiwibG9nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQU1BOzs7O0FBTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0VBQ0wsV0FBU0QsRUFBRSxDQUFDRSxTQURQO0VBR0xDLFVBQVUsRUFBRTtJQUNSQyxPQUFPLEVBQUVKLEVBQUUsQ0FBQ0ssSUFESjtJQUVSQyxPQUFPLEVBQUVOLEVBQUUsQ0FBQ08sTUFGSjtJQUdSQyxVQUFVLEVBQUVSLEVBQUUsQ0FBQ08sTUFIUDtJQUlSRSxTQUFTLEVBQUVULEVBQUUsQ0FBQ08sTUFKTjtJQUtSRyxVQUFVLEVBQUVWLEVBQUUsQ0FBQ08sTUFMUDtJQU1SSSxRQUFRLEVBQUVYLEVBQUUsQ0FBQ08sTUFOTDtJQU9SSyxLQUFLLEVBQUVaLEVBQUUsQ0FBQ2EsS0FQRjtJQVFSQyxHQUFHLEVBQUVkLEVBQUUsQ0FBQ2UsU0FSQTtJQVNSQyxRQUFRLEVBQUNoQixFQUFFLENBQUNLLElBVEo7SUFVUlksTUFBTSxFQUFDakIsRUFBRSxDQUFDSyxJQVZGO0lBV1JhLE1BQU0sRUFBQ2xCLEVBQUUsQ0FBQ0ssSUFYRjtJQVlSYyxRQUFRLEVBQUM7RUFaRCxDQUhQO0VBa0JMO0VBRUFDLE1BcEJLLG9CQW9CSTtJQUNMO0lBQ0EsSUFBSUMsR0FBRyxHQUFHckIsRUFBRSxDQUFDc0IsV0FBSCxDQUFlQyxTQUFmLENBQXlCLEtBQUtULEdBQTlCLEVBQW1DLElBQW5DLENBQVY7SUFDQWQsRUFBRSxDQUFDc0IsV0FBSCxDQUFlRSxTQUFmLENBQXlCSCxHQUF6QixFQUE4QixJQUE5QjtJQUNBLEtBQUtMLFFBQUwsQ0FBY1MsRUFBZCxDQUFpQnpCLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRcUIsU0FBUixDQUFrQkMsV0FBbkMsRUFBK0MsS0FBS0MsVUFBcEQsRUFBK0QsSUFBL0Q7SUFDQTVCLEVBQUUsQ0FBQzZCLFFBQUgsQ0FBWUMsbUJBQVosR0FBa0NDLE9BQWxDLEdBQTBDLElBQTFDO0lBRUEvQixFQUFFLENBQUNnQyxJQUFILENBQVFQLEVBQVIsQ0FBVyxnQkFBWCxFQUE0QixLQUFLUSxRQUFqQyxFQUEwQyxJQUExQztJQUNBakMsRUFBRSxDQUFDZ0MsSUFBSCxDQUFRUCxFQUFSLENBQVcsVUFBWCxFQUFzQixLQUFLUyxVQUEzQixFQUFzQyxJQUF0QztFQUNILENBN0JJO0VBK0JMQSxVQS9CSyxzQkErQk1DLElBL0JOLEVBK0JXQyxNQS9CWCxFQStCa0I7SUFBQTs7SUFDbkIsSUFBSUMsTUFBTSxHQUFHRixJQUFJLENBQUNHLFFBQWxCO0lBQ0F0QyxFQUFFLENBQUNnQyxJQUFILENBQVFPLEdBQVIsQ0FBWSxVQUFaO0lBQ0FKLElBQUksQ0FBQ0ssT0FBTDtJQUNBeEMsRUFBRSxDQUFDeUMsU0FBSCxDQUFhQyxJQUFiLENBQWtCLGVBQWxCLEVBQWtDMUMsRUFBRSxDQUFDTyxNQUFyQyxFQUE0QyxVQUFDb0MsR0FBRCxFQUFLQyxFQUFMLEVBQVU7TUFDbEQsSUFBSUMsS0FBSyxHQUFDN0MsRUFBRSxDQUFDOEMsV0FBSCxDQUFlRixFQUFmLENBQVY7TUFDQVIsTUFBTSxDQUFDVyxRQUFQLENBQWdCRixLQUFoQjtNQUNBQSxLQUFLLENBQUNHLEtBQU4sR0FBWSxHQUFaO01BQ0FILEtBQUssQ0FBQ0ksV0FBTixDQUFrQlosTUFBbEI7SUFDSCxDQUxEO0lBTUEsS0FBS2EsWUFBTCxDQUFrQixZQUFJO01BQ2xCLEtBQUksQ0FBQ0MsYUFBTCxDQUFtQixJQUFJbkQsRUFBRSxDQUFDb0QsSUFBUCxDQUFZZixNQUFNLENBQUNnQixDQUFuQixFQUFxQmhCLE1BQU0sQ0FBQ2lCLENBQVAsR0FBVyxFQUFoQyxFQUFtQyxDQUFuQyxDQUFuQixFQUF5RGxCLE1BQXpEOztNQUNBLEtBQUksQ0FBQ2MsWUFBTCxDQUFrQixZQUFJO1FBQ2xCLEtBQUksQ0FBQ3RCLFVBQUw7TUFDSCxDQUZELEVBRUUsQ0FGRjtJQUdILENBTEQsRUFLRSxDQUxGO0VBT0gsQ0FoREk7RUFpREx1QixhQWpESyx5QkFpRFNJLEdBakRULEVBaURhbkIsTUFqRGIsRUFpRG9CO0lBQ3JCLElBQUlvQixJQUFJLEdBQUcsSUFBWDtJQUNBeEQsRUFBRSxDQUFDeUMsU0FBSCxDQUFhQyxJQUFiLENBQWtCLGNBQWxCLEVBQWtDLFVBQVVDLEdBQVYsRUFBZWMsTUFBZixFQUF1QjtNQUNyRCxJQUFJQyxPQUFPLEdBQUcxRCxFQUFFLENBQUM4QyxXQUFILENBQWVXLE1BQWYsQ0FBZDtNQUNBQyxPQUFPLENBQUN0QixNQUFSLEdBQWlCQSxNQUFqQjtNQUNBc0IsT0FBTyxDQUFDVCxXQUFSLENBQW9CTSxHQUFwQjtNQUNBRyxPQUFPLENBQUNWLEtBQVIsR0FBZSxHQUFmO01BQ0FXLFVBQVUsQ0FBQyxZQUFVO1FBQ2pCRCxPQUFPLENBQUNFLE1BQVIsR0FBaUIsS0FBakI7UUFDQUYsT0FBTyxDQUFDbEIsT0FBUjtNQUNILENBSFMsRUFHUixJQUhRLENBQVY7SUFJSCxDQVREO0VBVUgsQ0E3REk7RUE4RExQLFFBOURLLG9CQThESTRCLFFBOURKLEVBOERhQyxNQTlEYixFQThEb0JDLE1BOURwQixFQThEMkJDLFNBOUQzQixFQThEcUNDLE9BOURyQyxFQThENkM7SUFDOUMsSUFBSVQsSUFBSSxHQUFHLElBQVg7SUFDQSxJQUFJVSxLQUFLLEdBQUdDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFaO0lBQ0EsSUFBSUMsS0FBSyxHQUFHLEtBQUtDLFlBQUwsQ0FBa0JQLE1BQWxCLENBQVo7SUFDQSxJQUFJUSxlQUFlLEdBQUcsS0FBS0MsV0FBTCxDQUFpQkgsS0FBakIsQ0FBdEI7SUFDQSxJQUFJSSxhQUFhLEdBQUcsS0FBS0QsV0FBTCxDQUFpQkgsS0FBakIsQ0FBcEI7SUFDQWIsSUFBSSxDQUFDcEQsT0FBTCxDQUFhMkMsUUFBYixDQUFzQjBCLGFBQXRCO0lBQ0FqQixJQUFJLENBQUNwRCxPQUFMLENBQWEyQyxRQUFiLENBQXNCd0IsZUFBdEI7SUFDQSxJQUFJRyxVQUFVLEdBQUcsQ0FBakI7SUFDQSxJQUFJQyxLQUFLLEdBQUcsQ0FBWjs7SUFDQSxJQUFHQyxNQUFNLENBQUNWLEtBQUQsQ0FBTixHQUFjLENBQWQsSUFBaUIsQ0FBcEIsRUFBc0I7TUFDbEIsSUFBR0wsUUFBUSxDQUFDUCxDQUFULEdBQWFRLE1BQU0sQ0FBQ1IsQ0FBdkIsRUFBeUI7UUFBQztRQUN0QmlCLGVBQWUsQ0FBQ00sS0FBaEIsR0FBc0IsRUFBdEI7UUFDQU4sZUFBZSxDQUFDdkIsS0FBaEIsR0FBc0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFDLEdBQWIsRUFBaUIsR0FBakIsRUFBcUIsR0FBckIsQ0FBdEI7UUFFQXFCLGFBQWEsQ0FBQ0ksS0FBZCxHQUFvQixFQUFwQjtRQUNBSixhQUFhLENBQUN6QixLQUFkLEdBQW9CLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVksR0FBWixFQUFnQixHQUFoQixFQUFvQixHQUFwQixDQUFwQjtRQUNBc0IsVUFBVSxHQUFDLENBQVg7TUFDSCxDQVBELE1BT0s7UUFBQztRQUNGSCxlQUFlLENBQUNNLEtBQWhCLEdBQXNCLEVBQXRCO1FBQ0FOLGVBQWUsQ0FBQ3ZCLEtBQWhCLEdBQXNCLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVksR0FBWixFQUFnQixHQUFoQixFQUFvQixHQUFwQixDQUF0QjtRQUVBcUIsYUFBYSxDQUFDSSxLQUFkLEdBQW9CLEVBQXBCO1FBQ0FKLGFBQWEsQ0FBQ3pCLEtBQWQsR0FBb0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFDLEdBQWIsRUFBaUIsR0FBakIsRUFBcUIsR0FBckIsQ0FBcEI7UUFDQXNCLFVBQVUsR0FBQyxDQUFDLENBQVo7TUFDSDtJQUVKLENBakJELE1BaUJNLElBQUdFLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLEdBQWMsQ0FBZCxJQUFpQixDQUFwQixFQUFzQjtNQUN4QixJQUFHTCxRQUFRLENBQUNQLENBQVQsR0FBYVEsTUFBTSxDQUFDUixDQUF2QixFQUF5QjtRQUN6QjtVQUNJaUIsZUFBZSxDQUFDTSxLQUFoQixHQUFzQixDQUF0QjtVQUNBTixlQUFlLENBQUN2QixLQUFoQixHQUFzQixJQUFJaEQsRUFBRSxDQUFDb0QsSUFBUCxDQUFZLENBQUMsQ0FBYixFQUFlLENBQWYsRUFBaUIsQ0FBakIsQ0FBdEI7VUFFQXFCLGFBQWEsQ0FBQ0ksS0FBZCxHQUFvQixDQUFwQjtVQUNBSixhQUFhLENBQUN6QixLQUFkLEdBQW9CLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVksQ0FBWixFQUFjLENBQWQsRUFBZ0IsQ0FBaEIsQ0FBcEI7VUFDQXNCLFVBQVUsR0FBQyxDQUFYO1FBQ0gsQ0FSRCxNQVFLO1FBQ0RILGVBQWUsQ0FBQ00sS0FBaEIsR0FBc0IsQ0FBdEI7UUFDQU4sZUFBZSxDQUFDdkIsS0FBaEIsR0FBc0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFaLEVBQWMsQ0FBZCxFQUFnQixDQUFoQixDQUF0QjtRQUVBcUIsYUFBYSxDQUFDSSxLQUFkLEdBQW9CLENBQXBCO1FBQ0FKLGFBQWEsQ0FBQ3pCLEtBQWQsR0FBb0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFDLENBQWIsRUFBZSxDQUFmLEVBQWlCLENBQWpCLENBQXBCO1FBQ0FzQixVQUFVLEdBQUMsQ0FBQyxDQUFaO01BQ0g7SUFFSjs7SUFDRCxJQUFJSSxTQUFTLEdBQUd0QixJQUFJLENBQUNwRCxPQUFMLENBQWEyRSxvQkFBYixDQUFrQ2pCLE1BQWxDLENBQWhCO0lBQ0EsSUFBSWtCLFVBQVUsR0FBR3hCLElBQUksQ0FBQ3BELE9BQUwsQ0FBYTJFLG9CQUFiLENBQWtDbEIsUUFBbEMsQ0FBakI7SUFDQVksYUFBYSxDQUFDeEIsV0FBZCxDQUEwQjZCLFNBQVMsQ0FBQ3pCLENBQXBDLEVBQXVDeUIsU0FBUyxDQUFDeEIsQ0FBakQsRUFBbUQsQ0FBbkQ7SUFDQWlCLGVBQWUsQ0FBQ3RCLFdBQWhCLENBQTRCK0IsVUFBVSxDQUFDM0IsQ0FBdkMsRUFBMEMyQixVQUFVLENBQUMxQixDQUFyRCxFQUF1RCxDQUF2RDtJQUNBdEQsRUFBRSxDQUFDaUYsS0FBSCxDQUFTUixhQUFULEVBQ0tTLEVBREwsQ0FDUSxHQURSLEVBQ1k7TUFBQ2xDLEtBQUssRUFBQzBCLFVBQVA7TUFBa0JTLE9BQU8sRUFBQztJQUExQixDQURaLEVBRUtDLElBRkwsQ0FFVSxZQUFJO01BQ05YLGFBQWEsQ0FBQ2pDLE9BQWQ7SUFDSCxDQUpMLEVBS0s2QyxLQUxMO0lBT0FyRixFQUFFLENBQUNpRixLQUFILENBQVNWLGVBQVQsRUFDS1csRUFETCxDQUNRLEdBRFIsRUFDYTtNQUFDbEMsS0FBSyxFQUFDLENBQUMwQixVQUFSO01BQW1CUyxPQUFPLEVBQUM7SUFBM0IsQ0FEYixFQUVLQyxJQUZMLENBRVUsWUFBTTtNQUNSYixlQUFlLENBQUMvQixPQUFoQjtJQUNILENBSkwsRUFLSzZDLEtBTEw7RUFNSCxDQTdISTtFQThITGYsWUE5SEssd0JBOEhRUCxNQTlIUixFQThIZTtJQUNoQixRQUFRQSxNQUFSO01BQ0ksS0FBSyxDQUFMO1FBQ0ksT0FBTyxTQUFQOztNQUNKLEtBQUssQ0FBTDtRQUNJLE9BQU8sWUFBUDs7TUFDSixLQUFLLENBQUw7UUFDSSxPQUFPLFVBQVA7O01BQ0osS0FBSyxDQUFMO1FBQ0ksT0FBTyxXQUFQO0lBUlI7RUFVSCxDQXpJSTtFQTBJTG5DLFVBMUlLLHdCQTBJTztJQUNSMEQsbUJBQUEsQ0FBUUMsUUFBUixHQUFpQixLQUFqQjtJQUNBdkYsRUFBRSxDQUFDNkIsUUFBSCxDQUFZMkQsU0FBWixDQUFzQixRQUF0QjtFQUNILENBN0lJO0VBK0lMSCxLQS9JSyxtQkErSUc7SUFDSixJQUFJN0IsSUFBSSxHQUFHLElBQVg7SUFDQWlDLENBQUMsQ0FBQ0MsS0FBRixHQUZJLENBR0o7O0lBQ0EsS0FBS3RGLE9BQUwsR0FBZUosRUFBRSxDQUFDMkYsSUFBSCxDQUFRLGtCQUFSLENBQWY7SUFDQSxLQUFLdkYsT0FBTCxDQUFhd0YsaUJBQWIsR0FMSSxDQU1KOztJQUNBLElBQUkxQixLQUFLLEdBQUdDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFaOztJQUNBLElBQUlRLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLElBQWlCLENBQXJCLEVBQXdCO01BQ3BCO01BQ0E7TUFDQUEsS0FBSyxHQUFHLENBQVI7TUFDQUMsT0FBTyxDQUFDMEIsUUFBUixDQUFpQixPQUFqQixFQUEwQixDQUExQjtNQUNBLEtBQUtqRSxVQUFMO01BQ0E7SUFDSDs7SUFDRCxJQUFHZ0QsTUFBTSxDQUFDVixLQUFELENBQU4sR0FBYyxDQUFkLElBQWlCLENBQXBCLEVBQXNCO01BQ2xCLEtBQUtqRCxNQUFMLENBQVlnQyxXQUFaLENBQXdCLE9BQXhCLEVBQWlDLEtBQUtoQyxNQUFMLENBQVlxQixRQUFaLENBQXFCZ0IsQ0FBckIsR0FBeUJnQyxtQkFBQSxDQUFRUSxhQUFsRSxFQUFpRixDQUFqRjtNQUNBLEtBQUs1RSxNQUFMLENBQVkrQixXQUFaLENBQXdCLENBQUMsT0FBekIsRUFBa0MsS0FBSy9CLE1BQUwsQ0FBWW9CLFFBQVosQ0FBcUJnQixDQUFyQixHQUF5QmdDLG1CQUFBLENBQVFRLGFBQW5FLEVBQWtGLENBQWxGO0lBQ0g7O0lBQ0QsSUFBR2xCLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLEdBQWMsQ0FBZCxJQUFpQixDQUFwQixFQUFzQjtNQUNsQixLQUFLakQsTUFBTCxDQUFZZ0MsV0FBWixDQUF3QixHQUF4QixFQUE2QixNQUFNcUMsbUJBQUEsQ0FBUVEsYUFBM0MsRUFBMEQsQ0FBMUQ7TUFDQSxLQUFLN0UsTUFBTCxDQUFZNEQsS0FBWixHQUFvQixDQUFwQjtNQUNBLEtBQUszRCxNQUFMLENBQVkrQixXQUFaLENBQXdCLENBQUMsR0FBekIsRUFBOEIsQ0FBQyxDQUFELEdBQUtxQyxtQkFBQSxDQUFRUSxhQUEzQyxFQUEwRCxDQUExRDtNQUNBLEtBQUs1RSxNQUFMLENBQVkyRCxLQUFaLEdBQW9CLEdBQXBCO0lBQ0gsQ0F6QkcsQ0EwQko7SUFDQTtJQUNBO0lBQ0E7SUFDQTs7O0lBRUE3RSxFQUFFLENBQUN5QyxTQUFILENBQWFDLElBQWIsQ0FBa0IsV0FBV3dCLEtBQTdCLEVBQW9DLFVBQVV2QixHQUFWLEVBQWVvRCxTQUFmLEVBQTBCO01BQzFELElBQUlDLGVBQWUsR0FBR0QsU0FBUyxDQUFDRSxJQUFoQztNQUNBUixDQUFDLENBQUNTLFNBQUYsR0FBY0YsZUFBZCxDQUYwRCxDQUcxRDs7TUFDQXhDLElBQUksQ0FBQzJDLFNBQUwsQ0FBZUgsZUFBZSxDQUFDSSxLQUEvQixFQUFxQ3hCLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLEdBQWMsQ0FBbkQ7SUFDSCxDQUxELEVBaENJLENBc0NKO0lBQ0E7RUFFSCxDQXhMSTtFQXlMTDtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBaUMsU0FoTUsscUJBZ01LQyxLQWhNTCxFQWdNV2xDLEtBaE1YLEVBZ01rQjtJQUNuQixJQUFJVixJQUFJLEdBQUcsSUFBWCxDQURtQixDQUVuQjs7SUFDQSxLQUFLLElBQUk2QyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRCxLQUFLLENBQUNFLE1BQTFCLEVBQWtDRCxDQUFDLEVBQW5DLEVBQXVDO01BQ25DLElBQUlFLFFBQVEsR0FBRyxLQUFLL0IsV0FBTCxDQUFpQjRCLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNoQyxLQUExQixDQUFmO01BQ0FiLElBQUksQ0FBQ3BELE9BQUwsQ0FBYTJDLFFBQWIsQ0FBc0J3RCxRQUF0QjtNQUNBLElBQUlDLFVBQVUsR0FBQyxFQUFmOztNQUNBLElBQUd0QyxLQUFLLElBQUUsQ0FBVixFQUFZO1FBQ1JzQyxVQUFVLEdBQUMsRUFBWDtRQUNBRCxRQUFRLENBQUN0RCxXQUFULENBQXFCbUQsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBU0ksR0FBVCxDQUFhLENBQWIsSUFBaUJMLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNJLEdBQVQsQ0FBYSxDQUFiLElBQWtCLEtBQXhELEVBQWdFTCxLQUFLLENBQUNDLENBQUQsQ0FBTCxDQUFTSSxHQUFULENBQWEsQ0FBYixJQUFpQkwsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBU0ksR0FBVCxDQUFhLENBQWIsSUFBa0IsS0FBbkMsR0FBNENuQixtQkFBQSxDQUFRUSxhQUFwRCxJQUFvRU0sS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBUzFCLEtBQVQsSUFBZ0IsQ0FBaEIsR0FBa0I2QixVQUFsQixHQUE2QixDQUFDQSxVQUFsRyxDQUFoRTtNQUNILENBSEQsTUFHTSxJQUFHdEMsS0FBSyxJQUFFLENBQVYsRUFBWTtRQUNkc0MsVUFBVSxHQUFDLENBQUMsQ0FBWjtRQUNBRCxRQUFRLENBQUN0RCxXQUFULENBQXFCbUQsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBU0ksR0FBVCxDQUFhLENBQWIsQ0FBckIsRUFBc0NMLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNJLEdBQVQsQ0FBYSxDQUFiLENBQXRDO01BQ0g7O01BQ0RGLFFBQVEsQ0FBQzFCLEtBQVQsR0FBZXVCLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNLLEdBQXhCOztNQUNBLElBQUd4QyxLQUFLLElBQUUsQ0FBVixFQUFZO1FBQ1JxQyxRQUFRLENBQUN2RCxLQUFULEdBQWUsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWWdELEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFULEdBQWlCb0QsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBUzFCLEtBQTFCLEdBQWtDVyxtQkFBQSxDQUFRcUIsU0FBdEQsRUFBZ0VQLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFULEdBQWlCc0MsbUJBQUEsQ0FBUXFCLFNBQXpGLEVBQW1HUCxLQUFLLENBQUNDLENBQUQsQ0FBTCxDQUFTckQsS0FBNUcsQ0FBZjtNQUNILENBRkQsTUFFTSxJQUFHa0IsS0FBSyxJQUFFLENBQVYsRUFBWTtRQUNkcUMsUUFBUSxDQUFDdkQsS0FBVCxHQUFlLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVlnRCxLQUFLLENBQUNDLENBQUQsQ0FBTCxDQUFTckQsS0FBVCxHQUFpQm9ELEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVMxQixLQUF0QyxFQUE0Q3lCLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFyRCxFQUEyRG9ELEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFwRSxDQUFmO01BQ0g7O01BQ0R5QyxDQUFDLENBQUNtQixRQUFGLENBQVdDLElBQVgsQ0FBZ0JOLFFBQWhCO01BQ0FkLENBQUMsQ0FBQ3FCLGNBQUYsR0FBbUJyQixDQUFDLENBQUNtQixRQUFGLENBQVdHLE1BQVgsRUFBbkIsQ0FsQm1DLENBbUJuQztJQUNIO0VBQ0osQ0F4Tkk7RUF5Tkx2QyxXQXpOSyx1QkF5Tk9ILEtBek5QLEVBeU5jO0lBQ2YsUUFBUUEsS0FBUjtNQUNJLEtBQUssU0FBTDtRQUNJLElBQUlsQyxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3hDLE9BQXBCLENBQVg7UUFDQSxPQUFPNkIsSUFBUDs7TUFDSixLQUFLLFlBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3RDLFVBQXBCLENBQVg7UUFDQSxPQUFPMkIsSUFBUDs7TUFDSixLQUFLLFVBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS25DLFFBQXBCLENBQVg7UUFDQSxPQUFPd0IsSUFBUDs7TUFDSixLQUFLLFlBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3BDLFVBQXBCLENBQVg7UUFDQSxPQUFPeUIsSUFBUDs7TUFDSixLQUFLLFdBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3JDLFNBQXBCLENBQVg7UUFDQSxPQUFPMEIsSUFBUDs7TUFDSjtRQUNJNkUsT0FBTyxDQUFDQyxHQUFSLENBQVksTUFBWjtJQWpCUjtFQW1CSCxDQTdPSSxDQThPTDs7QUE5T0ssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcbmltcG9ydCBHYW1lTWdyIGZyb20gXCIuL0dhbWVNZ3JcIjtcclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBCR19Ob2RlOiBjYy5Ob2RlLFxyXG4gICAgICAgIHJlZGJhbGw6IGNjLlByZWZhYixcclxuICAgICAgICB5ZWxsb3diYWxsOiBjYy5QcmVmYWIsXHJcbiAgICAgICAgZ3JlZW5iYWxsOiBjYy5QcmVmYWIsXHJcbiAgICAgICAgcHVycGxlYmFsbDogY2MuUHJlZmFiLFxyXG4gICAgICAgIGJsdWViYWxsOiBjYy5QcmVmYWIsXHJcbiAgICAgICAgTGV2ZWw6IGNjLkxhYmVsLFxyXG4gICAgICAgIEJHTTogY2MuQXVkaW9DbGlwLFxyXG4gICAgICAgIGJhY2tNZW51OmNjLk5vZGUsXHJcbiAgICAgICAgemh1emkxOmNjLk5vZGUsXHJcbiAgICAgICAgemh1emkyOmNjLk5vZGUsXHJcbiAgICAgICAgX2Z4X2xpc3Q6W11cclxuICAgIH0sXHJcblxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIC8v5pKt5pS+YmdtXHJcbiAgICAgICAgdmFyIGJnbSA9IGNjLmF1ZGlvRW5naW5lLnBsYXlNdXNpYyh0aGlzLkJHTSwgdHJ1ZSk7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUuc2V0Vm9sdW1lKGJnbSwgMC4xNSk7XHJcbiAgICAgICAgdGhpcy5iYWNrTWVudS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9TVEFSVCx0aGlzLmJhY2tUb01lbnUsdGhpcyk7XHJcbiAgICAgICAgY2MuZGlyZWN0b3IuZ2V0Q29sbGlzaW9uTWFuYWdlcigpLmVuYWJsZWQ9dHJ1ZTtcclxuXHJcbiAgICAgICAgY2MuZ2FtZS5vbihcInNob3dfYmFsbF9tYXNrXCIsdGhpcy5zaG93TWFzayx0aGlzKTtcclxuICAgICAgICBjYy5nYW1lLm9uKFwiZ2FtZW92ZXJcIix0aGlzLk9uR2FtZW92ZXIsdGhpcyk7XHJcbiAgICB9LFxyXG5cclxuICAgIE9uR2FtZW92ZXIobm9kZSxwYXJlbnQpe1xyXG4gICAgICAgIGxldCBuZXdQb3MgPSBub2RlLnBvc2l0aW9uO1xyXG4gICAgICAgIGNjLmdhbWUub2ZmKFwiZ2FtZW92ZXJcIik7XHJcbiAgICAgICAgbm9kZS5kZXN0cm95KCk7XHJcbiAgICAgICAgY2MucmVzb3VyY2VzLmxvYWQoXCJwZXJmYWIvY29sX2Z4XCIsY2MuUHJlZmFiLChlcnIsZngpPT57XHJcbiAgICAgICAgICAgIGxldCBuZXdGeD1jYy5pbnN0YW50aWF0ZShmeCk7XHJcbiAgICAgICAgICAgIHBhcmVudC5hZGRDaGlsZChuZXdGeCk7XHJcbiAgICAgICAgICAgIG5ld0Z4LnNjYWxlPTAuNTtcclxuICAgICAgICAgICAgbmV3Rnguc2V0UG9zaXRpb24obmV3UG9zKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKT0+e1xyXG4gICAgICAgICAgICB0aGlzLnNob3dXcm9uZ0ljb24obmV3IGNjLlZlYzMobmV3UG9zLngsbmV3UG9zLnkgLSA1MCwwKSxwYXJlbnQpO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKT0+e1xyXG4gICAgICAgICAgICAgICAgdGhpcy5iYWNrVG9NZW51KCk7XHJcbiAgICAgICAgICAgIH0sMik7XHJcbiAgICAgICAgfSwyKTtcclxuICAgICAgICBcclxuICAgIH0sXHJcbiAgICBzaG93V3JvbmdJY29uKHBvcyxwYXJlbnQpe1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBjYy5yZXNvdXJjZXMubG9hZChcInBlcmZhYi93cm9uZ1wiLCBmdW5jdGlvbiAoZXJyLCBwcmVmYWIpIHtcclxuICAgICAgICAgICAgbGV0IG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xyXG4gICAgICAgICAgICBuZXdOb2RlLnBhcmVudCA9IHBhcmVudDtcclxuICAgICAgICAgICAgbmV3Tm9kZS5zZXRQb3NpdGlvbihwb3MpO1xyXG4gICAgICAgICAgICBuZXdOb2RlLnNjYWxlID0xLjU7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgIG5ld05vZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBuZXdOb2RlLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfSwyMDAwKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIHNob3dNYXNrKHN0YXJ0UG9zLGVuZFBvcyxiYWxsSWQsc3RhcnRCYWxsLGVuZEJhbGwpe1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBsZXQgbGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgIGxldCBjb2xvciA9IHRoaXMuZ2V0SURCeUNvbG9yKGJhbGxJZCk7XHJcbiAgICAgICAgbGV0IHN0YXJ0UG9zTmV3QmFsbCA9IHRoaXMubG9hZF9wcmVmYWIoY29sb3IpO1xyXG4gICAgICAgIGxldCBlbmRQb3NOZXdCYWxsID0gdGhpcy5sb2FkX3ByZWZhYihjb2xvcik7XHJcbiAgICAgICAgc2VsZi5CR19Ob2RlLmFkZENoaWxkKGVuZFBvc05ld0JhbGwpO1xyXG4gICAgICAgIHNlbGYuQkdfTm9kZS5hZGRDaGlsZChzdGFydFBvc05ld0JhbGwpO1xyXG4gICAgICAgIHZhciBzY2FsZVZhbHVlID0gMTtcclxuICAgICAgICB2YXIgZmxpcFggPSAxO1xyXG4gICAgICAgIGlmKE51bWJlcihsZXZlbCkrMT09MSl7XHJcbiAgICAgICAgICAgIGlmKHN0YXJ0UG9zLnkgPCBlbmRQb3MueSl7Ly/ku47kuIvlvoDkuIrnlLvnur9cclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0zNTtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5zY2FsZT1uZXcgY2MuVmVjMygtMS42LDEuNiwxLjYpO1xyXG5cclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuYW5nbGU9MzU7XHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKDEuNiwxLjYsMS42KTtcclxuICAgICAgICAgICAgICAgIHNjYWxlVmFsdWU9MztcclxuICAgICAgICAgICAgfWVsc2V7Ly/ku47kuIrlvoDkuIvnlLvnur9cclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0zNTtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5zY2FsZT1uZXcgY2MuVmVjMygxLjYsMS42LDEuNik7XHJcblxyXG4gICAgICAgICAgICAgICAgZW5kUG9zTmV3QmFsbC5hbmdsZT0zNTtcclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuc2NhbGU9bmV3IGNjLlZlYzMoLTEuNiwxLjYsMS42KTtcclxuICAgICAgICAgICAgICAgIHNjYWxlVmFsdWU9LTM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfWVsc2UgaWYoTnVtYmVyKGxldmVsKSsxPT0yKXtcclxuICAgICAgICAgICAgaWYoc3RhcnRQb3MueSA8IGVuZFBvcy55KS8v5LuO5LiL5b6A5LiK55S757q/XHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0wO1xyXG4gICAgICAgICAgICAgICAgc3RhcnRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKC0xLDEsMSk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuYW5nbGU9MDtcclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuc2NhbGU9bmV3IGNjLlZlYzMoMSwxLDEpO1xyXG4gICAgICAgICAgICAgICAgc2NhbGVWYWx1ZT0yO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0wO1xyXG4gICAgICAgICAgICAgICAgc3RhcnRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKDEsMSwxKTtcclxuXHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLmFuZ2xlPTA7XHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKC0xLDEsMSk7XHJcbiAgICAgICAgICAgICAgICBzY2FsZVZhbHVlPS0yO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgZmluYWxfcG9zID0gc2VsZi5CR19Ob2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKGVuZFBvcyk7XHJcbiAgICAgICAgbGV0IGZpbmFsX3BvczEgPSBzZWxmLkJHX05vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoc3RhcnRQb3MpO1xyXG4gICAgICAgIGVuZFBvc05ld0JhbGwuc2V0UG9zaXRpb24oZmluYWxfcG9zLngsIGZpbmFsX3Bvcy55LDApO1xyXG4gICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5zZXRQb3NpdGlvbihmaW5hbF9wb3MxLngsIGZpbmFsX3BvczEueSwwKTtcclxuICAgICAgICBjYy50d2VlbihlbmRQb3NOZXdCYWxsKVxyXG4gICAgICAgICAgICAudG8oMC4zLHtzY2FsZTpzY2FsZVZhbHVlLG9wYWNpdHk6MH0pXHJcbiAgICAgICAgICAgIC5jYWxsKCgpPT57XHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnN0YXJ0KClcclxuXHJcbiAgICAgICAgY2MudHdlZW4oc3RhcnRQb3NOZXdCYWxsKVxyXG4gICAgICAgICAgICAudG8oMC4zLCB7c2NhbGU6LXNjYWxlVmFsdWUsb3BhY2l0eTowfSlcclxuICAgICAgICAgICAgLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc3RhcnRQb3NOZXdCYWxsLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnN0YXJ0KClcclxuICAgIH0sXHJcbiAgICBnZXRJREJ5Q29sb3IoYmFsbElkKXtcclxuICAgICAgICBzd2l0Y2ggKGJhbGxJZCkge1xyXG4gICAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJyZWRiYWxsXCI7XHJcbiAgICAgICAgICAgIGNhc2UgMTpcclxuICAgICAgICAgICAgICAgIHJldHVybiBcInllbGxvd2JhbGxcIjtcclxuICAgICAgICAgICAgY2FzZSAyOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiYmx1ZWJhbGxcIjtcclxuICAgICAgICAgICAgY2FzZSAzOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiZ3JlZW5iYWxsXCI7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGJhY2tUb01lbnUoKXtcclxuICAgICAgICBHYW1lTWdyLmdhbWVPdmVyPWZhbHNlO1xyXG4gICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgnbGF1bmNoJyk7XHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICBHLnJlc2V0KCk7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ+a4heepukcnKTtcclxuICAgICAgICB0aGlzLkJHX05vZGUgPSBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0JHJyk7XHJcbiAgICAgICAgdGhpcy5CR19Ob2RlLnJlbW92ZUFsbENoaWxkcmVuKCk7XHJcbiAgICAgICAgLy/or7vlj5blvZPliY3mmK/lk6rkuIDkuKrlhbPljaFcclxuICAgICAgICBsZXQgbGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgIGlmIChOdW1iZXIobGV2ZWwpID49IDIpIHtcclxuICAgICAgICAgICAgLy/lhajpg6jpgJrlhbNcclxuICAgICAgICAgICAgLy9jYy5maW5kKCdDYW52YXMvVElQUy/lhajpg6jpgJrlhbMnKS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICBsZXZlbCA9IDA7XHJcbiAgICAgICAgICAgIFN0b3JhZ2UuU2V0X0luZm8oJ2xldmVsJywgMCk7XHJcbiAgICAgICAgICAgIHRoaXMuYmFja1RvTWVudSgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKE51bWJlcihsZXZlbCkrMT09MSl7XHJcbiAgICAgICAgICAgIHRoaXMuemh1emkxLnNldFBvc2l0aW9uKDQ0Ni45MjQsIHRoaXMuemh1emkxLnBvc2l0aW9uLnkgLSBHYW1lTWdyLkdsb2JhbE9mZnNldFksIDApO1xyXG4gICAgICAgICAgICB0aGlzLnpodXppMi5zZXRQb3NpdGlvbigtMzk4LjkyNCwgdGhpcy56aHV6aTIucG9zaXRpb24ueSAtIEdhbWVNZ3IuR2xvYmFsT2Zmc2V0WSwgMCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKE51bWJlcihsZXZlbCkrMT09Mil7XHJcbiAgICAgICAgICAgIHRoaXMuemh1emkxLnNldFBvc2l0aW9uKDQ2NSwgMzg0IC0gR2FtZU1nci5HbG9iYWxPZmZzZXRZLCAwKTtcclxuICAgICAgICAgICAgdGhpcy56aHV6aTEuYW5nbGUgPSAwO1xyXG4gICAgICAgICAgICB0aGlzLnpodXppMi5zZXRQb3NpdGlvbigtNDU1LCAtNiAtIEdhbWVNZ3IuR2xvYmFsT2Zmc2V0WSwgMCk7XHJcbiAgICAgICAgICAgIHRoaXMuemh1emkyLmFuZ2xlID0gMTgwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBpZiAoY2Muc3lzLnBsYXRmb3JtID09PSBjYy5zeXMuQU5EUk9JRCkge1xyXG4gICAgICAgIC8vICAgICBzZWxmLkxldmVsLnN0cmluZyA9ICdMZXZlbCAnICsgKE51bWJlcihsZXZlbCkgKyAxKTtcclxuICAgICAgICAvLyB9IGVsc2Uge1xyXG4gICAgICAgIC8vICAgICBzZWxmLkxldmVsLnN0cmluZyA9ICflhbPljaEgJyArIChOdW1iZXIobGV2ZWwpICsgMSk7XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICBjYy5yZXNvdXJjZXMubG9hZCgnbGV2ZWwvJyArIGxldmVsLCBmdW5jdGlvbiAoZXJyLCBqc29uQXNzZXQpIHtcclxuICAgICAgICAgICAgbGV0IGxldmVsX2pzb25faW5mbyA9IGpzb25Bc3NldC5qc29uO1xyXG4gICAgICAgICAgICBHLkxldmVsSnNvbiA9IGxldmVsX2pzb25faW5mbztcclxuICAgICAgICAgICAgLy9zZWxmLmxvYWRiZyhsZXZlbF9qc29uX2luZm8uYmcpO1xyXG4gICAgICAgICAgICBzZWxmLmxvYWRiYWxscyhsZXZlbF9qc29uX2luZm8uYmFsbHMsTnVtYmVyKGxldmVsKSsxKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyBHLkFuYWx5c3RpY0xvZ2luKCk7XHJcbiAgICAgICAgLy8gRy53eHRvcHNoYXJlKCk7XHJcblxyXG4gICAgfSxcclxuICAgIC8vIGxvYWRiZyhzdHIpIHtcclxuICAgIC8vICAgICAvLyBjb25zb2xlLmxvZyhzdHIpO1xyXG4gICAgLy8gICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIC8vICAgICBjYy5yZXNvdXJjZXMubG9hZChcIkJHL1wiICsgc3RyLCBjYy5TcHJpdGVGcmFtZSwgZnVuY3Rpb24gKGVyciwgc3ByaXRlRnJhbWUpIHtcclxuICAgIC8vICAgICAgICAgc2VsZi5CR19Ob2RlLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gc3ByaXRlRnJhbWU7XHJcbiAgICAvLyAgICAgfSk7XHJcbiAgICAvLyB9LFxyXG4gICAgbG9hZGJhbGxzKGJhbGxzLGxldmVsKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIC8vIEcuc3Rva2VsaXN0ID0gYmFsbHMubGVuZ3RoO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYmFsbHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IG5ld25vZGUxID0gdGhpcy5sb2FkX3ByZWZhYihiYWxsc1tpXS5jb2xvcik7XHJcbiAgICAgICAgICAgIHNlbGYuQkdfTm9kZS5hZGRDaGlsZChuZXdub2RlMSk7XHJcbiAgICAgICAgICAgIGxldCBiYWxsT2Zmc2V0PTEwO1xyXG4gICAgICAgICAgICBpZihsZXZlbD09MSl7XHJcbiAgICAgICAgICAgICAgICBiYWxsT2Zmc2V0PTEwXHJcbiAgICAgICAgICAgICAgICBuZXdub2RlMS5zZXRQb3NpdGlvbihiYWxsc1tpXS5Qb3NbMF0rKGJhbGxzW2ldLlBvc1swXSAqIDAuMTk0KSwgYmFsbHNbaV0uUG9zWzFdLShiYWxsc1tpXS5Qb3NbMF0gKiAwLjE5NCkgLSBHYW1lTWdyLkdsb2JhbE9mZnNldFkgLShiYWxsc1tpXS5mbGlwWD09MT9iYWxsT2Zmc2V0Oi1iYWxsT2Zmc2V0KSk7XHJcbiAgICAgICAgICAgIH1lbHNlIGlmKGxldmVsPT0yKXtcclxuICAgICAgICAgICAgICAgIGJhbGxPZmZzZXQ9LTg7XHJcbiAgICAgICAgICAgICAgICBuZXdub2RlMS5zZXRQb3NpdGlvbihiYWxsc1tpXS5Qb3NbMF0sIGJhbGxzW2ldLlBvc1sxXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbmV3bm9kZTEuYW5nbGU9YmFsbHNbaV0uUm90O1xyXG4gICAgICAgICAgICBpZihsZXZlbD09MSl7XHJcbiAgICAgICAgICAgICAgICBuZXdub2RlMS5zY2FsZT1uZXcgY2MuVmVjMyhiYWxsc1tpXS5zY2FsZSAqIGJhbGxzW2ldLmZsaXBYICogR2FtZU1nci50dWJlU2NhbGUsYmFsbHNbaV0uc2NhbGUgKiBHYW1lTWdyLnR1YmVTY2FsZSxiYWxsc1tpXS5zY2FsZSlcclxuICAgICAgICAgICAgfWVsc2UgaWYobGV2ZWw9PTIpe1xyXG4gICAgICAgICAgICAgICAgbmV3bm9kZTEuc2NhbGU9bmV3IGNjLlZlYzMoYmFsbHNbaV0uc2NhbGUgKiBiYWxsc1tpXS5mbGlwWCxiYWxsc1tpXS5zY2FsZSxiYWxsc1tpXS5zY2FsZSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBHLkJhbGxMaXN0LnB1c2gobmV3bm9kZTEpO1xyXG4gICAgICAgICAgICBHLm9yaWdpbkJhbGxMaXN0ID0gRy5CYWxsTGlzdC5jb25jYXQoKTtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2cobmV3bm9kZTEucG9zaXRpb24pXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGxvYWRfcHJlZmFiKGNvbG9yKSB7XHJcbiAgICAgICAgc3dpdGNoIChjb2xvcikge1xyXG4gICAgICAgICAgICBjYXNlIFwicmVkYmFsbFwiOlxyXG4gICAgICAgICAgICAgICAgdmFyIG5vZGUgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnJlZGJhbGwpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5vZGU7XHJcbiAgICAgICAgICAgIGNhc2UgXCJ5ZWxsb3diYWxsXCI6XHJcbiAgICAgICAgICAgICAgICB2YXIgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMueWVsbG93YmFsbCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbm9kZTtcclxuICAgICAgICAgICAgY2FzZSBcImJsdWViYWxsXCI6XHJcbiAgICAgICAgICAgICAgICB2YXIgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMuYmx1ZWJhbGwpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5vZGU7XHJcbiAgICAgICAgICAgIGNhc2UgXCJwdXJwbGViYWxsXCI6XHJcbiAgICAgICAgICAgICAgICB2YXIgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMucHVycGxlYmFsbCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbm9kZTtcclxuICAgICAgICAgICAgY2FzZSBcImdyZWVuYmFsbFwiOlxyXG4gICAgICAgICAgICAgICAgdmFyIG5vZGUgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLmdyZWVuYmFsbCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbm9kZTtcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCfpopzoibLkuI3lr7knKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9LFxyXG59KTtcclxuXHJcbiJdfQ==