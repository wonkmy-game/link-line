
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/storage.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '725b4q08gtGBp6DhgZUlHGw', 'storage');
// scripts/storage.js

"use strict";

window.Storage = {
  Change_storage: function Change_storage(Type, Number) {
    var Storage_number = Storage.Get_Info(Type);
    Storage_number += Number;
    cc.sys.localStorage.setItem(Type, Storage_number);
  },
  Get_Info: function Get_Info(Type) {
    var starnum = cc.sys.localStorage.getItem(Type);

    if (!starnum) {
      starnum = 0;
    } else {
      starnum = parseInt(starnum);
    }

    return starnum;
  },
  Set_Info: function Set_Info(Type, starnum) {
    cc.sys.localStorage.setItem(Type, starnum);
  }
}; //wx.getOpenDataContext().postMessage({
//     message: Level_Pass_Now
// });
//cc.sys.localStorage.setItem(key, value)
//cc.sys.localStorage.getItem(key)

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcc3RvcmFnZS5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCJTdG9yYWdlIiwiQ2hhbmdlX3N0b3JhZ2UiLCJUeXBlIiwiTnVtYmVyIiwiU3RvcmFnZV9udW1iZXIiLCJHZXRfSW5mbyIsImNjIiwic3lzIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsInN0YXJudW0iLCJnZXRJdGVtIiwicGFyc2VJbnQiLCJTZXRfSW5mbyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsTUFBTSxDQUFDQyxPQUFQLEdBQWlCO0VBQ2JDLGNBRGEsMEJBQ0dDLElBREgsRUFDUUMsTUFEUixFQUNnQjtJQUN6QixJQUFJQyxjQUFjLEdBQUdKLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQkgsSUFBakIsQ0FBckI7SUFDQUUsY0FBYyxJQUFJRCxNQUFsQjtJQUNBRyxFQUFFLENBQUNDLEdBQUgsQ0FBT0MsWUFBUCxDQUFvQkMsT0FBcEIsQ0FBNEJQLElBQTVCLEVBQWlDRSxjQUFqQztFQUNILENBTFk7RUFNYkMsUUFOYSxvQkFNSEgsSUFORyxFQU1FO0lBQ1gsSUFBSVEsT0FBTyxHQUFHSixFQUFFLENBQUNDLEdBQUgsQ0FBT0MsWUFBUCxDQUFvQkcsT0FBcEIsQ0FBNEJULElBQTVCLENBQWQ7O0lBQ0ksSUFBRyxDQUFDUSxPQUFKLEVBQVk7TUFDUkEsT0FBTyxHQUFHLENBQVY7SUFDSCxDQUZELE1BRUs7TUFDREEsT0FBTyxHQUFHRSxRQUFRLENBQUNGLE9BQUQsQ0FBbEI7SUFDSDs7SUFDRCxPQUFPQSxPQUFQO0VBQ1AsQ0FkWTtFQWViRyxRQWZhLG9CQWVIWCxJQWZHLEVBZUVRLE9BZkYsRUFlVTtJQUNuQkosRUFBRSxDQUFDQyxHQUFILENBQU9DLFlBQVAsQ0FBb0JDLE9BQXBCLENBQTRCUCxJQUE1QixFQUFpQ1EsT0FBakM7RUFDSDtBQWpCWSxDQUFqQixFQW1CQTtBQUNBO0FBQ0E7QUFFQTtBQUNBIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJ3aW5kb3cuU3RvcmFnZSA9IHtcclxuICAgIENoYW5nZV9zdG9yYWdlIChUeXBlLE51bWJlcikge1xyXG4gICAgICAgIHZhciBTdG9yYWdlX251bWJlciA9IFN0b3JhZ2UuR2V0X0luZm8oVHlwZSk7XHJcbiAgICAgICAgU3RvcmFnZV9udW1iZXIgKz0gTnVtYmVyO1xyXG4gICAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShUeXBlLFN0b3JhZ2VfbnVtYmVyKTtcclxuICAgIH0sXHJcbiAgICBHZXRfSW5mbyAoVHlwZSl7XHJcbiAgICAgICAgdmFyIHN0YXJudW0gPSBjYy5zeXMubG9jYWxTdG9yYWdlLmdldEl0ZW0oVHlwZSk7XHJcbiAgICAgICAgICAgIGlmKCFzdGFybnVtKXtcclxuICAgICAgICAgICAgICAgIHN0YXJudW0gPSAwO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHN0YXJudW0gPSBwYXJzZUludChzdGFybnVtKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gc3Rhcm51bTtcclxuICAgIH0sXHJcbiAgICBTZXRfSW5mbyAoVHlwZSxzdGFybnVtKXtcclxuICAgICAgICBjYy5zeXMubG9jYWxTdG9yYWdlLnNldEl0ZW0oVHlwZSxzdGFybnVtKTtcclxuICAgIH0sXHJcbn1cclxuLy93eC5nZXRPcGVuRGF0YUNvbnRleHQoKS5wb3N0TWVzc2FnZSh7XHJcbi8vICAgICBtZXNzYWdlOiBMZXZlbF9QYXNzX05vd1xyXG4vLyB9KTtcclxuXHJcbi8vY2Muc3lzLmxvY2FsU3RvcmFnZS5zZXRJdGVtKGtleSwgdmFsdWUpXHJcbi8vY2Muc3lzLmxvY2FsU3RvcmFnZS5nZXRJdGVtKGtleSkiXX0=