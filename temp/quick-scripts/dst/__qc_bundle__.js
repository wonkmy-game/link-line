
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/migration/use_v2.1-2.2.1_cc.Toggle_event');
require('./assets/scripts/DianziMove');
require('./assets/scripts/G');
require('./assets/scripts/GameMgr');
require('./assets/scripts/GameStart');
require('./assets/scripts/LevelBtn');
require('./assets/scripts/SelectPanel');
require('./assets/scripts/close_tip');
require('./assets/scripts/draw');
require('./assets/scripts/launch_scene');
require('./assets/scripts/newuser_guide');
require('./assets/scripts/next_level');
require('./assets/scripts/replay');
require('./assets/scripts/storage');
require('./assets/scripts/testlevel');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/GameMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7f90apk5fFBZryIK89Dr9zE', 'GameMgr');
// scripts/GameMgr.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GameMgr = /** @class */ (function (_super) {
    __extends(GameMgr, _super);
    function GameMgr() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GameMgr.prototype.start = function () {
    };
    GameMgr.gameOver = false;
    GameMgr.GlobalOffsetY = 180;
    GameMgr.tubeScale = 1.194;
    GameMgr = __decorate([
        ccclass
    ], GameMgr);
    return GameMgr;
}(cc.Component));
exports.default = GameMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcR2FtZU1nci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLDRFQUE0RTtBQUM1RSxtQkFBbUI7QUFDbkIsc0ZBQXNGO0FBQ3RGLDhCQUE4QjtBQUM5QixzRkFBc0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVoRixJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFxQywyQkFBWTtJQUFqRDs7SUFPQSxDQUFDO0lBRmEsdUJBQUssR0FBZjtJQUNBLENBQUM7SUFMTSxnQkFBUSxHQUFTLEtBQUssQ0FBQztJQUN2QixxQkFBYSxHQUFVLEdBQUcsQ0FBQztJQUMzQixpQkFBUyxHQUFVLEtBQUssQ0FBQztJQUhmLE9BQU87UUFEM0IsT0FBTztPQUNhLE9BQU8sQ0FPM0I7SUFBRCxjQUFDO0NBUEQsQUFPQyxDQVBvQyxFQUFFLENBQUMsU0FBUyxHQU9oRDtrQkFQb0IsT0FBTyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZU1nciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBzdGF0aWMgZ2FtZU92ZXI6Ym9vbGVhbj1mYWxzZTtcclxuICAgIHN0YXRpYyBHbG9iYWxPZmZzZXRZOm51bWJlciA9IDE4MDtcclxuICAgIHN0YXRpYyB0dWJlU2NhbGU6bnVtYmVyID0gMS4xOTQ7XHJcbiAgICBcclxuICAgIHByb3RlY3RlZCBzdGFydCgpOiB2b2lkIHtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/G.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b842b/E6UdPn4O/8uJbdUIS', 'G');
// scripts/G.js

"use strict";

window.G = {
  originBallList: [],
  //用于重置功能啊老哥
  BallList: [],
  //每次刚开始的时候加载当前关卡的balllist node，每次完成一条线的时候，删除掉对应的balls;
  storkelist: [],
  //一共几条线，每条线的点存在这里，是二维数组
  LevelJson: null,
  check_all_storke_interset: function check_all_storke_interset() {
    // console.log(this.storkelist)
    if (this.storkelist.length < 2) {
      return false;
    } //最多会有5条，不管几条，循环对比，每两条都要判断下是否相交


    for (var i = 0; i < this.storkelist.length; i++) {
      //每一条都和index比自己大的线对比
      for (var j = 0; j < this.storkelist.length; j++) {
        if (j > i) {
          var res = this.check_two_storke_interset(this.storkelist[i], this.storkelist[j]);

          if (res != false) {
            console.log('G:' + res);
            return res;
          }
        }
      }
    }

    return false;
  },
  check_two_storke_interset: function check_two_storke_interset(list1, list2) {
    //判断两个数组是否香交，不交叉返回false，交叉返回交叉点坐标
    //先遍历list1中每个点到list2中每个点距离，复杂的N方
    for (var i = 0; i < list1.length - 1; i++) {
      for (var j = 0; j < list2.length - 1; j++) {
        //判断两点是否相交，如果滑动过快，这个方法会判断不出来
        var a = list1[i][0] - list2[j][0];
        var b = list1[i][1] - list2[j][1];
        var dis = Math.sqrt(a * a + b * b);

        if (dis < 20) {
          return list1[i];
        } //改用判断线段是否相交的方法,来判断是否重叠,但是这个方法无法知道具体交叉的点是哪个，退而求其次，取某个线段的起点
        // let res = this.checkCross(list1[i],list1[i+1],list2[j],list2[j+1])
        // if(res){
        //     return list1[i];
        // }

      }
    }

    return false;
  },
  reset: function reset() {
    //清空G里面的信息
    G.BallList = [];
    G.storkelist = [];
    G.LevelJson = null;
  },
  //计算向量叉乘  
  crossMul: function crossMul(v1, v2) {
    return v1.x * v2.y - v1.y * v2.x;
  },
  //判断两条线段是否相交  
  checkCross: function checkCross(p1, p2, p3, p4) {
    var v1_1 = {
      x: p1.x - p3.x,
      y: p1.y - p3.y
    };
    var v1_2 = {
      x: p2.x - p3.x,
      y: p2.y - p3.y
    };
    var v1_3 = {
      x: p4.x - p3.x,
      y: p4.y - p3.y
    };
    var vv1 = this.crossMul(v1_1, v1_3) * this.crossMul(v1_2, v1_3);
    var v2_1 = {
      x: p3.x - p1.x,
      y: p3.y - p1.y
    };
    var v2_2 = {
      x: p4.x - p1.x,
      y: p4.y - p1.y
    };
    var v2_3 = {
      x: p2.x - p1.x,
      y: p2.y - p1.y
    };
    var vv2 = this.crossMul(v2_1, v2_3) * this.crossMul(v2_2, v2_3);
    return vv1 < 0 && vv2 < 0 ? true : false;
  },
  ShowChayeRewardAD: function ShowChayeRewardAD() {
    //admob
    if (cc.sys.platform == cc.sys.ANDROID) {
      var res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Interstitial_Reward", "ShowInterstitialRewardAD", "()I");
      return res;
    }
  },
  ShowRewardAD: function ShowRewardAD(TYPE) {
    if (cc.sys.platform == cc.sys.ANDROID) {
      var res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Reward", "ShowRewardAD", "()I");
      return res;
    } else if (cc.sys.platform == cc.sys.WECHAT_GAME) {
      if (window["rewardedVideoAd"] != undefined) {
        window["rewardedVideoAd"].show()["catch"](function () {
          console.log('激励视频 广告显示失败');
        });
        window['reward_type'] = TYPE;
      } else {
        cc.find("Canvas/dialog").getComponent(cc.Label).string = '暂无广告资源，请稍后再试！';
        setTimeout(function () {
          cc.find("Canvas/dialog").active = false;
        }, 2);
      }
    }
  },
  GetRewardAD: function GetRewardAD() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      var res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Reward", "GetRewardAD", "()I");
      return res;
    }

    return true;
  },
  GetInterstitialRewardAD: function GetInterstitialRewardAD() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      var res = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Interstitial_Reward", "GetInterstitialRewardAD", "()I");
      return res;
    }

    return true;
  },
  ShowInterstitialAD: function ShowInterstitialAD() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      jsb.reflection.callStaticMethod("org/cocos2dx/javascript/admob/Interstitial", "ShowInterstitialAD", "()V");
    } else if (cc.sys.platform == cc.sys.WECHAT_GAME) {
      if (window["interstitialAd"] != undefined) {
        window["interstitialAd"].show()["catch"](function (err) {
          console.log(err); // Global.bannerAd.show().catch(err => {console.log(err)});
        });
      }
    }
  },
  AnalysticLogin: function AnalysticLogin() {
    if (cc.sys.platform == cc.sys.ANDROID) {
      return;
    } // return true;


    cocosAnalytics.init({
      appID: "647407103",
      // 游戏ID
      version: "1.0.0",
      // 游戏/应用版本号
      storeID: "WECHAT",
      // 分发渠道
      engine: "cocos" // 游戏引擎

    }); //开启(关闭)本地日志的输出

    cocosAnalytics.enableDebug(false); // 开始登陆

    cocosAnalytics.CAAccount.loginStart({
      channel: 'WECHAT' // 获客渠道，指获取该客户的广告渠道信息   

    }); // 登陆成功

    cocosAnalytics.CAAccount.loginSuccess({
      userID: "id",
      age: 1,
      // 年龄
      sex: 1,
      // 性别：1为男，2为女，其它表示未知
      channel: 'WECHAT' // 获客渠道，指获取该客户的广告渠道信息   

    });
  },
  AnalysticCustomEvent: function AnalysticCustomEvent(key, value) {
    if (cc.sys.platform == cc.sys.ANDROID) {
      return;
    }

    var EventId = key;
    var EventValue = {
      'Vaule': value
    }; // 事件完成
    // 参数：事件ID（必填）, 不得超过30个字符

    cocosAnalytics.CACustomEvent.onSuccess(EventId, EventValue);
  },
  wxtopshare: function wxtopshare() {
    if (cc.sys.platform == cc.sys.WECHAT_GAME) {
      wx.showShareMenu({
        success: function success(res) {
          console.log('开启被动转发成功！');
        },
        fail: function fail(res) {
          console.log(res);
          console.log('开启被动转发失败！');
        }
      });
      wx.onShareAppMessage(function () {
        return {
          title: '画线小挑战~' // imageUrlId: '3In1gKQRT2eZ5PbRwWY1Gw==',
          // imageUrl: 'https://mmocgame.qpic.cn/wechatgame/H8OjaAYSeOhg2YwBicoYRP80nr6fhoPCEIWjCiaVDoKmCMCSRL0ibGMzRbQmRmsCcDk/0'

        };
      });
    }
  },
  share: function share() {
    if (cc.sys.platform == cc.sys.WECHAT_GAME) {
      // let index = new Random_Num().random(9);
      // console.log(WXImgids[Number(index)]);
      wx.shareAppMessage({
        title: '画线小挑战~' // imageUrlId: WXImgids[index[0]].id,
        // imageUrl: WXImgids[index[0]].url

      });
    }
  }
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcRy5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCJHIiwib3JpZ2luQmFsbExpc3QiLCJCYWxsTGlzdCIsInN0b3JrZWxpc3QiLCJMZXZlbEpzb24iLCJjaGVja19hbGxfc3RvcmtlX2ludGVyc2V0IiwibGVuZ3RoIiwiaSIsImoiLCJyZXMiLCJjaGVja190d29fc3RvcmtlX2ludGVyc2V0IiwiY29uc29sZSIsImxvZyIsImxpc3QxIiwibGlzdDIiLCJhIiwiYiIsImRpcyIsIk1hdGgiLCJzcXJ0IiwicmVzZXQiLCJjcm9zc011bCIsInYxIiwidjIiLCJ4IiwieSIsImNoZWNrQ3Jvc3MiLCJwMSIsInAyIiwicDMiLCJwNCIsInYxXzEiLCJ2MV8yIiwidjFfMyIsInZ2MSIsInYyXzEiLCJ2Ml8yIiwidjJfMyIsInZ2MiIsIlNob3dDaGF5ZVJld2FyZEFEIiwiY2MiLCJzeXMiLCJwbGF0Zm9ybSIsIkFORFJPSUQiLCJqc2IiLCJyZWZsZWN0aW9uIiwiY2FsbFN0YXRpY01ldGhvZCIsIlNob3dSZXdhcmRBRCIsIlRZUEUiLCJXRUNIQVRfR0FNRSIsInVuZGVmaW5lZCIsInNob3ciLCJmaW5kIiwiZ2V0Q29tcG9uZW50IiwiTGFiZWwiLCJzdHJpbmciLCJzZXRUaW1lb3V0IiwiYWN0aXZlIiwiR2V0UmV3YXJkQUQiLCJHZXRJbnRlcnN0aXRpYWxSZXdhcmRBRCIsIlNob3dJbnRlcnN0aXRpYWxBRCIsImVyciIsIkFuYWx5c3RpY0xvZ2luIiwiY29jb3NBbmFseXRpY3MiLCJpbml0IiwiYXBwSUQiLCJ2ZXJzaW9uIiwic3RvcmVJRCIsImVuZ2luZSIsImVuYWJsZURlYnVnIiwiQ0FBY2NvdW50IiwibG9naW5TdGFydCIsImNoYW5uZWwiLCJsb2dpblN1Y2Nlc3MiLCJ1c2VySUQiLCJhZ2UiLCJzZXgiLCJBbmFseXN0aWNDdXN0b21FdmVudCIsImtleSIsInZhbHVlIiwiRXZlbnRJZCIsIkV2ZW50VmFsdWUiLCJDQUN1c3RvbUV2ZW50Iiwib25TdWNjZXNzIiwid3h0b3BzaGFyZSIsInd4Iiwic2hvd1NoYXJlTWVudSIsInN1Y2Nlc3MiLCJmYWlsIiwib25TaGFyZUFwcE1lc3NhZ2UiLCJ0aXRsZSIsInNoYXJlIiwic2hhcmVBcHBNZXNzYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxNQUFNLENBQUNDLENBQVAsR0FBUztFQUNMQyxjQUFjLEVBQUMsRUFEVjtFQUNhO0VBQ2xCQyxRQUFRLEVBQUMsRUFGSjtFQUVRO0VBQ2JDLFVBQVUsRUFBQyxFQUhOO0VBR1M7RUFDZEMsU0FBUyxFQUFDLElBSkw7RUFLTEMseUJBTEssdUNBS3NCO0lBQ3ZCO0lBQ0EsSUFBRyxLQUFLRixVQUFMLENBQWdCRyxNQUFoQixHQUF5QixDQUE1QixFQUE4QjtNQUMxQixPQUFPLEtBQVA7SUFDSCxDQUpzQixDQUt2Qjs7O0lBQ0EsS0FBSSxJQUFJQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUUsS0FBS0osVUFBTCxDQUFnQkcsTUFBL0IsRUFBc0NDLENBQUMsRUFBdkMsRUFBMEM7TUFDdEM7TUFDQSxLQUFJLElBQUlDLENBQUMsR0FBQyxDQUFWLEVBQVlBLENBQUMsR0FBRSxLQUFLTCxVQUFMLENBQWdCRyxNQUEvQixFQUFzQ0UsQ0FBQyxFQUF2QyxFQUEwQztRQUN0QyxJQUFHQSxDQUFDLEdBQUNELENBQUwsRUFBTztVQUNILElBQUlFLEdBQUcsR0FBRyxLQUFLQyx5QkFBTCxDQUErQixLQUFLUCxVQUFMLENBQWdCSSxDQUFoQixDQUEvQixFQUFrRCxLQUFLSixVQUFMLENBQWdCSyxDQUFoQixDQUFsRCxDQUFWOztVQUNBLElBQUdDLEdBQUcsSUFBRSxLQUFSLEVBQWM7WUFBRUUsT0FBTyxDQUFDQyxHQUFSLENBQVksT0FBS0gsR0FBakI7WUFBdUIsT0FBT0EsR0FBUDtVQUFhO1FBRXZEO01BQ0o7SUFDSjs7SUFDRCxPQUFPLEtBQVA7RUFDSCxDQXRCSTtFQXVCTEMseUJBdkJLLHFDQXVCcUJHLEtBdkJyQixFQXVCMkJDLEtBdkIzQixFQXVCaUM7SUFDbEM7SUFDQTtJQUNBLEtBQUksSUFBSVAsQ0FBQyxHQUFDLENBQVYsRUFBYUEsQ0FBQyxHQUFDTSxLQUFLLENBQUNQLE1BQU4sR0FBYSxDQUE1QixFQUE4QkMsQ0FBQyxFQUEvQixFQUFrQztNQUM5QixLQUFJLElBQUlDLENBQUMsR0FBQyxDQUFWLEVBQWFBLENBQUMsR0FBRU0sS0FBSyxDQUFDUixNQUFOLEdBQWEsQ0FBN0IsRUFBK0JFLENBQUMsRUFBaEMsRUFBbUM7UUFDL0I7UUFDQSxJQUFJTyxDQUFDLEdBQUdGLEtBQUssQ0FBQ04sQ0FBRCxDQUFMLENBQVMsQ0FBVCxJQUFZTyxLQUFLLENBQUNOLENBQUQsQ0FBTCxDQUFTLENBQVQsQ0FBcEI7UUFDQSxJQUFJUSxDQUFDLEdBQUdILEtBQUssQ0FBQ04sQ0FBRCxDQUFMLENBQVMsQ0FBVCxJQUFZTyxLQUFLLENBQUNOLENBQUQsQ0FBTCxDQUFTLENBQVQsQ0FBcEI7UUFDQSxJQUFJUyxHQUFHLEdBQUdDLElBQUksQ0FBQ0MsSUFBTCxDQUFVSixDQUFDLEdBQUNBLENBQUYsR0FBSUMsQ0FBQyxHQUFDQSxDQUFoQixDQUFWOztRQUNBLElBQUdDLEdBQUcsR0FBRyxFQUFULEVBQVk7VUFDUixPQUFPSixLQUFLLENBQUNOLENBQUQsQ0FBWjtRQUNILENBUDhCLENBVWhDO1FBRUM7UUFDQTtRQUNBO1FBQ0E7O01BRUg7SUFDSjs7SUFDRCxPQUFPLEtBQVA7RUFDSCxDQS9DSTtFQWdETGEsS0FoREssbUJBZ0RFO0lBQ0g7SUFDQXBCLENBQUMsQ0FBQ0UsUUFBRixHQUFhLEVBQWI7SUFDQUYsQ0FBQyxDQUFDRyxVQUFGLEdBQWUsRUFBZjtJQUNBSCxDQUFDLENBQUNJLFNBQUYsR0FBYyxJQUFkO0VBQ0gsQ0FyREk7RUF1RFQ7RUFDS2lCLFFBeERJLG9CQXdES0MsRUF4REwsRUF3RFFDLEVBeERSLEVBd0RXO0lBQ1osT0FBU0QsRUFBRSxDQUFDRSxDQUFILEdBQUtELEVBQUUsQ0FBQ0UsQ0FBUixHQUFVSCxFQUFFLENBQUNHLENBQUgsR0FBS0YsRUFBRSxDQUFDQyxDQUEzQjtFQUNILENBMURJO0VBMkRUO0VBQ0tFLFVBNURJLHNCQTRET0MsRUE1RFAsRUE0RFVDLEVBNURWLEVBNERhQyxFQTVEYixFQTREZ0JDLEVBNURoQixFQTREbUI7SUFDcEIsSUFBSUMsSUFBSSxHQUFDO01BQUNQLENBQUMsRUFBQ0csRUFBRSxDQUFDSCxDQUFILEdBQUtLLEVBQUUsQ0FBQ0wsQ0FBWDtNQUFhQyxDQUFDLEVBQUNFLEVBQUUsQ0FBQ0YsQ0FBSCxHQUFLSSxFQUFFLENBQUNKO0lBQXZCLENBQVQ7SUFDQSxJQUFJTyxJQUFJLEdBQUM7TUFBQ1IsQ0FBQyxFQUFDSSxFQUFFLENBQUNKLENBQUgsR0FBS0ssRUFBRSxDQUFDTCxDQUFYO01BQWFDLENBQUMsRUFBQ0csRUFBRSxDQUFDSCxDQUFILEdBQUtJLEVBQUUsQ0FBQ0o7SUFBdkIsQ0FBVDtJQUNBLElBQUlRLElBQUksR0FBQztNQUFDVCxDQUFDLEVBQUNNLEVBQUUsQ0FBQ04sQ0FBSCxHQUFLSyxFQUFFLENBQUNMLENBQVg7TUFBYUMsQ0FBQyxFQUFDSyxFQUFFLENBQUNMLENBQUgsR0FBS0ksRUFBRSxDQUFDSjtJQUF2QixDQUFUO0lBQ0EsSUFBSVMsR0FBRyxHQUFDLEtBQUtiLFFBQUwsQ0FBY1UsSUFBZCxFQUFtQkUsSUFBbkIsSUFBeUIsS0FBS1osUUFBTCxDQUFjVyxJQUFkLEVBQW1CQyxJQUFuQixDQUFqQztJQUNBLElBQUlFLElBQUksR0FBQztNQUFDWCxDQUFDLEVBQUNLLEVBQUUsQ0FBQ0wsQ0FBSCxHQUFLRyxFQUFFLENBQUNILENBQVg7TUFBYUMsQ0FBQyxFQUFDSSxFQUFFLENBQUNKLENBQUgsR0FBS0UsRUFBRSxDQUFDRjtJQUF2QixDQUFUO0lBQ0EsSUFBSVcsSUFBSSxHQUFDO01BQUNaLENBQUMsRUFBQ00sRUFBRSxDQUFDTixDQUFILEdBQUtHLEVBQUUsQ0FBQ0gsQ0FBWDtNQUFhQyxDQUFDLEVBQUNLLEVBQUUsQ0FBQ0wsQ0FBSCxHQUFLRSxFQUFFLENBQUNGO0lBQXZCLENBQVQ7SUFDQSxJQUFJWSxJQUFJLEdBQUM7TUFBQ2IsQ0FBQyxFQUFDSSxFQUFFLENBQUNKLENBQUgsR0FBS0csRUFBRSxDQUFDSCxDQUFYO01BQWFDLENBQUMsRUFBQ0csRUFBRSxDQUFDSCxDQUFILEdBQUtFLEVBQUUsQ0FBQ0Y7SUFBdkIsQ0FBVDtJQUNBLElBQUlhLEdBQUcsR0FBQyxLQUFLakIsUUFBTCxDQUFjYyxJQUFkLEVBQW1CRSxJQUFuQixJQUF5QixLQUFLaEIsUUFBTCxDQUFjZSxJQUFkLEVBQW1CQyxJQUFuQixDQUFqQztJQUNBLE9BQVFILEdBQUcsR0FBQyxDQUFKLElBQU9JLEdBQUcsR0FBQyxDQUFaLEdBQWUsSUFBZixHQUFvQixLQUEzQjtFQUNILENBdEVJO0VBdUVMQyxpQkF2RUssK0JBdUVlO0lBQ2hCO0lBQ04sSUFBSUMsRUFBRSxDQUFDQyxHQUFILENBQU9DLFFBQVAsSUFBbUJGLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPRSxPQUE5QixFQUFzQztNQUNyQyxJQUFJbEMsR0FBRyxHQUFHbUMsR0FBRyxDQUFDQyxVQUFKLENBQWVDLGdCQUFmLENBQWdDLG1EQUFoQyxFQUFxRiwwQkFBckYsRUFBaUgsS0FBakgsQ0FBVjtNQUNBLE9BQU9yQyxHQUFQO0lBQ0E7RUFFRSxDQTlFSTtFQStFTHNDLFlBL0VLLHdCQStFUUMsSUEvRVIsRUErRWE7SUFDcEIsSUFBSVIsRUFBRSxDQUFDQyxHQUFILENBQU9DLFFBQVAsSUFBbUJGLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPRSxPQUE5QixFQUF1QztNQUN0QyxJQUFJbEMsR0FBRyxHQUFHbUMsR0FBRyxDQUFDQyxVQUFKLENBQWVDLGdCQUFmLENBQWdDLHNDQUFoQyxFQUF3RSxjQUF4RSxFQUF3RixLQUF4RixDQUFWO01BQ0EsT0FBT3JDLEdBQVA7SUFDQSxDQUhELE1BR00sSUFBRytCLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPQyxRQUFQLElBQW1CRixFQUFFLENBQUNDLEdBQUgsQ0FBT1EsV0FBN0IsRUFBMEM7TUFDckMsSUFBSWxELE1BQU0sQ0FBQyxpQkFBRCxDQUFOLElBQTZCbUQsU0FBakMsRUFBNEM7UUFDM0NuRCxNQUFNLENBQUMsaUJBQUQsQ0FBTixDQUEwQm9ELElBQTFCLFlBQ2lCLFlBQU07VUFDVHhDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVo7UUFDVCxDQUhMO1FBSU9iLE1BQU0sQ0FBQyxhQUFELENBQU4sR0FBd0JpRCxJQUF4QjtNQUNYLENBTkcsTUFNRztRQUNOUixFQUFFLENBQUNZLElBQUgsQ0FBUSxlQUFSLEVBQXlCQyxZQUF6QixDQUFzQ2IsRUFBRSxDQUFDYyxLQUF6QyxFQUFnREMsTUFBaEQsR0FBeUQsZUFBekQ7UUFDSUMsVUFBVSxDQUFDLFlBQU07VUFDUGhCLEVBQUUsQ0FBQ1ksSUFBSCxDQUFRLGVBQVIsRUFBeUJLLE1BQXpCLEdBQWtDLEtBQWxDO1FBQ2IsQ0FGYSxFQUVYLENBRlcsQ0FBVjtNQUdKO0lBQ0Q7RUFFSixDQWxHSTtFQW1HTEMsV0FuR0sseUJBbUdRO0lBQ2YsSUFBSWxCLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPQyxRQUFQLElBQW1CRixFQUFFLENBQUNDLEdBQUgsQ0FBT0UsT0FBOUIsRUFBdUM7TUFDdEMsSUFBSWxDLEdBQUcsR0FBR21DLEdBQUcsQ0FBQ0MsVUFBSixDQUFlQyxnQkFBZixDQUFnQyxzQ0FBaEMsRUFBd0UsYUFBeEUsRUFBdUYsS0FBdkYsQ0FBVjtNQUNBLE9BQU9yQyxHQUFQO0lBQ0E7O0lBQ0ssT0FBTyxJQUFQO0VBQ0gsQ0F6R0k7RUEwR0xrRCx1QkExR0sscUNBMEdvQjtJQUMzQixJQUFJbkIsRUFBRSxDQUFDQyxHQUFILENBQU9DLFFBQVAsSUFBbUJGLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPRSxPQUE5QixFQUF1QztNQUN0QyxJQUFJbEMsR0FBRyxHQUFHbUMsR0FBRyxDQUFDQyxVQUFKLENBQWVDLGdCQUFmLENBQWdDLG1EQUFoQyxFQUFxRix5QkFBckYsRUFBZ0gsS0FBaEgsQ0FBVjtNQUNBLE9BQU9yQyxHQUFQO0lBQ0E7O0lBQ0ssT0FBTyxJQUFQO0VBQ0gsQ0FoSEk7RUFpSExtRCxrQkFqSEssZ0NBaUhlO0lBQ2hCLElBQUlwQixFQUFFLENBQUNDLEdBQUgsQ0FBT0MsUUFBUCxJQUFtQkYsRUFBRSxDQUFDQyxHQUFILENBQU9FLE9BQTlCLEVBQXVDO01BQ25DQyxHQUFHLENBQUNDLFVBQUosQ0FBZUMsZ0JBQWYsQ0FBZ0MsNENBQWhDLEVBQThFLG9CQUE5RSxFQUFvRyxLQUFwRztJQUNILENBRkQsTUFFTyxJQUFJTixFQUFFLENBQUNDLEdBQUgsQ0FBT0MsUUFBUCxJQUFtQkYsRUFBRSxDQUFDQyxHQUFILENBQU9RLFdBQTlCLEVBQTJDO01BQzlDLElBQUlsRCxNQUFNLENBQUMsZ0JBQUQsQ0FBTixJQUE0Qm1ELFNBQWhDLEVBQTJDO1FBQzdDbkQsTUFBTSxDQUFDLGdCQUFELENBQU4sQ0FBeUJvRCxJQUF6QixHQUFnQyxPQUFoQyxFQUF5QyxVQUFVVSxHQUFWLEVBQWU7VUFDdkRsRCxPQUFPLENBQUNDLEdBQVIsQ0FBWWlELEdBQVosRUFEdUQsQ0FDckM7UUFDbEIsQ0FGRDtNQUdBO0lBQ0Q7RUFDSixDQTNISTtFQTRIUkMsY0E1SFEsNEJBNEhTO0lBQ1YsSUFBSXRCLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPQyxRQUFQLElBQW1CRixFQUFFLENBQUNDLEdBQUgsQ0FBT0UsT0FBOUIsRUFBdUM7TUFDbkM7SUFDSCxDQUhTLENBSVY7OztJQUNOb0IsY0FBYyxDQUFDQyxJQUFmLENBQW9CO01BQ25CQyxLQUFLLEVBQUUsV0FEWTtNQUNjO01BQ2pDQyxPQUFPLEVBQUUsT0FGVTtNQUVTO01BQzVCQyxPQUFPLEVBQUUsUUFIVTtNQUdJO01BQ3ZCQyxNQUFNLEVBQUUsT0FKVyxDQUlPOztJQUpQLENBQXBCLEVBTGdCLENBV2hCOztJQUNBTCxjQUFjLENBQUNNLFdBQWYsQ0FBMkIsS0FBM0IsRUFaZ0IsQ0FhaEI7O0lBQ0FOLGNBQWMsQ0FBQ08sU0FBZixDQUF5QkMsVUFBekIsQ0FBb0M7TUFDbkNDLE9BQU8sRUFBRSxRQUQwQixDQUNoQjs7SUFEZ0IsQ0FBcEMsRUFkZ0IsQ0FrQmhCOztJQUNBVCxjQUFjLENBQUNPLFNBQWYsQ0FBeUJHLFlBQXpCLENBQXNDO01BQ3JDQyxNQUFNLEVBQUUsSUFENkI7TUFFckNDLEdBQUcsRUFBRSxDQUZnQztNQUVqQjtNQUNwQkMsR0FBRyxFQUFFLENBSGdDO01BR2pCO01BQ3BCSixPQUFPLEVBQUUsUUFKNEIsQ0FJbEI7O0lBSmtCLENBQXRDO0VBT0EsQ0F0Sk87RUF1SlJLLG9CQXZKUSxnQ0F1SmFDLEdBdkpiLEVBdUprQkMsS0F2SmxCLEVBdUp5QjtJQUMxQixJQUFJdkMsRUFBRSxDQUFDQyxHQUFILENBQU9DLFFBQVAsSUFBbUJGLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPRSxPQUE5QixFQUF1QztNQUNuQztJQUNIOztJQUNQLElBQUlxQyxPQUFPLEdBQUdGLEdBQWQ7SUFDQSxJQUFJRyxVQUFVLEdBQUc7TUFDaEIsU0FBU0Y7SUFETyxDQUFqQixDQUxnQyxDQVFoQztJQUNBOztJQUNBaEIsY0FBYyxDQUFDbUIsYUFBZixDQUE2QkMsU0FBN0IsQ0FBdUNILE9BQXZDLEVBQWdEQyxVQUFoRDtFQUNBLENBbEtPO0VBbUtSRyxVQW5LUSx3QkFtS0s7SUFDWixJQUFJNUMsRUFBRSxDQUFDQyxHQUFILENBQU9DLFFBQVAsSUFBbUJGLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPUSxXQUE5QixFQUEyQztNQUMxQ29DLEVBQUUsQ0FBQ0MsYUFBSCxDQUFpQjtRQUNoQkMsT0FBTyxFQUFFLGlCQUFDOUUsR0FBRCxFQUFTO1VBQ2pCRSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxXQUFaO1FBQ0EsQ0FIZTtRQUloQjRFLElBQUksRUFBRSxjQUFDL0UsR0FBRCxFQUFTO1VBQ2RFLE9BQU8sQ0FBQ0MsR0FBUixDQUFZSCxHQUFaO1VBQ0FFLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLFdBQVo7UUFDQTtNQVBlLENBQWpCO01BVUF5RSxFQUFFLENBQUNJLGlCQUFILENBQXFCLFlBQVk7UUFDaEMsT0FBTztVQUNOQyxLQUFLLEVBQUUsUUFERCxDQUVOO1VBQ0E7O1FBSE0sQ0FBUDtNQUtBLENBTkQ7SUFPQTtFQUVELENBeExPO0VBeUxMQyxLQXpMSyxtQkF5TEc7SUFDSixJQUFJbkQsRUFBRSxDQUFDQyxHQUFILENBQU9DLFFBQVAsSUFBbUJGLEVBQUUsQ0FBQ0MsR0FBSCxDQUFPUSxXQUE5QixFQUEyQztNQUN2QztNQUNBO01BQ0FvQyxFQUFFLENBQUNPLGVBQUgsQ0FBbUI7UUFDZkYsS0FBSyxFQUFFLFFBRFEsQ0FFZjtRQUNBOztNQUhlLENBQW5CO0lBS0g7RUFDSjtBQW5NSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJ3aW5kb3cuRz17XHJcbiAgICBvcmlnaW5CYWxsTGlzdDpbXSwvL+eUqOS6jumHjee9ruWKn+iDveWViuiAgeWTpVxyXG4gICAgQmFsbExpc3Q6W10sIC8v5q+P5qyh5Yia5byA5aeL55qE5pe25YCZ5Yqg6L295b2T5YmN5YWz5Y2h55qEYmFsbGxpc3Qgbm9kZe+8jOavj+asoeWujOaIkOS4gOadoee6v+eahOaXtuWAme+8jOWIoOmZpOaOieWvueW6lOeahGJhbGxzO1xyXG4gICAgc3RvcmtlbGlzdDpbXSwvL+S4gOWFseWHoOadoee6v++8jOavj+adoee6v+eahOeCueWtmOWcqOi/memHjO+8jOaYr+S6jOe7tOaVsOe7hFxyXG4gICAgTGV2ZWxKc29uOm51bGwsXHJcbiAgICBjaGVja19hbGxfc3RvcmtlX2ludGVyc2V0KCl7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5zdG9ya2VsaXN0KVxyXG4gICAgICAgIGlmKHRoaXMuc3RvcmtlbGlzdC5sZW5ndGggPCAyKXtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvL+acgOWkmuS8muaciTXmnaHvvIzkuI3nrqHlh6DmnaHvvIzlvqrnjq/lr7nmr5TvvIzmr4/kuKTmnaHpg73opoHliKTmlq3kuIvmmK/lkKbnm7jkuqRcclxuICAgICAgICBmb3IobGV0IGk9MDtpPCB0aGlzLnN0b3JrZWxpc3QubGVuZ3RoO2krKyl7XHJcbiAgICAgICAgICAgIC8v5q+P5LiA5p2h6YO95ZKMaW5kZXjmr5Toh6rlt7HlpKfnmoTnur/lr7nmr5RcclxuICAgICAgICAgICAgZm9yKGxldCBqPTA7ajwgdGhpcy5zdG9ya2VsaXN0Lmxlbmd0aDtqKyspe1xyXG4gICAgICAgICAgICAgICAgaWYoaj5pKXtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzID0gdGhpcy5jaGVja190d29fc3RvcmtlX2ludGVyc2V0KHRoaXMuc3RvcmtlbGlzdFtpXSx0aGlzLnN0b3JrZWxpc3Rbal0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlcyE9ZmFsc2UpeyBjb25zb2xlLmxvZygnRzonK3Jlcyk7IHJldHVybiByZXM7IH1cclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9LFxyXG4gICAgY2hlY2tfdHdvX3N0b3JrZV9pbnRlcnNldChsaXN0MSxsaXN0Mil7XHJcbiAgICAgICAgLy/liKTmlq3kuKTkuKrmlbDnu4TmmK/lkKbpppnkuqTvvIzkuI3kuqTlj4nov5Tlm55mYWxzZe+8jOS6pOWPiei/lOWbnuS6pOWPieeCueWdkOagh1xyXG4gICAgICAgIC8v5YWI6YGN5Y6GbGlzdDHkuK3mr4/kuKrngrnliLBsaXN0MuS4reavj+S4queCuei3neemu++8jOWkjeadgueahE7mlrlcclxuICAgICAgICBmb3IobGV0IGk9MDsgaTxsaXN0MS5sZW5ndGgtMTtpKyspe1xyXG4gICAgICAgICAgICBmb3IobGV0IGo9MDsgajwgbGlzdDIubGVuZ3RoLTE7aisrKXtcclxuICAgICAgICAgICAgICAgIC8v5Yik5pat5Lik54K55piv5ZCm55u45Lqk77yM5aaC5p6c5ruR5Yqo6L+H5b+r77yM6L+Z5Liq5pa55rOV5Lya5Yik5pat5LiN5Ye65p2lXHJcbiAgICAgICAgICAgICAgICB2YXIgYSA9IGxpc3QxW2ldWzBdLWxpc3QyW2pdWzBdO1xyXG4gICAgICAgICAgICAgICAgdmFyIGIgPSBsaXN0MVtpXVsxXS1saXN0MltqXVsxXTtcclxuICAgICAgICAgICAgICAgIHZhciBkaXMgPSBNYXRoLnNxcnQoYSphK2IqYik7XHJcbiAgICAgICAgICAgICAgICBpZihkaXMgPCAyMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGxpc3QxW2ldO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgICAgICAgIC8v5pS555So5Yik5pat57q/5q615piv5ZCm55u45Lqk55qE5pa55rOVLOadpeWIpOaWreaYr+WQpumHjeWPoCzkvYbmmK/ov5nkuKrmlrnms5Xml6Dms5Xnn6XpgZPlhbfkvZPkuqTlj4nnmoTngrnmmK/lk6rkuKrvvIzpgIDogIzmsYLlhbbmrKHvvIzlj5bmn5DkuKrnur/mrrXnmoTotbfngrlcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBsZXQgcmVzID0gdGhpcy5jaGVja0Nyb3NzKGxpc3QxW2ldLGxpc3QxW2krMV0sbGlzdDJbal0sbGlzdDJbaisxXSlcclxuICAgICAgICAgICAgICAgIC8vIGlmKHJlcyl7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgcmV0dXJuIGxpc3QxW2ldO1xyXG4gICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9LFxyXG4gICAgcmVzZXQoKXtcclxuICAgICAgICAvL+a4heepukfph4zpnaLnmoTkv6Hmga9cclxuICAgICAgICBHLkJhbGxMaXN0ID0gW107XHJcbiAgICAgICAgRy5zdG9ya2VsaXN0ID0gW107XHJcbiAgICAgICAgRy5MZXZlbEpzb24gPSBudWxsO1xyXG4gICAgfSxcclxuICAgIFxyXG4vL+iuoeeul+WQkemHj+WPieS5mCAgXHJcbiAgICAgY3Jvc3NNdWwodjEsdjIpeyAgXHJcbiAgICAgICAgcmV0dXJuICAgdjEueCp2Mi55LXYxLnkqdjIueDsgIFxyXG4gICAgfSAsIFxyXG4vL+WIpOaWreS4pOadoee6v+auteaYr+WQpuebuOS6pCAgXHJcbiAgICAgY2hlY2tDcm9zcyhwMSxwMixwMyxwNCl7ICBcclxuICAgICAgICB2YXIgdjFfMT17eDpwMS54LXAzLngseTpwMS55LXAzLnl9IFxyXG4gICAgICAgIHZhciB2MV8yPXt4OnAyLngtcDMueCx5OnAyLnktcDMueX0gXHJcbiAgICAgICAgdmFyIHYxXzM9e3g6cDQueC1wMy54LHk6cDQueS1wMy55fSBcclxuICAgICAgICB2YXIgdnYxPXRoaXMuY3Jvc3NNdWwodjFfMSx2MV8zKSp0aGlzLmNyb3NzTXVsKHYxXzIsdjFfMykgIFxyXG4gICAgICAgIHZhciB2Ml8xPXt4OnAzLngtcDEueCx5OnAzLnktcDEueX0gIFxyXG4gICAgICAgIHZhciB2Ml8yPXt4OnA0LngtcDEueCx5OnA0LnktcDEueX0gIFxyXG4gICAgICAgIHZhciB2Ml8zPXt4OnAyLngtcDEueCx5OnAyLnktcDEueX0gXHJcbiAgICAgICAgdmFyIHZ2Mj10aGlzLmNyb3NzTXVsKHYyXzEsdjJfMykqdGhpcy5jcm9zc011bCh2Ml8yLHYyXzMpO1xyXG4gICAgICAgIHJldHVybiAodnYxPDAmJnZ2MjwwKT90cnVlOmZhbHNlIFxyXG4gICAgfSxcclxuICAgIFNob3dDaGF5ZVJld2FyZEFEKCkge1xyXG4gICAgICAgIC8vYWRtb2JcclxuXHRcdGlmIChjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLkFORFJPSUQpe1xyXG5cdFx0XHRsZXQgcmVzID0ganNiLnJlZmxlY3Rpb24uY2FsbFN0YXRpY01ldGhvZChcIm9yZy9jb2NvczJkeC9qYXZhc2NyaXB0L2FkbW9iL0ludGVyc3RpdGlhbF9SZXdhcmRcIiwgXCJTaG93SW50ZXJzdGl0aWFsUmV3YXJkQURcIiwgXCIoKUlcIik7XHJcblx0XHRcdHJldHVybiByZXM7XHJcblx0XHR9XHJcbiAgICAgICAgXHJcbiAgICB9LFxyXG4gICAgU2hvd1Jld2FyZEFEKFRZUEUpe1xyXG5cdFx0aWYgKGNjLnN5cy5wbGF0Zm9ybSA9PSBjYy5zeXMuQU5EUk9JRCkge1xyXG5cdFx0XHRsZXQgcmVzID0ganNiLnJlZmxlY3Rpb24uY2FsbFN0YXRpY01ldGhvZChcIm9yZy9jb2NvczJkeC9qYXZhc2NyaXB0L2FkbW9iL1Jld2FyZFwiLCBcIlNob3dSZXdhcmRBRFwiLCBcIigpSVwiKTtcclxuXHRcdFx0cmV0dXJuIHJlcztcclxuXHRcdH1lbHNlIGlmKGNjLnN5cy5wbGF0Zm9ybSA9PSBjYy5zeXMuV0VDSEFUX0dBTUUpIHtcclxuICAgICAgICAgICAgIGlmICh3aW5kb3dbXCJyZXdhcmRlZFZpZGVvQWRcIl0gIT0gdW5kZWZpbmVkKSB7XHJcblx0XHQgICAgICAgICAgICB3aW5kb3dbXCJyZXdhcmRlZFZpZGVvQWRcIl0uc2hvdygpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygn5r+A5Yqx6KeG6aKRIOW5v+WRiuaYvuekuuWksei0pScpXHJcblx0XHQgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAgd2luZG93WydyZXdhcmRfdHlwZSddID0gVFlQRTtcclxuXHQgICAgICAgIH0gZWxzZSB7XHJcblx0XHQgICAgICAgIGNjLmZpbmQoXCJDYW52YXMvZGlhbG9nXCIpLmdldENvbXBvbmVudChjYy5MYWJlbCkuc3RyaW5nID0gJ+aaguaXoOW5v+WRiui1hOa6kO+8jOivt+eojeWQjuWGjeivle+8gSc7XHJcblx0XHQgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2MuZmluZChcIkNhbnZhcy9kaWFsb2dcIikuYWN0aXZlID0gZmFsc2U7XHJcblx0XHQgICAgICAgIH0sIDIpO1xyXG5cdCAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgIH0sXHJcbiAgICBHZXRSZXdhcmRBRCgpe1xyXG5cdFx0aWYgKGNjLnN5cy5wbGF0Zm9ybSA9PSBjYy5zeXMuQU5EUk9JRCkge1xyXG5cdFx0XHRsZXQgcmVzID0ganNiLnJlZmxlY3Rpb24uY2FsbFN0YXRpY01ldGhvZChcIm9yZy9jb2NvczJkeC9qYXZhc2NyaXB0L2FkbW9iL1Jld2FyZFwiLCBcIkdldFJld2FyZEFEXCIsIFwiKClJXCIpO1xyXG5cdFx0XHRyZXR1cm4gcmVzO1xyXG5cdFx0fVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSxcclxuICAgIEdldEludGVyc3RpdGlhbFJld2FyZEFEKCl7XHJcblx0XHRpZiAoY2Muc3lzLnBsYXRmb3JtID09IGNjLnN5cy5BTkRST0lEKSB7XHJcblx0XHRcdGxldCByZXMgPSBqc2IucmVmbGVjdGlvbi5jYWxsU3RhdGljTWV0aG9kKFwib3JnL2NvY29zMmR4L2phdmFzY3JpcHQvYWRtb2IvSW50ZXJzdGl0aWFsX1Jld2FyZFwiLCBcIkdldEludGVyc3RpdGlhbFJld2FyZEFEXCIsIFwiKClJXCIpO1xyXG5cdFx0XHRyZXR1cm4gcmVzO1xyXG5cdFx0fVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSxcclxuICAgIFNob3dJbnRlcnN0aXRpYWxBRCgpe1xyXG4gICAgICAgIGlmIChjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLkFORFJPSUQpIHtcclxuICAgICAgICAgICAganNiLnJlZmxlY3Rpb24uY2FsbFN0YXRpY01ldGhvZChcIm9yZy9jb2NvczJkeC9qYXZhc2NyaXB0L2FkbW9iL0ludGVyc3RpdGlhbFwiLCBcIlNob3dJbnRlcnN0aXRpYWxBRFwiLCBcIigpVlwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGNjLnN5cy5wbGF0Zm9ybSA9PSBjYy5zeXMuV0VDSEFUX0dBTUUpIHtcclxuICAgICAgICAgICAgaWYgKHdpbmRvd1tcImludGVyc3RpdGlhbEFkXCJdICE9IHVuZGVmaW5lZCkge1xyXG5cdFx0ICAgICAgICB3aW5kb3dbXCJpbnRlcnN0aXRpYWxBZFwiXS5zaG93KClbXCJjYXRjaFwiXShmdW5jdGlvbiAoZXJyKSB7XHJcblx0XHRcdCAgICAgICAgY29uc29sZS5sb2coZXJyKTsgLy8gR2xvYmFsLmJhbm5lckFkLnNob3coKS5jYXRjaChlcnIgPT4ge2NvbnNvbGUubG9nKGVycil9KTtcclxuXHRcdCAgICAgICAgfSk7XHJcblx0ICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHRBbmFseXN0aWNMb2dpbigpIHtcclxuICAgICAgICBpZiAoY2Muc3lzLnBsYXRmb3JtID09IGNjLnN5cy5BTkRST0lEKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gcmV0dXJuIHRydWU7XHJcblx0XHRjb2Nvc0FuYWx5dGljcy5pbml0KHtcclxuXHRcdFx0YXBwSUQ6IFwiNjQ3NDA3MTAzXCIsICAgICAgICAgICAgICAvLyDmuLjmiI9JRFxyXG5cdFx0XHR2ZXJzaW9uOiBcIjEuMC4wXCIsICAgICAgICAgICAvLyDmuLjmiI8v5bqU55So54mI5pys5Y+3XHJcblx0XHRcdHN0b3JlSUQ6IFwiV0VDSEFUXCIsICAgICAvLyDliIblj5HmuKDpgZNcclxuXHRcdFx0ZW5naW5lOiBcImNvY29zXCIgICAgICAgICAgIC8vIOa4uOaIj+W8leaTjlxyXG5cdFx0fSk7XHJcblx0XHQvL+W8gOWQryjlhbPpl60p5pys5Zyw5pel5b+X55qE6L6T5Ye6XHJcblx0XHRjb2Nvc0FuYWx5dGljcy5lbmFibGVEZWJ1ZyhmYWxzZSk7XHJcblx0XHQvLyDlvIDlp4vnmbvpmYZcclxuXHRcdGNvY29zQW5hbHl0aWNzLkNBQWNjb3VudC5sb2dpblN0YXJ0KHtcclxuXHRcdFx0Y2hhbm5lbDogJ1dFQ0hBVCcgIC8vIOiOt+Wuoua4oOmBk++8jOaMh+iOt+WPluivpeWuouaIt+eahOW5v+WRiua4oOmBk+S/oeaBryAgIFxyXG5cdFx0fSk7XHJcblxyXG5cdFx0Ly8g55m76ZmG5oiQ5YqfXHJcblx0XHRjb2Nvc0FuYWx5dGljcy5DQUFjY291bnQubG9naW5TdWNjZXNzKHtcclxuXHRcdFx0dXNlcklEOiBcImlkXCIsXHJcblx0XHRcdGFnZTogMSwgICAgICAgICAgICAgLy8g5bm06b6EXHJcblx0XHRcdHNleDogMSwgICAgICAgICAgICAgLy8g5oCn5Yir77yaMeS4uueUt++8jDLkuLrlpbPvvIzlhbblroPooajnpLrmnKrnn6VcclxuXHRcdFx0Y2hhbm5lbDogJ1dFQ0hBVCcgIC8vIOiOt+Wuoua4oOmBk++8jOaMh+iOt+WPluivpeWuouaIt+eahOW5v+WRiua4oOmBk+S/oeaBryAgIFxyXG5cdFx0fSk7XHJcblxyXG5cdH0sXHJcblx0QW5hbHlzdGljQ3VzdG9tRXZlbnQoa2V5LCB2YWx1ZSkge1xyXG4gICAgICAgIGlmIChjYy5zeXMucGxhdGZvcm0gPT0gY2Muc3lzLkFORFJPSUQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHRcdGxldCBFdmVudElkID0ga2V5O1xyXG5cdFx0bGV0IEV2ZW50VmFsdWUgPSB7XHJcblx0XHRcdCdWYXVsZSc6IHZhbHVlXHJcblx0XHR9XHJcblx0XHQvLyDkuovku7blrozmiJBcclxuXHRcdC8vIOWPguaVsO+8muS6i+S7tklE77yI5b+F5aGr77yJLCDkuI3lvpfotoXov4czMOS4quWtl+esplxyXG5cdFx0Y29jb3NBbmFseXRpY3MuQ0FDdXN0b21FdmVudC5vblN1Y2Nlc3MoRXZlbnRJZCwgRXZlbnRWYWx1ZSk7XHJcblx0fSxcclxuXHR3eHRvcHNoYXJlKCkge1xyXG5cdFx0aWYgKGNjLnN5cy5wbGF0Zm9ybSA9PSBjYy5zeXMuV0VDSEFUX0dBTUUpIHtcclxuXHRcdFx0d3guc2hvd1NoYXJlTWVudSh7XHJcblx0XHRcdFx0c3VjY2VzczogKHJlcykgPT4ge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coJ+W8gOWQr+iiq+WKqOi9rOWPkeaIkOWKn++8gScpO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0ZmFpbDogKHJlcykgPT4ge1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2cocmVzKTtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKCflvIDlkK/ooqvliqjovazlj5HlpLHotKXvvIEnKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdFx0d3gub25TaGFyZUFwcE1lc3NhZ2UoZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRcdHJldHVybiB7XHJcblx0XHRcdFx0XHR0aXRsZTogJ+eUu+e6v+Wwj+aMkeaImH4nLFxyXG5cdFx0XHRcdFx0Ly8gaW1hZ2VVcmxJZDogJzNJbjFnS1FSVDJlWjVQYlJ3V1kxR3c9PScsXHJcblx0XHRcdFx0XHQvLyBpbWFnZVVybDogJ2h0dHBzOi8vbW1vY2dhbWUucXBpYy5jbi93ZWNoYXRnYW1lL0g4T2phQVlTZU9oZzJZd0JpY29ZUlA4MG5yNmZob1BDRUlXakNpYVZEb0ttQ01DU1JMMGliR016UmJRbVJtc0NjRGsvMCdcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHR9XHJcblxyXG5cdH0sXHJcbiAgICBzaGFyZSgpIHtcclxuICAgICAgICBpZiAoY2Muc3lzLnBsYXRmb3JtID09IGNjLnN5cy5XRUNIQVRfR0FNRSkge1xyXG4gICAgICAgICAgICAvLyBsZXQgaW5kZXggPSBuZXcgUmFuZG9tX051bSgpLnJhbmRvbSg5KTtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coV1hJbWdpZHNbTnVtYmVyKGluZGV4KV0pO1xyXG4gICAgICAgICAgICB3eC5zaGFyZUFwcE1lc3NhZ2Uoe1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6ICfnlLvnur/lsI/mjJHmiJh+JyxcclxuICAgICAgICAgICAgICAgIC8vIGltYWdlVXJsSWQ6IFdYSW1naWRzW2luZGV4WzBdXS5pZCxcclxuICAgICAgICAgICAgICAgIC8vIGltYWdlVXJsOiBXWEltZ2lkc1tpbmRleFswXV0udXJsXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufTsiXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/migration/use_v2.1-2.2.1_cc.Toggle_event.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8e82ccm49JKA6QMzomA53kV', 'use_v2.1-2.2.1_cc.Toggle_event');
// migration/use_v2.1-2.2.1_cc.Toggle_event.js

"use strict";

/*
 * This script is automatically generated by Cocos Creator and is only used for projects compatible with the v2.1.0 ～ 2.2.1 version.
 * You do not need to manually add this script in any other project.
 * If you don't use cc.Toggle in your project, you can delete this script directly.
 * If your project is hosted in VCS such as git, submit this script together.
 *
 * 此脚本由 Cocos Creator 自动生成，仅用于兼容 v2.1.0 ~ 2.2.1 版本的工程，
 * 你无需在任何其它项目中手动添加此脚本。
 * 如果你的项目中没用到 Toggle，可直接删除该脚本。
 * 如果你的项目有托管于 git 等版本库，请将此脚本一并上传。
 */
if (cc.Toggle) {
  // Whether to trigger 'toggle' and 'checkEvents' events when modifying 'toggle.isChecked' in the code
  // 在代码中修改 'toggle.isChecked' 时是否触发 'toggle' 与 'checkEvents' 事件
  cc.Toggle._triggerEventInScript_isChecked = true;
}

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcbWlncmF0aW9uXFx1c2VfdjIuMS0yLjIuMV9jYy5Ub2dnbGVfZXZlbnQuanMiXSwibmFtZXMiOlsiY2MiLCJUb2dnbGUiLCJfdHJpZ2dlckV2ZW50SW5TY3JpcHRfaXNDaGVja2VkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSxJQUFJQSxFQUFFLENBQUNDLE1BQVAsRUFBZTtFQUNYO0VBQ0E7RUFDQUQsRUFBRSxDQUFDQyxNQUFILENBQVVDLCtCQUFWLEdBQTRDLElBQTVDO0FBQ0giLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiAqIFRoaXMgc2NyaXB0IGlzIGF1dG9tYXRpY2FsbHkgZ2VuZXJhdGVkIGJ5IENvY29zIENyZWF0b3IgYW5kIGlzIG9ubHkgdXNlZCBmb3IgcHJvamVjdHMgY29tcGF0aWJsZSB3aXRoIHRoZSB2Mi4xLjAg772eIDIuMi4xIHZlcnNpb24uXHJcbiAqIFlvdSBkbyBub3QgbmVlZCB0byBtYW51YWxseSBhZGQgdGhpcyBzY3JpcHQgaW4gYW55IG90aGVyIHByb2plY3QuXHJcbiAqIElmIHlvdSBkb24ndCB1c2UgY2MuVG9nZ2xlIGluIHlvdXIgcHJvamVjdCwgeW91IGNhbiBkZWxldGUgdGhpcyBzY3JpcHQgZGlyZWN0bHkuXHJcbiAqIElmIHlvdXIgcHJvamVjdCBpcyBob3N0ZWQgaW4gVkNTIHN1Y2ggYXMgZ2l0LCBzdWJtaXQgdGhpcyBzY3JpcHQgdG9nZXRoZXIuXHJcbiAqXHJcbiAqIOatpOiEmuacrOeUsSBDb2NvcyBDcmVhdG9yIOiHquWKqOeUn+aIkO+8jOS7heeUqOS6juWFvOWuuSB2Mi4xLjAgfiAyLjIuMSDniYjmnKznmoTlt6XnqIvvvIxcclxuICog5L2g5peg6ZyA5Zyo5Lu75L2V5YW25a6D6aG555uu5Lit5omL5Yqo5re75Yqg5q2k6ISa5pys44CCXHJcbiAqIOWmguaenOS9oOeahOmhueebruS4reayoeeUqOWIsCBUb2dnbGXvvIzlj6/nm7TmjqXliKDpmaTor6XohJrmnKzjgIJcclxuICog5aaC5p6c5L2g55qE6aG555uu5pyJ5omY566h5LqOIGdpdCDnrYnniYjmnKzlupPvvIzor7flsIbmraTohJrmnKzkuIDlubbkuIrkvKDjgIJcclxuICovXHJcblxyXG5pZiAoY2MuVG9nZ2xlKSB7XHJcbiAgICAvLyBXaGV0aGVyIHRvIHRyaWdnZXIgJ3RvZ2dsZScgYW5kICdjaGVja0V2ZW50cycgZXZlbnRzIHdoZW4gbW9kaWZ5aW5nICd0b2dnbGUuaXNDaGVja2VkJyBpbiB0aGUgY29kZVxyXG4gICAgLy8g5Zyo5Luj56CB5Lit5L+u5pS5ICd0b2dnbGUuaXNDaGVja2VkJyDml7bmmK/lkKbop6blj5EgJ3RvZ2dsZScg5LiOICdjaGVja0V2ZW50cycg5LqL5Lu2XHJcbiAgICBjYy5Ub2dnbGUuX3RyaWdnZXJFdmVudEluU2NyaXB0X2lzQ2hlY2tlZCA9IHRydWU7XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/LevelBtn.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b7f29fC9ndHkJ0nB5H6FRN+', 'LevelBtn');
// scripts/LevelBtn.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var COOKIE_LEVEL = "level";
var LevelBtn = /** @class */ (function (_super) {
    __extends(LevelBtn, _super);
    function LevelBtn() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.id = -1;
        _this.numNumber = null;
        return _this;
    }
    LevelBtn.prototype.init = function (id) {
        this.id = id;
        this.numNumber.string = String(this.id);
    };
    LevelBtn.prototype.goToLevel = function () {
        Storage.Set_Info('level', this.numNumber.string - 1);
        cc.director.loadScene("Home");
    };
    __decorate([
        property({ type: cc.Label })
    ], LevelBtn.prototype, "numNumber", void 0);
    LevelBtn = __decorate([
        ccclass
    ], LevelBtn);
    return LevelBtn;
}(cc.Component));
exports.default = LevelBtn;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcTGV2ZWxCdG4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQiw0RUFBNEU7QUFDNUUsbUJBQW1CO0FBQ25CLHNGQUFzRjtBQUN0Riw4QkFBOEI7QUFDOUIsc0ZBQXNGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJaEYsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFDNUMsSUFBTSxZQUFZLEdBQUcsT0FBTyxDQUFBO0FBRTVCO0lBQXNDLDRCQUFZO0lBQWxEO1FBQUEscUVBaUJDO1FBaEJHLFFBQUUsR0FBVyxDQUFDLENBQUMsQ0FBQztRQUdoQixlQUFTLEdBQWEsSUFBSSxDQUFDOztJQWEvQixDQUFDO0lBWEcsdUJBQUksR0FBSixVQUFLLEVBQVU7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNiLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELDRCQUFTLEdBQVQ7UUFDSSxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztRQUVyRCxFQUFFLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBWEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOytDQUNGO0lBSlYsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQWlCNUI7SUFBRCxlQUFDO0NBakJELEFBaUJDLENBakJxQyxFQUFFLENBQUMsU0FBUyxHQWlCakQ7a0JBakJvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cblxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuY29uc3QgQ09PS0lFX0xFVkVMID0gXCJsZXZlbFwiXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTGV2ZWxCdG4gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIGlkOiBudW1iZXIgPSAtMTtcblxuICAgIEBwcm9wZXJ0eSh7IHR5cGU6IGNjLkxhYmVsIH0pXG4gICAgbnVtTnVtYmVyOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBpbml0KGlkOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xuICAgICAgICB0aGlzLm51bU51bWJlci5zdHJpbmcgPSBTdHJpbmcodGhpcy5pZCk7XG4gICAgfVxuXG4gICAgZ29Ub0xldmVsKCkge1xuICAgICAgICBTdG9yYWdlLlNldF9JbmZvKCdsZXZlbCcsIHRoaXMubnVtTnVtYmVyLnN0cmluZyAtIDEpO1xuXG4gICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZShcIkhvbWVcIik7XG4gICAgfVxuXG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/close_tip.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0332eDs/ptLRZzgGKTRm85Z', 'close_tip');
// scripts/close_tip.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    TIPS_BG_Node: cc.Node,
    ButtonAudio: cc.AudioClip
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {// this.Draw = cc.find('Canvas/MainBG/Draw')
  },
  start: function start() {
    var self = this;
    this.node.on('click', function (button) {
      //The event is a custom event, you could get the Button component via first argument

      /*通过模块化脚本操作
      var GameStart = require("GameStart");
      new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
      */

      /*通过找组件操作
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      */

      /*直接重新加载场景
      cc.director.loadScene("Home");
      */
      //展示提示，首先将提示的图片找到
      cc.find('Canvas/TIPS/Tip').active = false; //音效

      cc.audioEngine.playEffect(self.ButtonAudio, false);
    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcY2xvc2VfdGlwLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiVElQU19CR19Ob2RlIiwiTm9kZSIsIkJ1dHRvbkF1ZGlvIiwiQXVkaW9DbGlwIiwib25Mb2FkIiwic3RhcnQiLCJzZWxmIiwibm9kZSIsIm9uIiwiYnV0dG9uIiwiZmluZCIsImFjdGl2ZSIsImF1ZGlvRW5naW5lIiwicGxheUVmZmVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLFlBQVksRUFBQ0osRUFBRSxDQUFDSyxJQURSO0lBRVJDLFdBQVcsRUFBQ04sRUFBRSxDQUFDTztFQUZQLENBSFA7RUFRTDtFQUVBQyxNQVZLLG9CQVVLLENBQ047RUFDSCxDQVpJO0VBY0xDLEtBZEssbUJBY0k7SUFDTCxJQUFJQyxJQUFJLEdBQUcsSUFBWDtJQUNBLEtBQUtDLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBVUMsTUFBVixFQUFrQjtNQUNwQzs7TUFDQTtBQUNaO0FBQ0E7QUFDQTs7TUFDWTtBQUNaO0FBQ0E7O01BQ1k7QUFDWjtBQUNBO01BQ1c7TUFDQWIsRUFBRSxDQUFDYyxJQUFILENBQVEsaUJBQVIsRUFBMkJDLE1BQTNCLEdBQW9DLEtBQXBDLENBYnFDLENBY3JDOztNQUNBZixFQUFFLENBQUNnQixXQUFILENBQWVDLFVBQWYsQ0FBMEJQLElBQUksQ0FBQ0osV0FBL0IsRUFBNEMsS0FBNUM7SUFDRCxDQWhCRjtFQWlCSCxDQWpDSSxDQW1DTDs7QUFuQ0ssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcblxyXG5jYy5DbGFzcyh7XHJcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXHJcblxyXG4gICAgcHJvcGVydGllczoge1xyXG4gICAgICAgIFRJUFNfQkdfTm9kZTpjYy5Ob2RlLFxyXG4gICAgICAgIEJ1dHRvbkF1ZGlvOmNjLkF1ZGlvQ2xpcCxcclxuICAgIH0sXHJcblxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuICAgICAgICAvLyB0aGlzLkRyYXcgPSBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0RyYXcnKVxyXG4gICAgfSxcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLCBmdW5jdGlvbiAoYnV0dG9uKSB7XHJcbiAgICAgICAgICAgIC8vVGhlIGV2ZW50IGlzIGEgY3VzdG9tIGV2ZW50LCB5b3UgY291bGQgZ2V0IHRoZSBCdXR0b24gY29tcG9uZW50IHZpYSBmaXJzdCBhcmd1bWVudFxyXG4gICAgICAgICAgICAvKumAmui/h+aooeWdl+WMluiEmuacrOaTjeS9nFxyXG4gICAgICAgICAgICB2YXIgR2FtZVN0YXJ0ID0gcmVxdWlyZShcIkdhbWVTdGFydFwiKTtcclxuICAgICAgICAgICAgbmV3IEdhbWVTdGFydCgpLnN0YXJ0KCk7IC8vIOiuv+mXrumdmeaAgeWPmOmHj+S8muaKpemUme+8jOmHh+eUqOe7hOS7tuafpeaJvueahOaWueazlVxyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKumAmui/h+aJvue7hOS7tuaTjeS9nFxyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHJykuZ2V0Q29tcG9uZW50KCdHYW1lU3RhcnQnKS5zdGFydCgpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKuebtOaOpemHjeaWsOWKoOi9veWcuuaZr1xyXG4gICAgICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJIb21lXCIpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgIC8v5bGV56S65o+Q56S677yM6aaW5YWI5bCG5o+Q56S655qE5Zu+54mH5om+5YiwXHJcbiAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL1RJUFMvVGlwJykuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgLy/pn7PmlYhcclxuICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHNlbGYuQnV0dG9uQXVkaW8sIGZhbHNlKTtcclxuICAgICAgICAgfSlcclxuICAgIH0sXHJcblxyXG4gICAgLy8gdXBkYXRlIChkdCkge30sXHJcbn0pO1xyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/GameStart.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '52952eOu/BK950/ZvvmzZ4x', 'GameStart');
// scripts/GameStart.js

"use strict";

var _GameMgr = _interopRequireDefault(require("./GameMgr"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    BG_Node: cc.Node,
    redball: cc.Prefab,
    yellowball: cc.Prefab,
    greenball: cc.Prefab,
    purpleball: cc.Prefab,
    blueball: cc.Prefab,
    Level: cc.Label,
    BGM: cc.AudioClip,
    backMenu: cc.Node,
    zhuzi1: cc.Node,
    zhuzi2: cc.Node,
    _fx_list: []
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    //播放bgm
    var bgm = cc.audioEngine.playMusic(this.BGM, true);
    cc.audioEngine.setVolume(bgm, 0.15);
    this.backMenu.on(cc.Node.EventType.TOUCH_START, this.backToMenu, this);
    cc.director.getCollisionManager().enabled = true;
    cc.game.on("show_ball_mask", this.showMask, this);
    cc.game.on("gameover", this.OnGameover, this);
  },
  OnGameover: function OnGameover(node, parent) {
    var _this = this;

    var newPos = node.position;
    cc.game.off("gameover");
    node.destroy();
    cc.resources.load("perfab/col_fx", cc.Prefab, function (err, fx) {
      var newFx = cc.instantiate(fx);
      parent.addChild(newFx);
      newFx.scale = 0.5;
      newFx.setPosition(newPos);
    });
    this.scheduleOnce(function () {
      _this.showWrongIcon(new cc.Vec3(newPos.x, newPos.y - 50, 0), parent);

      _this.scheduleOnce(function () {
        _this.backToMenu();
      }, 2);
    }, 2);
  },
  showWrongIcon: function showWrongIcon(pos, parent) {
    var self = this;
    cc.resources.load("perfab/wrong", function (err, prefab) {
      var newNode = cc.instantiate(prefab);
      newNode.parent = parent;
      newNode.setPosition(pos);
      newNode.scale = 1.5;
      setTimeout(function () {
        newNode.active = false;
        newNode.destroy();
      }, 2000);
    });
  },
  showMask: function showMask(startPos, endPos, ballId, startBall, endBall) {
    var self = this;
    var level = Storage.Get_Info('level');
    var color = this.getIDByColor(ballId);
    var startPosNewBall = this.load_prefab(color);
    var endPosNewBall = this.load_prefab(color);
    self.BG_Node.addChild(endPosNewBall);
    self.BG_Node.addChild(startPosNewBall);
    var scaleValue = 1;
    var flipX = 1;

    if (Number(level) + 1 == 1) {
      if (startPos.y < endPos.y) {
        //从下往上画线
        startPosNewBall.angle = 35;
        startPosNewBall.scale = new cc.Vec3(-1.6, 1.6, 1.6);
        endPosNewBall.angle = 35;
        endPosNewBall.scale = new cc.Vec3(1.6, 1.6, 1.6);
        scaleValue = 3;
      } else {
        //从上往下画线
        startPosNewBall.angle = 35;
        startPosNewBall.scale = new cc.Vec3(1.6, 1.6, 1.6);
        endPosNewBall.angle = 35;
        endPosNewBall.scale = new cc.Vec3(-1.6, 1.6, 1.6);
        scaleValue = -3;
      }
    } else if (Number(level) + 1 == 2) {
      if (startPos.y < endPos.y) //从下往上画线
        {
          startPosNewBall.angle = 0;
          startPosNewBall.scale = new cc.Vec3(-1, 1, 1);
          endPosNewBall.angle = 0;
          endPosNewBall.scale = new cc.Vec3(1, 1, 1);
          scaleValue = 2;
        } else {
        startPosNewBall.angle = 0;
        startPosNewBall.scale = new cc.Vec3(1, 1, 1);
        endPosNewBall.angle = 0;
        endPosNewBall.scale = new cc.Vec3(-1, 1, 1);
        scaleValue = -2;
      }
    }

    var final_pos = self.BG_Node.convertToNodeSpaceAR(endPos);
    var final_pos1 = self.BG_Node.convertToNodeSpaceAR(startPos);
    endPosNewBall.setPosition(final_pos.x, final_pos.y, 0);
    startPosNewBall.setPosition(final_pos1.x, final_pos1.y, 0);
    cc.tween(endPosNewBall).to(0.3, {
      scale: scaleValue,
      opacity: 0
    }).call(function () {
      endPosNewBall.destroy();
    }).start();
    cc.tween(startPosNewBall).to(0.3, {
      scale: -scaleValue,
      opacity: 0
    }).call(function () {
      startPosNewBall.destroy();
    }).start();
  },
  getIDByColor: function getIDByColor(ballId) {
    switch (ballId) {
      case 0:
        return "redball";

      case 1:
        return "yellowball";

      case 2:
        return "blueball";

      case 3:
        return "greenball";
    }
  },
  backToMenu: function backToMenu() {
    _GameMgr["default"].gameOver = false;
    cc.director.loadScene('launch');
  },
  start: function start() {
    var self = this;
    G.reset(); // console.log('清空G');

    this.BG_Node = cc.find('Canvas/MainBG/BG');
    this.BG_Node.removeAllChildren(); //读取当前是哪一个关卡

    var level = Storage.Get_Info('level');

    if (Number(level) >= 2) {
      //全部通关
      //cc.find('Canvas/TIPS/全部通关').active = true;
      level = 0;
      Storage.Set_Info('level', 0);
      this.backToMenu();
      return;
    }

    if (Number(level) + 1 == 1) {
      this.zhuzi1.setPosition(446.924, this.zhuzi1.position.y - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi2.setPosition(-398.924, this.zhuzi2.position.y - _GameMgr["default"].GlobalOffsetY, 0);
    }

    if (Number(level) + 1 == 2) {
      this.zhuzi1.setPosition(465, 384 - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi1.angle = 0;
      this.zhuzi2.setPosition(-455, -6 - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi2.angle = 180;
    } // if (cc.sys.platform === cc.sys.ANDROID) {
    //     self.Level.string = 'Level ' + (Number(level) + 1);
    // } else {
    //     self.Level.string = '关卡 ' + (Number(level) + 1);
    // }


    cc.resources.load('level/' + level, function (err, jsonAsset) {
      var level_json_info = jsonAsset.json;
      G.LevelJson = level_json_info; //self.loadbg(level_json_info.bg);

      self.loadballs(level_json_info.balls, Number(level) + 1);
    }); // G.AnalysticLogin();
    // G.wxtopshare();
  },
  // loadbg(str) {
  //     // console.log(str);
  //     var self = this;
  //     cc.resources.load("BG/" + str, cc.SpriteFrame, function (err, spriteFrame) {
  //         self.BG_Node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
  //     });
  // },
  loadballs: function loadballs(balls, level) {
    var self = this; // G.stokelist = balls.length;

    for (var i = 0; i < balls.length; i++) {
      var newnode1 = this.load_prefab(balls[i].color);
      self.BG_Node.addChild(newnode1);
      var ballOffset = 10;

      if (level == 1) {
        ballOffset = 10;
        newnode1.setPosition(balls[i].Pos[0] + balls[i].Pos[0] * 0.194, balls[i].Pos[1] - balls[i].Pos[0] * 0.194 - _GameMgr["default"].GlobalOffsetY - (balls[i].flipX == 1 ? ballOffset : -ballOffset));
      } else if (level == 2) {
        ballOffset = -8;
        newnode1.setPosition(balls[i].Pos[0], balls[i].Pos[1]);
      }

      newnode1.angle = balls[i].Rot;

      if (level == 1) {
        newnode1.scale = new cc.Vec3(balls[i].scale * balls[i].flipX * _GameMgr["default"].tubeScale, balls[i].scale * _GameMgr["default"].tubeScale, balls[i].scale);
      } else if (level == 2) {
        newnode1.scale = new cc.Vec3(balls[i].scale * balls[i].flipX, balls[i].scale, balls[i].scale);
      }

      G.BallList.push(newnode1);
      G.originBallList = G.BallList.concat(); // console.log(newnode1.position)
    }
  },
  load_prefab: function load_prefab(color) {
    switch (color) {
      case "redball":
        var node = cc.instantiate(this.redball);
        return node;

      case "yellowball":
        var node = cc.instantiate(this.yellowball);
        return node;

      case "blueball":
        var node = cc.instantiate(this.blueball);
        return node;

      case "purpleball":
        var node = cc.instantiate(this.purpleball);
        return node;

      case "greenball":
        var node = cc.instantiate(this.greenball);
        return node;

      default:
        console.log('颜色不对');
    }
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcR2FtZVN0YXJ0LmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiQkdfTm9kZSIsIk5vZGUiLCJyZWRiYWxsIiwiUHJlZmFiIiwieWVsbG93YmFsbCIsImdyZWVuYmFsbCIsInB1cnBsZWJhbGwiLCJibHVlYmFsbCIsIkxldmVsIiwiTGFiZWwiLCJCR00iLCJBdWRpb0NsaXAiLCJiYWNrTWVudSIsInpodXppMSIsInpodXppMiIsIl9meF9saXN0Iiwib25Mb2FkIiwiYmdtIiwiYXVkaW9FbmdpbmUiLCJwbGF5TXVzaWMiLCJzZXRWb2x1bWUiLCJvbiIsIkV2ZW50VHlwZSIsIlRPVUNIX1NUQVJUIiwiYmFja1RvTWVudSIsImRpcmVjdG9yIiwiZ2V0Q29sbGlzaW9uTWFuYWdlciIsImVuYWJsZWQiLCJnYW1lIiwic2hvd01hc2siLCJPbkdhbWVvdmVyIiwibm9kZSIsInBhcmVudCIsIm5ld1BvcyIsInBvc2l0aW9uIiwib2ZmIiwiZGVzdHJveSIsInJlc291cmNlcyIsImxvYWQiLCJlcnIiLCJmeCIsIm5ld0Z4IiwiaW5zdGFudGlhdGUiLCJhZGRDaGlsZCIsInNjYWxlIiwic2V0UG9zaXRpb24iLCJzY2hlZHVsZU9uY2UiLCJzaG93V3JvbmdJY29uIiwiVmVjMyIsIngiLCJ5IiwicG9zIiwic2VsZiIsInByZWZhYiIsIm5ld05vZGUiLCJzZXRUaW1lb3V0IiwiYWN0aXZlIiwic3RhcnRQb3MiLCJlbmRQb3MiLCJiYWxsSWQiLCJzdGFydEJhbGwiLCJlbmRCYWxsIiwibGV2ZWwiLCJTdG9yYWdlIiwiR2V0X0luZm8iLCJjb2xvciIsImdldElEQnlDb2xvciIsInN0YXJ0UG9zTmV3QmFsbCIsImxvYWRfcHJlZmFiIiwiZW5kUG9zTmV3QmFsbCIsInNjYWxlVmFsdWUiLCJmbGlwWCIsIk51bWJlciIsImFuZ2xlIiwiZmluYWxfcG9zIiwiY29udmVydFRvTm9kZVNwYWNlQVIiLCJmaW5hbF9wb3MxIiwidHdlZW4iLCJ0byIsIm9wYWNpdHkiLCJjYWxsIiwic3RhcnQiLCJHYW1lTWdyIiwiZ2FtZU92ZXIiLCJsb2FkU2NlbmUiLCJHIiwicmVzZXQiLCJmaW5kIiwicmVtb3ZlQWxsQ2hpbGRyZW4iLCJTZXRfSW5mbyIsIkdsb2JhbE9mZnNldFkiLCJqc29uQXNzZXQiLCJsZXZlbF9qc29uX2luZm8iLCJqc29uIiwiTGV2ZWxKc29uIiwibG9hZGJhbGxzIiwiYmFsbHMiLCJpIiwibGVuZ3RoIiwibmV3bm9kZTEiLCJiYWxsT2Zmc2V0IiwiUG9zIiwiUm90IiwidHViZVNjYWxlIiwiQmFsbExpc3QiLCJwdXNoIiwib3JpZ2luQmFsbExpc3QiLCJjb25jYXQiLCJjb25zb2xlIiwibG9nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQU1BOzs7O0FBTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0VBQ0wsV0FBU0QsRUFBRSxDQUFDRSxTQURQO0VBR0xDLFVBQVUsRUFBRTtJQUNSQyxPQUFPLEVBQUVKLEVBQUUsQ0FBQ0ssSUFESjtJQUVSQyxPQUFPLEVBQUVOLEVBQUUsQ0FBQ08sTUFGSjtJQUdSQyxVQUFVLEVBQUVSLEVBQUUsQ0FBQ08sTUFIUDtJQUlSRSxTQUFTLEVBQUVULEVBQUUsQ0FBQ08sTUFKTjtJQUtSRyxVQUFVLEVBQUVWLEVBQUUsQ0FBQ08sTUFMUDtJQU1SSSxRQUFRLEVBQUVYLEVBQUUsQ0FBQ08sTUFOTDtJQU9SSyxLQUFLLEVBQUVaLEVBQUUsQ0FBQ2EsS0FQRjtJQVFSQyxHQUFHLEVBQUVkLEVBQUUsQ0FBQ2UsU0FSQTtJQVNSQyxRQUFRLEVBQUNoQixFQUFFLENBQUNLLElBVEo7SUFVUlksTUFBTSxFQUFDakIsRUFBRSxDQUFDSyxJQVZGO0lBV1JhLE1BQU0sRUFBQ2xCLEVBQUUsQ0FBQ0ssSUFYRjtJQVlSYyxRQUFRLEVBQUM7RUFaRCxDQUhQO0VBa0JMO0VBRUFDLE1BcEJLLG9CQW9CSTtJQUNMO0lBQ0EsSUFBSUMsR0FBRyxHQUFHckIsRUFBRSxDQUFDc0IsV0FBSCxDQUFlQyxTQUFmLENBQXlCLEtBQUtULEdBQTlCLEVBQW1DLElBQW5DLENBQVY7SUFDQWQsRUFBRSxDQUFDc0IsV0FBSCxDQUFlRSxTQUFmLENBQXlCSCxHQUF6QixFQUE4QixJQUE5QjtJQUNBLEtBQUtMLFFBQUwsQ0FBY1MsRUFBZCxDQUFpQnpCLEVBQUUsQ0FBQ0ssSUFBSCxDQUFRcUIsU0FBUixDQUFrQkMsV0FBbkMsRUFBK0MsS0FBS0MsVUFBcEQsRUFBK0QsSUFBL0Q7SUFDQTVCLEVBQUUsQ0FBQzZCLFFBQUgsQ0FBWUMsbUJBQVosR0FBa0NDLE9BQWxDLEdBQTBDLElBQTFDO0lBRUEvQixFQUFFLENBQUNnQyxJQUFILENBQVFQLEVBQVIsQ0FBVyxnQkFBWCxFQUE0QixLQUFLUSxRQUFqQyxFQUEwQyxJQUExQztJQUNBakMsRUFBRSxDQUFDZ0MsSUFBSCxDQUFRUCxFQUFSLENBQVcsVUFBWCxFQUFzQixLQUFLUyxVQUEzQixFQUFzQyxJQUF0QztFQUNILENBN0JJO0VBK0JMQSxVQS9CSyxzQkErQk1DLElBL0JOLEVBK0JXQyxNQS9CWCxFQStCa0I7SUFBQTs7SUFDbkIsSUFBSUMsTUFBTSxHQUFHRixJQUFJLENBQUNHLFFBQWxCO0lBQ0F0QyxFQUFFLENBQUNnQyxJQUFILENBQVFPLEdBQVIsQ0FBWSxVQUFaO0lBQ0FKLElBQUksQ0FBQ0ssT0FBTDtJQUNBeEMsRUFBRSxDQUFDeUMsU0FBSCxDQUFhQyxJQUFiLENBQWtCLGVBQWxCLEVBQWtDMUMsRUFBRSxDQUFDTyxNQUFyQyxFQUE0QyxVQUFDb0MsR0FBRCxFQUFLQyxFQUFMLEVBQVU7TUFDbEQsSUFBSUMsS0FBSyxHQUFDN0MsRUFBRSxDQUFDOEMsV0FBSCxDQUFlRixFQUFmLENBQVY7TUFDQVIsTUFBTSxDQUFDVyxRQUFQLENBQWdCRixLQUFoQjtNQUNBQSxLQUFLLENBQUNHLEtBQU4sR0FBWSxHQUFaO01BQ0FILEtBQUssQ0FBQ0ksV0FBTixDQUFrQlosTUFBbEI7SUFDSCxDQUxEO0lBTUEsS0FBS2EsWUFBTCxDQUFrQixZQUFJO01BQ2xCLEtBQUksQ0FBQ0MsYUFBTCxDQUFtQixJQUFJbkQsRUFBRSxDQUFDb0QsSUFBUCxDQUFZZixNQUFNLENBQUNnQixDQUFuQixFQUFxQmhCLE1BQU0sQ0FBQ2lCLENBQVAsR0FBVyxFQUFoQyxFQUFtQyxDQUFuQyxDQUFuQixFQUF5RGxCLE1BQXpEOztNQUNBLEtBQUksQ0FBQ2MsWUFBTCxDQUFrQixZQUFJO1FBQ2xCLEtBQUksQ0FBQ3RCLFVBQUw7TUFDSCxDQUZELEVBRUUsQ0FGRjtJQUdILENBTEQsRUFLRSxDQUxGO0VBT0gsQ0FoREk7RUFpREx1QixhQWpESyx5QkFpRFNJLEdBakRULEVBaURhbkIsTUFqRGIsRUFpRG9CO0lBQ3JCLElBQUlvQixJQUFJLEdBQUcsSUFBWDtJQUNBeEQsRUFBRSxDQUFDeUMsU0FBSCxDQUFhQyxJQUFiLENBQWtCLGNBQWxCLEVBQWtDLFVBQVVDLEdBQVYsRUFBZWMsTUFBZixFQUF1QjtNQUNyRCxJQUFJQyxPQUFPLEdBQUcxRCxFQUFFLENBQUM4QyxXQUFILENBQWVXLE1BQWYsQ0FBZDtNQUNBQyxPQUFPLENBQUN0QixNQUFSLEdBQWlCQSxNQUFqQjtNQUNBc0IsT0FBTyxDQUFDVCxXQUFSLENBQW9CTSxHQUFwQjtNQUNBRyxPQUFPLENBQUNWLEtBQVIsR0FBZSxHQUFmO01BQ0FXLFVBQVUsQ0FBQyxZQUFVO1FBQ2pCRCxPQUFPLENBQUNFLE1BQVIsR0FBaUIsS0FBakI7UUFDQUYsT0FBTyxDQUFDbEIsT0FBUjtNQUNILENBSFMsRUFHUixJQUhRLENBQVY7SUFJSCxDQVREO0VBVUgsQ0E3REk7RUE4RExQLFFBOURLLG9CQThESTRCLFFBOURKLEVBOERhQyxNQTlEYixFQThEb0JDLE1BOURwQixFQThEMkJDLFNBOUQzQixFQThEcUNDLE9BOURyQyxFQThENkM7SUFDOUMsSUFBSVQsSUFBSSxHQUFHLElBQVg7SUFDQSxJQUFJVSxLQUFLLEdBQUdDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFaO0lBQ0EsSUFBSUMsS0FBSyxHQUFHLEtBQUtDLFlBQUwsQ0FBa0JQLE1BQWxCLENBQVo7SUFDQSxJQUFJUSxlQUFlLEdBQUcsS0FBS0MsV0FBTCxDQUFpQkgsS0FBakIsQ0FBdEI7SUFDQSxJQUFJSSxhQUFhLEdBQUcsS0FBS0QsV0FBTCxDQUFpQkgsS0FBakIsQ0FBcEI7SUFDQWIsSUFBSSxDQUFDcEQsT0FBTCxDQUFhMkMsUUFBYixDQUFzQjBCLGFBQXRCO0lBQ0FqQixJQUFJLENBQUNwRCxPQUFMLENBQWEyQyxRQUFiLENBQXNCd0IsZUFBdEI7SUFDQSxJQUFJRyxVQUFVLEdBQUcsQ0FBakI7SUFDQSxJQUFJQyxLQUFLLEdBQUcsQ0FBWjs7SUFDQSxJQUFHQyxNQUFNLENBQUNWLEtBQUQsQ0FBTixHQUFjLENBQWQsSUFBaUIsQ0FBcEIsRUFBc0I7TUFDbEIsSUFBR0wsUUFBUSxDQUFDUCxDQUFULEdBQWFRLE1BQU0sQ0FBQ1IsQ0FBdkIsRUFBeUI7UUFBQztRQUN0QmlCLGVBQWUsQ0FBQ00sS0FBaEIsR0FBc0IsRUFBdEI7UUFDQU4sZUFBZSxDQUFDdkIsS0FBaEIsR0FBc0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFDLEdBQWIsRUFBaUIsR0FBakIsRUFBcUIsR0FBckIsQ0FBdEI7UUFFQXFCLGFBQWEsQ0FBQ0ksS0FBZCxHQUFvQixFQUFwQjtRQUNBSixhQUFhLENBQUN6QixLQUFkLEdBQW9CLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVksR0FBWixFQUFnQixHQUFoQixFQUFvQixHQUFwQixDQUFwQjtRQUNBc0IsVUFBVSxHQUFDLENBQVg7TUFDSCxDQVBELE1BT0s7UUFBQztRQUNGSCxlQUFlLENBQUNNLEtBQWhCLEdBQXNCLEVBQXRCO1FBQ0FOLGVBQWUsQ0FBQ3ZCLEtBQWhCLEdBQXNCLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVksR0FBWixFQUFnQixHQUFoQixFQUFvQixHQUFwQixDQUF0QjtRQUVBcUIsYUFBYSxDQUFDSSxLQUFkLEdBQW9CLEVBQXBCO1FBQ0FKLGFBQWEsQ0FBQ3pCLEtBQWQsR0FBb0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFDLEdBQWIsRUFBaUIsR0FBakIsRUFBcUIsR0FBckIsQ0FBcEI7UUFDQXNCLFVBQVUsR0FBQyxDQUFDLENBQVo7TUFDSDtJQUVKLENBakJELE1BaUJNLElBQUdFLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLEdBQWMsQ0FBZCxJQUFpQixDQUFwQixFQUFzQjtNQUN4QixJQUFHTCxRQUFRLENBQUNQLENBQVQsR0FBYVEsTUFBTSxDQUFDUixDQUF2QixFQUF5QjtRQUN6QjtVQUNJaUIsZUFBZSxDQUFDTSxLQUFoQixHQUFzQixDQUF0QjtVQUNBTixlQUFlLENBQUN2QixLQUFoQixHQUFzQixJQUFJaEQsRUFBRSxDQUFDb0QsSUFBUCxDQUFZLENBQUMsQ0FBYixFQUFlLENBQWYsRUFBaUIsQ0FBakIsQ0FBdEI7VUFFQXFCLGFBQWEsQ0FBQ0ksS0FBZCxHQUFvQixDQUFwQjtVQUNBSixhQUFhLENBQUN6QixLQUFkLEdBQW9CLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVksQ0FBWixFQUFjLENBQWQsRUFBZ0IsQ0FBaEIsQ0FBcEI7VUFDQXNCLFVBQVUsR0FBQyxDQUFYO1FBQ0gsQ0FSRCxNQVFLO1FBQ0RILGVBQWUsQ0FBQ00sS0FBaEIsR0FBc0IsQ0FBdEI7UUFDQU4sZUFBZSxDQUFDdkIsS0FBaEIsR0FBc0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFaLEVBQWMsQ0FBZCxFQUFnQixDQUFoQixDQUF0QjtRQUVBcUIsYUFBYSxDQUFDSSxLQUFkLEdBQW9CLENBQXBCO1FBQ0FKLGFBQWEsQ0FBQ3pCLEtBQWQsR0FBb0IsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWSxDQUFDLENBQWIsRUFBZSxDQUFmLEVBQWlCLENBQWpCLENBQXBCO1FBQ0FzQixVQUFVLEdBQUMsQ0FBQyxDQUFaO01BQ0g7SUFFSjs7SUFDRCxJQUFJSSxTQUFTLEdBQUd0QixJQUFJLENBQUNwRCxPQUFMLENBQWEyRSxvQkFBYixDQUFrQ2pCLE1BQWxDLENBQWhCO0lBQ0EsSUFBSWtCLFVBQVUsR0FBR3hCLElBQUksQ0FBQ3BELE9BQUwsQ0FBYTJFLG9CQUFiLENBQWtDbEIsUUFBbEMsQ0FBakI7SUFDQVksYUFBYSxDQUFDeEIsV0FBZCxDQUEwQjZCLFNBQVMsQ0FBQ3pCLENBQXBDLEVBQXVDeUIsU0FBUyxDQUFDeEIsQ0FBakQsRUFBbUQsQ0FBbkQ7SUFDQWlCLGVBQWUsQ0FBQ3RCLFdBQWhCLENBQTRCK0IsVUFBVSxDQUFDM0IsQ0FBdkMsRUFBMEMyQixVQUFVLENBQUMxQixDQUFyRCxFQUF1RCxDQUF2RDtJQUNBdEQsRUFBRSxDQUFDaUYsS0FBSCxDQUFTUixhQUFULEVBQ0tTLEVBREwsQ0FDUSxHQURSLEVBQ1k7TUFBQ2xDLEtBQUssRUFBQzBCLFVBQVA7TUFBa0JTLE9BQU8sRUFBQztJQUExQixDQURaLEVBRUtDLElBRkwsQ0FFVSxZQUFJO01BQ05YLGFBQWEsQ0FBQ2pDLE9BQWQ7SUFDSCxDQUpMLEVBS0s2QyxLQUxMO0lBT0FyRixFQUFFLENBQUNpRixLQUFILENBQVNWLGVBQVQsRUFDS1csRUFETCxDQUNRLEdBRFIsRUFDYTtNQUFDbEMsS0FBSyxFQUFDLENBQUMwQixVQUFSO01BQW1CUyxPQUFPLEVBQUM7SUFBM0IsQ0FEYixFQUVLQyxJQUZMLENBRVUsWUFBTTtNQUNSYixlQUFlLENBQUMvQixPQUFoQjtJQUNILENBSkwsRUFLSzZDLEtBTEw7RUFNSCxDQTdISTtFQThITGYsWUE5SEssd0JBOEhRUCxNQTlIUixFQThIZTtJQUNoQixRQUFRQSxNQUFSO01BQ0ksS0FBSyxDQUFMO1FBQ0ksT0FBTyxTQUFQOztNQUNKLEtBQUssQ0FBTDtRQUNJLE9BQU8sWUFBUDs7TUFDSixLQUFLLENBQUw7UUFDSSxPQUFPLFVBQVA7O01BQ0osS0FBSyxDQUFMO1FBQ0ksT0FBTyxXQUFQO0lBUlI7RUFVSCxDQXpJSTtFQTBJTG5DLFVBMUlLLHdCQTBJTztJQUNSMEQsbUJBQUEsQ0FBUUMsUUFBUixHQUFpQixLQUFqQjtJQUNBdkYsRUFBRSxDQUFDNkIsUUFBSCxDQUFZMkQsU0FBWixDQUFzQixRQUF0QjtFQUNILENBN0lJO0VBK0lMSCxLQS9JSyxtQkErSUc7SUFDSixJQUFJN0IsSUFBSSxHQUFHLElBQVg7SUFDQWlDLENBQUMsQ0FBQ0MsS0FBRixHQUZJLENBR0o7O0lBQ0EsS0FBS3RGLE9BQUwsR0FBZUosRUFBRSxDQUFDMkYsSUFBSCxDQUFRLGtCQUFSLENBQWY7SUFDQSxLQUFLdkYsT0FBTCxDQUFhd0YsaUJBQWIsR0FMSSxDQU1KOztJQUNBLElBQUkxQixLQUFLLEdBQUdDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFaOztJQUNBLElBQUlRLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLElBQWlCLENBQXJCLEVBQXdCO01BQ3BCO01BQ0E7TUFDQUEsS0FBSyxHQUFHLENBQVI7TUFDQUMsT0FBTyxDQUFDMEIsUUFBUixDQUFpQixPQUFqQixFQUEwQixDQUExQjtNQUNBLEtBQUtqRSxVQUFMO01BQ0E7SUFDSDs7SUFDRCxJQUFHZ0QsTUFBTSxDQUFDVixLQUFELENBQU4sR0FBYyxDQUFkLElBQWlCLENBQXBCLEVBQXNCO01BQ2xCLEtBQUtqRCxNQUFMLENBQVlnQyxXQUFaLENBQXdCLE9BQXhCLEVBQWlDLEtBQUtoQyxNQUFMLENBQVlxQixRQUFaLENBQXFCZ0IsQ0FBckIsR0FBeUJnQyxtQkFBQSxDQUFRUSxhQUFsRSxFQUFpRixDQUFqRjtNQUNBLEtBQUs1RSxNQUFMLENBQVkrQixXQUFaLENBQXdCLENBQUMsT0FBekIsRUFBa0MsS0FBSy9CLE1BQUwsQ0FBWW9CLFFBQVosQ0FBcUJnQixDQUFyQixHQUF5QmdDLG1CQUFBLENBQVFRLGFBQW5FLEVBQWtGLENBQWxGO0lBQ0g7O0lBQ0QsSUFBR2xCLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLEdBQWMsQ0FBZCxJQUFpQixDQUFwQixFQUFzQjtNQUNsQixLQUFLakQsTUFBTCxDQUFZZ0MsV0FBWixDQUF3QixHQUF4QixFQUE2QixNQUFNcUMsbUJBQUEsQ0FBUVEsYUFBM0MsRUFBMEQsQ0FBMUQ7TUFDQSxLQUFLN0UsTUFBTCxDQUFZNEQsS0FBWixHQUFvQixDQUFwQjtNQUNBLEtBQUszRCxNQUFMLENBQVkrQixXQUFaLENBQXdCLENBQUMsR0FBekIsRUFBOEIsQ0FBQyxDQUFELEdBQUtxQyxtQkFBQSxDQUFRUSxhQUEzQyxFQUEwRCxDQUExRDtNQUNBLEtBQUs1RSxNQUFMLENBQVkyRCxLQUFaLEdBQW9CLEdBQXBCO0lBQ0gsQ0F6QkcsQ0EwQko7SUFDQTtJQUNBO0lBQ0E7SUFDQTs7O0lBRUE3RSxFQUFFLENBQUN5QyxTQUFILENBQWFDLElBQWIsQ0FBa0IsV0FBV3dCLEtBQTdCLEVBQW9DLFVBQVV2QixHQUFWLEVBQWVvRCxTQUFmLEVBQTBCO01BQzFELElBQUlDLGVBQWUsR0FBR0QsU0FBUyxDQUFDRSxJQUFoQztNQUNBUixDQUFDLENBQUNTLFNBQUYsR0FBY0YsZUFBZCxDQUYwRCxDQUcxRDs7TUFDQXhDLElBQUksQ0FBQzJDLFNBQUwsQ0FBZUgsZUFBZSxDQUFDSSxLQUEvQixFQUFxQ3hCLE1BQU0sQ0FBQ1YsS0FBRCxDQUFOLEdBQWMsQ0FBbkQ7SUFDSCxDQUxELEVBaENJLENBc0NKO0lBQ0E7RUFFSCxDQXhMSTtFQXlMTDtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBaUMsU0FoTUsscUJBZ01LQyxLQWhNTCxFQWdNV2xDLEtBaE1YLEVBZ01rQjtJQUNuQixJQUFJVixJQUFJLEdBQUcsSUFBWCxDQURtQixDQUVuQjs7SUFDQSxLQUFLLElBQUk2QyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRCxLQUFLLENBQUNFLE1BQTFCLEVBQWtDRCxDQUFDLEVBQW5DLEVBQXVDO01BQ25DLElBQUlFLFFBQVEsR0FBRyxLQUFLL0IsV0FBTCxDQUFpQjRCLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNoQyxLQUExQixDQUFmO01BQ0FiLElBQUksQ0FBQ3BELE9BQUwsQ0FBYTJDLFFBQWIsQ0FBc0J3RCxRQUF0QjtNQUNBLElBQUlDLFVBQVUsR0FBQyxFQUFmOztNQUNBLElBQUd0QyxLQUFLLElBQUUsQ0FBVixFQUFZO1FBQ1JzQyxVQUFVLEdBQUMsRUFBWDtRQUNBRCxRQUFRLENBQUN0RCxXQUFULENBQXFCbUQsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBU0ksR0FBVCxDQUFhLENBQWIsSUFBaUJMLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNJLEdBQVQsQ0FBYSxDQUFiLElBQWtCLEtBQXhELEVBQWdFTCxLQUFLLENBQUNDLENBQUQsQ0FBTCxDQUFTSSxHQUFULENBQWEsQ0FBYixJQUFpQkwsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBU0ksR0FBVCxDQUFhLENBQWIsSUFBa0IsS0FBbkMsR0FBNENuQixtQkFBQSxDQUFRUSxhQUFwRCxJQUFvRU0sS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBUzFCLEtBQVQsSUFBZ0IsQ0FBaEIsR0FBa0I2QixVQUFsQixHQUE2QixDQUFDQSxVQUFsRyxDQUFoRTtNQUNILENBSEQsTUFHTSxJQUFHdEMsS0FBSyxJQUFFLENBQVYsRUFBWTtRQUNkc0MsVUFBVSxHQUFDLENBQUMsQ0FBWjtRQUNBRCxRQUFRLENBQUN0RCxXQUFULENBQXFCbUQsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBU0ksR0FBVCxDQUFhLENBQWIsQ0FBckIsRUFBc0NMLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNJLEdBQVQsQ0FBYSxDQUFiLENBQXRDO01BQ0g7O01BQ0RGLFFBQVEsQ0FBQzFCLEtBQVQsR0FBZXVCLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNLLEdBQXhCOztNQUNBLElBQUd4QyxLQUFLLElBQUUsQ0FBVixFQUFZO1FBQ1JxQyxRQUFRLENBQUN2RCxLQUFULEdBQWUsSUFBSWhELEVBQUUsQ0FBQ29ELElBQVAsQ0FBWWdELEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFULEdBQWlCb0QsS0FBSyxDQUFDQyxDQUFELENBQUwsQ0FBUzFCLEtBQTFCLEdBQWtDVyxtQkFBQSxDQUFRcUIsU0FBdEQsRUFBZ0VQLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFULEdBQWlCc0MsbUJBQUEsQ0FBUXFCLFNBQXpGLEVBQW1HUCxLQUFLLENBQUNDLENBQUQsQ0FBTCxDQUFTckQsS0FBNUcsQ0FBZjtNQUNILENBRkQsTUFFTSxJQUFHa0IsS0FBSyxJQUFFLENBQVYsRUFBWTtRQUNkcUMsUUFBUSxDQUFDdkQsS0FBVCxHQUFlLElBQUloRCxFQUFFLENBQUNvRCxJQUFQLENBQVlnRCxLQUFLLENBQUNDLENBQUQsQ0FBTCxDQUFTckQsS0FBVCxHQUFpQm9ELEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVMxQixLQUF0QyxFQUE0Q3lCLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFyRCxFQUEyRG9ELEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLENBQVNyRCxLQUFwRSxDQUFmO01BQ0g7O01BQ0R5QyxDQUFDLENBQUNtQixRQUFGLENBQVdDLElBQVgsQ0FBZ0JOLFFBQWhCO01BQ0FkLENBQUMsQ0FBQ3FCLGNBQUYsR0FBbUJyQixDQUFDLENBQUNtQixRQUFGLENBQVdHLE1BQVgsRUFBbkIsQ0FsQm1DLENBbUJuQztJQUNIO0VBQ0osQ0F4Tkk7RUF5Tkx2QyxXQXpOSyx1QkF5Tk9ILEtBek5QLEVBeU5jO0lBQ2YsUUFBUUEsS0FBUjtNQUNJLEtBQUssU0FBTDtRQUNJLElBQUlsQyxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3hDLE9BQXBCLENBQVg7UUFDQSxPQUFPNkIsSUFBUDs7TUFDSixLQUFLLFlBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3RDLFVBQXBCLENBQVg7UUFDQSxPQUFPMkIsSUFBUDs7TUFDSixLQUFLLFVBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS25DLFFBQXBCLENBQVg7UUFDQSxPQUFPd0IsSUFBUDs7TUFDSixLQUFLLFlBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3BDLFVBQXBCLENBQVg7UUFDQSxPQUFPeUIsSUFBUDs7TUFDSixLQUFLLFdBQUw7UUFDSSxJQUFJQSxJQUFJLEdBQUduQyxFQUFFLENBQUM4QyxXQUFILENBQWUsS0FBS3JDLFNBQXBCLENBQVg7UUFDQSxPQUFPMEIsSUFBUDs7TUFDSjtRQUNJNkUsT0FBTyxDQUFDQyxHQUFSLENBQVksTUFBWjtJQWpCUjtFQW1CSCxDQTdPSSxDQThPTDs7QUE5T0ssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcbmltcG9ydCBHYW1lTWdyIGZyb20gXCIuL0dhbWVNZ3JcIjtcclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBCR19Ob2RlOiBjYy5Ob2RlLFxyXG4gICAgICAgIHJlZGJhbGw6IGNjLlByZWZhYixcclxuICAgICAgICB5ZWxsb3diYWxsOiBjYy5QcmVmYWIsXHJcbiAgICAgICAgZ3JlZW5iYWxsOiBjYy5QcmVmYWIsXHJcbiAgICAgICAgcHVycGxlYmFsbDogY2MuUHJlZmFiLFxyXG4gICAgICAgIGJsdWViYWxsOiBjYy5QcmVmYWIsXHJcbiAgICAgICAgTGV2ZWw6IGNjLkxhYmVsLFxyXG4gICAgICAgIEJHTTogY2MuQXVkaW9DbGlwLFxyXG4gICAgICAgIGJhY2tNZW51OmNjLk5vZGUsXHJcbiAgICAgICAgemh1emkxOmNjLk5vZGUsXHJcbiAgICAgICAgemh1emkyOmNjLk5vZGUsXHJcbiAgICAgICAgX2Z4X2xpc3Q6W11cclxuICAgIH0sXHJcblxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIC8v5pKt5pS+YmdtXHJcbiAgICAgICAgdmFyIGJnbSA9IGNjLmF1ZGlvRW5naW5lLnBsYXlNdXNpYyh0aGlzLkJHTSwgdHJ1ZSk7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUuc2V0Vm9sdW1lKGJnbSwgMC4xNSk7XHJcbiAgICAgICAgdGhpcy5iYWNrTWVudS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9TVEFSVCx0aGlzLmJhY2tUb01lbnUsdGhpcyk7XHJcbiAgICAgICAgY2MuZGlyZWN0b3IuZ2V0Q29sbGlzaW9uTWFuYWdlcigpLmVuYWJsZWQ9dHJ1ZTtcclxuXHJcbiAgICAgICAgY2MuZ2FtZS5vbihcInNob3dfYmFsbF9tYXNrXCIsdGhpcy5zaG93TWFzayx0aGlzKTtcclxuICAgICAgICBjYy5nYW1lLm9uKFwiZ2FtZW92ZXJcIix0aGlzLk9uR2FtZW92ZXIsdGhpcyk7XHJcbiAgICB9LFxyXG5cclxuICAgIE9uR2FtZW92ZXIobm9kZSxwYXJlbnQpe1xyXG4gICAgICAgIGxldCBuZXdQb3MgPSBub2RlLnBvc2l0aW9uO1xyXG4gICAgICAgIGNjLmdhbWUub2ZmKFwiZ2FtZW92ZXJcIik7XHJcbiAgICAgICAgbm9kZS5kZXN0cm95KCk7XHJcbiAgICAgICAgY2MucmVzb3VyY2VzLmxvYWQoXCJwZXJmYWIvY29sX2Z4XCIsY2MuUHJlZmFiLChlcnIsZngpPT57XHJcbiAgICAgICAgICAgIGxldCBuZXdGeD1jYy5pbnN0YW50aWF0ZShmeCk7XHJcbiAgICAgICAgICAgIHBhcmVudC5hZGRDaGlsZChuZXdGeCk7XHJcbiAgICAgICAgICAgIG5ld0Z4LnNjYWxlPTAuNTtcclxuICAgICAgICAgICAgbmV3Rnguc2V0UG9zaXRpb24obmV3UG9zKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKT0+e1xyXG4gICAgICAgICAgICB0aGlzLnNob3dXcm9uZ0ljb24obmV3IGNjLlZlYzMobmV3UG9zLngsbmV3UG9zLnkgLSA1MCwwKSxwYXJlbnQpO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKT0+e1xyXG4gICAgICAgICAgICAgICAgdGhpcy5iYWNrVG9NZW51KCk7XHJcbiAgICAgICAgICAgIH0sMik7XHJcbiAgICAgICAgfSwyKTtcclxuICAgICAgICBcclxuICAgIH0sXHJcbiAgICBzaG93V3JvbmdJY29uKHBvcyxwYXJlbnQpe1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBjYy5yZXNvdXJjZXMubG9hZChcInBlcmZhYi93cm9uZ1wiLCBmdW5jdGlvbiAoZXJyLCBwcmVmYWIpIHtcclxuICAgICAgICAgICAgbGV0IG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xyXG4gICAgICAgICAgICBuZXdOb2RlLnBhcmVudCA9IHBhcmVudDtcclxuICAgICAgICAgICAgbmV3Tm9kZS5zZXRQb3NpdGlvbihwb3MpO1xyXG4gICAgICAgICAgICBuZXdOb2RlLnNjYWxlID0xLjU7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgIG5ld05vZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBuZXdOb2RlLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfSwyMDAwKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIHNob3dNYXNrKHN0YXJ0UG9zLGVuZFBvcyxiYWxsSWQsc3RhcnRCYWxsLGVuZEJhbGwpe1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBsZXQgbGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgIGxldCBjb2xvciA9IHRoaXMuZ2V0SURCeUNvbG9yKGJhbGxJZCk7XHJcbiAgICAgICAgbGV0IHN0YXJ0UG9zTmV3QmFsbCA9IHRoaXMubG9hZF9wcmVmYWIoY29sb3IpO1xyXG4gICAgICAgIGxldCBlbmRQb3NOZXdCYWxsID0gdGhpcy5sb2FkX3ByZWZhYihjb2xvcik7XHJcbiAgICAgICAgc2VsZi5CR19Ob2RlLmFkZENoaWxkKGVuZFBvc05ld0JhbGwpO1xyXG4gICAgICAgIHNlbGYuQkdfTm9kZS5hZGRDaGlsZChzdGFydFBvc05ld0JhbGwpO1xyXG4gICAgICAgIHZhciBzY2FsZVZhbHVlID0gMTtcclxuICAgICAgICB2YXIgZmxpcFggPSAxO1xyXG4gICAgICAgIGlmKE51bWJlcihsZXZlbCkrMT09MSl7XHJcbiAgICAgICAgICAgIGlmKHN0YXJ0UG9zLnkgPCBlbmRQb3MueSl7Ly/ku47kuIvlvoDkuIrnlLvnur9cclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0zNTtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5zY2FsZT1uZXcgY2MuVmVjMygtMS42LDEuNiwxLjYpO1xyXG5cclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuYW5nbGU9MzU7XHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKDEuNiwxLjYsMS42KTtcclxuICAgICAgICAgICAgICAgIHNjYWxlVmFsdWU9MztcclxuICAgICAgICAgICAgfWVsc2V7Ly/ku47kuIrlvoDkuIvnlLvnur9cclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0zNTtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5zY2FsZT1uZXcgY2MuVmVjMygxLjYsMS42LDEuNik7XHJcblxyXG4gICAgICAgICAgICAgICAgZW5kUG9zTmV3QmFsbC5hbmdsZT0zNTtcclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuc2NhbGU9bmV3IGNjLlZlYzMoLTEuNiwxLjYsMS42KTtcclxuICAgICAgICAgICAgICAgIHNjYWxlVmFsdWU9LTM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfWVsc2UgaWYoTnVtYmVyKGxldmVsKSsxPT0yKXtcclxuICAgICAgICAgICAgaWYoc3RhcnRQb3MueSA8IGVuZFBvcy55KS8v5LuO5LiL5b6A5LiK55S757q/XHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0wO1xyXG4gICAgICAgICAgICAgICAgc3RhcnRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKC0xLDEsMSk7XHJcbiAgICBcclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuYW5nbGU9MDtcclxuICAgICAgICAgICAgICAgIGVuZFBvc05ld0JhbGwuc2NhbGU9bmV3IGNjLlZlYzMoMSwxLDEpO1xyXG4gICAgICAgICAgICAgICAgc2NhbGVWYWx1ZT0yO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5hbmdsZT0wO1xyXG4gICAgICAgICAgICAgICAgc3RhcnRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKDEsMSwxKTtcclxuXHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLmFuZ2xlPTA7XHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLnNjYWxlPW5ldyBjYy5WZWMzKC0xLDEsMSk7XHJcbiAgICAgICAgICAgICAgICBzY2FsZVZhbHVlPS0yO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgZmluYWxfcG9zID0gc2VsZi5CR19Ob2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKGVuZFBvcyk7XHJcbiAgICAgICAgbGV0IGZpbmFsX3BvczEgPSBzZWxmLkJHX05vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoc3RhcnRQb3MpO1xyXG4gICAgICAgIGVuZFBvc05ld0JhbGwuc2V0UG9zaXRpb24oZmluYWxfcG9zLngsIGZpbmFsX3Bvcy55LDApO1xyXG4gICAgICAgIHN0YXJ0UG9zTmV3QmFsbC5zZXRQb3NpdGlvbihmaW5hbF9wb3MxLngsIGZpbmFsX3BvczEueSwwKTtcclxuICAgICAgICBjYy50d2VlbihlbmRQb3NOZXdCYWxsKVxyXG4gICAgICAgICAgICAudG8oMC4zLHtzY2FsZTpzY2FsZVZhbHVlLG9wYWNpdHk6MH0pXHJcbiAgICAgICAgICAgIC5jYWxsKCgpPT57XHJcbiAgICAgICAgICAgICAgICBlbmRQb3NOZXdCYWxsLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnN0YXJ0KClcclxuXHJcbiAgICAgICAgY2MudHdlZW4oc3RhcnRQb3NOZXdCYWxsKVxyXG4gICAgICAgICAgICAudG8oMC4zLCB7c2NhbGU6LXNjYWxlVmFsdWUsb3BhY2l0eTowfSlcclxuICAgICAgICAgICAgLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc3RhcnRQb3NOZXdCYWxsLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnN0YXJ0KClcclxuICAgIH0sXHJcbiAgICBnZXRJREJ5Q29sb3IoYmFsbElkKXtcclxuICAgICAgICBzd2l0Y2ggKGJhbGxJZCkge1xyXG4gICAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJyZWRiYWxsXCI7XHJcbiAgICAgICAgICAgIGNhc2UgMTpcclxuICAgICAgICAgICAgICAgIHJldHVybiBcInllbGxvd2JhbGxcIjtcclxuICAgICAgICAgICAgY2FzZSAyOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiYmx1ZWJhbGxcIjtcclxuICAgICAgICAgICAgY2FzZSAzOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiZ3JlZW5iYWxsXCI7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGJhY2tUb01lbnUoKXtcclxuICAgICAgICBHYW1lTWdyLmdhbWVPdmVyPWZhbHNlO1xyXG4gICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgnbGF1bmNoJyk7XHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICBHLnJlc2V0KCk7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ+a4heepukcnKTtcclxuICAgICAgICB0aGlzLkJHX05vZGUgPSBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0JHJyk7XHJcbiAgICAgICAgdGhpcy5CR19Ob2RlLnJlbW92ZUFsbENoaWxkcmVuKCk7XHJcbiAgICAgICAgLy/or7vlj5blvZPliY3mmK/lk6rkuIDkuKrlhbPljaFcclxuICAgICAgICBsZXQgbGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgIGlmIChOdW1iZXIobGV2ZWwpID49IDIpIHtcclxuICAgICAgICAgICAgLy/lhajpg6jpgJrlhbNcclxuICAgICAgICAgICAgLy9jYy5maW5kKCdDYW52YXMvVElQUy/lhajpg6jpgJrlhbMnKS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICBsZXZlbCA9IDA7XHJcbiAgICAgICAgICAgIFN0b3JhZ2UuU2V0X0luZm8oJ2xldmVsJywgMCk7XHJcbiAgICAgICAgICAgIHRoaXMuYmFja1RvTWVudSgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKE51bWJlcihsZXZlbCkrMT09MSl7XHJcbiAgICAgICAgICAgIHRoaXMuemh1emkxLnNldFBvc2l0aW9uKDQ0Ni45MjQsIHRoaXMuemh1emkxLnBvc2l0aW9uLnkgLSBHYW1lTWdyLkdsb2JhbE9mZnNldFksIDApO1xyXG4gICAgICAgICAgICB0aGlzLnpodXppMi5zZXRQb3NpdGlvbigtMzk4LjkyNCwgdGhpcy56aHV6aTIucG9zaXRpb24ueSAtIEdhbWVNZ3IuR2xvYmFsT2Zmc2V0WSwgMCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKE51bWJlcihsZXZlbCkrMT09Mil7XHJcbiAgICAgICAgICAgIHRoaXMuemh1emkxLnNldFBvc2l0aW9uKDQ2NSwgMzg0IC0gR2FtZU1nci5HbG9iYWxPZmZzZXRZLCAwKTtcclxuICAgICAgICAgICAgdGhpcy56aHV6aTEuYW5nbGUgPSAwO1xyXG4gICAgICAgICAgICB0aGlzLnpodXppMi5zZXRQb3NpdGlvbigtNDU1LCAtNiAtIEdhbWVNZ3IuR2xvYmFsT2Zmc2V0WSwgMCk7XHJcbiAgICAgICAgICAgIHRoaXMuemh1emkyLmFuZ2xlID0gMTgwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBpZiAoY2Muc3lzLnBsYXRmb3JtID09PSBjYy5zeXMuQU5EUk9JRCkge1xyXG4gICAgICAgIC8vICAgICBzZWxmLkxldmVsLnN0cmluZyA9ICdMZXZlbCAnICsgKE51bWJlcihsZXZlbCkgKyAxKTtcclxuICAgICAgICAvLyB9IGVsc2Uge1xyXG4gICAgICAgIC8vICAgICBzZWxmLkxldmVsLnN0cmluZyA9ICflhbPljaEgJyArIChOdW1iZXIobGV2ZWwpICsgMSk7XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICBjYy5yZXNvdXJjZXMubG9hZCgnbGV2ZWwvJyArIGxldmVsLCBmdW5jdGlvbiAoZXJyLCBqc29uQXNzZXQpIHtcclxuICAgICAgICAgICAgbGV0IGxldmVsX2pzb25faW5mbyA9IGpzb25Bc3NldC5qc29uO1xyXG4gICAgICAgICAgICBHLkxldmVsSnNvbiA9IGxldmVsX2pzb25faW5mbztcclxuICAgICAgICAgICAgLy9zZWxmLmxvYWRiZyhsZXZlbF9qc29uX2luZm8uYmcpO1xyXG4gICAgICAgICAgICBzZWxmLmxvYWRiYWxscyhsZXZlbF9qc29uX2luZm8uYmFsbHMsTnVtYmVyKGxldmVsKSsxKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyBHLkFuYWx5c3RpY0xvZ2luKCk7XHJcbiAgICAgICAgLy8gRy53eHRvcHNoYXJlKCk7XHJcblxyXG4gICAgfSxcclxuICAgIC8vIGxvYWRiZyhzdHIpIHtcclxuICAgIC8vICAgICAvLyBjb25zb2xlLmxvZyhzdHIpO1xyXG4gICAgLy8gICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIC8vICAgICBjYy5yZXNvdXJjZXMubG9hZChcIkJHL1wiICsgc3RyLCBjYy5TcHJpdGVGcmFtZSwgZnVuY3Rpb24gKGVyciwgc3ByaXRlRnJhbWUpIHtcclxuICAgIC8vICAgICAgICAgc2VsZi5CR19Ob2RlLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gc3ByaXRlRnJhbWU7XHJcbiAgICAvLyAgICAgfSk7XHJcbiAgICAvLyB9LFxyXG4gICAgbG9hZGJhbGxzKGJhbGxzLGxldmVsKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIC8vIEcuc3Rva2VsaXN0ID0gYmFsbHMubGVuZ3RoO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYmFsbHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IG5ld25vZGUxID0gdGhpcy5sb2FkX3ByZWZhYihiYWxsc1tpXS5jb2xvcik7XHJcbiAgICAgICAgICAgIHNlbGYuQkdfTm9kZS5hZGRDaGlsZChuZXdub2RlMSk7XHJcbiAgICAgICAgICAgIGxldCBiYWxsT2Zmc2V0PTEwO1xyXG4gICAgICAgICAgICBpZihsZXZlbD09MSl7XHJcbiAgICAgICAgICAgICAgICBiYWxsT2Zmc2V0PTEwXHJcbiAgICAgICAgICAgICAgICBuZXdub2RlMS5zZXRQb3NpdGlvbihiYWxsc1tpXS5Qb3NbMF0rKGJhbGxzW2ldLlBvc1swXSAqIDAuMTk0KSwgYmFsbHNbaV0uUG9zWzFdLShiYWxsc1tpXS5Qb3NbMF0gKiAwLjE5NCkgLSBHYW1lTWdyLkdsb2JhbE9mZnNldFkgLShiYWxsc1tpXS5mbGlwWD09MT9iYWxsT2Zmc2V0Oi1iYWxsT2Zmc2V0KSk7XHJcbiAgICAgICAgICAgIH1lbHNlIGlmKGxldmVsPT0yKXtcclxuICAgICAgICAgICAgICAgIGJhbGxPZmZzZXQ9LTg7XHJcbiAgICAgICAgICAgICAgICBuZXdub2RlMS5zZXRQb3NpdGlvbihiYWxsc1tpXS5Qb3NbMF0sIGJhbGxzW2ldLlBvc1sxXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbmV3bm9kZTEuYW5nbGU9YmFsbHNbaV0uUm90O1xyXG4gICAgICAgICAgICBpZihsZXZlbD09MSl7XHJcbiAgICAgICAgICAgICAgICBuZXdub2RlMS5zY2FsZT1uZXcgY2MuVmVjMyhiYWxsc1tpXS5zY2FsZSAqIGJhbGxzW2ldLmZsaXBYICogR2FtZU1nci50dWJlU2NhbGUsYmFsbHNbaV0uc2NhbGUgKiBHYW1lTWdyLnR1YmVTY2FsZSxiYWxsc1tpXS5zY2FsZSlcclxuICAgICAgICAgICAgfWVsc2UgaWYobGV2ZWw9PTIpe1xyXG4gICAgICAgICAgICAgICAgbmV3bm9kZTEuc2NhbGU9bmV3IGNjLlZlYzMoYmFsbHNbaV0uc2NhbGUgKiBiYWxsc1tpXS5mbGlwWCxiYWxsc1tpXS5zY2FsZSxiYWxsc1tpXS5zY2FsZSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBHLkJhbGxMaXN0LnB1c2gobmV3bm9kZTEpO1xyXG4gICAgICAgICAgICBHLm9yaWdpbkJhbGxMaXN0ID0gRy5CYWxsTGlzdC5jb25jYXQoKTtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2cobmV3bm9kZTEucG9zaXRpb24pXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGxvYWRfcHJlZmFiKGNvbG9yKSB7XHJcbiAgICAgICAgc3dpdGNoIChjb2xvcikge1xyXG4gICAgICAgICAgICBjYXNlIFwicmVkYmFsbFwiOlxyXG4gICAgICAgICAgICAgICAgdmFyIG5vZGUgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnJlZGJhbGwpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5vZGU7XHJcbiAgICAgICAgICAgIGNhc2UgXCJ5ZWxsb3diYWxsXCI6XHJcbiAgICAgICAgICAgICAgICB2YXIgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMueWVsbG93YmFsbCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbm9kZTtcclxuICAgICAgICAgICAgY2FzZSBcImJsdWViYWxsXCI6XHJcbiAgICAgICAgICAgICAgICB2YXIgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMuYmx1ZWJhbGwpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5vZGU7XHJcbiAgICAgICAgICAgIGNhc2UgXCJwdXJwbGViYWxsXCI6XHJcbiAgICAgICAgICAgICAgICB2YXIgbm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMucHVycGxlYmFsbCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbm9kZTtcclxuICAgICAgICAgICAgY2FzZSBcImdyZWVuYmFsbFwiOlxyXG4gICAgICAgICAgICAgICAgdmFyIG5vZGUgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLmdyZWVuYmFsbCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbm9kZTtcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCfpopzoibLkuI3lr7knKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9LFxyXG59KTtcclxuXHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/SelectPanel.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '52b6e5B1OpDmqrDtECFVJbm', 'SelectPanel');
// scripts/SelectPanel.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var LevelBtn_1 = require("./LevelBtn");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SelectPanel = /** @class */ (function (_super) {
    __extends(SelectPanel, _super);
    function SelectPanel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.level_btn = null;
        _this.scrollView = null;
        return _this;
    }
    SelectPanel.prototype.onLoad = function () {
        this.node.getChildByName("splash").on(cc.Node.EventType.TOUCH_START, this.hideSelectPanel, this);
        this.generatorLevelBtn();
    };
    SelectPanel.prototype.generatorLevelBtn = function () {
        var content = this.scrollView.node.getChildByName("view").getChildByName("content");
        content.width = 644;
        // content.height = 22 * 80;
        // content.height = 22;
        for (var i = 1; i <= 2; i++) {
            var newLevel_btn = cc.instantiate(this.level_btn);
            newLevel_btn.setParent(this.scrollView.node.getChildByName("view").getChildByName("content"));
            newLevel_btn.setPosition(0, (i - 1) * -100);
            newLevel_btn.getComponent(LevelBtn_1.default).init(i);
        }
    };
    SelectPanel.prototype.hideSelectPanel = function () {
        this.node.active = false;
    };
    __decorate([
        property({ type: cc.Prefab })
    ], SelectPanel.prototype, "level_btn", void 0);
    __decorate([
        property({ type: cc.ScrollView })
    ], SelectPanel.prototype, "scrollView", void 0);
    SelectPanel = __decorate([
        ccclass
    ], SelectPanel);
    return SelectPanel;
}(cc.Component));
exports.default = SelectPanel;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU2VsZWN0UGFuZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQiw0RUFBNEU7QUFDNUUsbUJBQW1CO0FBQ25CLHNGQUFzRjtBQUN0Riw4QkFBOEI7QUFDOUIsc0ZBQXNGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFdEYsdUNBQWtDO0FBRTVCLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXlDLCtCQUFZO0lBQXJEO1FBQUEscUVBaUNDO1FBOUJHLGVBQVMsR0FBYyxJQUFJLENBQUM7UUFHNUIsZ0JBQVUsR0FBa0IsSUFBSSxDQUFDOztJQTJCckMsQ0FBQztJQXRCYSw0QkFBTSxHQUFoQjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUMsSUFBSSxDQUFDLGVBQWUsRUFBQyxJQUFJLENBQUMsQ0FBQztRQUUvRixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsdUNBQWlCLEdBQWpCO1FBQ0ksSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNwRixPQUFPLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUNwQiw0QkFBNEI7UUFDNUIsdUJBQXVCO1FBQ3ZCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDekIsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbEQsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDOUYsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QyxZQUFZLENBQUMsWUFBWSxDQUFDLGtCQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDL0M7SUFDTCxDQUFDO0lBRUQscUNBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFDLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBN0JEO1FBREMsUUFBUSxDQUFDLEVBQUMsSUFBSSxFQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUMsQ0FBQztrREFDQztJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFDLElBQUksRUFBQyxFQUFFLENBQUMsVUFBVSxFQUFDLENBQUM7bURBQ0U7SUFOaEIsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQWlDL0I7SUFBRCxrQkFBQztDQWpDRCxBQWlDQyxDQWpDd0MsRUFBRSxDQUFDLFNBQVMsR0FpQ3BEO2tCQWpDb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgTGV2ZWxCdG4gZnJvbSBcIi4vTGV2ZWxCdG5cIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTZWxlY3RQYW5lbCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoe3R5cGU6Y2MuUHJlZmFifSlcbiAgICBsZXZlbF9idG46IGNjLlByZWZhYiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoe3R5cGU6Y2MuU2Nyb2xsVmlld30pXG4gICAgc2Nyb2xsVmlldzogY2MuU2Nyb2xsVmlldyA9IG51bGw7XG5cblxuICAgIGNmZ0FycjpBcnJheTxudW1iZXI+XG5cbiAgICBwcm90ZWN0ZWQgb25Mb2FkKCk6IHZvaWQge1xuICAgICAgICB0aGlzLm5vZGUuZ2V0Q2hpbGRCeU5hbWUoXCJzcGxhc2hcIikub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsdGhpcy5oaWRlU2VsZWN0UGFuZWwsdGhpcyk7XG5cbiAgICAgICAgdGhpcy5nZW5lcmF0b3JMZXZlbEJ0bigpO1xuICAgIH1cblxuICAgIGdlbmVyYXRvckxldmVsQnRuKCkge1xuICAgICAgICBsZXQgY29udGVudCA9IHRoaXMuc2Nyb2xsVmlldy5ub2RlLmdldENoaWxkQnlOYW1lKFwidmlld1wiKS5nZXRDaGlsZEJ5TmFtZShcImNvbnRlbnRcIik7XG4gICAgICAgIGNvbnRlbnQud2lkdGggPSA2NDQ7XG4gICAgICAgIC8vIGNvbnRlbnQuaGVpZ2h0ID0gMjIgKiA4MDtcbiAgICAgICAgLy8gY29udGVudC5oZWlnaHQgPSAyMjtcbiAgICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gMjsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgbmV3TGV2ZWxfYnRuID0gY2MuaW5zdGFudGlhdGUodGhpcy5sZXZlbF9idG4pO1xuICAgICAgICAgICAgbmV3TGV2ZWxfYnRuLnNldFBhcmVudCh0aGlzLnNjcm9sbFZpZXcubm9kZS5nZXRDaGlsZEJ5TmFtZShcInZpZXdcIikuZ2V0Q2hpbGRCeU5hbWUoXCJjb250ZW50XCIpKTtcbiAgICAgICAgICAgIG5ld0xldmVsX2J0bi5zZXRQb3NpdGlvbigwLChpLTEpKi0xMDApO1xuICAgICAgICAgICAgbmV3TGV2ZWxfYnRuLmdldENvbXBvbmVudChMZXZlbEJ0bikuaW5pdChpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGhpZGVTZWxlY3RQYW5lbCgpe1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlPWZhbHNlO1xuICAgIH1cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/newuser_guide.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2c272vE7ptCJp9ujU5sPYLd', 'newuser_guide');
// scripts/newuser_guide.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    Guide: cc.Node,
    level: "0"
  },
  // LIFE-CYCLE CALLBACKS:
  start: function start() {
    // 判断是不是第一关，是的话，加个提示的手势
    this.level = Storage.Get_Info('level');

    if (this.level == "0") {
      this.showguidelist();
    } else {
      this.Guide.active = false;
    }
  },
  showguidelist: function showguidelist() {
    this.Guide.active = true; //用户还没画
    // this.Guide.setPosition(cc.v2(0, 240))

    cc.tween(this.Guide).to(0.01, {
      position: cc.v2(0, 240)
    }).bezierTo(2, cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200)).union().repeatForever().start();
  },
  close: function close() {
    this.level = Storage.Get_Info('level');

    if (this.level != "0") {
      return;
    }

    cc.tween(this.Guide).stopAll;
    this.Guide.active = false;
  },
  showguidelist_again: function showguidelist_again() {
    this.level = Storage.Get_Info('level');

    if (this.level != "0") {
      return;
    }

    this.Guide.active = true;

    if (G.BallList.length > 2) {
      //用户还没画
      cc.tween(this.Guide).to(0.01, {
        position: cc.v2(0, 240)
      }).bezierTo(2, cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200)).union().repeatForever().start();
    } else {
      //用户画了，要判断画的哪条
      if (G.BallList[0]._name == "redball") {
        //hard code hahaha
        cc.tween(this.Guide).to(0.01, {
          position: cc.v2(0, 240)
        }).bezierTo(2, cc.v2(0, 240), cc.v2(-500, 20), cc.v2(0, -200)).union().repeatForever().start();
      } else {
        cc.tween(this.Guide).to(0.01, {
          position: cc.v2(0, 0)
        }).bezierTo(2, cc.v2(0, 0), cc.v2(500, -225), cc.v2(0, -450)).union().repeatForever().start();
      }
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcbmV3dXNlcl9ndWlkZS5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsIkd1aWRlIiwiTm9kZSIsImxldmVsIiwic3RhcnQiLCJTdG9yYWdlIiwiR2V0X0luZm8iLCJzaG93Z3VpZGVsaXN0IiwiYWN0aXZlIiwidHdlZW4iLCJ0byIsInBvc2l0aW9uIiwidjIiLCJiZXppZXJUbyIsInVuaW9uIiwicmVwZWF0Rm9yZXZlciIsImNsb3NlIiwic3RvcEFsbCIsInNob3dndWlkZWxpc3RfYWdhaW4iLCJHIiwiQmFsbExpc3QiLCJsZW5ndGgiLCJfbmFtZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLEtBQUssRUFBQ0osRUFBRSxDQUFDSyxJQUREO0lBRVJDLEtBQUssRUFBQztFQUZFLENBSFA7RUFTTDtFQUVBQyxLQVhLLG1CQVdJO0lBQ0w7SUFDQSxLQUFLRCxLQUFMLEdBQWFFLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFiOztJQUNBLElBQUcsS0FBS0gsS0FBTCxJQUFjLEdBQWpCLEVBQXFCO01BQ2pCLEtBQUtJLGFBQUw7SUFDSCxDQUZELE1BRUs7TUFDRCxLQUFLTixLQUFMLENBQVdPLE1BQVgsR0FBb0IsS0FBcEI7SUFDSDtFQUNKLENBbkJJO0VBcUJMRCxhQXJCSywyQkFxQlk7SUFDYixLQUFLTixLQUFMLENBQVdPLE1BQVgsR0FBb0IsSUFBcEIsQ0FEYSxDQUViO0lBQ0E7O0lBQ0FYLEVBQUUsQ0FBQ1ksS0FBSCxDQUFTLEtBQUtSLEtBQWQsRUFDS1MsRUFETCxDQUNRLElBRFIsRUFDYTtNQUFDQyxRQUFRLEVBQUNkLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxHQUFUO0lBQVYsQ0FEYixFQUVLQyxRQUZMLENBRWMsQ0FGZCxFQUVnQmhCLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxHQUFULENBRmhCLEVBRStCZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFDLEdBQVAsRUFBWSxFQUFaLENBRi9CLEVBRWdEZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBQyxHQUFWLENBRmhELEVBR0tFLEtBSEwsR0FJS0MsYUFKTCxHQUtLWCxLQUxMO0VBTUgsQ0EvQkk7RUFnQ0xZLEtBaENLLG1CQWdDRTtJQUNILEtBQUtiLEtBQUwsR0FBYUUsT0FBTyxDQUFDQyxRQUFSLENBQWlCLE9BQWpCLENBQWI7O0lBQ0EsSUFBRyxLQUFLSCxLQUFMLElBQWMsR0FBakIsRUFBcUI7TUFDakI7SUFDSDs7SUFDRE4sRUFBRSxDQUFDWSxLQUFILENBQVMsS0FBS1IsS0FBZCxFQUFxQmdCLE9BQXJCO0lBQ0EsS0FBS2hCLEtBQUwsQ0FBV08sTUFBWCxHQUFvQixLQUFwQjtFQUNILENBdkNJO0VBeUNMVSxtQkF6Q0ssaUNBeUNrQjtJQUNuQixLQUFLZixLQUFMLEdBQWFFLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFiOztJQUNBLElBQUcsS0FBS0gsS0FBTCxJQUFjLEdBQWpCLEVBQXFCO01BQ2pCO0lBQ0g7O0lBQ0QsS0FBS0YsS0FBTCxDQUFXTyxNQUFYLEdBQW9CLElBQXBCOztJQUNBLElBQUdXLENBQUMsQ0FBQ0MsUUFBRixDQUFXQyxNQUFYLEdBQW1CLENBQXRCLEVBQXdCO01BQ3BCO01BQ0F4QixFQUFFLENBQUNZLEtBQUgsQ0FBUyxLQUFLUixLQUFkLEVBQ0NTLEVBREQsQ0FDSSxJQURKLEVBQ1M7UUFBQ0MsUUFBUSxFQUFDZCxFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsR0FBVDtNQUFWLENBRFQsRUFFQ0MsUUFGRCxDQUVVLENBRlYsRUFFWWhCLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxHQUFULENBRlosRUFFMkJmLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQUMsR0FBUCxFQUFZLEVBQVosQ0FGM0IsRUFFNENmLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFDLEdBQVYsQ0FGNUMsRUFHQ0UsS0FIRCxHQUlDQyxhQUpELEdBS0NYLEtBTEQ7SUFNSCxDQVJELE1BUUs7TUFDRDtNQUNBLElBQUdlLENBQUMsQ0FBQ0MsUUFBRixDQUFXLENBQVgsRUFBY0UsS0FBZCxJQUF1QixTQUExQixFQUFvQztRQUNoQztRQUNBekIsRUFBRSxDQUFDWSxLQUFILENBQVMsS0FBS1IsS0FBZCxFQUNDUyxFQURELENBQ0ksSUFESixFQUNTO1VBQUNDLFFBQVEsRUFBQ2QsRUFBRSxDQUFDZSxFQUFILENBQU0sQ0FBTixFQUFTLEdBQVQ7UUFBVixDQURULEVBRUNDLFFBRkQsQ0FFVSxDQUZWLEVBRVloQixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsR0FBVCxDQUZaLEVBRTJCZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFDLEdBQVAsRUFBWSxFQUFaLENBRjNCLEVBRTRDZixFQUFFLENBQUNlLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBQyxHQUFWLENBRjVDLEVBR0NFLEtBSEQsR0FJQ0MsYUFKRCxHQUtDWCxLQUxEO01BTUgsQ0FSRCxNQVFLO1FBQ0RQLEVBQUUsQ0FBQ1ksS0FBSCxDQUFTLEtBQUtSLEtBQWQsRUFDQ1MsRUFERCxDQUNJLElBREosRUFDUztVQUFDQyxRQUFRLEVBQUNkLEVBQUUsQ0FBQ2UsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFUO1FBQVYsQ0FEVCxFQUVDQyxRQUZELENBRVUsQ0FGVixFQUVZaEIsRUFBRSxDQUFDZSxFQUFILENBQU0sQ0FBTixFQUFTLENBQVQsQ0FGWixFQUV5QmYsRUFBRSxDQUFDZSxFQUFILENBQU0sR0FBTixFQUFXLENBQUMsR0FBWixDQUZ6QixFQUUyQ2YsRUFBRSxDQUFDZSxFQUFILENBQU0sQ0FBTixFQUFTLENBQUMsR0FBVixDQUYzQyxFQUdDRSxLQUhELEdBSUNDLGFBSkQsR0FLQ1gsS0FMRDtNQU1IO0lBQ0o7RUFDSjtBQTFFSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgR3VpZGU6Y2MuTm9kZSxcclxuICAgICAgICBsZXZlbDpcIjBcIixcclxuICAgICAgIFxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgLy8g5Yik5pat5piv5LiN5piv56ys5LiA5YWz77yM5piv55qE6K+d77yM5Yqg5Liq5o+Q56S655qE5omL5Yq/XHJcbiAgICAgICAgdGhpcy5sZXZlbCA9IFN0b3JhZ2UuR2V0X0luZm8oJ2xldmVsJyk7XHJcbiAgICAgICAgaWYodGhpcy5sZXZlbCA9PSBcIjBcIil7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd2d1aWRlbGlzdCgpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgc2hvd2d1aWRlbGlzdCAoKSB7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIC8v55So5oi36L+Y5rKh55S7XHJcbiAgICAgICAgLy8gdGhpcy5HdWlkZS5zZXRQb3NpdGlvbihjYy52MigwLCAyNDApKVxyXG4gICAgICAgIGNjLnR3ZWVuKHRoaXMuR3VpZGUpXHJcbiAgICAgICAgICAgIC50bygwLjAxLHtwb3NpdGlvbjpjYy52MigwLCAyNDApfSlcclxuICAgICAgICAgICAgLmJlemllclRvKDIsY2MudjIoMCwgMjQwKSwgY2MudjIoLTUwMCwgMjApLCBjYy52MigwLCAtMjAwKSlcclxuICAgICAgICAgICAgLnVuaW9uKClcclxuICAgICAgICAgICAgLnJlcGVhdEZvcmV2ZXIoKVxyXG4gICAgICAgICAgICAuc3RhcnQoKTtcclxuICAgIH0sXHJcbiAgICBjbG9zZSgpe1xyXG4gICAgICAgIHRoaXMubGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgIGlmKHRoaXMubGV2ZWwgIT0gXCIwXCIpe1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNjLnR3ZWVuKHRoaXMuR3VpZGUpLnN0b3BBbGw7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgIH0sXHJcblxyXG4gICAgc2hvd2d1aWRlbGlzdF9hZ2FpbiAoKSB7XHJcbiAgICAgICAgdGhpcy5sZXZlbCA9IFN0b3JhZ2UuR2V0X0luZm8oJ2xldmVsJyk7XHJcbiAgICAgICAgaWYodGhpcy5sZXZlbCAhPSBcIjBcIil7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIGlmKEcuQmFsbExpc3QubGVuZ3RoID4yKXtcclxuICAgICAgICAgICAgLy/nlKjmiLfov5jmsqHnlLtcclxuICAgICAgICAgICAgY2MudHdlZW4odGhpcy5HdWlkZSlcclxuICAgICAgICAgICAgLnRvKDAuMDEse3Bvc2l0aW9uOmNjLnYyKDAsIDI0MCl9KVxyXG4gICAgICAgICAgICAuYmV6aWVyVG8oMixjYy52MigwLCAyNDApLCBjYy52MigtNTAwLCAyMCksIGNjLnYyKDAsIC0yMDApKVxyXG4gICAgICAgICAgICAudW5pb24oKVxyXG4gICAgICAgICAgICAucmVwZWF0Rm9yZXZlcigpXHJcbiAgICAgICAgICAgIC5zdGFydCgpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAvL+eUqOaIt+eUu+S6hu+8jOimgeWIpOaWreeUu+eahOWTquadoVxyXG4gICAgICAgICAgICBpZihHLkJhbGxMaXN0WzBdLl9uYW1lID09IFwicmVkYmFsbFwiKXtcclxuICAgICAgICAgICAgICAgIC8vaGFyZCBjb2RlIGhhaGFoYVxyXG4gICAgICAgICAgICAgICAgY2MudHdlZW4odGhpcy5HdWlkZSlcclxuICAgICAgICAgICAgICAgIC50bygwLjAxLHtwb3NpdGlvbjpjYy52MigwLCAyNDApfSlcclxuICAgICAgICAgICAgICAgIC5iZXppZXJUbygyLGNjLnYyKDAsIDI0MCksIGNjLnYyKC01MDAsIDIwKSwgY2MudjIoMCwgLTIwMCkpXHJcbiAgICAgICAgICAgICAgICAudW5pb24oKVxyXG4gICAgICAgICAgICAgICAgLnJlcGVhdEZvcmV2ZXIoKVxyXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgY2MudHdlZW4odGhpcy5HdWlkZSlcclxuICAgICAgICAgICAgICAgIC50bygwLjAxLHtwb3NpdGlvbjpjYy52MigwLCAwKX0pXHJcbiAgICAgICAgICAgICAgICAuYmV6aWVyVG8oMixjYy52MigwLCAwKSwgY2MudjIoNTAwLCAtMjI1KSwgY2MudjIoMCwgLTQ1MCkpXHJcbiAgICAgICAgICAgICAgICAudW5pb24oKVxyXG4gICAgICAgICAgICAgICAgLnJlcGVhdEZvcmV2ZXIoKVxyXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG59KTtcclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/launch_scene.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'eb8b18ojodL0o38e4QBAcnM', 'launch_scene');
// scripts/launch_scene.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LaunchScene = /** @class */ (function (_super) {
    __extends(LaunchScene, _super);
    function LaunchScene() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.select_panel = null;
        _this.start_btn = null;
        return _this;
    }
    LaunchScene.prototype.onLoad = function () {
        this.start_btn.on(cc.Node.EventType.TOUCH_START, this.startGame, this);
    };
    LaunchScene.prototype.startGame = function () {
        this.select_panel.active = true;
    };
    LaunchScene.prototype.start = function () {
    };
    __decorate([
        property({ type: cc.Node })
    ], LaunchScene.prototype, "select_panel", void 0);
    __decorate([
        property({ type: cc.Node })
    ], LaunchScene.prototype, "start_btn", void 0);
    LaunchScene = __decorate([
        ccclass
    ], LaunchScene);
    return LaunchScene;
}(cc.Component));
exports.default = LaunchScene;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcbGF1bmNoX3NjZW5lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNNLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXlDLCtCQUFZO0lBQXJEO1FBQUEscUVBb0JDO1FBakJHLGtCQUFZLEdBQVksSUFBSSxDQUFDO1FBRzdCLGVBQVMsR0FBWSxJQUFJLENBQUM7O0lBYzlCLENBQUM7SUFaRyw0QkFBTSxHQUFOO1FBR0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUE7SUFDMUUsQ0FBQztJQUVELCtCQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDcEMsQ0FBQztJQUVELDJCQUFLLEdBQUw7SUFDQSxDQUFDO0lBaEJEO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztxREFDQztJQUc3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7a0RBQ0Y7SUFOVCxXQUFXO1FBRC9CLE9BQU87T0FDYSxXQUFXLENBb0IvQjtJQUFELGtCQUFDO0NBcEJELEFBb0JDLENBcEJ3QyxFQUFFLENBQUMsU0FBUyxHQW9CcEQ7a0JBcEJvQixXQUFXIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBMYXVuY2hTY2VuZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuTm9kZSB9KVxyXG4gICAgc2VsZWN0X3BhbmVsOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5Ob2RlIH0pXHJcbiAgICBzdGFydF9idG46IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIG9uTG9hZCgpIHtcclxuXHJcblxyXG4gICAgICAgIHRoaXMuc3RhcnRfYnRuLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX1NUQVJULCB0aGlzLnN0YXJ0R2FtZSwgdGhpcylcclxuICAgIH1cclxuXHJcbiAgICBzdGFydEdhbWUoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RfcGFuZWwuYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgIH1cclxufSJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/replay.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '45b59rn59JF1JEASzsa37Iw', 'replay');
// scripts/replay.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    ButtonAudio: cc.AudioClip
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {},
  start: function start() {
    var self = this;
    this.node.on('click', function (button) {
      //The event is a custom event, you could get the Button component via first argument

      /*通过模块化脚本操作
      var GameStart = require("GameStart");
      new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
      */

      /*通过找组件操作
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      */

      /*直接重新加载场景
      cc.director.loadScene("Home");
      */
      G.reset();
      Storage.Set_Info('level', "0");
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      cc.find('Canvas/TIPS/全部通关').active = false; //音效

      cc.audioEngine.playEffect(self.ButtonAudio, false);
    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xccmVwbGF5LmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiQnV0dG9uQXVkaW8iLCJBdWRpb0NsaXAiLCJvbkxvYWQiLCJzdGFydCIsInNlbGYiLCJub2RlIiwib24iLCJidXR0b24iLCJHIiwicmVzZXQiLCJTdG9yYWdlIiwiU2V0X0luZm8iLCJmaW5kIiwiZ2V0Q29tcG9uZW50IiwiYWN0aXZlIiwiYXVkaW9FbmdpbmUiLCJwbGF5RWZmZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztFQUNMLFdBQVNELEVBQUUsQ0FBQ0UsU0FEUDtFQUdMQyxVQUFVLEVBQUU7SUFDUkMsV0FBVyxFQUFDSixFQUFFLENBQUNLO0VBRFAsQ0FIUDtFQU9MO0VBRUFDLE1BVEssb0JBU0ssQ0FDVCxDQVZJO0VBWUxDLEtBWkssbUJBWUk7SUFDTCxJQUFJQyxJQUFJLEdBQUcsSUFBWDtJQUNBLEtBQUtDLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBVUMsTUFBVixFQUFrQjtNQUNwQzs7TUFDQTtBQUNaO0FBQ0E7QUFDQTs7TUFDWTtBQUNaO0FBQ0E7O01BQ1k7QUFDWjtBQUNBO01BQ1lDLENBQUMsQ0FBQ0MsS0FBRjtNQUNBQyxPQUFPLENBQUNDLFFBQVIsQ0FBaUIsT0FBakIsRUFBeUIsR0FBekI7TUFDQWYsRUFBRSxDQUFDZ0IsSUFBSCxDQUFRLGVBQVIsRUFBeUJDLFlBQXpCLENBQXNDLFdBQXRDLEVBQW1EVixLQUFuRDtNQUNBUCxFQUFFLENBQUNnQixJQUFILENBQVEsa0JBQVIsRUFBNEJFLE1BQTVCLEdBQXFDLEtBQXJDLENBZm9DLENBZ0JwQzs7TUFDRGxCLEVBQUUsQ0FBQ21CLFdBQUgsQ0FBZUMsVUFBZixDQUEwQlosSUFBSSxDQUFDSixXQUEvQixFQUE0QyxLQUE1QztJQUNELENBbEJGO0VBbUJILENBakNJLENBbUNMOztBQW5DSyxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgQnV0dG9uQXVkaW86Y2MuQXVkaW9DbGlwLFxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgfSxcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLCBmdW5jdGlvbiAoYnV0dG9uKSB7XHJcbiAgICAgICAgICAgIC8vVGhlIGV2ZW50IGlzIGEgY3VzdG9tIGV2ZW50LCB5b3UgY291bGQgZ2V0IHRoZSBCdXR0b24gY29tcG9uZW50IHZpYSBmaXJzdCBhcmd1bWVudFxyXG4gICAgICAgICAgICAvKumAmui/h+aooeWdl+WMluiEmuacrOaTjeS9nFxyXG4gICAgICAgICAgICB2YXIgR2FtZVN0YXJ0ID0gcmVxdWlyZShcIkdhbWVTdGFydFwiKTtcclxuICAgICAgICAgICAgbmV3IEdhbWVTdGFydCgpLnN0YXJ0KCk7IC8vIOiuv+mXrumdmeaAgeWPmOmHj+S8muaKpemUme+8jOmHh+eUqOe7hOS7tuafpeaJvueahOaWueazlVxyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKumAmui/h+aJvue7hOS7tuaTjeS9nFxyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHJykuZ2V0Q29tcG9uZW50KCdHYW1lU3RhcnQnKS5zdGFydCgpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAvKuebtOaOpemHjeaWsOWKoOi9veWcuuaZr1xyXG4gICAgICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJIb21lXCIpO1xyXG4gICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBHLnJlc2V0KCk7XHJcbiAgICAgICAgICAgIFN0b3JhZ2UuU2V0X0luZm8oJ2xldmVsJyxcIjBcIik7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy9NYWluQkcnKS5nZXRDb21wb25lbnQoJ0dhbWVTdGFydCcpLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgIGNjLmZpbmQoJ0NhbnZhcy9USVBTL+WFqOmDqOmAmuWFsycpLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvL+mfs+aViFxyXG4gICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3Qoc2VsZi5CdXR0b25BdWRpbywgZmFsc2UpO1xyXG4gICAgICAgICB9KVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyB1cGRhdGUgKGR0KSB7fSxcclxufSk7XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/draw.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5a774yGBkRKFLeJgWD8+//E', 'draw');
// scripts/draw.js

"use strict";

var _GameMgr = _interopRequireDefault(require("./GameMgr"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

cc.Class({
  "extends": cc.Component,
  properties: {
    MainBG: null,
    Draw: cc.Node,
    dz: cc.Prefab,
    StartBall: null,
    EndBall: null,
    storkelist: [],
    //将线中的点存在数组中，数组的长度存在G.stokelist
    fail: cc.AudioClip,
    pass: cc.AudioClip,
    victory: cc.AudioClip,
    idX: 0,
    idY: 0
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    //cc.log(cc.find('Canvas/MainBG/draw/start').getBoundingBox());
    this.MainBG = cc.find('Canvas/MainBG/BG');
    this.Draw = cc.find('Canvas/MainBG/Draw'); //加载当前的balllist，
  },
  start: function start() {
    this.node.on(cc.Node.EventType.TOUCH_START, function (event) {
      //console.log('Touched!')
      var vec2 = new cc.Vec2(event.getLocationX(), event.getLocationY());
      var node_vec2 = this.MainBG.convertToNodeSpaceAR(vec2); // 关闭新手引导

      this.closeguide();
      var res = this.checkinstartballs(node_vec2);

      if (res) {
        this.draw_list();
      }
    }, this);
    this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
      //判断目标是否命中；
      this.node.off(cc.Node.EventType.TOUCH_MOVE, null, this);
      this.node.getComponent(cc.Graphics).clear();
      this.storkelist = []; // 打开新手引导

      this.openguide();
    }, this);
  },
  CheckTongGuan: function CheckTongGuan() {
    if (G.BallList.length > 0) {
      return false;
    } else {
      return true;
    }
  },
  checkinstartballs: function checkinstartballs(node_vec2) {
    // this.StartBall = G.BallList[0];
    // return true;
    //console.log(G.BallList);
    for (var i = 0; i < G.BallList.length; i++) {
      if (G.BallList[i].getBoundingBox().contains(node_vec2)) {
        this.StartBall = G.BallList[i];
        return true;
      }
    }

    return false;
  },
  checkinendballs: function checkinendballs(node_vec2) {
    if (this.StartBall == null) return false;
    var name = this.StartBall.name;
    var id = this.StartBall._id; // console.log(id)
    // console.log(name)

    for (var i = 0; i < G.BallList.length; i++) {
      if (G.BallList[i].name == name && G.BallList[i]._id != id) {
        if (G.BallList[i].getBoundingBox().contains(node_vec2)) {
          this.EndBall = G.BallList[i];
          return true;
        }
      }
    }

    return false;
  },
  draw_list: function draw_list() {
    var self = this;
    var ctx = this.node.getComponent(cc.Graphics);
    var v = self.StartBall.convertToWorldSpaceAR(cc.v2(0, 0));
    ctx.moveTo(v.x, v.y);
    this.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
      var _this = this;

      //这里要判断event的点，是否在BG的外面，是的话，引用错误
      var vec2 = new cc.Vec2(event.getLocationX(), event.getLocationY());
      var node_vec2 = this.MainBG.convertToNodeSpaceAR(vec2); // console.log(vec2);console.log(this.MainBG.position)
      //             console.log(node_vec2)
      //             console.log(this.MainBG.getBoundingBoxToWorld())
      //判断是不是在背景范围内

      if (this.MainBG.getBoundingBoxToWorld().contains(vec2)) {
        var res_checkinendballs = this.checkinendballs(node_vec2); //判断是不是到终点球

        if (!res_checkinendballs) {
          //不在就继续画
          ctx.lineTo(event.getLocationX(), event.getLocationY()); // console.log('触摸生效');

          ctx.strokeColor = self.stroke_color(self.StartBall._name); //console.log(self.StartBall._name);

          ctx.stroke();
          ctx.moveTo(event.getLocationX(), event.getLocationY());
          this.storkelist.push(vec2); //  this.storkelist.push([event.getLocationX(),event.getLocationY()]);
        } else {
          //在终点球
          this.node.off(cc.Node.EventType.TOUCH_MOVE, null, this);
          G.storkelist.push(this.storkelist);
          var res_check_all_storke_interset = G.check_all_storke_interset(); //判断是不是和其他线交叉

          if (res_check_all_storke_interset != false) {
            //将交点处展示X号
            this.showWrongIcon(res_check_all_storke_interset); //与当前已有线相交，需要回退当前线

            this.node.getComponent(cc.Graphics).clear(); //然后回退G.storkelist

            this.remove(G.storkelist, this.storkelist);
            this.storkelist = [];
          } else {
            //开始下一次画线
            var endballzhongdian = self.EndBall.convertToWorldSpaceAR(cc.v2(0, 0));
            ctx.lineTo(endballzhongdian.x, endballzhongdian.y); // console.log(self.EndBall.convertToWorldSpaceAR(cc.v2(0,0)))

            var ballName = self.StartBall._name;
            ctx.strokeColor = self.stroke_color(self.StartBall._name);
            ctx.stroke(); //当前画布取消监听事件

            this.node.off(cc.Node.EventType.TOUCH_START, null, this);
            this.node.off(cc.Node.EventType.TOUCH_END, null, this); // console.log(self.EndBall);

            var startBall = self.StartBall;
            var endBall = self.EndBall; //在这里产生电子，从this.storkelist的起点跑到终点

            self.dianziMove(self, ballName, v, endballzhongdian, startBall, endBall); //移除两个ball

            this.remove(G.BallList, this.EndBall); //console.log(G.BallList)

            this.remove(G.BallList, this.StartBall);
            this.StartBall = null;
            this.EndBall = null; //判断是否成功通关

            var tongguan = this.CheckTongGuan();

            if (tongguan) {
              this.scheduleOnce(function () {
                _this.StartNextLevel();
              }, 4);
            } else {
              //加载另外一个draw。覆盖在当前draw之上
              cc.resources.load("perfab/draw", function (err, prefab) {
                var newNode = cc.instantiate(prefab); // console.log("成功绘制一条线");

                self.Draw.addChild(newNode);
              });
            }
          }
        }
      } else {
        //注销移动事件的监听
        this.node.off(cc.Node.EventType.TOUCH_MOVE, null, this); //显示wrong
        //console.log(node_vec2)

        this.showWrongIcon(vec2); //清除画线

        this.node.getComponent(cc.Graphics).clear();
        this.storkelist = [];
      }
    }, this);
  },
  dianziMove: function dianziMove(self, name, startPos, endPos, startBall, endBall) {
    var _this2 = this;

    this.schedule(function () {
      if (_GameMgr["default"].gameOver == true) return;
      var newdzNode = cc.instantiate(_this2.dz);
      self.Draw.addChild(newdzNode);
      newdzNode.scale = new cc.Vec3(0.7, 0.7, 0.7);
      newdzNode.getComponent('DianziMove').init(_this2.storkelist, self, name);
      newdzNode.getComponent('DianziMove').startMove(_this2.storkelist, self, startPos, endPos, startBall, endBall);
    }, 0.25);
  },
  stroke_color: function stroke_color(str) {
    switch (str) {
      case 'redball':
        return cc.Color.RED;

      case 'yellowball':
        return cc.Color.YELLOW;

      case 'blueball':
        return cc.Color.BLUE;

      case 'purpleball':
        return new cc.Color(206, 14, 237, 255);

      case 'greenball':
        return cc.Color.GREEN;

      default:
        return cc.Color.BLACK;
    }
  },
  remove: function remove(list, item) {
    for (var i = 0; i < list.length; i++) {
      if (list[i] == item) {
        return list.splice(i, 1);
      }
    }
  },
  showWrongIcon: function showWrongIcon(pos) {
    var self = this;
    cc.resources.load("perfab/wrong", function (err, prefab) {
      var newNode = cc.instantiate(prefab);
      newNode.parent = cc.find('Canvas/TIPS');
      newNode.scale = 1.5;
      var node_vec2 = cc.find('Canvas/TIPS').convertToNodeSpaceAR(pos);
      newNode.setPosition(node_vec2); //音效

      cc.audioEngine.playEffect(self.fail, false);
      setTimeout(function () {
        newNode.active = false;
        newNode.destroy();
      }, 500);
    });
  },
  showRightIcon: function showRightIcon() {
    var self = this;
    cc.resources.load("perfab/right", function (err, prefab) {
      // console.log(err)
      var right = cc.instantiate(prefab);
      var gameStart = cc.find('Canvas/MainBG').getComponent('GameStart');
      right.parent = cc.find('Canvas/TIPS'); //let node_vec2 = cc.find('Canvas/TIPS').convertToNodeSpaceAR(cc.v2(pos[0],pos[1]));

      right.setPosition(0, 191); //音效

      cc.audioEngine.playEffect(self.pass, false);
      setTimeout(function () {
        right.active = false;
        right.destroy(); //直接进入下一关

        gameStart.zhuzi1.setPosition(395, 384, 0);
        gameStart.zhuzi1.angle = 0;
        gameStart.zhuzi2.setPosition(-396, -6, 0);
        gameStart.zhuzi2.angle = 180;
        gameStart.start();
        cc.find('Canvas/MainBG/Draw').removeAllChildren();
        cc.resources.load("perfab/draw", function (err, prefab) {
          var newNode = cc.instantiate(prefab);
          newNode.parent = cc.find('Canvas/MainBG/Draw');
        });
      }, 2500);
    });
  },
  StartNextLevel: function StartNextLevel() {
    if (_GameMgr["default"].gameOver == true) return;
    var level = Storage.Get_Info('level');
    Storage.Set_Info('level', Number(level) + 1);
    this.showRightIcon();
  },
  closeguide: function closeguide() {
    cc.find('Canvas/MainBG/tip').getComponent('newuser_guide').close();
  },
  openguide: function openguide() {
    cc.find('Canvas/MainBG/tip').getComponent('newuser_guide').showguidelist_again();
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcZHJhdy5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsIk1haW5CRyIsIkRyYXciLCJOb2RlIiwiZHoiLCJQcmVmYWIiLCJTdGFydEJhbGwiLCJFbmRCYWxsIiwic3RvcmtlbGlzdCIsImZhaWwiLCJBdWRpb0NsaXAiLCJwYXNzIiwidmljdG9yeSIsImlkWCIsImlkWSIsIm9uTG9hZCIsImZpbmQiLCJzdGFydCIsIm5vZGUiLCJvbiIsIkV2ZW50VHlwZSIsIlRPVUNIX1NUQVJUIiwiZXZlbnQiLCJ2ZWMyIiwiVmVjMiIsImdldExvY2F0aW9uWCIsImdldExvY2F0aW9uWSIsIm5vZGVfdmVjMiIsImNvbnZlcnRUb05vZGVTcGFjZUFSIiwiY2xvc2VndWlkZSIsInJlcyIsImNoZWNraW5zdGFydGJhbGxzIiwiZHJhd19saXN0IiwiVE9VQ0hfRU5EIiwib2ZmIiwiVE9VQ0hfTU9WRSIsImdldENvbXBvbmVudCIsIkdyYXBoaWNzIiwiY2xlYXIiLCJvcGVuZ3VpZGUiLCJDaGVja1RvbmdHdWFuIiwiRyIsIkJhbGxMaXN0IiwibGVuZ3RoIiwiaSIsImdldEJvdW5kaW5nQm94IiwiY29udGFpbnMiLCJjaGVja2luZW5kYmFsbHMiLCJuYW1lIiwiaWQiLCJfaWQiLCJzZWxmIiwiY3R4IiwidiIsImNvbnZlcnRUb1dvcmxkU3BhY2VBUiIsInYyIiwibW92ZVRvIiwieCIsInkiLCJnZXRCb3VuZGluZ0JveFRvV29ybGQiLCJyZXNfY2hlY2tpbmVuZGJhbGxzIiwibGluZVRvIiwic3Ryb2tlQ29sb3IiLCJzdHJva2VfY29sb3IiLCJfbmFtZSIsInN0cm9rZSIsInB1c2giLCJyZXNfY2hlY2tfYWxsX3N0b3JrZV9pbnRlcnNldCIsImNoZWNrX2FsbF9zdG9ya2VfaW50ZXJzZXQiLCJzaG93V3JvbmdJY29uIiwicmVtb3ZlIiwiZW5kYmFsbHpob25nZGlhbiIsImJhbGxOYW1lIiwic3RhcnRCYWxsIiwiZW5kQmFsbCIsImRpYW56aU1vdmUiLCJ0b25nZ3VhbiIsInNjaGVkdWxlT25jZSIsIlN0YXJ0TmV4dExldmVsIiwicmVzb3VyY2VzIiwibG9hZCIsImVyciIsInByZWZhYiIsIm5ld05vZGUiLCJpbnN0YW50aWF0ZSIsImFkZENoaWxkIiwic3RhcnRQb3MiLCJlbmRQb3MiLCJzY2hlZHVsZSIsIkdhbWVNZ3IiLCJnYW1lT3ZlciIsIm5ld2R6Tm9kZSIsInNjYWxlIiwiVmVjMyIsImluaXQiLCJzdGFydE1vdmUiLCJzdHIiLCJDb2xvciIsIlJFRCIsIllFTExPVyIsIkJMVUUiLCJHUkVFTiIsIkJMQUNLIiwibGlzdCIsIml0ZW0iLCJzcGxpY2UiLCJwb3MiLCJwYXJlbnQiLCJzZXRQb3NpdGlvbiIsImF1ZGlvRW5naW5lIiwicGxheUVmZmVjdCIsInNldFRpbWVvdXQiLCJhY3RpdmUiLCJkZXN0cm95Iiwic2hvd1JpZ2h0SWNvbiIsInJpZ2h0IiwiZ2FtZVN0YXJ0Iiwiemh1emkxIiwiYW5nbGUiLCJ6aHV6aTIiLCJyZW1vdmVBbGxDaGlsZHJlbiIsImxldmVsIiwiU3RvcmFnZSIsIkdldF9JbmZvIiwiU2V0X0luZm8iLCJOdW1iZXIiLCJjbG9zZSIsInNob3dndWlkZWxpc3RfYWdhaW4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7Ozs7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1RDLE1BQU0sRUFBQyxJQURFO0lBRVRDLElBQUksRUFBQ0wsRUFBRSxDQUFDTSxJQUZDO0lBR1RDLEVBQUUsRUFBQ1AsRUFBRSxDQUFDUSxNQUhHO0lBSVRDLFNBQVMsRUFBQyxJQUpEO0lBS1RDLE9BQU8sRUFBQyxJQUxDO0lBTVRDLFVBQVUsRUFBQyxFQU5GO0lBTVM7SUFDbEJDLElBQUksRUFBQ1osRUFBRSxDQUFDYSxTQVBDO0lBUVRDLElBQUksRUFBQ2QsRUFBRSxDQUFDYSxTQVJDO0lBU1RFLE9BQU8sRUFBQ2YsRUFBRSxDQUFDYSxTQVRGO0lBVVRHLEdBQUcsRUFBQyxDQVZLO0lBV1RDLEdBQUcsRUFBQztFQVhLLENBSFA7RUFpQkw7RUFFQUMsTUFuQkssb0JBbUJLO0lBQ047SUFDQSxLQUFLZCxNQUFMLEdBQWNKLEVBQUUsQ0FBQ21CLElBQUgsQ0FBUSxrQkFBUixDQUFkO0lBQ0EsS0FBS2QsSUFBTCxHQUFZTCxFQUFFLENBQUNtQixJQUFILENBQVEsb0JBQVIsQ0FBWixDQUhNLENBSU47RUFDSCxDQXhCSTtFQTBCTEMsS0ExQkssbUJBMEJJO0lBQ0wsS0FBS0MsSUFBTCxDQUFVQyxFQUFWLENBQWF0QixFQUFFLENBQUNNLElBQUgsQ0FBUWlCLFNBQVIsQ0FBa0JDLFdBQS9CLEVBQTRDLFVBQVVDLEtBQVYsRUFBaUI7TUFDekQ7TUFDQSxJQUFJQyxJQUFJLEdBQUcsSUFBSTFCLEVBQUUsQ0FBQzJCLElBQVAsQ0FBWUYsS0FBSyxDQUFDRyxZQUFOLEVBQVosRUFBaUNILEtBQUssQ0FBQ0ksWUFBTixFQUFqQyxDQUFYO01BQ0EsSUFBSUMsU0FBUyxHQUFHLEtBQUsxQixNQUFMLENBQVkyQixvQkFBWixDQUFpQ0wsSUFBakMsQ0FBaEIsQ0FIeUQsQ0FJekQ7O01BQ0EsS0FBS00sVUFBTDtNQUNBLElBQUlDLEdBQUcsR0FBRyxLQUFLQyxpQkFBTCxDQUF1QkosU0FBdkIsQ0FBVjs7TUFDQSxJQUFHRyxHQUFILEVBQU87UUFDSCxLQUFLRSxTQUFMO01BQ0g7SUFDSixDQVZELEVBVUUsSUFWRjtJQVdBLEtBQUtkLElBQUwsQ0FBVUMsRUFBVixDQUFhdEIsRUFBRSxDQUFDTSxJQUFILENBQVFpQixTQUFSLENBQWtCYSxTQUEvQixFQUEwQyxVQUFVWCxLQUFWLEVBQWlCO01BQ3ZEO01BQ0EsS0FBS0osSUFBTCxDQUFVZ0IsR0FBVixDQUFjckMsRUFBRSxDQUFDTSxJQUFILENBQVFpQixTQUFSLENBQWtCZSxVQUFoQyxFQUE0QyxJQUE1QyxFQUFrRCxJQUFsRDtNQUNBLEtBQUtqQixJQUFMLENBQVVrQixZQUFWLENBQXVCdkMsRUFBRSxDQUFDd0MsUUFBMUIsRUFBb0NDLEtBQXBDO01BQ0EsS0FBSzlCLFVBQUwsR0FBa0IsRUFBbEIsQ0FKdUQsQ0FLdkQ7O01BQ0EsS0FBSytCLFNBQUw7SUFDSCxDQVBELEVBT0UsSUFQRjtFQVNILENBL0NJO0VBZ0RMQyxhQWhESywyQkFnRFU7SUFDWCxJQUFHQyxDQUFDLENBQUNDLFFBQUYsQ0FBV0MsTUFBWCxHQUFvQixDQUF2QixFQUF5QjtNQUNyQixPQUFPLEtBQVA7SUFDSCxDQUZELE1BRUs7TUFFRCxPQUFPLElBQVA7SUFDSDtFQUNKLENBdkRJO0VBd0RMWixpQkF4REssNkJBd0RhSixTQXhEYixFQXdEdUI7SUFDeEI7SUFDQTtJQUNBO0lBQ0EsS0FBSSxJQUFJaUIsQ0FBQyxHQUFDLENBQVYsRUFBYUEsQ0FBQyxHQUFFSCxDQUFDLENBQUNDLFFBQUYsQ0FBV0MsTUFBM0IsRUFBa0NDLENBQUMsRUFBbkMsRUFBc0M7TUFDbEMsSUFBR0gsQ0FBQyxDQUFDQyxRQUFGLENBQVdFLENBQVgsRUFBY0MsY0FBZCxHQUErQkMsUUFBL0IsQ0FBd0NuQixTQUF4QyxDQUFILEVBQXNEO1FBQ25ELEtBQUtyQixTQUFMLEdBQWlCbUMsQ0FBQyxDQUFDQyxRQUFGLENBQVdFLENBQVgsQ0FBakI7UUFDQSxPQUFPLElBQVA7TUFDRjtJQUNKOztJQUNELE9BQU8sS0FBUDtFQUNILENBbkVJO0VBb0VMRyxlQXBFSywyQkFvRVdwQixTQXBFWCxFQW9FcUI7SUFDdEIsSUFBRyxLQUFLckIsU0FBTCxJQUFrQixJQUFyQixFQUEyQixPQUFPLEtBQVA7SUFDM0IsSUFBSTBDLElBQUksR0FBRyxLQUFLMUMsU0FBTCxDQUFlMEMsSUFBMUI7SUFDQSxJQUFJQyxFQUFFLEdBQUcsS0FBSzNDLFNBQUwsQ0FBZTRDLEdBQXhCLENBSHNCLENBSXRCO0lBQ0E7O0lBQ0EsS0FBSSxJQUFJTixDQUFDLEdBQUMsQ0FBVixFQUFhQSxDQUFDLEdBQUVILENBQUMsQ0FBQ0MsUUFBRixDQUFXQyxNQUEzQixFQUFrQ0MsQ0FBQyxFQUFuQyxFQUFzQztNQUNsQyxJQUFHSCxDQUFDLENBQUNDLFFBQUYsQ0FBV0UsQ0FBWCxFQUFjSSxJQUFkLElBQXNCQSxJQUF0QixJQUE4QlAsQ0FBQyxDQUFDQyxRQUFGLENBQVdFLENBQVgsRUFBY00sR0FBZCxJQUFxQkQsRUFBdEQsRUFBeUQ7UUFDckQsSUFBR1IsQ0FBQyxDQUFDQyxRQUFGLENBQVdFLENBQVgsRUFBY0MsY0FBZCxHQUErQkMsUUFBL0IsQ0FBd0NuQixTQUF4QyxDQUFILEVBQXNEO1VBQ2xELEtBQUtwQixPQUFMLEdBQWVrQyxDQUFDLENBQUNDLFFBQUYsQ0FBV0UsQ0FBWCxDQUFmO1VBQ0EsT0FBTyxJQUFQO1FBQ0g7TUFDSjtJQUNKOztJQUNELE9BQU8sS0FBUDtFQUNILENBbkZJO0VBb0ZMWixTQXBGSyx1QkFvRk07SUFDUCxJQUFJbUIsSUFBSSxHQUFHLElBQVg7SUFDQSxJQUFJQyxHQUFHLEdBQUcsS0FBS2xDLElBQUwsQ0FBVWtCLFlBQVYsQ0FBdUJ2QyxFQUFFLENBQUN3QyxRQUExQixDQUFWO0lBQ0EsSUFBSWdCLENBQUMsR0FBR0YsSUFBSSxDQUFDN0MsU0FBTCxDQUFlZ0QscUJBQWYsQ0FBcUN6RCxFQUFFLENBQUMwRCxFQUFILENBQU0sQ0FBTixFQUFRLENBQVIsQ0FBckMsQ0FBUjtJQUNBSCxHQUFHLENBQUNJLE1BQUosQ0FBV0gsQ0FBQyxDQUFDSSxDQUFiLEVBQWVKLENBQUMsQ0FBQ0ssQ0FBakI7SUFDQSxLQUFLeEMsSUFBTCxDQUFVQyxFQUFWLENBQWF0QixFQUFFLENBQUNNLElBQUgsQ0FBUWlCLFNBQVIsQ0FBa0JlLFVBQS9CLEVBQTJDLFVBQVViLEtBQVYsRUFBaUI7TUFBQTs7TUFDeEQ7TUFDQSxJQUFJQyxJQUFJLEdBQUcsSUFBSTFCLEVBQUUsQ0FBQzJCLElBQVAsQ0FBWUYsS0FBSyxDQUFDRyxZQUFOLEVBQVosRUFBaUNILEtBQUssQ0FBQ0ksWUFBTixFQUFqQyxDQUFYO01BQ0EsSUFBSUMsU0FBUyxHQUFHLEtBQUsxQixNQUFMLENBQVkyQixvQkFBWixDQUFpQ0wsSUFBakMsQ0FBaEIsQ0FId0QsQ0FJeEQ7TUFDQTtNQUNBO01BQ0E7O01BQ0EsSUFBRyxLQUFLdEIsTUFBTCxDQUFZMEQscUJBQVosR0FBb0NiLFFBQXBDLENBQTZDdkIsSUFBN0MsQ0FBSCxFQUFzRDtRQUNqRCxJQUFJcUMsbUJBQW1CLEdBQUcsS0FBS2IsZUFBTCxDQUFxQnBCLFNBQXJCLENBQTFCLENBRGlELENBRWxEOztRQUNBLElBQUcsQ0FBQ2lDLG1CQUFKLEVBQXdCO1VBQ3BCO1VBQ0FSLEdBQUcsQ0FBQ1MsTUFBSixDQUFXdkMsS0FBSyxDQUFDRyxZQUFOLEVBQVgsRUFBZ0NILEtBQUssQ0FBQ0ksWUFBTixFQUFoQyxFQUZvQixDQUdwQjs7VUFDQTBCLEdBQUcsQ0FBQ1UsV0FBSixHQUFrQlgsSUFBSSxDQUFDWSxZQUFMLENBQWtCWixJQUFJLENBQUM3QyxTQUFMLENBQWUwRCxLQUFqQyxDQUFsQixDQUpvQixDQUtwQjs7VUFDQ1osR0FBRyxDQUFDYSxNQUFKO1VBQ0FiLEdBQUcsQ0FBQ0ksTUFBSixDQUFXbEMsS0FBSyxDQUFDRyxZQUFOLEVBQVgsRUFBZ0NILEtBQUssQ0FBQ0ksWUFBTixFQUFoQztVQUNBLEtBQUtsQixVQUFMLENBQWdCMEQsSUFBaEIsQ0FBcUIzQyxJQUFyQixFQVJtQixDQVNwQjtRQUNILENBVkQsTUFVSztVQUNEO1VBQ0EsS0FBS0wsSUFBTCxDQUFVZ0IsR0FBVixDQUFjckMsRUFBRSxDQUFDTSxJQUFILENBQVFpQixTQUFSLENBQWtCZSxVQUFoQyxFQUE0QyxJQUE1QyxFQUFrRCxJQUFsRDtVQUNBTSxDQUFDLENBQUNqQyxVQUFGLENBQWEwRCxJQUFiLENBQWtCLEtBQUsxRCxVQUF2QjtVQUNBLElBQUkyRCw2QkFBNkIsR0FBRzFCLENBQUMsQ0FBQzJCLHlCQUFGLEVBQXBDLENBSkMsQ0FLRDs7VUFDQSxJQUFHRCw2QkFBNkIsSUFBSSxLQUFwQyxFQUEwQztZQUN0QztZQUNBLEtBQUtFLGFBQUwsQ0FBbUJGLDZCQUFuQixFQUZzQyxDQUd0Qzs7WUFDQSxLQUFLakQsSUFBTCxDQUFVa0IsWUFBVixDQUF1QnZDLEVBQUUsQ0FBQ3dDLFFBQTFCLEVBQW9DQyxLQUFwQyxHQUpzQyxDQUt0Qzs7WUFDQSxLQUFLZ0MsTUFBTCxDQUFZN0IsQ0FBQyxDQUFDakMsVUFBZCxFQUF5QixLQUFLQSxVQUE5QjtZQUNBLEtBQUtBLFVBQUwsR0FBa0IsRUFBbEI7VUFDSCxDQVJELE1BUUs7WUFDRDtZQUNBLElBQUkrRCxnQkFBZ0IsR0FBR3BCLElBQUksQ0FBQzVDLE9BQUwsQ0FBYStDLHFCQUFiLENBQW1DekQsRUFBRSxDQUFDMEQsRUFBSCxDQUFNLENBQU4sRUFBUSxDQUFSLENBQW5DLENBQXZCO1lBQ0FILEdBQUcsQ0FBQ1MsTUFBSixDQUFXVSxnQkFBZ0IsQ0FBQ2QsQ0FBNUIsRUFBOEJjLGdCQUFnQixDQUFDYixDQUEvQyxFQUhDLENBSUQ7O1lBQ0EsSUFBSWMsUUFBUSxHQUFHckIsSUFBSSxDQUFDN0MsU0FBTCxDQUFlMEQsS0FBOUI7WUFDQVosR0FBRyxDQUFDVSxXQUFKLEdBQWtCWCxJQUFJLENBQUNZLFlBQUwsQ0FBa0JaLElBQUksQ0FBQzdDLFNBQUwsQ0FBZTBELEtBQWpDLENBQWxCO1lBRUFaLEdBQUcsQ0FBQ2EsTUFBSixHQVJDLENBU0Q7O1lBQ0EsS0FBSy9DLElBQUwsQ0FBVWdCLEdBQVYsQ0FBY3JDLEVBQUUsQ0FBQ00sSUFBSCxDQUFRaUIsU0FBUixDQUFrQkMsV0FBaEMsRUFBNkMsSUFBN0MsRUFBbUQsSUFBbkQ7WUFDQSxLQUFLSCxJQUFMLENBQVVnQixHQUFWLENBQWNyQyxFQUFFLENBQUNNLElBQUgsQ0FBUWlCLFNBQVIsQ0FBa0JhLFNBQWhDLEVBQTJDLElBQTNDLEVBQWlELElBQWpELEVBWEMsQ0FZRDs7WUFDQSxJQUFJd0MsU0FBUyxHQUFHdEIsSUFBSSxDQUFDN0MsU0FBckI7WUFDQSxJQUFJb0UsT0FBTyxHQUFHdkIsSUFBSSxDQUFDNUMsT0FBbkIsQ0FkQyxDQWVEOztZQUNBNEMsSUFBSSxDQUFDd0IsVUFBTCxDQUFnQnhCLElBQWhCLEVBQXFCcUIsUUFBckIsRUFBOEJuQixDQUE5QixFQUFnQ2tCLGdCQUFoQyxFQUFpREUsU0FBakQsRUFBMkRDLE9BQTNELEVBaEJDLENBaUJEOztZQUNBLEtBQUtKLE1BQUwsQ0FBWTdCLENBQUMsQ0FBQ0MsUUFBZCxFQUF1QixLQUFLbkMsT0FBNUIsRUFsQkMsQ0FtQkQ7O1lBQ0EsS0FBSytELE1BQUwsQ0FBWTdCLENBQUMsQ0FBQ0MsUUFBZCxFQUF1QixLQUFLcEMsU0FBNUI7WUFFQSxLQUFLQSxTQUFMLEdBQWlCLElBQWpCO1lBQ0EsS0FBS0MsT0FBTCxHQUFlLElBQWYsQ0F2QkMsQ0F3QkQ7O1lBQ0MsSUFBSXFFLFFBQVEsR0FBRyxLQUFLcEMsYUFBTCxFQUFmOztZQUNBLElBQUdvQyxRQUFILEVBQVk7Y0FDVCxLQUFLQyxZQUFMLENBQWtCLFlBQUk7Z0JBQ2xCLEtBQUksQ0FBQ0MsY0FBTDtjQUNILENBRkQsRUFFRSxDQUZGO1lBR0YsQ0FKRCxNQUlLO2NBQ0Y7Y0FDQWpGLEVBQUUsQ0FBQ2tGLFNBQUgsQ0FBYUMsSUFBYixDQUFrQixhQUFsQixFQUFpQyxVQUFVQyxHQUFWLEVBQWVDLE1BQWYsRUFBdUI7Z0JBQ3BELElBQUlDLE9BQU8sR0FBR3RGLEVBQUUsQ0FBQ3VGLFdBQUgsQ0FBZUYsTUFBZixDQUFkLENBRG9ELENBRXBEOztnQkFDQS9CLElBQUksQ0FBQ2pELElBQUwsQ0FBVW1GLFFBQVYsQ0FBbUJGLE9BQW5CO2NBQ0YsQ0FKRjtZQUtGO1VBQ0w7UUFDSjtNQUNKLENBbkVELE1BbUVLO1FBQ0Q7UUFDQSxLQUFLakUsSUFBTCxDQUFVZ0IsR0FBVixDQUFjckMsRUFBRSxDQUFDTSxJQUFILENBQVFpQixTQUFSLENBQWtCZSxVQUFoQyxFQUE0QyxJQUE1QyxFQUFrRCxJQUFsRCxFQUZDLENBR0Y7UUFDQTs7UUFDQSxLQUFLa0MsYUFBTCxDQUFtQjlDLElBQW5CLEVBTEUsQ0FNRjs7UUFDQSxLQUFLTCxJQUFMLENBQVVrQixZQUFWLENBQXVCdkMsRUFBRSxDQUFDd0MsUUFBMUIsRUFBb0NDLEtBQXBDO1FBQ0EsS0FBSzlCLFVBQUwsR0FBa0IsRUFBbEI7TUFDRjtJQUVKLENBdEZELEVBc0ZHLElBdEZIO0VBdUZILENBaExJO0VBaUxMbUUsVUFqTEssc0JBaUxNeEIsSUFqTE4sRUFpTFdILElBakxYLEVBaUxnQnNDLFFBakxoQixFQWlMeUJDLE1Bakx6QixFQWlMZ0NkLFNBakxoQyxFQWlMMENDLE9BakwxQyxFQWlMbUQ7SUFBQTs7SUFDcEQsS0FBS2MsUUFBTCxDQUFjLFlBQU07TUFDaEIsSUFBR0MsbUJBQUEsQ0FBUUMsUUFBUixJQUFrQixJQUFyQixFQUEwQjtNQUMxQixJQUFJQyxTQUFTLEdBQUc5RixFQUFFLENBQUN1RixXQUFILENBQWUsTUFBSSxDQUFDaEYsRUFBcEIsQ0FBaEI7TUFDQStDLElBQUksQ0FBQ2pELElBQUwsQ0FBVW1GLFFBQVYsQ0FBbUJNLFNBQW5CO01BQ0FBLFNBQVMsQ0FBQ0MsS0FBVixHQUFrQixJQUFJL0YsRUFBRSxDQUFDZ0csSUFBUCxDQUFZLEdBQVosRUFBZ0IsR0FBaEIsRUFBb0IsR0FBcEIsQ0FBbEI7TUFDQUYsU0FBUyxDQUFDdkQsWUFBVixDQUF1QixZQUF2QixFQUFxQzBELElBQXJDLENBQTBDLE1BQUksQ0FBQ3RGLFVBQS9DLEVBQTBEMkMsSUFBMUQsRUFBZ0VILElBQWhFO01BQ0EyQyxTQUFTLENBQUN2RCxZQUFWLENBQXVCLFlBQXZCLEVBQXFDMkQsU0FBckMsQ0FBK0MsTUFBSSxDQUFDdkYsVUFBcEQsRUFBZ0UyQyxJQUFoRSxFQUFxRW1DLFFBQXJFLEVBQThFQyxNQUE5RSxFQUFxRmQsU0FBckYsRUFBK0ZDLE9BQS9GO0lBQ0gsQ0FQRCxFQU9HLElBUEg7RUFRSCxDQTFMSTtFQTJMTFgsWUEzTEssd0JBMkxRaUMsR0EzTFIsRUEyTFk7SUFDYixRQUFPQSxHQUFQO01BQ0ksS0FBSyxTQUFMO1FBQ0ksT0FBUW5HLEVBQUUsQ0FBQ29HLEtBQUgsQ0FBU0MsR0FBakI7O01BQ0osS0FBSyxZQUFMO1FBQ0ksT0FBUXJHLEVBQUUsQ0FBQ29HLEtBQUgsQ0FBU0UsTUFBakI7O01BQ0osS0FBSyxVQUFMO1FBQ0ksT0FBUXRHLEVBQUUsQ0FBQ29HLEtBQUgsQ0FBU0csSUFBakI7O01BQ0osS0FBSyxZQUFMO1FBQ0ksT0FBUSxJQUFJdkcsRUFBRSxDQUFDb0csS0FBUCxDQUFhLEdBQWIsRUFBaUIsRUFBakIsRUFBb0IsR0FBcEIsRUFBd0IsR0FBeEIsQ0FBUjs7TUFDSixLQUFLLFdBQUw7UUFDSSxPQUFRcEcsRUFBRSxDQUFDb0csS0FBSCxDQUFTSSxLQUFqQjs7TUFDSjtRQUNJLE9BQVF4RyxFQUFFLENBQUNvRyxLQUFILENBQVNLLEtBQWpCO0lBWlI7RUFlSCxDQTNNSTtFQTRNTGhDLE1BNU1LLGtCQTRNRWlDLElBNU1GLEVBNE1PQyxJQTVNUCxFQTRNWTtJQUNiLEtBQUksSUFBSTVELENBQUMsR0FBQyxDQUFWLEVBQWFBLENBQUMsR0FBRTJELElBQUksQ0FBQzVELE1BQXJCLEVBQTRCQyxDQUFDLEVBQTdCLEVBQWdDO01BQzVCLElBQUkyRCxJQUFJLENBQUMzRCxDQUFELENBQUosSUFBVzRELElBQWYsRUFBb0I7UUFDakIsT0FBT0QsSUFBSSxDQUFDRSxNQUFMLENBQVk3RCxDQUFaLEVBQWMsQ0FBZCxDQUFQO01BQ0Y7SUFDSjtFQUNKLENBbE5JO0VBbU5MeUIsYUFuTksseUJBbU5TcUMsR0FuTlQsRUFtTmE7SUFDZCxJQUFJdkQsSUFBSSxHQUFHLElBQVg7SUFDQXRELEVBQUUsQ0FBQ2tGLFNBQUgsQ0FBYUMsSUFBYixDQUFrQixjQUFsQixFQUFrQyxVQUFVQyxHQUFWLEVBQWVDLE1BQWYsRUFBdUI7TUFDckQsSUFBSUMsT0FBTyxHQUFHdEYsRUFBRSxDQUFDdUYsV0FBSCxDQUFlRixNQUFmLENBQWQ7TUFDQUMsT0FBTyxDQUFDd0IsTUFBUixHQUFpQjlHLEVBQUUsQ0FBQ21CLElBQUgsQ0FBUSxhQUFSLENBQWpCO01BQ0FtRSxPQUFPLENBQUNTLEtBQVIsR0FBYyxHQUFkO01BQ0EsSUFBSWpFLFNBQVMsR0FBRzlCLEVBQUUsQ0FBQ21CLElBQUgsQ0FBUSxhQUFSLEVBQXVCWSxvQkFBdkIsQ0FBNEM4RSxHQUE1QyxDQUFoQjtNQUNBdkIsT0FBTyxDQUFDeUIsV0FBUixDQUFvQmpGLFNBQXBCLEVBTHFELENBTXJEOztNQUNBOUIsRUFBRSxDQUFDZ0gsV0FBSCxDQUFlQyxVQUFmLENBQTBCM0QsSUFBSSxDQUFDMUMsSUFBL0IsRUFBcUMsS0FBckM7TUFDQXNHLFVBQVUsQ0FBQyxZQUFVO1FBQ2pCNUIsT0FBTyxDQUFDNkIsTUFBUixHQUFpQixLQUFqQjtRQUNBN0IsT0FBTyxDQUFDOEIsT0FBUjtNQUVILENBSlMsRUFJUixHQUpRLENBQVY7SUFLSCxDQWJEO0VBY0gsQ0FuT0k7RUFvT0xDLGFBcE9LLDJCQW9PVTtJQUNYLElBQUkvRCxJQUFJLEdBQUcsSUFBWDtJQUNBdEQsRUFBRSxDQUFDa0YsU0FBSCxDQUFhQyxJQUFiLENBQWtCLGNBQWxCLEVBQWtDLFVBQVVDLEdBQVYsRUFBZUMsTUFBZixFQUF1QjtNQUNyRDtNQUNBLElBQUlpQyxLQUFLLEdBQUd0SCxFQUFFLENBQUN1RixXQUFILENBQWVGLE1BQWYsQ0FBWjtNQUNBLElBQUlrQyxTQUFTLEdBQUd2SCxFQUFFLENBQUNtQixJQUFILENBQVEsZUFBUixFQUF5Qm9CLFlBQXpCLENBQXNDLFdBQXRDLENBQWhCO01BQ0ErRSxLQUFLLENBQUNSLE1BQU4sR0FBZTlHLEVBQUUsQ0FBQ21CLElBQUgsQ0FBUSxhQUFSLENBQWYsQ0FKcUQsQ0FLckQ7O01BQ0FtRyxLQUFLLENBQUNQLFdBQU4sQ0FBa0IsQ0FBbEIsRUFBb0IsR0FBcEIsRUFOcUQsQ0FPckQ7O01BQ0EvRyxFQUFFLENBQUNnSCxXQUFILENBQWVDLFVBQWYsQ0FBMEIzRCxJQUFJLENBQUN4QyxJQUEvQixFQUFxQyxLQUFyQztNQUNBb0csVUFBVSxDQUFDLFlBQVU7UUFDakJJLEtBQUssQ0FBQ0gsTUFBTixHQUFlLEtBQWY7UUFDQUcsS0FBSyxDQUFDRixPQUFOLEdBRmlCLENBR2pCOztRQUVBRyxTQUFTLENBQUNDLE1BQVYsQ0FBaUJULFdBQWpCLENBQTZCLEdBQTdCLEVBQWtDLEdBQWxDLEVBQXVDLENBQXZDO1FBQ0FRLFNBQVMsQ0FBQ0MsTUFBVixDQUFpQkMsS0FBakIsR0FBeUIsQ0FBekI7UUFDQUYsU0FBUyxDQUFDRyxNQUFWLENBQWlCWCxXQUFqQixDQUE2QixDQUFDLEdBQTlCLEVBQW1DLENBQUMsQ0FBcEMsRUFBdUMsQ0FBdkM7UUFDQVEsU0FBUyxDQUFDRyxNQUFWLENBQWlCRCxLQUFqQixHQUF5QixHQUF6QjtRQUVBRixTQUFTLENBQUNuRyxLQUFWO1FBQ0FwQixFQUFFLENBQUNtQixJQUFILENBQVEsb0JBQVIsRUFBOEJ3RyxpQkFBOUI7UUFDQTNILEVBQUUsQ0FBQ2tGLFNBQUgsQ0FBYUMsSUFBYixDQUFrQixhQUFsQixFQUFpQyxVQUFVQyxHQUFWLEVBQWVDLE1BQWYsRUFBdUI7VUFDcEQsSUFBSUMsT0FBTyxHQUFHdEYsRUFBRSxDQUFDdUYsV0FBSCxDQUFlRixNQUFmLENBQWQ7VUFDQUMsT0FBTyxDQUFDd0IsTUFBUixHQUFpQjlHLEVBQUUsQ0FBQ21CLElBQUgsQ0FBUSxvQkFBUixDQUFqQjtRQUNILENBSEQ7TUFJSCxDQWhCUyxFQWdCUixJQWhCUSxDQUFWO0lBaUJILENBMUJEO0VBMkJILENBalFJO0VBa1FMOEQsY0FsUUssNEJBa1FXO0lBQ1osSUFBR1csbUJBQUEsQ0FBUUMsUUFBUixJQUFrQixJQUFyQixFQUEwQjtJQUMxQixJQUFJK0IsS0FBSyxHQUFHQyxPQUFPLENBQUNDLFFBQVIsQ0FBaUIsT0FBakIsQ0FBWjtJQUNBRCxPQUFPLENBQUNFLFFBQVIsQ0FBaUIsT0FBakIsRUFBeUJDLE1BQU0sQ0FBQ0osS0FBRCxDQUFOLEdBQWMsQ0FBdkM7SUFDQSxLQUFLUCxhQUFMO0VBQ0gsQ0F2UUk7RUF3UUxyRixVQXhRSyx3QkF3UU87SUFDUmhDLEVBQUUsQ0FBQ21CLElBQUgsQ0FBUSxtQkFBUixFQUE2Qm9CLFlBQTdCLENBQTBDLGVBQTFDLEVBQTJEMEYsS0FBM0Q7RUFDSCxDQTFRSTtFQTJRTHZGLFNBM1FLLHVCQTJRTTtJQUNQMUMsRUFBRSxDQUFDbUIsSUFBSCxDQUFRLG1CQUFSLEVBQTZCb0IsWUFBN0IsQ0FBMEMsZUFBMUMsRUFBMkQyRixtQkFBM0Q7RUFDSCxDQTdRSSxDQStRTDs7QUEvUUssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4vR2FtZU1nclwiO1xyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgIE1haW5CRzpudWxsLFxyXG4gICAgICAgRHJhdzpjYy5Ob2RlLFxyXG4gICAgICAgZHo6Y2MuUHJlZmFiLFxyXG4gICAgICAgU3RhcnRCYWxsOm51bGwsXHJcbiAgICAgICBFbmRCYWxsOm51bGwsXHJcbiAgICAgICBzdG9ya2VsaXN0OltdLCAgICAvL+Wwhue6v+S4reeahOeCueWtmOWcqOaVsOe7hOS4re+8jOaVsOe7hOeahOmVv+W6puWtmOWcqEcuc3Rva2VsaXN0XHJcbiAgICAgICBmYWlsOmNjLkF1ZGlvQ2xpcCxcclxuICAgICAgIHBhc3M6Y2MuQXVkaW9DbGlwLFxyXG4gICAgICAgdmljdG9yeTpjYy5BdWRpb0NsaXAsXHJcbiAgICAgICBpZFg6MCxcclxuICAgICAgIGlkWTowLFxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgICAgIC8vY2MubG9nKGNjLmZpbmQoJ0NhbnZhcy9NYWluQkcvZHJhdy9zdGFydCcpLmdldEJvdW5kaW5nQm94KCkpO1xyXG4gICAgICAgIHRoaXMuTWFpbkJHID0gY2MuZmluZCgnQ2FudmFzL01haW5CRy9CRycpXHJcbiAgICAgICAgdGhpcy5EcmF3ID0gY2MuZmluZCgnQ2FudmFzL01haW5CRy9EcmF3JylcclxuICAgICAgICAvL+WKoOi9veW9k+WJjeeahGJhbGxsaXN077yMXHJcbiAgICB9LFxyXG5cclxuICAgIHN0YXJ0ICgpIHtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKCdUb3VjaGVkIScpXHJcbiAgICAgICAgICAgIGxldCB2ZWMyID0gbmV3IGNjLlZlYzIoZXZlbnQuZ2V0TG9jYXRpb25YKCksZXZlbnQuZ2V0TG9jYXRpb25ZKCkpO1xyXG4gICAgICAgICAgICBsZXQgbm9kZV92ZWMyID0gdGhpcy5NYWluQkcuY29udmVydFRvTm9kZVNwYWNlQVIodmVjMik7XHJcbiAgICAgICAgICAgIC8vIOWFs+mXreaWsOaJi+W8leWvvFxyXG4gICAgICAgICAgICB0aGlzLmNsb3NlZ3VpZGUoKTtcclxuICAgICAgICAgICAgbGV0IHJlcyA9IHRoaXMuY2hlY2tpbnN0YXJ0YmFsbHMobm9kZV92ZWMyKTtcclxuICAgICAgICAgICAgaWYocmVzKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuZHJhd19saXN0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LHRoaXMpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9FTkQsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgICAvL+WIpOaWreebruagh+aYr+WQpuWRveS4re+8m1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUub2ZmKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX01PVkUsIG51bGwsIHRoaXMpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLkdyYXBoaWNzKS5jbGVhcigpO1xyXG4gICAgICAgICAgICB0aGlzLnN0b3JrZWxpc3QgPSBbXTtcclxuICAgICAgICAgICAgLy8g5omT5byA5paw5omL5byV5a+8XHJcbiAgICAgICAgICAgIHRoaXMub3Blbmd1aWRlKCk7XHJcbiAgICAgICAgfSx0aGlzKVxyXG4gICAgICAgXHJcbiAgICB9LFxyXG4gICAgQ2hlY2tUb25nR3Vhbigpe1xyXG4gICAgICAgIGlmKEcuQmFsbExpc3QubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9ZWxzZXtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjaGVja2luc3RhcnRiYWxscyhub2RlX3ZlYzIpe1xyXG4gICAgICAgIC8vIHRoaXMuU3RhcnRCYWxsID0gRy5CYWxsTGlzdFswXTtcclxuICAgICAgICAvLyByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKEcuQmFsbExpc3QpO1xyXG4gICAgICAgIGZvcihsZXQgaT0wOyBpPCBHLkJhbGxMaXN0Lmxlbmd0aDtpKyspe1xyXG4gICAgICAgICAgICBpZihHLkJhbGxMaXN0W2ldLmdldEJvdW5kaW5nQm94KCkuY29udGFpbnMobm9kZV92ZWMyKSl7XHJcbiAgICAgICAgICAgICAgIHRoaXMuU3RhcnRCYWxsID0gRy5CYWxsTGlzdFtpXTtcclxuICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSxcclxuICAgIGNoZWNraW5lbmRiYWxscyhub2RlX3ZlYzIpe1xyXG4gICAgICAgIGlmKHRoaXMuU3RhcnRCYWxsID09IG51bGwpIHJldHVybiBmYWxzZTtcclxuICAgICAgICBsZXQgbmFtZSA9IHRoaXMuU3RhcnRCYWxsLm5hbWU7XHJcbiAgICAgICAgbGV0IGlkID0gdGhpcy5TdGFydEJhbGwuX2lkO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGlkKVxyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKG5hbWUpXHJcbiAgICAgICAgZm9yKGxldCBpPTA7IGk8IEcuQmFsbExpc3QubGVuZ3RoO2krKyl7XHJcbiAgICAgICAgICAgIGlmKEcuQmFsbExpc3RbaV0ubmFtZSA9PSBuYW1lICYmIEcuQmFsbExpc3RbaV0uX2lkICE9IGlkKXtcclxuICAgICAgICAgICAgICAgIGlmKEcuQmFsbExpc3RbaV0uZ2V0Qm91bmRpbmdCb3goKS5jb250YWlucyhub2RlX3ZlYzIpKXtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLkVuZEJhbGwgPSBHLkJhbGxMaXN0W2ldO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0sXHJcbiAgICBkcmF3X2xpc3QoKXtcclxuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgdmFyIGN0eCA9IHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuR3JhcGhpY3MpO1xyXG4gICAgICAgIGxldCB2ID0gc2VsZi5TdGFydEJhbGwuY29udmVydFRvV29ybGRTcGFjZUFSKGNjLnYyKDAsMCkpXHJcbiAgICAgICAgY3R4Lm1vdmVUbyh2Lngsdi55KTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfTU9WRSwgZnVuY3Rpb24gKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIC8v6L+Z6YeM6KaB5Yik5patZXZlbnTnmoTngrnvvIzmmK/lkKblnKhCR+eahOWklumdou+8jOaYr+eahOivne+8jOW8leeUqOmUmeivr1xyXG4gICAgICAgICAgICBsZXQgdmVjMiA9IG5ldyBjYy5WZWMyKGV2ZW50LmdldExvY2F0aW9uWCgpLGV2ZW50LmdldExvY2F0aW9uWSgpKTtcclxuICAgICAgICAgICAgbGV0IG5vZGVfdmVjMiA9IHRoaXMuTWFpbkJHLmNvbnZlcnRUb05vZGVTcGFjZUFSKHZlYzIpO1xyXG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyh2ZWMyKTtjb25zb2xlLmxvZyh0aGlzLk1haW5CRy5wb3NpdGlvbilcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgY29uc29sZS5sb2cobm9kZV92ZWMyKVxyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLk1haW5CRy5nZXRCb3VuZGluZ0JveFRvV29ybGQoKSlcclxuICAgICAgICAgICAgLy/liKTmlq3mmK/kuI3mmK/lnKjog4zmma/ojIPlm7TlhoVcclxuICAgICAgICAgICAgaWYodGhpcy5NYWluQkcuZ2V0Qm91bmRpbmdCb3hUb1dvcmxkKCkuY29udGFpbnModmVjMikpe1xyXG4gICAgICAgICAgICAgICAgIGxldCByZXNfY2hlY2tpbmVuZGJhbGxzID0gdGhpcy5jaGVja2luZW5kYmFsbHMobm9kZV92ZWMyKTtcclxuICAgICAgICAgICAgICAgIC8v5Yik5pat5piv5LiN5piv5Yiw57uI54K555CDXHJcbiAgICAgICAgICAgICAgICBpZighcmVzX2NoZWNraW5lbmRiYWxscyl7XHJcbiAgICAgICAgICAgICAgICAgICAgLy/kuI3lnKjlsLHnu6fnu63nlLtcclxuICAgICAgICAgICAgICAgICAgICBjdHgubGluZVRvKGV2ZW50LmdldExvY2F0aW9uWCgpLGV2ZW50LmdldExvY2F0aW9uWSgpKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZygn6Kem5pG455Sf5pWIJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgY3R4LnN0cm9rZUNvbG9yID0gc2VsZi5zdHJva2VfY29sb3Ioc2VsZi5TdGFydEJhbGwuX25hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2coc2VsZi5TdGFydEJhbGwuX25hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICBjdHguc3Ryb2tlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgIGN0eC5tb3ZlVG8oZXZlbnQuZ2V0TG9jYXRpb25YKCksZXZlbnQuZ2V0TG9jYXRpb25ZKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b3JrZWxpc3QucHVzaCh2ZWMyKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgdGhpcy5zdG9ya2VsaXN0LnB1c2goW2V2ZW50LmdldExvY2F0aW9uWCgpLGV2ZW50LmdldExvY2F0aW9uWSgpXSk7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAvL+WcqOe7iOeCueeQg1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS5vZmYoY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfTU9WRSwgbnVsbCwgdGhpcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgRy5zdG9ya2VsaXN0LnB1c2godGhpcy5zdG9ya2VsaXN0KTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzX2NoZWNrX2FsbF9zdG9ya2VfaW50ZXJzZXQgPSBHLmNoZWNrX2FsbF9zdG9ya2VfaW50ZXJzZXQoKTtcclxuICAgICAgICAgICAgICAgICAgICAvL+WIpOaWreaYr+S4jeaYr+WSjOWFtuS7lue6v+S6pOWPiVxyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc19jaGVja19hbGxfc3RvcmtlX2ludGVyc2V0ICE9IGZhbHNlKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy/lsIbkuqTngrnlpITlsZXnpLpY5Y+3XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd1dyb25nSWNvbihyZXNfY2hlY2tfYWxsX3N0b3JrZV9pbnRlcnNldCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8v5LiO5b2T5YmN5bey5pyJ57q/55u45Lqk77yM6ZyA6KaB5Zue6YCA5b2T5YmN57q/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuR3JhcGhpY3MpLmNsZWFyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8v54S25ZCO5Zue6YCARy5zdG9ya2VsaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVtb3ZlKEcuc3RvcmtlbGlzdCx0aGlzLnN0b3JrZWxpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b3JrZWxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy/lvIDlp4vkuIvkuIDmrKHnlLvnur9cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGVuZGJhbGx6aG9uZ2RpYW4gPSBzZWxmLkVuZEJhbGwuY29udmVydFRvV29ybGRTcGFjZUFSKGNjLnYyKDAsMCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjdHgubGluZVRvKGVuZGJhbGx6aG9uZ2RpYW4ueCxlbmRiYWxsemhvbmdkaWFuLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhzZWxmLkVuZEJhbGwuY29udmVydFRvV29ybGRTcGFjZUFSKGNjLnYyKDAsMCkpKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYmFsbE5hbWUgPSBzZWxmLlN0YXJ0QmFsbC5fbmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3R4LnN0cm9rZUNvbG9yID0gc2VsZi5zdHJva2VfY29sb3Ioc2VsZi5TdGFydEJhbGwuX25hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3R4LnN0cm9rZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+W9k+WJjeeUu+W4g+WPlua2iOebkeWQrOS6i+S7tlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vZGUub2ZmKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX1NUQVJULCBudWxsLCB0aGlzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlLm9mZihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9FTkQsIG51bGwsIHRoaXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhzZWxmLkVuZEJhbGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgc3RhcnRCYWxsID0gc2VsZi5TdGFydEJhbGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBlbmRCYWxsID0gc2VsZi5FbmRCYWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+WcqOi/memHjOS6p+eUn+eUteWtkO+8jOS7jnRoaXMuc3RvcmtlbGlzdOeahOi1t+eCuei3keWIsOe7iOeCuVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLmRpYW56aU1vdmUoc2VsZixiYWxsTmFtZSx2LGVuZGJhbGx6aG9uZ2RpYW4sc3RhcnRCYWxsLGVuZEJhbGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+enu+mZpOS4pOS4qmJhbGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW1vdmUoRy5CYWxsTGlzdCx0aGlzLkVuZEJhbGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKEcuQmFsbExpc3QpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVtb3ZlKEcuQmFsbExpc3QsdGhpcy5TdGFydEJhbGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5TdGFydEJhbGwgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLkVuZEJhbGwgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL+WIpOaWreaYr+WQpuaIkOWKn+mAmuWFs1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRvbmdndWFuID0gdGhpcy5DaGVja1RvbmdHdWFuKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBpZih0b25nZ3Vhbil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKT0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuU3RhcnROZXh0TGV2ZWwoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sNCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8v5Yqg6L295Y+m5aSW5LiA5LiqZHJhd+OAguimhuebluWcqOW9k+WJjWRyYXfkuYvkuIpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwicGVyZmFiL2RyYXdcIiwgZnVuY3Rpb24gKGVyciwgcHJlZmFiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwi5oiQ5Yqf57uY5Yi25LiA5p2h57q/XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuRHJhdy5hZGRDaGlsZChuZXdOb2RlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9ICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgLy/ms6jplIDnp7vliqjkuovku7bnmoTnm5HlkKxcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS5vZmYoY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfTU9WRSwgbnVsbCwgdGhpcyk7XHJcbiAgICAgICAgICAgICAgIC8v5pi+56S6d3JvbmdcclxuICAgICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhub2RlX3ZlYzIpXHJcbiAgICAgICAgICAgICAgIHRoaXMuc2hvd1dyb25nSWNvbih2ZWMyKTtcclxuICAgICAgICAgICAgICAgLy/muIXpmaTnlLvnur9cclxuICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5HcmFwaGljcykuY2xlYXIoKTtcclxuICAgICAgICAgICAgICAgdGhpcy5zdG9ya2VsaXN0ID0gW107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgIH0sIHRoaXMpO1xyXG4gICAgfSxcclxuICAgIGRpYW56aU1vdmUoc2VsZixuYW1lLHN0YXJ0UG9zLGVuZFBvcyxzdGFydEJhbGwsZW5kQmFsbCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGUoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZihHYW1lTWdyLmdhbWVPdmVyPT10cnVlKXJldHVybjtcclxuICAgICAgICAgICAgdmFyIG5ld2R6Tm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMuZHopO1xyXG4gICAgICAgICAgICBzZWxmLkRyYXcuYWRkQ2hpbGQobmV3ZHpOb2RlKTtcclxuICAgICAgICAgICAgbmV3ZHpOb2RlLnNjYWxlID0gbmV3IGNjLlZlYzMoMC43LDAuNywwLjcpO1xyXG4gICAgICAgICAgICBuZXdkek5vZGUuZ2V0Q29tcG9uZW50KCdEaWFuemlNb3ZlJykuaW5pdCh0aGlzLnN0b3JrZWxpc3Qsc2VsZiwgbmFtZSk7XHJcbiAgICAgICAgICAgIG5ld2R6Tm9kZS5nZXRDb21wb25lbnQoJ0RpYW56aU1vdmUnKS5zdGFydE1vdmUodGhpcy5zdG9ya2VsaXN0LCBzZWxmLHN0YXJ0UG9zLGVuZFBvcyxzdGFydEJhbGwsZW5kQmFsbCk7XHJcbiAgICAgICAgfSwgMC4yNSk7XHJcbiAgICB9LFxyXG4gICAgc3Ryb2tlX2NvbG9yKHN0cil7XHJcbiAgICAgICAgc3dpdGNoKHN0cil7XHJcbiAgICAgICAgICAgIGNhc2UgJ3JlZGJhbGwnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICBjYy5Db2xvci5SRUQ7XHJcbiAgICAgICAgICAgIGNhc2UgJ3llbGxvd2JhbGwnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICBjYy5Db2xvci5ZRUxMT1c7XHJcbiAgICAgICAgICAgIGNhc2UgJ2JsdWViYWxsJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiAgY2MuQ29sb3IuQkxVRTtcclxuICAgICAgICAgICAgY2FzZSAncHVycGxlYmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gIG5ldyBjYy5Db2xvcigyMDYsMTQsMjM3LDI1NSk7XHJcbiAgICAgICAgICAgIGNhc2UgJ2dyZWVuYmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gIGNjLkNvbG9yLkdSRUVOO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICBjYy5Db2xvci5CTEFDSztcclxuICAgICAgICB9XHJcblxyXG4gICAgfSxcclxuICAgIHJlbW92ZShsaXN0LGl0ZW0pe1xyXG4gICAgICAgIGZvcihsZXQgaT0wOyBpPCBsaXN0Lmxlbmd0aDtpKyspe1xyXG4gICAgICAgICAgICBpZiAobGlzdFtpXSA9PSBpdGVtKXtcclxuICAgICAgICAgICAgICAgcmV0dXJuIGxpc3Quc3BsaWNlKGksMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgc2hvd1dyb25nSWNvbihwb3Mpe1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBjYy5yZXNvdXJjZXMubG9hZChcInBlcmZhYi93cm9uZ1wiLCBmdW5jdGlvbiAoZXJyLCBwcmVmYWIpIHtcclxuICAgICAgICAgICAgbGV0IG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xyXG4gICAgICAgICAgICBuZXdOb2RlLnBhcmVudCA9IGNjLmZpbmQoJ0NhbnZhcy9USVBTJyk7XHJcbiAgICAgICAgICAgIG5ld05vZGUuc2NhbGU9MS41O1xyXG4gICAgICAgICAgICBsZXQgbm9kZV92ZWMyID0gY2MuZmluZCgnQ2FudmFzL1RJUFMnKS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihwb3MpO1xyXG4gICAgICAgICAgICBuZXdOb2RlLnNldFBvc2l0aW9uKG5vZGVfdmVjMik7XHJcbiAgICAgICAgICAgIC8v6Z+z5pWIXHJcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3Qoc2VsZi5mYWlsLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgIG5ld05vZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBuZXdOb2RlLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9LDUwMClcclxuICAgICAgICB9KTtcclxuICAgIH0sXHJcbiAgICBzaG93UmlnaHRJY29uKCl7XHJcbiAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwicGVyZmFiL3JpZ2h0XCIsIGZ1bmN0aW9uIChlcnIsIHByZWZhYikge1xyXG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhlcnIpXHJcbiAgICAgICAgICAgIGxldCByaWdodCA9IGNjLmluc3RhbnRpYXRlKHByZWZhYik7XHJcbiAgICAgICAgICAgIGxldCBnYW1lU3RhcnQgPSBjYy5maW5kKCdDYW52YXMvTWFpbkJHJykuZ2V0Q29tcG9uZW50KCdHYW1lU3RhcnQnKTtcclxuICAgICAgICAgICAgcmlnaHQucGFyZW50ID0gY2MuZmluZCgnQ2FudmFzL1RJUFMnKTtcclxuICAgICAgICAgICAgLy9sZXQgbm9kZV92ZWMyID0gY2MuZmluZCgnQ2FudmFzL1RJUFMnKS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihjYy52Mihwb3NbMF0scG9zWzFdKSk7XHJcbiAgICAgICAgICAgIHJpZ2h0LnNldFBvc2l0aW9uKDAsMTkxKTtcclxuICAgICAgICAgICAgLy/pn7PmlYhcclxuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChzZWxmLnBhc3MsIGZhbHNlKTtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgcmlnaHQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICByaWdodC5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgICAgICAvL+ebtOaOpei/m+WFpeS4i+S4gOWFs1xyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBnYW1lU3RhcnQuemh1emkxLnNldFBvc2l0aW9uKDM5NSwgMzg0LCAwKTtcclxuICAgICAgICAgICAgICAgIGdhbWVTdGFydC56aHV6aTEuYW5nbGUgPSAwO1xyXG4gICAgICAgICAgICAgICAgZ2FtZVN0YXJ0LnpodXppMi5zZXRQb3NpdGlvbigtMzk2LCAtNiwgMCk7XHJcbiAgICAgICAgICAgICAgICBnYW1lU3RhcnQuemh1emkyLmFuZ2xlID0gMTgwO1xyXG5cclxuICAgICAgICAgICAgICAgIGdhbWVTdGFydC5zdGFydCgpO1xyXG4gICAgICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL01haW5CRy9EcmF3JykucmVtb3ZlQWxsQ2hpbGRyZW4oKTtcclxuICAgICAgICAgICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwicGVyZmFiL2RyYXdcIiwgZnVuY3Rpb24gKGVyciwgcHJlZmFiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xyXG4gICAgICAgICAgICAgICAgICAgIG5ld05vZGUucGFyZW50ID0gY2MuZmluZCgnQ2FudmFzL01haW5CRy9EcmF3Jyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSwyNTAwKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIFN0YXJ0TmV4dExldmVsKCl7XHJcbiAgICAgICAgaWYoR2FtZU1nci5nYW1lT3Zlcj09dHJ1ZSlyZXR1cm47XHJcbiAgICAgICAgbGV0IGxldmVsID0gU3RvcmFnZS5HZXRfSW5mbygnbGV2ZWwnKTtcclxuICAgICAgICBTdG9yYWdlLlNldF9JbmZvKCdsZXZlbCcsTnVtYmVyKGxldmVsKSsxKTtcclxuICAgICAgICB0aGlzLnNob3dSaWdodEljb24oKTtcclxuICAgIH0sXHJcbiAgICBjbG9zZWd1aWRlKCl7XHJcbiAgICAgICAgY2MuZmluZCgnQ2FudmFzL01haW5CRy90aXAnKS5nZXRDb21wb25lbnQoJ25ld3VzZXJfZ3VpZGUnKS5jbG9zZSgpO1xyXG4gICAgfSxcclxuICAgIG9wZW5ndWlkZSgpe1xyXG4gICAgICAgIGNjLmZpbmQoJ0NhbnZhcy9NYWluQkcvdGlwJykuZ2V0Q29tcG9uZW50KCduZXd1c2VyX2d1aWRlJykuc2hvd2d1aWRlbGlzdF9hZ2FpbigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9LFxyXG59KTtcclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/next_level.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c13a9ud2dhNwJ0nXd0mJQtS', 'next_level');
// scripts/next_level.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    Draw: cc.Node,
    ButtonAudio: cc.AudioClip
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {// this.Draw = cc.find('Canvas/MainBG/Draw')
  },
  start: function start() {
    var self = this;
    this.node.on('click', function (button) {
      //The event is a custom event, you could get the Button component via first argument

      /*通过模块化脚本操作
      var GameStart = require("GameStart");
      new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
      */

      /*通过找组件操作
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      */

      /*直接重新加载场景
      cc.director.loadScene("Home");
      */
      console.log('xiayiguanchufale');
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      cc.find('Canvas/TIPS/PASS_BG').active = false;
      self.Draw.removeAllChildren();
      cc.resources.load("perfab/draw", function (err, prefab) {
        var newNode = cc.instantiate(prefab);
        newNode.parent = self.Draw;
      }); //音效

      cc.audioEngine.playEffect(self.ButtonAudio, false);
      var level = Storage.Get_Info('level');

      if (Number(level) % 2 == 0) {
        G.ShowInterstitialAD();
      } // G.AnalysticCustomEvent('LEVEL', level);

    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcbmV4dF9sZXZlbC5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsIkRyYXciLCJOb2RlIiwiQnV0dG9uQXVkaW8iLCJBdWRpb0NsaXAiLCJvbkxvYWQiLCJzdGFydCIsInNlbGYiLCJub2RlIiwib24iLCJidXR0b24iLCJjb25zb2xlIiwibG9nIiwiZmluZCIsImdldENvbXBvbmVudCIsImFjdGl2ZSIsInJlbW92ZUFsbENoaWxkcmVuIiwicmVzb3VyY2VzIiwibG9hZCIsImVyciIsInByZWZhYiIsIm5ld05vZGUiLCJpbnN0YW50aWF0ZSIsInBhcmVudCIsImF1ZGlvRW5naW5lIiwicGxheUVmZmVjdCIsImxldmVsIiwiU3RvcmFnZSIsIkdldF9JbmZvIiwiTnVtYmVyIiwiRyIsIlNob3dJbnRlcnN0aXRpYWxBRCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLElBQUksRUFBQ0osRUFBRSxDQUFDSyxJQURBO0lBRVJDLFdBQVcsRUFBQ04sRUFBRSxDQUFDTztFQUZQLENBSFA7RUFRTDtFQUVBQyxNQVZLLG9CQVVLLENBQ047RUFDSCxDQVpJO0VBY0xDLEtBZEssbUJBY0k7SUFDTCxJQUFJQyxJQUFJLEdBQUcsSUFBWDtJQUNBLEtBQUtDLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBVUMsTUFBVixFQUFrQjtNQUNwQzs7TUFDQTtBQUNaO0FBQ0E7QUFDQTs7TUFDWTtBQUNaO0FBQ0E7O01BQ1k7QUFDWjtBQUNBO01BQ1lDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGtCQUFaO01BQ0FmLEVBQUUsQ0FBQ2dCLElBQUgsQ0FBUSxlQUFSLEVBQXlCQyxZQUF6QixDQUFzQyxXQUF0QyxFQUFtRFIsS0FBbkQ7TUFDQVQsRUFBRSxDQUFDZ0IsSUFBSCxDQUFRLHFCQUFSLEVBQStCRSxNQUEvQixHQUF3QyxLQUF4QztNQUNBUixJQUFJLENBQUNOLElBQUwsQ0FBVWUsaUJBQVY7TUFDQW5CLEVBQUUsQ0FBQ29CLFNBQUgsQ0FBYUMsSUFBYixDQUFrQixhQUFsQixFQUFpQyxVQUFVQyxHQUFWLEVBQWVDLE1BQWYsRUFBdUI7UUFDcEQsSUFBSUMsT0FBTyxHQUFHeEIsRUFBRSxDQUFDeUIsV0FBSCxDQUFlRixNQUFmLENBQWQ7UUFDQUMsT0FBTyxDQUFDRSxNQUFSLEdBQWlCaEIsSUFBSSxDQUFDTixJQUF0QjtNQUNGLENBSEYsRUFoQm9DLENBb0JwQzs7TUFDREosRUFBRSxDQUFDMkIsV0FBSCxDQUFlQyxVQUFmLENBQTBCbEIsSUFBSSxDQUFDSixXQUEvQixFQUE0QyxLQUE1QztNQUNBLElBQUl1QixLQUFLLEdBQUdDLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQixPQUFqQixDQUFaOztNQUNDLElBQUdDLE1BQU0sQ0FBQ0gsS0FBRCxDQUFOLEdBQWdCLENBQWhCLElBQXFCLENBQXhCLEVBQTBCO1FBQ2xDSSxDQUFDLENBQUNDLGtCQUFGO01BQ1MsQ0F6Qm1DLENBMEJwQzs7SUFDRixDQTNCRjtFQTRCSCxDQTVDSSxDQThDTDs7QUE5Q0ssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gY2MuQ2xhc3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcclxuLy8gTGVhcm4gQXR0cmlidXRlOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXHJcblxyXG5jYy5DbGFzcyh7XHJcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXHJcblxyXG4gICAgcHJvcGVydGllczoge1xyXG4gICAgICAgIERyYXc6Y2MuTm9kZSxcclxuICAgICAgICBCdXR0b25BdWRpbzpjYy5BdWRpb0NsaXAsXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIG9uTG9hZCAoKSB7XHJcbiAgICAgICAgLy8gdGhpcy5EcmF3ID0gY2MuZmluZCgnQ2FudmFzL01haW5CRy9EcmF3JylcclxuICAgIH0sXHJcblxyXG4gICAgc3RhcnQgKCkge1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICB0aGlzLm5vZGUub24oJ2NsaWNrJywgZnVuY3Rpb24gKGJ1dHRvbikge1xyXG4gICAgICAgICAgICAvL1RoZSBldmVudCBpcyBhIGN1c3RvbSBldmVudCwgeW91IGNvdWxkIGdldCB0aGUgQnV0dG9uIGNvbXBvbmVudCB2aWEgZmlyc3QgYXJndW1lbnRcclxuICAgICAgICAgICAgLyrpgJrov4fmqKHlnZfljJbohJrmnKzmk43kvZxcclxuICAgICAgICAgICAgdmFyIEdhbWVTdGFydCA9IHJlcXVpcmUoXCJHYW1lU3RhcnRcIik7XHJcbiAgICAgICAgICAgIG5ldyBHYW1lU3RhcnQoKS5zdGFydCgpOyAvLyDorr/pl67pnZnmgIHlj5jph4/kvJrmiqXplJnvvIzph4fnlKjnu4Tku7bmn6Xmib7nmoTmlrnms5VcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgLyrpgJrov4fmib7nu4Tku7bmk43kvZxcclxuICAgICAgICAgICAgY2MuZmluZCgnQ2FudmFzL01haW5CRycpLmdldENvbXBvbmVudCgnR2FtZVN0YXJ0Jykuc3RhcnQoKTtcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgLyrnm7TmjqXph43mlrDliqDovb3lnLrmma9cclxuICAgICAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKFwiSG9tZVwiKTtcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ3hpYXlpZ3VhbmNodWZhbGUnKVxyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHJykuZ2V0Q29tcG9uZW50KCdHYW1lU3RhcnQnKS5zdGFydCgpO1xyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvVElQUy9QQVNTX0JHJykuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHNlbGYuRHJhdy5yZW1vdmVBbGxDaGlsZHJlbigpO1xyXG4gICAgICAgICAgICBjYy5yZXNvdXJjZXMubG9hZChcInBlcmZhYi9kcmF3XCIsIGZ1bmN0aW9uIChlcnIsIHByZWZhYikge1xyXG4gICAgICAgICAgICAgICAgdmFyIG5ld05vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xyXG4gICAgICAgICAgICAgICAgbmV3Tm9kZS5wYXJlbnQgPSBzZWxmLkRyYXc7XHJcbiAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgLy/pn7PmlYhcclxuICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHNlbGYuQnV0dG9uQXVkaW8sIGZhbHNlKTtcclxuICAgICAgICAgICBsZXQgbGV2ZWwgPSBTdG9yYWdlLkdldF9JbmZvKCdsZXZlbCcpO1xyXG4gICAgICAgICAgICBpZihOdW1iZXIobGV2ZWwpICUgMiA9PSAwKXtcclxuXHRcdFx0XHRHLlNob3dJbnRlcnN0aXRpYWxBRCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIEcuQW5hbHlzdGljQ3VzdG9tRXZlbnQoJ0xFVkVMJywgbGV2ZWwpO1xyXG4gICAgICAgICB9KVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyB1cGRhdGUgKGR0KSB7fSxcclxufSk7XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/testlevel.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6cb6bzgeEdAsZBWFQOu1leV', 'testlevel');
// scripts/testlevel.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    editlevel: cc.Node
  },
  // LIFE-CYCLE CALLBACKS:
  // onLoad () {},
  start: function start() {
    var str1 = {
      "tips": "300",
      "bg": "yuan",
      "balls": [{
        "color": "red",
        "Pos": [{
          "x": 123,
          "y": 123
        }, {
          "x": 234,
          "y": 234
        }]
      }, {
        "color": "green",
        "Pos": [{
          "x": 0,
          "y": 340
        }, {
          "x": 0,
          "y": -100
        }]
      }, {
        "color": "yellow",
        "Pos": [{
          "x": 0,
          "y": -350
        }, {
          "x": 0,
          "y": 100
        }]
      }]
    };
    var obj = JSON.stringify(str1, null, '\t'); //this.saveForBrowser(obj,"level");

    this.node.on('click', function () {
      cc.resources.load("perfab/draw", function (err, prefab) {
        var newNode = cc.instantiate(prefab);
        newNode.parent = cc.find('Canvas/MainBG/Draw');
      });
    });
    this.editlevel.on('click', function () {
      cc.find('Canvas/MainBG/Draw').removeAllChildren();
      G.reset();
      G.BallList = cc.find('Canvas/MainBG/BG').children.concat();
    });
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcdGVzdGxldmVsLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiZWRpdGxldmVsIiwiTm9kZSIsInN0YXJ0Iiwic3RyMSIsIm9iaiIsIkpTT04iLCJzdHJpbmdpZnkiLCJub2RlIiwib24iLCJyZXNvdXJjZXMiLCJsb2FkIiwiZXJyIiwicHJlZmFiIiwibmV3Tm9kZSIsImluc3RhbnRpYXRlIiwicGFyZW50IiwiZmluZCIsInJlbW92ZUFsbENoaWxkcmVuIiwiRyIsInJlc2V0IiwiQmFsbExpc3QiLCJjaGlsZHJlbiIsImNvbmNhdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7RUFDTCxXQUFTRCxFQUFFLENBQUNFLFNBRFA7RUFHTEMsVUFBVSxFQUFFO0lBQ1JDLFNBQVMsRUFBQ0osRUFBRSxDQUFDSztFQURMLENBSFA7RUFPTDtFQUVBO0VBRUFDLEtBWEssbUJBV0k7SUFDTCxJQUFJQyxJQUFJLEdBQUc7TUFBQyxRQUFPLEtBQVI7TUFDWCxNQUFLLE1BRE07TUFFWCxTQUFRLENBQ0o7UUFDSSxTQUFRLEtBRFo7UUFFSSxPQUFNLENBQUM7VUFBQyxLQUFJLEdBQUw7VUFBUyxLQUFJO1FBQWIsQ0FBRCxFQUFtQjtVQUFDLEtBQUksR0FBTDtVQUFTLEtBQUk7UUFBYixDQUFuQjtNQUZWLENBREksRUFLSjtRQUNDLFNBQVEsT0FEVDtRQUVDLE9BQU0sQ0FBQztVQUFDLEtBQUksQ0FBTDtVQUFPLEtBQUk7UUFBWCxDQUFELEVBQWlCO1VBQUMsS0FBSSxDQUFMO1VBQU8sS0FBSSxDQUFDO1FBQVosQ0FBakI7TUFGUCxDQUxJLEVBU0g7UUFDQyxTQUFRLFFBRFQ7UUFFQyxPQUFNLENBQUM7VUFBQyxLQUFJLENBQUw7VUFBTyxLQUFJLENBQUM7UUFBWixDQUFELEVBQWtCO1VBQUMsS0FBSSxDQUFMO1VBQU8sS0FBSTtRQUFYLENBQWxCO01BRlAsQ0FURztJQUZHLENBQVg7SUFnQkEsSUFBSUMsR0FBRyxHQUFDQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUgsSUFBZixFQUFvQixJQUFwQixFQUF5QixJQUF6QixDQUFSLENBakJLLENBa0JMOztJQUNBLEtBQUtJLElBQUwsQ0FBVUMsRUFBVixDQUFhLE9BQWIsRUFBcUIsWUFBVTtNQUMzQlosRUFBRSxDQUFDYSxTQUFILENBQWFDLElBQWIsQ0FBa0IsYUFBbEIsRUFBaUMsVUFBVUMsR0FBVixFQUFlQyxNQUFmLEVBQXVCO1FBQ3BELElBQUlDLE9BQU8sR0FBR2pCLEVBQUUsQ0FBQ2tCLFdBQUgsQ0FBZUYsTUFBZixDQUFkO1FBQ0FDLE9BQU8sQ0FBQ0UsTUFBUixHQUFpQm5CLEVBQUUsQ0FBQ29CLElBQUgsQ0FBUSxvQkFBUixDQUFqQjtNQUNGLENBSEY7SUFJSCxDQUxEO0lBTUEsS0FBS2hCLFNBQUwsQ0FBZVEsRUFBZixDQUFrQixPQUFsQixFQUEwQixZQUFVO01BQ2hDWixFQUFFLENBQUNvQixJQUFILENBQVEsb0JBQVIsRUFBOEJDLGlCQUE5QjtNQUNBQyxDQUFDLENBQUNDLEtBQUY7TUFDQUQsQ0FBQyxDQUFDRSxRQUFGLEdBQWF4QixFQUFFLENBQUNvQixJQUFILENBQVEsa0JBQVIsRUFBNEJLLFFBQTVCLENBQXFDQyxNQUFyQyxFQUFiO0lBQ0gsQ0FKRDtFQUtILENBekNJLENBNENMOztBQTVDSyxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgZWRpdGxldmVsOmNjLk5vZGUsXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIC8vIG9uTG9hZCAoKSB7fSxcclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdmFyIHN0cjEgPSB7XCJ0aXBzXCI6XCIzMDBcIixcclxuICAgICAgICBcImJnXCI6XCJ5dWFuXCIsXHJcbiAgICAgICAgXCJiYWxsc1wiOltcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgXCJjb2xvclwiOlwicmVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIlBvc1wiOlt7XCJ4XCI6MTIzLFwieVwiOjEyM30se1wieFwiOjIzNCxcInlcIjoyMzR9XVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICBcImNvbG9yXCI6XCJncmVlblwiLFxyXG4gICAgICAgICAgICAgXCJQb3NcIjpbe1wieFwiOjAsXCJ5XCI6MzQwfSx7XCJ4XCI6MCxcInlcIjotMTAwfV1cclxuICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgXCJjb2xvclwiOlwieWVsbG93XCIsXHJcbiAgICAgICAgICAgICAgXCJQb3NcIjpbe1wieFwiOjAsXCJ5XCI6LTM1MH0se1wieFwiOjAsXCJ5XCI6MTAwfV1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgXX07XHJcbiAgICAgICAgbGV0IG9iaj1KU09OLnN0cmluZ2lmeShzdHIxLG51bGwsJ1xcdCcpXHJcbiAgICAgICAgLy90aGlzLnNhdmVGb3JCcm93c2VyKG9iaixcImxldmVsXCIpO1xyXG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwicGVyZmFiL2RyYXdcIiwgZnVuY3Rpb24gKGVyciwgcHJlZmFiKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgbmV3Tm9kZSA9IGNjLmluc3RhbnRpYXRlKHByZWZhYik7XHJcbiAgICAgICAgICAgICAgICBuZXdOb2RlLnBhcmVudCA9IGNjLmZpbmQoJ0NhbnZhcy9NYWluQkcvRHJhdycpO1xyXG4gICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5lZGl0bGV2ZWwub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0RyYXcnKS5yZW1vdmVBbGxDaGlsZHJlbigpO1xyXG4gICAgICAgICAgICBHLnJlc2V0KCk7XHJcbiAgICAgICAgICAgIEcuQmFsbExpc3QgPSBjYy5maW5kKCdDYW52YXMvTWFpbkJHL0JHJykuY2hpbGRyZW4uY29uY2F0KCk7XHJcbiAgICAgICAgfSlcclxuICAgIH0sXHJcbiAgIFxyXG5cclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9LFxyXG59KTtcclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/DianziMove.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0571e5EiyhCjJDWYBkC+u9Q', 'DianziMove');
// scripts/DianziMove.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("./GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DianziMove = /** @class */ (function (_super) {
    __extends(DianziMove, _super);
    function DianziMove() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ID = -1;
        _this.waypoint_id = 0;
        return _this;
    }
    DianziMove.prototype.init = function (storkelist, self, name) {
        var _this = this;
        var spName = this.stroke_color(name);
        this.ID = this.getIDByColor(spName);
        cc.resources.load("dzs/" + spName, cc.SpriteFrame, function (err, sf) {
            _this.node.getComponent(cc.Sprite).spriteFrame = sf;
        });
        var canvasPos1 = storkelist[0];
        var worldPos1 = self.node.convertToWorldSpaceAR(canvasPos1);
        var nodePos1 = self.Draw.convertToNodeSpaceAR(worldPos1);
        this.node.setPosition(nodePos1);
    };
    DianziMove.prototype.startMove = function (storkelist, self, startPos, endPos, startBall, endBall) {
        var _this = this;
        if (GameMgr_1.default.gameOver == true)
            return;
        if (this.waypoint_id >= storkelist.length) {
            cc.game.emit("show_ball_mask", startPos, endPos, this.ID, startBall, endBall);
            this.node.destroy();
            return;
        }
        var canvasPos = storkelist[this.waypoint_id];
        var worldPos = self.node.convertToWorldSpaceAR(canvasPos);
        var nodePos = self.Draw.convertToNodeSpaceAR(worldPos);
        cc.tween(this.node).to(0.02, { position: nodePos }).call(function () {
            _this.waypoint_id += 2;
            _this.startMove(storkelist, self, startPos, endPos, startBall, endBall);
        }).start();
    };
    DianziMove.prototype.getIDByColor = function (colorName) {
        switch (colorName) {
            case "dz-red":
                return 0;
            case "dz-yellow":
                return 1;
            case "dz-blue":
                return 2;
            case "dz-green":
                return 3;
        }
    };
    DianziMove.prototype.stroke_color = function (str) {
        switch (str) {
            case 'redball':
                return "dz-red";
            case 'yellowball':
                return "dz-yellow";
            case 'blueball':
                return "dz-blue";
            case 'greenball':
                return "dz-green";
        }
    };
    DianziMove.prototype.onCollisionEnter = function (other, self) {
        if (other.node.getComponent('DianziMove').ID != this.ID) {
            GameMgr_1.default.gameOver = true;
            cc.game.emit("gameover", this.node, this.node.parent);
        }
    };
    DianziMove = __decorate([
        ccclass
    ], DianziMove);
    return DianziMove;
}(cc.Component));
exports.default = DianziMove;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcRGlhbnppTW92ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLDRFQUE0RTtBQUM1RSxtQkFBbUI7QUFDbkIsc0ZBQXNGO0FBQ3RGLDhCQUE4QjtBQUM5QixzRkFBc0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUV0RixxQ0FBZ0M7QUFFMUIsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBd0MsOEJBQVk7SUFBcEQ7UUFBQSxxRUFtRUM7UUFqRUcsUUFBRSxHQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ2IsaUJBQVcsR0FBUSxDQUFDLENBQUM7O0lBZ0V6QixDQUFDO0lBOURHLHlCQUFJLEdBQUosVUFBSyxVQUFVLEVBQUMsSUFBSSxFQUFDLElBQUk7UUFBekIsaUJBVUM7UUFURyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxFQUFFLEdBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUMsTUFBTSxFQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUMsVUFBQyxHQUFHLEVBQUMsRUFBaUI7WUFDakUsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBQyxFQUFFLENBQUM7UUFDckQsQ0FBQyxDQUFDLENBQUE7UUFDRixJQUFJLFVBQVUsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDL0IsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM1RCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCw4QkFBUyxHQUFULFVBQVUsVUFBVSxFQUFDLElBQUksRUFBQyxRQUFRLEVBQUMsTUFBTSxFQUFDLFNBQWlCLEVBQUMsT0FBZTtRQUEzRSxpQkFlQztRQWRHLElBQUcsaUJBQU8sQ0FBQyxRQUFRLElBQUUsSUFBSTtZQUFDLE9BQU87UUFDakMsSUFBRyxJQUFJLENBQUMsV0FBVyxJQUFFLFVBQVUsQ0FBQyxNQUFNLEVBQUM7WUFDbkMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUMsUUFBUSxFQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLFNBQVMsRUFBQyxPQUFPLENBQUMsQ0FBQztZQUV6RSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3BCLE9BQU87U0FDVjtRQUNELElBQUksU0FBUyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDN0MsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDckQsS0FBSSxDQUFDLFdBQVcsSUFBRSxDQUFDLENBQUM7WUFDcEIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUMsSUFBSSxFQUFDLFFBQVEsRUFBQyxNQUFNLEVBQUMsU0FBUyxFQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3RFLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVELGlDQUFZLEdBQVosVUFBYSxTQUFTO1FBQ2xCLFFBQVEsU0FBUyxFQUFFO1lBQ2YsS0FBSyxRQUFRO2dCQUNULE9BQU8sQ0FBQyxDQUFDO1lBQ2IsS0FBSyxXQUFXO2dCQUNaLE9BQU8sQ0FBQyxDQUFDO1lBQ2IsS0FBSyxTQUFTO2dCQUNWLE9BQU8sQ0FBQyxDQUFDO1lBQ2IsS0FBSyxVQUFVO2dCQUNYLE9BQU8sQ0FBQyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQztJQUVELGlDQUFZLEdBQVosVUFBYSxHQUFHO1FBQ1osUUFBTyxHQUFHLEVBQUM7WUFDUCxLQUFLLFNBQVM7Z0JBQ1YsT0FBTyxRQUFRLENBQUM7WUFDcEIsS0FBSyxZQUFZO2dCQUNiLE9BQU8sV0FBVyxDQUFDO1lBQ3ZCLEtBQUssVUFBVTtnQkFDWCxPQUFPLFNBQVMsQ0FBQTtZQUNwQixLQUFLLFdBQVc7Z0JBQ1osT0FBTyxVQUFVLENBQUE7U0FDeEI7SUFFTCxDQUFDO0lBRUQscUNBQWdCLEdBQWhCLFVBQWlCLEtBQUssRUFBRSxJQUFJO1FBQ3hCLElBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUM7WUFDbkQsaUJBQU8sQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDO1lBQ3RCLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBQyxJQUFJLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDdEQ7SUFDTCxDQUFDO0lBbEVnQixVQUFVO1FBRDlCLE9BQU87T0FDYSxVQUFVLENBbUU5QjtJQUFELGlCQUFDO0NBbkVELEFBbUVDLENBbkV1QyxFQUFFLENBQUMsU0FBUyxHQW1FbkQ7a0JBbkVvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yLzIuNC9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci8yLjQvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXHJcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvMi40L21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4vR2FtZU1nclwiO1xyXG5cclxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEaWFuemlNb3ZlIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuXHJcbiAgICBJRDpudW1iZXI9LTE7XHJcbiAgICB3YXlwb2ludF9pZDpudW1iZXI9MDtcclxuXHJcbiAgICBpbml0KHN0b3JrZWxpc3Qsc2VsZixuYW1lKXtcclxuICAgICAgICB2YXIgc3BOYW1lID0gdGhpcy5zdHJva2VfY29sb3IobmFtZSk7XHJcbiAgICAgICAgdGhpcy5JRD10aGlzLmdldElEQnlDb2xvcihzcE5hbWUpO1xyXG4gICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwiZHpzL1wiK3NwTmFtZSxjYy5TcHJpdGVGcmFtZSwoZXJyLHNmOmNjLlNwcml0ZUZyYW1lKT0+e1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWU9c2Y7XHJcbiAgICAgICAgfSlcclxuICAgICAgICB2YXIgY2FudmFzUG9zMSA9IHN0b3JrZWxpc3RbMF07XHJcbiAgICAgICAgdmFyIHdvcmxkUG9zMSA9IHNlbGYubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoY2FudmFzUG9zMSk7XHJcbiAgICAgICAgdmFyIG5vZGVQb3MxID0gc2VsZi5EcmF3LmNvbnZlcnRUb05vZGVTcGFjZUFSKHdvcmxkUG9zMSk7XHJcbiAgICAgICAgdGhpcy5ub2RlLnNldFBvc2l0aW9uKG5vZGVQb3MxKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydE1vdmUoc3RvcmtlbGlzdCxzZWxmLHN0YXJ0UG9zLGVuZFBvcyxzdGFydEJhbGw6Y2MuTm9kZSxlbmRCYWxsOmNjLk5vZGUpe1xyXG4gICAgICAgIGlmKEdhbWVNZ3IuZ2FtZU92ZXI9PXRydWUpcmV0dXJuO1xyXG4gICAgICAgIGlmKHRoaXMud2F5cG9pbnRfaWQ+PXN0b3JrZWxpc3QubGVuZ3RoKXtcclxuICAgICAgICAgICAgY2MuZ2FtZS5lbWl0KFwic2hvd19iYWxsX21hc2tcIixzdGFydFBvcyxlbmRQb3MsdGhpcy5JRCxzdGFydEJhbGwsZW5kQmFsbCk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciBjYW52YXNQb3MgPSBzdG9ya2VsaXN0W3RoaXMud2F5cG9pbnRfaWRdO1xyXG4gICAgICAgIHZhciB3b3JsZFBvcyA9IHNlbGYubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoY2FudmFzUG9zKTtcclxuICAgICAgICB2YXIgbm9kZVBvcyA9IHNlbGYuRHJhdy5jb252ZXJ0VG9Ob2RlU3BhY2VBUih3b3JsZFBvcyk7XHJcbiAgICAgICAgY2MudHdlZW4odGhpcy5ub2RlKS50bygwLjAyLCB7IHBvc2l0aW9uOiBub2RlUG9zIH0pLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLndheXBvaW50X2lkKz0yO1xyXG4gICAgICAgICAgICB0aGlzLnN0YXJ0TW92ZShzdG9ya2VsaXN0LHNlbGYsc3RhcnRQb3MsZW5kUG9zLHN0YXJ0QmFsbCxlbmRCYWxsKTtcclxuICAgICAgICB9KS5zdGFydCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldElEQnlDb2xvcihjb2xvck5hbWUpe1xyXG4gICAgICAgIHN3aXRjaCAoY29sb3JOYW1lKSB7XHJcbiAgICAgICAgICAgIGNhc2UgXCJkei1yZWRcIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiAwO1xyXG4gICAgICAgICAgICBjYXNlIFwiZHoteWVsbG93XCI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gMTtcclxuICAgICAgICAgICAgY2FzZSBcImR6LWJsdWVcIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiAyO1xyXG4gICAgICAgICAgICBjYXNlIFwiZHotZ3JlZW5cIjpcclxuICAgICAgICAgICAgICAgIHJldHVybiAzO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdHJva2VfY29sb3Ioc3RyKXtcclxuICAgICAgICBzd2l0Y2goc3RyKXtcclxuICAgICAgICAgICAgY2FzZSAncmVkYmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJkei1yZWRcIjtcclxuICAgICAgICAgICAgY2FzZSAneWVsbG93YmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJkei15ZWxsb3dcIjtcclxuICAgICAgICAgICAgY2FzZSAnYmx1ZWJhbGwnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiZHotYmx1ZVwiXHJcbiAgICAgICAgICAgIGNhc2UgJ2dyZWVuYmFsbCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJkei1ncmVlblwiXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuICAgIFxyXG4gICAgb25Db2xsaXNpb25FbnRlcihvdGhlcizCoHNlbGYpwqB7XHJcbiAgICAgICAgaWYob3RoZXIubm9kZS5nZXRDb21wb25lbnQoJ0RpYW56aU1vdmUnKS5JRCAhPSB0aGlzLklEKXtcclxuICAgICAgICAgICAgR2FtZU1nci5nYW1lT3Zlcj10cnVlO1xyXG4gICAgICAgICAgICBjYy5nYW1lLmVtaXQoXCJnYW1lb3ZlclwiLHRoaXMubm9kZSx0aGlzLm5vZGUucGFyZW50KVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/storage.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '725b4q08gtGBp6DhgZUlHGw', 'storage');
// scripts/storage.js

"use strict";

window.Storage = {
  Change_storage: function Change_storage(Type, Number) {
    var Storage_number = Storage.Get_Info(Type);
    Storage_number += Number;
    cc.sys.localStorage.setItem(Type, Storage_number);
  },
  Get_Info: function Get_Info(Type) {
    var starnum = cc.sys.localStorage.getItem(Type);

    if (!starnum) {
      starnum = 0;
    } else {
      starnum = parseInt(starnum);
    }

    return starnum;
  },
  Set_Info: function Set_Info(Type, starnum) {
    cc.sys.localStorage.setItem(Type, starnum);
  }
}; //wx.getOpenDataContext().postMessage({
//     message: Level_Pass_Now
// });
//cc.sys.localStorage.setItem(key, value)
//cc.sys.localStorage.getItem(key)

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcc3RvcmFnZS5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCJTdG9yYWdlIiwiQ2hhbmdlX3N0b3JhZ2UiLCJUeXBlIiwiTnVtYmVyIiwiU3RvcmFnZV9udW1iZXIiLCJHZXRfSW5mbyIsImNjIiwic3lzIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsInN0YXJudW0iLCJnZXRJdGVtIiwicGFyc2VJbnQiLCJTZXRfSW5mbyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsTUFBTSxDQUFDQyxPQUFQLEdBQWlCO0VBQ2JDLGNBRGEsMEJBQ0dDLElBREgsRUFDUUMsTUFEUixFQUNnQjtJQUN6QixJQUFJQyxjQUFjLEdBQUdKLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQkgsSUFBakIsQ0FBckI7SUFDQUUsY0FBYyxJQUFJRCxNQUFsQjtJQUNBRyxFQUFFLENBQUNDLEdBQUgsQ0FBT0MsWUFBUCxDQUFvQkMsT0FBcEIsQ0FBNEJQLElBQTVCLEVBQWlDRSxjQUFqQztFQUNILENBTFk7RUFNYkMsUUFOYSxvQkFNSEgsSUFORyxFQU1FO0lBQ1gsSUFBSVEsT0FBTyxHQUFHSixFQUFFLENBQUNDLEdBQUgsQ0FBT0MsWUFBUCxDQUFvQkcsT0FBcEIsQ0FBNEJULElBQTVCLENBQWQ7O0lBQ0ksSUFBRyxDQUFDUSxPQUFKLEVBQVk7TUFDUkEsT0FBTyxHQUFHLENBQVY7SUFDSCxDQUZELE1BRUs7TUFDREEsT0FBTyxHQUFHRSxRQUFRLENBQUNGLE9BQUQsQ0FBbEI7SUFDSDs7SUFDRCxPQUFPQSxPQUFQO0VBQ1AsQ0FkWTtFQWViRyxRQWZhLG9CQWVIWCxJQWZHLEVBZUVRLE9BZkYsRUFlVTtJQUNuQkosRUFBRSxDQUFDQyxHQUFILENBQU9DLFlBQVAsQ0FBb0JDLE9BQXBCLENBQTRCUCxJQUE1QixFQUFpQ1EsT0FBakM7RUFDSDtBQWpCWSxDQUFqQixFQW1CQTtBQUNBO0FBQ0E7QUFFQTtBQUNBIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJ3aW5kb3cuU3RvcmFnZSA9IHtcclxuICAgIENoYW5nZV9zdG9yYWdlIChUeXBlLE51bWJlcikge1xyXG4gICAgICAgIHZhciBTdG9yYWdlX251bWJlciA9IFN0b3JhZ2UuR2V0X0luZm8oVHlwZSk7XHJcbiAgICAgICAgU3RvcmFnZV9udW1iZXIgKz0gTnVtYmVyO1xyXG4gICAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShUeXBlLFN0b3JhZ2VfbnVtYmVyKTtcclxuICAgIH0sXHJcbiAgICBHZXRfSW5mbyAoVHlwZSl7XHJcbiAgICAgICAgdmFyIHN0YXJudW0gPSBjYy5zeXMubG9jYWxTdG9yYWdlLmdldEl0ZW0oVHlwZSk7XHJcbiAgICAgICAgICAgIGlmKCFzdGFybnVtKXtcclxuICAgICAgICAgICAgICAgIHN0YXJudW0gPSAwO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHN0YXJudW0gPSBwYXJzZUludChzdGFybnVtKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gc3Rhcm51bTtcclxuICAgIH0sXHJcbiAgICBTZXRfSW5mbyAoVHlwZSxzdGFybnVtKXtcclxuICAgICAgICBjYy5zeXMubG9jYWxTdG9yYWdlLnNldEl0ZW0oVHlwZSxzdGFybnVtKTtcclxuICAgIH0sXHJcbn1cclxuLy93eC5nZXRPcGVuRGF0YUNvbnRleHQoKS5wb3N0TWVzc2FnZSh7XHJcbi8vICAgICBtZXNzYWdlOiBMZXZlbF9QYXNzX05vd1xyXG4vLyB9KTtcclxuXHJcbi8vY2Muc3lzLmxvY2FsU3RvcmFnZS5zZXRJdGVtKGtleSwgdmFsdWUpXHJcbi8vY2Muc3lzLmxvY2FsU3RvcmFnZS5nZXRJdGVtKGtleSkiXX0=
//------QC-SOURCE-SPLIT------
