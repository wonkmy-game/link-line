
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/migration/use_v2.1-2.2.1_cc.Toggle_event');
require('./assets/scripts/DianziMove');
require('./assets/scripts/G');
require('./assets/scripts/GameMgr');
require('./assets/scripts/GameStart');
require('./assets/scripts/LevelBtn');
require('./assets/scripts/SelectPanel');
require('./assets/scripts/close_tip');
require('./assets/scripts/draw');
require('./assets/scripts/launch_scene');
require('./assets/scripts/newuser_guide');
require('./assets/scripts/next_level');
require('./assets/scripts/replay');
require('./assets/scripts/storage');
require('./assets/scripts/testlevel');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();