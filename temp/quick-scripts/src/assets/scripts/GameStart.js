"use strict";
cc._RF.push(module, '52952eOu/BK950/ZvvmzZ4x', 'GameStart');
// scripts/GameStart.js

"use strict";

var _GameMgr = _interopRequireDefault(require("./GameMgr"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    BG_Node: cc.Node,
    redball: cc.Prefab,
    yellowball: cc.Prefab,
    greenball: cc.Prefab,
    purpleball: cc.Prefab,
    blueball: cc.Prefab,
    Level: cc.Label,
    BGM: cc.AudioClip,
    backMenu: cc.Node,
    zhuzi1: cc.Node,
    zhuzi2: cc.Node,
    _fx_list: []
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    //播放bgm
    var bgm = cc.audioEngine.playMusic(this.BGM, true);
    cc.audioEngine.setVolume(bgm, 0.15);
    this.backMenu.on(cc.Node.EventType.TOUCH_START, this.backToMenu, this);
    cc.director.getCollisionManager().enabled = true;
    cc.game.on("show_ball_mask", this.showMask, this);
    cc.game.on("gameover", this.OnGameover, this);
  },
  OnGameover: function OnGameover(node, parent) {
    var _this = this;

    var newPos = node.position;
    cc.game.off("gameover");
    node.destroy();
    cc.resources.load("perfab/col_fx", cc.Prefab, function (err, fx) {
      var newFx = cc.instantiate(fx);
      parent.addChild(newFx);
      newFx.scale = 0.5;
      newFx.setPosition(newPos);
    });
    this.scheduleOnce(function () {
      _this.showWrongIcon(new cc.Vec3(newPos.x, newPos.y - 50, 0), parent);

      _this.scheduleOnce(function () {
        _this.backToMenu();
      }, 2);
    }, 2);
  },
  showWrongIcon: function showWrongIcon(pos, parent) {
    var self = this;
    cc.resources.load("perfab/wrong", function (err, prefab) {
      var newNode = cc.instantiate(prefab);
      newNode.parent = parent;
      newNode.setPosition(pos);
      newNode.scale = 1.5;
      setTimeout(function () {
        newNode.active = false;
        newNode.destroy();
      }, 2000);
    });
  },
  showMask: function showMask(startPos, endPos, ballId, startBall, endBall) {
    var self = this;
    var level = Storage.Get_Info('level');
    var color = this.getIDByColor(ballId);
    var startPosNewBall = this.load_prefab(color);
    var endPosNewBall = this.load_prefab(color);
    self.BG_Node.addChild(endPosNewBall);
    self.BG_Node.addChild(startPosNewBall);
    var scaleValue = 1;
    var flipX = 1;

    if (Number(level) + 1 == 1) {
      if (startPos.y < endPos.y) {
        //从下往上画线
        startPosNewBall.angle = 35;
        startPosNewBall.scale = new cc.Vec3(-1.6, 1.6, 1.6);
        endPosNewBall.angle = 35;
        endPosNewBall.scale = new cc.Vec3(1.6, 1.6, 1.6);
        scaleValue = 3;
      } else {
        //从上往下画线
        startPosNewBall.angle = 35;
        startPosNewBall.scale = new cc.Vec3(1.6, 1.6, 1.6);
        endPosNewBall.angle = 35;
        endPosNewBall.scale = new cc.Vec3(-1.6, 1.6, 1.6);
        scaleValue = -3;
      }
    } else if (Number(level) + 1 == 2) {
      if (startPos.y < endPos.y) //从下往上画线
        {
          startPosNewBall.angle = 0;
          startPosNewBall.scale = new cc.Vec3(-1, 1, 1);
          endPosNewBall.angle = 0;
          endPosNewBall.scale = new cc.Vec3(1, 1, 1);
          scaleValue = 2;
        } else {
        startPosNewBall.angle = 0;
        startPosNewBall.scale = new cc.Vec3(1, 1, 1);
        endPosNewBall.angle = 0;
        endPosNewBall.scale = new cc.Vec3(-1, 1, 1);
        scaleValue = -2;
      }
    }

    var final_pos = self.BG_Node.convertToNodeSpaceAR(endPos);
    var final_pos1 = self.BG_Node.convertToNodeSpaceAR(startPos);
    endPosNewBall.setPosition(final_pos.x, final_pos.y, 0);
    startPosNewBall.setPosition(final_pos1.x, final_pos1.y, 0);
    cc.tween(endPosNewBall).to(0.3, {
      scale: scaleValue,
      opacity: 0
    }).call(function () {
      endPosNewBall.destroy();
    }).start();
    cc.tween(startPosNewBall).to(0.3, {
      scale: -scaleValue,
      opacity: 0
    }).call(function () {
      startPosNewBall.destroy();
    }).start();
  },
  getIDByColor: function getIDByColor(ballId) {
    switch (ballId) {
      case 0:
        return "redball";

      case 1:
        return "yellowball";

      case 2:
        return "blueball";

      case 3:
        return "greenball";
    }
  },
  backToMenu: function backToMenu() {
    _GameMgr["default"].gameOver = false;
    cc.director.loadScene('launch');
  },
  start: function start() {
    var self = this;
    G.reset(); // console.log('清空G');

    this.BG_Node = cc.find('Canvas/MainBG/BG');
    this.BG_Node.removeAllChildren(); //读取当前是哪一个关卡

    var level = Storage.Get_Info('level');

    if (Number(level) >= 2) {
      //全部通关
      //cc.find('Canvas/TIPS/全部通关').active = true;
      level = 0;
      Storage.Set_Info('level', 0);
      this.backToMenu();
      return;
    }

    if (Number(level) + 1 == 1) {
      this.zhuzi1.setPosition(446.924, this.zhuzi1.position.y - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi2.setPosition(-398.924, this.zhuzi2.position.y - _GameMgr["default"].GlobalOffsetY, 0);
    }

    if (Number(level) + 1 == 2) {
      this.zhuzi1.setPosition(465, 384 - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi1.angle = 0;
      this.zhuzi2.setPosition(-455, -6 - _GameMgr["default"].GlobalOffsetY, 0);
      this.zhuzi2.angle = 180;
    } // if (cc.sys.platform === cc.sys.ANDROID) {
    //     self.Level.string = 'Level ' + (Number(level) + 1);
    // } else {
    //     self.Level.string = '关卡 ' + (Number(level) + 1);
    // }


    cc.resources.load('level/' + level, function (err, jsonAsset) {
      var level_json_info = jsonAsset.json;
      G.LevelJson = level_json_info; //self.loadbg(level_json_info.bg);

      self.loadballs(level_json_info.balls, Number(level) + 1);
    }); // G.AnalysticLogin();
    // G.wxtopshare();
  },
  // loadbg(str) {
  //     // console.log(str);
  //     var self = this;
  //     cc.resources.load("BG/" + str, cc.SpriteFrame, function (err, spriteFrame) {
  //         self.BG_Node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
  //     });
  // },
  loadballs: function loadballs(balls, level) {
    var self = this; // G.stokelist = balls.length;

    for (var i = 0; i < balls.length; i++) {
      var newnode1 = this.load_prefab(balls[i].color);
      self.BG_Node.addChild(newnode1);
      var ballOffset = 10;

      if (level == 1) {
        ballOffset = 10;
        newnode1.setPosition(balls[i].Pos[0] + balls[i].Pos[0] * 0.194, balls[i].Pos[1] - balls[i].Pos[0] * 0.194 - _GameMgr["default"].GlobalOffsetY - (balls[i].flipX == 1 ? ballOffset : -ballOffset));
      } else if (level == 2) {
        ballOffset = -8;
        newnode1.setPosition(balls[i].Pos[0], balls[i].Pos[1]);
      }

      newnode1.angle = balls[i].Rot;

      if (level == 1) {
        newnode1.scale = new cc.Vec3(balls[i].scale * balls[i].flipX * _GameMgr["default"].tubeScale, balls[i].scale * _GameMgr["default"].tubeScale, balls[i].scale);
      } else if (level == 2) {
        newnode1.scale = new cc.Vec3(balls[i].scale * balls[i].flipX, balls[i].scale, balls[i].scale);
      }

      G.BallList.push(newnode1);
      G.originBallList = G.BallList.concat(); // console.log(newnode1.position)
    }
  },
  load_prefab: function load_prefab(color) {
    switch (color) {
      case "redball":
        var node = cc.instantiate(this.redball);
        return node;

      case "yellowball":
        var node = cc.instantiate(this.yellowball);
        return node;

      case "blueball":
        var node = cc.instantiate(this.blueball);
        return node;

      case "purpleball":
        var node = cc.instantiate(this.purpleball);
        return node;

      case "greenball":
        var node = cc.instantiate(this.greenball);
        return node;

      default:
        console.log('颜色不对');
    }
  } // update (dt) {},

});

cc._RF.pop();