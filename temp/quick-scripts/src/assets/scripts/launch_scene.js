"use strict";
cc._RF.push(module, 'eb8b18ojodL0o38e4QBAcnM', 'launch_scene');
// scripts/launch_scene.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var LaunchScene = /** @class */ (function (_super) {
    __extends(LaunchScene, _super);
    function LaunchScene() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.select_panel = null;
        _this.start_btn = null;
        return _this;
    }
    LaunchScene.prototype.onLoad = function () {
        this.start_btn.on(cc.Node.EventType.TOUCH_START, this.startGame, this);
    };
    LaunchScene.prototype.startGame = function () {
        this.select_panel.active = true;
    };
    LaunchScene.prototype.start = function () {
    };
    __decorate([
        property({ type: cc.Node })
    ], LaunchScene.prototype, "select_panel", void 0);
    __decorate([
        property({ type: cc.Node })
    ], LaunchScene.prototype, "start_btn", void 0);
    LaunchScene = __decorate([
        ccclass
    ], LaunchScene);
    return LaunchScene;
}(cc.Component));
exports.default = LaunchScene;

cc._RF.pop();