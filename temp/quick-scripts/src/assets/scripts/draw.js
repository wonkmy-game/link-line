"use strict";
cc._RF.push(module, '5a774yGBkRKFLeJgWD8+//E', 'draw');
// scripts/draw.js

"use strict";

var _GameMgr = _interopRequireDefault(require("./GameMgr"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

cc.Class({
  "extends": cc.Component,
  properties: {
    MainBG: null,
    Draw: cc.Node,
    dz: cc.Prefab,
    StartBall: null,
    EndBall: null,
    storkelist: [],
    //将线中的点存在数组中，数组的长度存在G.stokelist
    fail: cc.AudioClip,
    pass: cc.AudioClip,
    victory: cc.AudioClip,
    idX: 0,
    idY: 0
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    //cc.log(cc.find('Canvas/MainBG/draw/start').getBoundingBox());
    this.MainBG = cc.find('Canvas/MainBG/BG');
    this.Draw = cc.find('Canvas/MainBG/Draw'); //加载当前的balllist，
  },
  start: function start() {
    this.node.on(cc.Node.EventType.TOUCH_START, function (event) {
      //console.log('Touched!')
      var vec2 = new cc.Vec2(event.getLocationX(), event.getLocationY());
      var node_vec2 = this.MainBG.convertToNodeSpaceAR(vec2); // 关闭新手引导

      this.closeguide();
      var res = this.checkinstartballs(node_vec2);

      if (res) {
        this.draw_list();
      }
    }, this);
    this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
      //判断目标是否命中；
      this.node.off(cc.Node.EventType.TOUCH_MOVE, null, this);
      this.node.getComponent(cc.Graphics).clear();
      this.storkelist = []; // 打开新手引导

      this.openguide();
    }, this);
  },
  CheckTongGuan: function CheckTongGuan() {
    if (G.BallList.length > 0) {
      return false;
    } else {
      return true;
    }
  },
  checkinstartballs: function checkinstartballs(node_vec2) {
    // this.StartBall = G.BallList[0];
    // return true;
    //console.log(G.BallList);
    for (var i = 0; i < G.BallList.length; i++) {
      if (G.BallList[i].getBoundingBox().contains(node_vec2)) {
        this.StartBall = G.BallList[i];
        return true;
      }
    }

    return false;
  },
  checkinendballs: function checkinendballs(node_vec2) {
    if (this.StartBall == null) return false;
    var name = this.StartBall.name;
    var id = this.StartBall._id; // console.log(id)
    // console.log(name)

    for (var i = 0; i < G.BallList.length; i++) {
      if (G.BallList[i].name == name && G.BallList[i]._id != id) {
        if (G.BallList[i].getBoundingBox().contains(node_vec2)) {
          this.EndBall = G.BallList[i];
          return true;
        }
      }
    }

    return false;
  },
  draw_list: function draw_list() {
    var self = this;
    var ctx = this.node.getComponent(cc.Graphics);
    var v = self.StartBall.convertToWorldSpaceAR(cc.v2(0, 0));
    ctx.moveTo(v.x, v.y);
    this.node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
      var _this = this;

      //这里要判断event的点，是否在BG的外面，是的话，引用错误
      var vec2 = new cc.Vec2(event.getLocationX(), event.getLocationY());
      var node_vec2 = this.MainBG.convertToNodeSpaceAR(vec2); // console.log(vec2);console.log(this.MainBG.position)
      //             console.log(node_vec2)
      //             console.log(this.MainBG.getBoundingBoxToWorld())
      //判断是不是在背景范围内

      if (this.MainBG.getBoundingBoxToWorld().contains(vec2)) {
        var res_checkinendballs = this.checkinendballs(node_vec2); //判断是不是到终点球

        if (!res_checkinendballs) {
          //不在就继续画
          ctx.lineTo(event.getLocationX(), event.getLocationY()); // console.log('触摸生效');

          ctx.strokeColor = self.stroke_color(self.StartBall._name); //console.log(self.StartBall._name);

          ctx.stroke();
          ctx.moveTo(event.getLocationX(), event.getLocationY());
          this.storkelist.push(vec2); //  this.storkelist.push([event.getLocationX(),event.getLocationY()]);
        } else {
          //在终点球
          this.node.off(cc.Node.EventType.TOUCH_MOVE, null, this);
          G.storkelist.push(this.storkelist);
          var res_check_all_storke_interset = G.check_all_storke_interset(); //判断是不是和其他线交叉

          if (res_check_all_storke_interset != false) {
            //将交点处展示X号
            this.showWrongIcon(res_check_all_storke_interset); //与当前已有线相交，需要回退当前线

            this.node.getComponent(cc.Graphics).clear(); //然后回退G.storkelist

            this.remove(G.storkelist, this.storkelist);
            this.storkelist = [];
          } else {
            //开始下一次画线
            var endballzhongdian = self.EndBall.convertToWorldSpaceAR(cc.v2(0, 0));
            ctx.lineTo(endballzhongdian.x, endballzhongdian.y); // console.log(self.EndBall.convertToWorldSpaceAR(cc.v2(0,0)))

            var ballName = self.StartBall._name;
            ctx.strokeColor = self.stroke_color(self.StartBall._name);
            ctx.stroke(); //当前画布取消监听事件

            this.node.off(cc.Node.EventType.TOUCH_START, null, this);
            this.node.off(cc.Node.EventType.TOUCH_END, null, this); // console.log(self.EndBall);

            var startBall = self.StartBall;
            var endBall = self.EndBall; //在这里产生电子，从this.storkelist的起点跑到终点

            self.dianziMove(self, ballName, v, endballzhongdian, startBall, endBall); //移除两个ball

            this.remove(G.BallList, this.EndBall); //console.log(G.BallList)

            this.remove(G.BallList, this.StartBall);
            this.StartBall = null;
            this.EndBall = null; //判断是否成功通关

            var tongguan = this.CheckTongGuan();

            if (tongguan) {
              this.scheduleOnce(function () {
                _this.StartNextLevel();
              }, 4);
            } else {
              //加载另外一个draw。覆盖在当前draw之上
              cc.resources.load("perfab/draw", function (err, prefab) {
                var newNode = cc.instantiate(prefab); // console.log("成功绘制一条线");

                self.Draw.addChild(newNode);
              });
            }
          }
        }
      } else {
        //注销移动事件的监听
        this.node.off(cc.Node.EventType.TOUCH_MOVE, null, this); //显示wrong
        //console.log(node_vec2)

        this.showWrongIcon(vec2); //清除画线

        this.node.getComponent(cc.Graphics).clear();
        this.storkelist = [];
      }
    }, this);
  },
  dianziMove: function dianziMove(self, name, startPos, endPos, startBall, endBall) {
    var _this2 = this;

    this.schedule(function () {
      if (_GameMgr["default"].gameOver == true) return;
      var newdzNode = cc.instantiate(_this2.dz);
      self.Draw.addChild(newdzNode);
      newdzNode.scale = new cc.Vec3(0.7, 0.7, 0.7);
      newdzNode.getComponent('DianziMove').init(_this2.storkelist, self, name);
      newdzNode.getComponent('DianziMove').startMove(_this2.storkelist, self, startPos, endPos, startBall, endBall);
    }, 0.25);
  },
  stroke_color: function stroke_color(str) {
    switch (str) {
      case 'redball':
        return cc.Color.RED;

      case 'yellowball':
        return cc.Color.YELLOW;

      case 'blueball':
        return cc.Color.BLUE;

      case 'purpleball':
        return new cc.Color(206, 14, 237, 255);

      case 'greenball':
        return cc.Color.GREEN;

      default:
        return cc.Color.BLACK;
    }
  },
  remove: function remove(list, item) {
    for (var i = 0; i < list.length; i++) {
      if (list[i] == item) {
        return list.splice(i, 1);
      }
    }
  },
  showWrongIcon: function showWrongIcon(pos) {
    var self = this;
    cc.resources.load("perfab/wrong", function (err, prefab) {
      var newNode = cc.instantiate(prefab);
      newNode.parent = cc.find('Canvas/TIPS');
      newNode.scale = 1.5;
      var node_vec2 = cc.find('Canvas/TIPS').convertToNodeSpaceAR(pos);
      newNode.setPosition(node_vec2); //音效

      cc.audioEngine.playEffect(self.fail, false);
      setTimeout(function () {
        newNode.active = false;
        newNode.destroy();
      }, 500);
    });
  },
  showRightIcon: function showRightIcon() {
    var self = this;
    cc.resources.load("perfab/right", function (err, prefab) {
      // console.log(err)
      var right = cc.instantiate(prefab);
      var gameStart = cc.find('Canvas/MainBG').getComponent('GameStart');
      right.parent = cc.find('Canvas/TIPS'); //let node_vec2 = cc.find('Canvas/TIPS').convertToNodeSpaceAR(cc.v2(pos[0],pos[1]));

      right.setPosition(0, 191); //音效

      cc.audioEngine.playEffect(self.pass, false);
      setTimeout(function () {
        right.active = false;
        right.destroy(); //直接进入下一关

        gameStart.zhuzi1.setPosition(395, 384, 0);
        gameStart.zhuzi1.angle = 0;
        gameStart.zhuzi2.setPosition(-396, -6, 0);
        gameStart.zhuzi2.angle = 180;
        gameStart.start();
        cc.find('Canvas/MainBG/Draw').removeAllChildren();
        cc.resources.load("perfab/draw", function (err, prefab) {
          var newNode = cc.instantiate(prefab);
          newNode.parent = cc.find('Canvas/MainBG/Draw');
        });
      }, 2500);
    });
  },
  StartNextLevel: function StartNextLevel() {
    if (_GameMgr["default"].gameOver == true) return;
    var level = Storage.Get_Info('level');
    Storage.Set_Info('level', Number(level) + 1);
    this.showRightIcon();
  },
  closeguide: function closeguide() {
    cc.find('Canvas/MainBG/tip').getComponent('newuser_guide').close();
  },
  openguide: function openguide() {
    cc.find('Canvas/MainBG/tip').getComponent('newuser_guide').showguidelist_again();
  } // update (dt) {},

});

cc._RF.pop();