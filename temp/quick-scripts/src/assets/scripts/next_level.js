"use strict";
cc._RF.push(module, 'c13a9ud2dhNwJ0nXd0mJQtS', 'next_level');
// scripts/next_level.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    Draw: cc.Node,
    ButtonAudio: cc.AudioClip
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {// this.Draw = cc.find('Canvas/MainBG/Draw')
  },
  start: function start() {
    var self = this;
    this.node.on('click', function (button) {
      //The event is a custom event, you could get the Button component via first argument

      /*通过模块化脚本操作
      var GameStart = require("GameStart");
      new GameStart().start(); // 访问静态变量会报错，采用组件查找的方法
      */

      /*通过找组件操作
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      */

      /*直接重新加载场景
      cc.director.loadScene("Home");
      */
      console.log('xiayiguanchufale');
      cc.find('Canvas/MainBG').getComponent('GameStart').start();
      cc.find('Canvas/TIPS/PASS_BG').active = false;
      self.Draw.removeAllChildren();
      cc.resources.load("perfab/draw", function (err, prefab) {
        var newNode = cc.instantiate(prefab);
        newNode.parent = self.Draw;
      }); //音效

      cc.audioEngine.playEffect(self.ButtonAudio, false);
      var level = Storage.Get_Info('level');

      if (Number(level) % 2 == 0) {
        G.ShowInterstitialAD();
      } // G.AnalysticCustomEvent('LEVEL', level);

    });
  } // update (dt) {},

});

cc._RF.pop();