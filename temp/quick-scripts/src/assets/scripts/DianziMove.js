"use strict";
cc._RF.push(module, '0571e5EiyhCjJDWYBkC+u9Q', 'DianziMove');
// scripts/DianziMove.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("./GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DianziMove = /** @class */ (function (_super) {
    __extends(DianziMove, _super);
    function DianziMove() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ID = -1;
        _this.waypoint_id = 0;
        return _this;
    }
    DianziMove.prototype.init = function (storkelist, self, name) {
        var _this = this;
        var spName = this.stroke_color(name);
        this.ID = this.getIDByColor(spName);
        cc.resources.load("dzs/" + spName, cc.SpriteFrame, function (err, sf) {
            _this.node.getComponent(cc.Sprite).spriteFrame = sf;
        });
        var canvasPos1 = storkelist[0];
        var worldPos1 = self.node.convertToWorldSpaceAR(canvasPos1);
        var nodePos1 = self.Draw.convertToNodeSpaceAR(worldPos1);
        this.node.setPosition(nodePos1);
    };
    DianziMove.prototype.startMove = function (storkelist, self, startPos, endPos, startBall, endBall) {
        var _this = this;
        if (GameMgr_1.default.gameOver == true)
            return;
        if (this.waypoint_id >= storkelist.length) {
            cc.game.emit("show_ball_mask", startPos, endPos, this.ID, startBall, endBall);
            this.node.destroy();
            return;
        }
        var canvasPos = storkelist[this.waypoint_id];
        var worldPos = self.node.convertToWorldSpaceAR(canvasPos);
        var nodePos = self.Draw.convertToNodeSpaceAR(worldPos);
        cc.tween(this.node).to(0.02, { position: nodePos }).call(function () {
            _this.waypoint_id += 2;
            _this.startMove(storkelist, self, startPos, endPos, startBall, endBall);
        }).start();
    };
    DianziMove.prototype.getIDByColor = function (colorName) {
        switch (colorName) {
            case "dz-red":
                return 0;
            case "dz-yellow":
                return 1;
            case "dz-blue":
                return 2;
            case "dz-green":
                return 3;
        }
    };
    DianziMove.prototype.stroke_color = function (str) {
        switch (str) {
            case 'redball':
                return "dz-red";
            case 'yellowball':
                return "dz-yellow";
            case 'blueball':
                return "dz-blue";
            case 'greenball':
                return "dz-green";
        }
    };
    DianziMove.prototype.onCollisionEnter = function (other, self) {
        if (other.node.getComponent('DianziMove').ID != this.ID) {
            GameMgr_1.default.gameOver = true;
            cc.game.emit("gameover", this.node, this.node.parent);
        }
    };
    DianziMove = __decorate([
        ccclass
    ], DianziMove);
    return DianziMove;
}(cc.Component));
exports.default = DianziMove;

cc._RF.pop();